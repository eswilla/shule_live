<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Grade extends Admin_Controller {

    /**
     * -----------------------------------------
     * 
     * ******* Address****************
     * INETS COMPANY LIMITED
     * P.O BOX 32258, DAR ES SALAAM
     * TANZANIA
     * 
     * 
     * *******Office Location *********
     * 11th block, Bima Road, Mikocheni B, Kinondoni, Dar es salaam
     * 
     * 
     * ********Contacts***************
     * Email: <info@inetstz.com>
     * Website: <www.inetstz.com>
     * Mobile: <+255 655 406 004>
     * Tel:    <+255 22 278 0228>
     * -----------------------------------------
     */
    function __construct() {
	parent::__construct();
	$this->load->model("grade_m");
	$language = $this->session->userdata('lang');
	$this->lang->load('grade', $language);
        $this->lang->load('email', $language);
    }

    public function index() {
	$usertype = $this->session->userdata("usertype");
	if ($usertype == "Admin") {
	    $id = htmlentities($this->uri->segment(3));
	    $this->data['classlevels'] = $this->classlevel_m->get_classlevel();
	    $this->data['set'] = $id;
	    if ((int) $id) {	
		$this->data['grades'] = $this->grade_m->get_order_by_grade(array('classlevel_id' => $id));
		$this->data["subview"] = "grade/index";
		$this->load->view('_layout_main', $this->data);
	    } else {
		$this->data["subview"] = "grade/index";
		$this->load->view('_layout_main', $this->data);
	    }
	} else {
	    $this->data["subview"] = "error";
	    $this->load->view('_layout_main', $this->data);
	}
    }

    public function grade_list() {
	$classID = $this->input->post('id');
	if ((int) $classID) {
	    $string = base_url("grade/index/$classID");
	    echo $string;
	} else {
	    redirect(base_url("grade/index"));
	}
    }

    protected function rules() {
	$rules = array(
	    array(
		'field' => 'grade',
		'label' => $this->lang->line("grade_name"),
		'rules' => 'trim|required|xss_clean|max_length[60]|callback_unique_grade'
	    ),
	    array(
		'field' => 'point',
		'label' => $this->lang->line("grade_point"),
		'rules' => 'trim|required|numeric|max_length[11]|xss_clean|callback_unique_point'
	    ),
	    array(
		'field' => 'gradefrom',
		'label' => $this->lang->line("grade_gradefrom"),
		'rules' => 'trim|required|numeric|max_length[11]|xss_clean|callback_unique_gradefrom'
	    ),
	    array(
		'field' => 'gradeupto',
		'label' => $this->lang->line("grade_gradeupto"),
		'rules' => 'trim|required|numeric|max_length[11]|xss_clean|callback_unique_gradeupto'
	    ),
	    array(
		'field' => 'note',
		'label' => $this->lang->line("grade_note"),
		'rules' => 'trim|max_length[200]|xss_clean|required'
	    ),
	    array(
		'field' => 'overall_note',
		'label' => $this->lang->line("overall_note"),
		'rules' => 'trim|max_length[400]|xss_clean|required'
	    ),
	    array(
		'field' => 'classlevel',
		'label' => $this->lang->line("classlevel"),
		'rules' => 'trim|max_length[200]|xss_clean'
	    )
	);
	return $rules;
    }

    public function add() {

	$usertype = $this->session->userdata("usertype");
        $setting = $this->setting_m->get_setting(1);
	if ($usertype == "Admin") {
	    $this->data['classlevels'] = $this->classlevel_m->get_classlevel();

	    if ($_POST) {
            $parents=$this->parentes_m->get_all_parents();
            $rules = $this->rules();
            $this->form_validation->set_rules($rules);


		if ($this->form_validation->run() == FALSE) {
            $this->data["subview"] = "grade/add";
		    $this->load->view('_layout_main', $this->data);

		}

        else {

		    $array = array(
			"grade" => $this->input->post("grade"),
			"point" => $this->input->post("point"),
			"gradefrom" => $this->input->post("gradefrom"),
			"gradeupto" => $this->input->post("gradeupto"),
			"note" => $this->input->post("note"),
			"overall_note" => $this->input->post("overall_note"),
			"classlevel_id" => $this->input->post("classlevel_id"),
		    );


		    $this->db->insert('grade', $array);
		    $this->session->set_flashdata('success', $this->lang->line('menu_success'));

            foreach($parents as $parent){
                $message=sprintf($this->lang->line('add_new_grade'),
                $parent->name,
                $setting->sname);
                $this->send_email( $parent->email,$setting->sname,$message);
                $this->send_sms($parent->phone,$message);
            }

		    redirect(base_url("grade/index"));
		   }
	    }
        else {
		$this->data["subview"] = "grade/add";
		$this->load->view('_layout_main', $this->data);
	    }
	} else {
	    $this->data["subview"] = "error";
	    $this->load->view('_layout_main', $this->data);
	   }
    }

    public function edit() {
	$usertype = $this->session->userdata("usertype");
        $setting = $this->setting_m->get_setting(1);
	if ($usertype == "Admin") {
	    $id = htmlentities(($this->uri->segment(3)));
	    $this->data['classlevels'] = $this->classlevel_m->get_classlevel();
	    if ((int) $id) {
		$this->data['grade'] = $this->grade_m->get_grade($id);
		if ($this->data['grade']) {
		    if ($_POST) {
            $parents=$this->parentes_m->get_all_parents();
			$rules = $this->rules();
			$this->form_validation->set_rules($rules);
			if ($this->form_validation->run() == FALSE) {
			    $this->data["subview"] = "grade/edit";
			    $this->load->view('_layout_main', $this->data);
			} else {
			    $array = array(
				"grade" => $this->input->post("grade"),
				"point" => $this->input->post("point"),
				"gradefrom" => $this->input->post("gradefrom"),
				"gradeupto" => $this->input->post("gradeupto"),
				"note" => $this->input->post("note"),
				"overall_note" => $this->input->post("overall_note"),
				"classlevel_id" => $this->input->post("classlevel_id")
			    );

			    $this->grade_m->update_grade($array, $id);
			    $this->session->set_flashdata('success', $this->lang->line('menu_success'));
                foreach($parents as $parent){
                    $message=sprintf($this->lang->line('edit_grade'),
                        $parent->name,
                        $setting->sname);
                    $this->send_email( $parent->email,$setting->sname,$message);
                    //$this->send_sms($parent->phone,$message);
                }
			    redirect(base_url("grade/index/".$this->input->post("classlevel_id")));
			}
		    } else {
			$this->data["subview"] = "grade/edit";
			$this->load->view('_layout_main', $this->data);
		    }
		} else {
		    $this->data["subview"] = "error";
		    $this->load->view('_layout_main', $this->data);
		}
	    } else {
		$this->data["subview"] = "error";
		$this->load->view('_layout_main', $this->data);
	    }
	} else {
	    $this->data["subview"] = "error";
	    $this->load->view('_layout_main', $this->data);
	}
    }

    public function delete() {
	$usertype = $this->session->userdata("usertype");
        $setting = $this->setting_m->get_setting(1);
	if ($usertype == "Admin") {
	    $id = htmlentities(($this->uri->segment(3)));
	    if ((int) $id) {
            $parents=$this->parentes_m->get_all_parents();
		$this->grade_m->delete_grade($id);
		$this->session->set_flashdata('success', $this->lang->line('menu_success'));

            foreach($parents as $parent){
                    $message=sprintf($this->lang->line('delete_grade'),
                    $parent->name);
                $this->send_email( $parent->email,$setting->sname,$message);
                $this->send_sms($parent->phone,$message);
            }
		redirect(base_url("grade/index"));
	    } else {
		redirect(base_url("grade/index"));
	    }
	} else {
	    $this->data["subview"] = "error";
	    $this->load->view('_layout_main', $this->data);
	}
    }

    public function unique_grade() {
	$id = htmlentities(($this->uri->segment(3)));
	if ((int) $id) {
	    $grade = $this->grade_m->get_order_by_grade(array("grade" => $this->input->post("grade"), "gradeID !=" => $id, 'classlevel_id' => $this->input->get_post('classlevel_id')));
	    if (count($grade)) {
		$this->form_validation->set_message("unique_grade", "%s already exists for this level");
		return FALSE;
	    }
	    return TRUE;
	} else {
	    $grade = $this->grade_m->get_order_by_grade(array("grade" => $this->input->post("grade"), 'classlevel_id' => $this->input->get_post('classlevel_id')));

	    if (count($grade)) {
		$this->form_validation->set_message("unique_grade", "%s already exists for this level");
		return FALSE;
	    }
	    return TRUE;
	}
    }

    public function unique_point() {
	$id = htmlentities(($this->uri->segment(3)));
	if ((int) $id) {
	    $grade = $this->grade_m->get_order_by_grade(array("point" => $this->input->post("point"), "gradeID !=" => $id, 'classlevel_id' => $this->input->get_post('classlevel_id')));
	    if (count($grade)) {
		$this->form_validation->set_message("unique_point", "%s already exists for this level");
		return FALSE;
	    }
	    return TRUE;
	} else {
	    $grade = $this->grade_m->get_order_by_grade(array("point" => $this->input->post("point"), 'classlevel_id' => $this->input->get_post('classlevel_id')));

	    if (count($grade)) {
		$this->form_validation->set_message("unique_point", "%s already exists for this level");
		return FALSE;
	    }
	    return TRUE;
	}
    }

    public function unique_gradefrom() {
	$id = htmlentities(($this->uri->segment(3)));
	if ((int) $id) {
	    $grade = $this->grade_m->get_order_by_grade(array("gradefrom" => $this->input->post("gradefrom"), "gradeID !=" => $id, 'classlevel_id' => $this->input->get_post('classlevel_id')));
	    if (count($grade)) {
		$this->form_validation->set_message("unique_gradefrom", "%s already exists for this level");
		return FALSE;
	    }
	    return TRUE;
	} else {
	    $grade = $this->grade_m->get_order_by_grade(array("gradefrom" => $this->input->post("gradefrom"), 'classlevel_id' => $this->input->get_post('classlevel_id')));

	    if (count($grade)) {
		$this->form_validation->set_message("unique_gradefrom", "%s already exists for this level");
		return FALSE;
	    }
	    return TRUE;
	}
    }

    public function unique_gradeupto() {
	$id = htmlentities(($this->uri->segment(3)));
	if ((int) $id) {
	    $grade = $this->grade_m->get_order_by_grade(array("gradeupto" => $this->input->post("gradeupto"), "gradeID !=" => $id, 'classlevel_id' => $this->input->get_post('classlevel_id')));
	    if (count($grade)) {
		$this->form_validation->set_message("unique_gradeupto", "%s already exists for this level");
		return FALSE;
	    }
	    return TRUE;
	} else {
	    $grade = $this->grade_m->get_order_by_grade(array("gradeupto" => $this->input->post("gradeupto"), 'classlevel_id' => $this->input->get_post('classlevel_id')));

	    if (count($grade)) {
		$this->form_validation->set_message("unique_gradeupto", "%s already exists for this level");
		return FALSE;
	    }
	    return TRUE;
	}
    }

}

/* End of file grade.php */
/* Location: .//D/xampp/htdocs/school/mvc/controllers/grade.php */