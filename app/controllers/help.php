<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of help
 *
 *  -----------------------------------------------------
 *  Copyright: INETS COMPANY LIMITED
 *  Website: www.inetstz.com
 *  Email: info@inetstz.com
 *  -----------------------------------------------------
 * @author Ephraim Swilla
 */
class Help extends Admin_Controller {

    //put your code here

    function __construct() {
	parent::__construct();
    }

    function index() {
	$this->load->view('help/manual/index');
    }

    function library_help() {
        $this->load->view('help/manual/library_help');
//	$this->data["subview"] = "help/library_help";
//	$this->load->view('_layout_main', $this->data);
    }

    function account_help() {
        $this->load->view('help/manual/account_help');
//	$this->data["subview"] = "help/account_help";
//	$this->load->view('_layout_main', $this->data);
    }

    function student_help() {

        $this->load->view('help/manual/student_help');
//	$this->data["subview"] = "help/student_help";
//	$this->load->view('_layout_main', $this->data);
    }

    function parent_help() {
        $this->load->view('help/manual/parent_help');
//	$this->data["subview"] = "help/parent_help";
//        $this->load->view('_layout_main', $this->data);
	//$this->data['usertype']='Parent';
	//$this->load->view('help/manual/index', $this->data);
    }

    function teacher_help() {
        $this->load->view('help/manual/teacher_help');
//	$this->data["subview"] = "help/teacher_help";
//	$this->load->view('_layout_main', $this->data);
    }

    function email() {
	echo send_email('owdeng@gmail.com', 'Template Testing', 'It is very good testing these things before letting them go!');
    }

}
