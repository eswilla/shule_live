<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Exam extends Admin_Controller {

    /**
     * -----------------------------------------
     *
     * ******* Address****************
     * INETS COMPANY LIMITED
     * P.O BOX 32258, DAR ES SALAAM
     * TANZANIA
     *
     *
     * *******Office Location *********
     * 11th block, Bima Road, Mikocheni B, Kinondoni, Dar es salaam
     *
     *
     * ********Contacts***************
     * Email: <info@inetstz.com>
     * Website: <www.inetstz.com>
     * Mobile: <+255 655 406 004>
     * Tel:    <+255 22 278 0228>
     * -----------------------------------------
     */
    function __construct() {
	parent::__construct();
	$language = $this->session->userdata('lang');
	$this->lang->load('exam', $language);
	$this->lang->load('email', $language);
    }

    public function index() {
	$usertype = $this->session->userdata("usertype");
	if ($usertype == "Admin") {
	    $this->data['exams'] = $this->exam_m->get_order_by_exam();
	    $this->data["subview"] = "exam/index";
	    $this->load->view('_layout_main', $this->data);
	} else {
	    $this->data["subview"] = "error";
	    $this->load->view('_layout_main', $this->data);
	}
    }

    protected function rules() {
	$rules = array(
	    array(
		'field' => 'exam',
		'label' => $this->lang->line("exam_name"),
		'rules' => 'trim|required|xss_clean|max_length[60]'
	    ),
	    array(
		'field' => 'date',
		'label' => $this->lang->line("exam_date"),
		'rules' => 'trim|required|max_length[20]|xss_clean'
	    ),
	    array(
		'field' => 'note',
		'label' => $this->lang->line("exam_note"),
		'rules' => 'trim|max_length[200]|xss_clean'
	    ),
	    array(
		'field' => 'classes',
		'label' => $this->lang->line("exam_class"),
		'rules' => 'callback_checkbovalue'
	    )
	);
	return $rules;
    }

    function checkbovalue() {
	$classes = $this->input->post('classe');
	if (empty($classes)) {
	    $this->form_validation->set_message("checkbovalue", "%s cannot be empty");
	    return FALSE;
	}
	return TRUE;
    }

    public function add() {
	$usertype = $this->session->userdata("usertype");
	$setting = $this->setting_m->get_setting(1);
	if ($usertype == "Admin") {

	    $this->data['classes'] = $this->classes_m->get_classes();

	    if ($_POST) {
		$parents = $this->parentes_m->get_all_parents();
		$classes = $this->input->post('classe');

		$rules = $this->rules();
		$this->form_validation->set_rules($rules);

		if ($this->form_validation->run() == FALSE) {
		    $this->data['form_validation'] = validation_errors();
		    $this->data["subview"] = "exam/add";
		    $this->load->view('_layout_main', $this->data);
		} else {


		    $array = array(
			"exam" => $this->input->post("exam"),
			"date" => date("Y-m-d", strtotime($this->input->post("date"))),
			"note" => $this->input->post("note")
		    );

		    $this->db->insert('exam', $array);

		    $last_inserted_exam_id = $this->db->insert_id();
		    foreach ($classes as $classe) {
			$newArray = array(
			    "exam_id" => $last_inserted_exam_id,
			    "class_id" => $classe
			);
			$this->db->insert("class_exam", $newArray);
		    }
		    $this->session->set_flashdata('success', $this->lang->line('menu_success'));
		    redirect(base_url("exam/index"));
		}
	    } else {
		$this->data["subview"] = "exam/add";
		$this->load->view('_layout_main', $this->data);
	    }
	} else {
	    $this->data["subview"] = "error";
	    $this->load->view('_layout_main', $this->data);
	}
    }

    public function edit() {
	$usertype = $this->session->userdata("usertype");
	if ($usertype == "Admin") {
	    $id = htmlentities(($this->uri->segment(3)));
	    if ((int) $id) {
		$this->data['exam'] = $this->exam_m->get_exam($id);
		$this->data['classes'] = $this->classes_m->get_classes();
		$this->data['exam_classes'] = $this->db->query('SELECT  b.class_id FROM ' . set_schema_name() . 'class_exam b WHERE b.exam_id=' . $id)->result();
		if ($this->data['exam']) {
		    if ($_POST) {
			$rules = $this->rules();
			$this->form_validation->set_rules($rules);
			if ($this->form_validation->run() == FALSE) {
			    $this->data["subview"] = "exam/edit";
			    $this->load->view('_layout_main', $this->data);
			} else {
			    $array = array(
				"exam" => $this->input->post("exam"),
				"date" => date("Y-m-d", strtotime($this->input->post("date"))),
				"note" => $this->input->post("note")
			    );

			    $this->exam_m->update_exam($array, $id);

			    $classesIDS = $this->input->post('classe');

			    $this->db->query('DELETE FROM ' . set_schema_name() . 'class_exam WHERE exam_id=' . $id);

			    foreach ($classesIDS as $class_id) {
				$section_subject_array = array(
				    "class_id" => $class_id,
				    "exam_id" => $id,
				);
				$this->db->insert('class_exam', $section_subject_array);
			    }

			    $this->session->set_flashdata('success', $this->lang->line('menu_success'));
			    redirect(base_url("exam/index"));
			}
		    } else {
			$this->data["subview"] = "exam/edit";
			$this->load->view('_layout_main', $this->data);
		    }
		} else {
		    $this->data["subview"] = "error";
		    $this->load->view('_layout_main', $this->data);
		}
	    } else {
		$this->data["subview"] = "error";
		$this->load->view('_layout_main', $this->data);
	    }
	} else {
	    $this->data["subview"] = "error";
	    $this->load->view('_layout_main', $this->data);
	}
    }

    public function getClassExam() {

	$classId = $this->input->post('id');
	$academic_year_id = $this->input->post('year_id');

	if ((int) $classId) {
	    $exams = $this->exam_m->get_exam_by_class($classId, $academic_year_id);
	    echo "<div class='form-group' >";
	    echo "<label for=\"all\"  class=\"col-sm-2 col-sm-offset-6 control-label\">
                 Select All
                  </label>";
	    echo " <input type=\"checkbox\" id='select_all' name=\"all\">";
	    echo "</div>";
	    foreach ($exams as $value) {
		echo "<div class='form-group' >";
		echo "<label for=\"$value->exam\" class=\"col-sm-2 col-sm-offset-2  control-label\">
                 $value->exam
                  </label>";
		echo " <input type=\"checkbox\" class='check' name=\"$value->exam\" value=\"$value->examID\">";
		echo "<div class='col-sm-4'>";
		echo "<div class='input-group'>";
		echo "  <input type='text' name=\"$value->exam \" id='percentage' class=\"form-control\">";
		echo "<span class=\"input-group-btn\">";
		echo "<button type=\"button\" class=\"btn btn-primary\">%</button>";

		echo "</span>";
		echo "</div>";
		echo "</div>";
		echo "</div>";
		echo "<script>
//select all checkboxes
$(\"#select_all\").change(function(){  //\"select all\" change
    $(\".check\").prop('checked', $(this).prop(\"checked\")); //change all \".check\" checked status
});

//\".check\" change
$('.check').change(function(){
    //uncheck \"select all\", if one of the listed checkbox item is unchecked
    if(false == $(this).prop(\"checked\")){ //if this item is unchecked
        $(\"#select_all\").prop('checked', false); //change \"select all\" checked status to false
    }
    //check \"select all\" if all checkbox items are checked
    if ($('.check:checked').length == $('.check').length ){
        $(\"#select_all\").prop('checked', true);
    }
});

		</script>";
	    }
	}
    }

    public function getExamByClass() {

	$classId = $this->input->post('id');
	if ((int) $classId) {
	    $exams = $this->exam_m->get_exam_by_class($classId);
	    echo "<option value='0'>", $this->lang->line("exam_name"), "</option>";
	    foreach ($exams as $value) {
		echo "<option value=\"$value->examID\">", $value->exam, "</option>";
	    }
	}
    }

    public function delete() {
	$usertype = $this->session->userdata("usertype");
	$setting = $this->setting_m->get_setting(1);
	if ($usertype == "Admin") {

	    $id = htmlentities(($this->uri->segment(3)));
	    if ((int) $id) {

		$exams = $this->exam_m->get_exam($id);
		$parents = $this->parentes_m->get_all_parents();

		$this->exam_m->delete_exam($id);
		$this->session->set_flashdata('success', $this->lang->line('menu_success'));
		foreach ($parents as $parent) {
		    $message = sprintf($this->lang->line('delete_exam'), $parent->name, $exams->exam);
		    $this->send_email($parent->email, $setting->sname, $message);
		    $this->send_sms($parent->phone, $message);
		}
		redirect(base_url("exam/index"));
	    } else {
		redirect(base_url("exam/index"));
	    }
	} else {
	    $this->data["subview"] = "error";
	    $this->load->view('_layout_main', $this->data);
	}
    }

    public function unique_exam() {
	$id = htmlentities(($this->uri->segment(3)));
	if ((int) $id) {
	    $exam = $this->exam_m->get_order_by_exam(array("exam" => $this->input->post("exam"), "examID !=" => $id));
	    if (count($exam)) {
		$this->form_validation->set_message("unique_exam", "%s already exists");
		return FALSE;
	    }
	    return TRUE;
	} else {
	    $exam = $this->exam_m->get_order_by_exam(array("exam" => $this->input->post("exam")));

	    if (count($exam)) {
		$this->form_validation->set_message("unique_exam", "%s already exists");
		return FALSE;
	    }
	    return TRUE;
	}
    }

    function date_valid($date) {
	if (strlen($date) < 10) {
	    $this->form_validation->set_message("date_valid", "%s is not valid mm/dd/yyyy");
	    return FALSE;
	} else {
	    $arr = explode("/", $date);
	    $dd = $arr[1];
	    $mm = $arr[0];
	    $yyyy = $arr[2];
	    if (checkdate($mm, $dd, $yyyy)) {
		return TRUE;
	    } else {
		$this->form_validation->set_message("date_valid", "%s is not valid mm/dd/yyyy");
		return FALSE;
	    }
	}
    }

    public function report() {
	$language = $this->session->userdata('lang');
	$this->lang->load('exam_report', $language);
	$this->data['exams'] = $this->exam_m->get_exam();
	$this->data['classes'] = $this->classes_m->get_classes();
	$classesID = $this->input->post("classesID");
	$this->data['subjectID'] = 0;
	$this->data['students'] = array();
	$year = date("Y");

	if ($_POST) {
	    $classID = $this->input->get_post('classesID');
	    $examID = $this->input->get_post('examID');
	    $subjectID = $this->input->get_post('subjectID');
	    $sectionID = $this->input->get_post('sectionID');
	    $academic_year_id = $this->input->get_post('academic_year_id');
	    if ($academic_year_id == '') {
		$this->session->set_flashdata('success', $this->lang->line('set_year'));
		redirect(base_url('exam/report'));
	    }
	    $year = date("Y");
	    //$this->data['eattendances'] = $this->eattendance_m->get_order_by_eattendance(array("examID" => $examID, "classesID" => $classID, "subjectID" => $subjectID, 'year' => $year));

	    if ($sectionID == 0) {
		//get normal query
		$view = 'report';
	    } else {
		$this->data['userclass'] = $this->classes_m->get_classes($classID);
		//get by section
		$view = 'section_report';
	    }
	    $this->data['sectionID'] = $sectionID;

	    if ($subjectID == 0) {
		//GET all subjects with total average and ranks

		$this->data['students'] = $sectionID == 0 ?
			$this->mark_m->get_mark_subject_avg_perclass($classID, $examID, $academic_year_id) :
			$this->mark_m->get_mark_subject_avg_persection($sectionID, $classID, $examID, $academic_year_id);

		$this->data['subjects'] = $sectionID == 0 ? $this->subject_m->get_order_by_subject(array("classesID" => $classesID)) : $this->subject_m->get_subject_in_section($sectionID);
	    } else {
		//get single subject with average
		$this->data['subjects'] = $this->subject_m->get_order_by_subject(array("classesID" => $classesID));
		$this->data['students'] = $this->student_m->get_order_by_student(array("classesID" => $classID));
	    }
	    $this->data['eattendances'] = array();

	    if (count($this->data['students'])) {

		if ($this->data['students']) {
		    $sections = $this->section_m->get_order_by_section(array("classesID" => $classesID));
		    $this->data['sections'] = $sections;
		    foreach ($sections as $key => $section) {
			$this->data['allsection'][$section->section] = $this->student_m->get_order_by_student(array('classesID' => $classesID, "sectionID" => $section->sectionID));
		    }
		} else {
		    $this->data['students'] = NULL;
		}

		// foreach ($students as $key => $student) {
		// 	$section = $this->section_m->get_section($student->sectionID);
		// 	if($section) {
		// 		$this->data['students'][$key] = (object) array_merge( (array)$student, array('ssection' => $section->section));
		// 	} else {
		// 		$this->data['students'][$key] = (object) array_merge( (array)$student, array('ssection' => $student->section));
		// 	}
		// }
		$this->data['examID'] = $examID;
		$this->data['classesID'] = $classesID;
		$this->data['subjectID'] = $subjectID;
	    }
	    $this->data["subview"] = 'exam/' . $view;
	    $this->load->view('_layout_main', $this->data);
	} else {
	    $this->data["subview"] = "exam/report";
	    $this->load->view('_layout_main', $this->data);
	}
    }

    public function get_academic_years() {
	$class_id = $this->input->post('id');
	$class = $this->classes_m->get_classes($class_id);
	$academics = $this->academic_year_m->get_order_by_academic(array('class_level_id' => $class->classlevel_id));
	echo "<option value='0'>" . $this->lang->line("exam_select_year") . "</option>";
	foreach ($academics as $academic) {
	    echo '<option value=' . $academic->id . '>' . $academic->name . '</option>';
	}
    }

    public function singlereport() {
	$id = htmlentities(($this->uri->segment(3)));
	$checkstudent = $this->student_m->get_single_student(array('studentID' => $id));
	if (count($checkstudent)) {
	    $classesID = $checkstudent->classesID;
	    $studentID = $checkstudent->studentID;
	    $student = $checkstudent;
	    $this->data['set'] = $id;
	    $this->data["student"] = $this->student_m->get_student($studentID);
	    $this->data["classes"] = $this->student_m->get_class($classesID);
	    $this->data["signature"] = $this->setting_m->get_signature();
	    $this->data["class_teacher_signature"] = $this->teacher_m->get_teacher_signature($this->data["student"]->sectionID);

	    if ($this->data["student"] && $this->data["classes"]) {
		$exam_id = $this->input->get_post('exam');
		$sectionID = $this->data['section_id'] = $this->input->get_post('section_id');
		$this->data["exams"] = $this->exam_m->get_exam($exam_id);
		$this->data["grades"] = $this->grade_m->get_order_by_grade(array('classlevel_id' => $this->data['classes']->classlevel_id));
		$sql = 'SELECT * FROM ' . set_schema_name() . 'mark WHERE "subjectID" IN ( SELECT subject_id FROM ' . set_schema_name() . 'subject_count WHERE student_id=' . $student->studentID . ' ) AND "studentID"=' . $student->studentID . ' and "classesID"=' . $student->classesID . ' AND "examID"=' . $exam_id . ' ';

		$this->data["marks"] = $this->db->query($sql)->result();

		$this->data['subjects'] = $this->db->query('SELECT * FROM ' . set_schema_name() . 'subject WHERE "subjectID" in (SELECT subject_id FROM ' . set_schema_name() . 'subject_section WHERE section_id=' . $this->data["student"]->sectionID . ')')->result();

		$this->data["section"] = $this->section_m->get_section($this->data['student']->sectionID);
		$where_student = $sectionID == '' ? array("classesID" => $classesID) :
			array("classesID" => $classesID, 'sectionID' => $this->data["student"]->sectionID);

		$this->data['total_students'] = $this->student_m->get_order_by_student($where_student);
		$this->data['total_students_section'] = $this->student_m->get_order_by_student(array("classesID" => $classesID, 'sectionID' => $this->data["student"]->sectionID));

		$this->data['report'] = $this->get_rank_per_all_subjects($exam_id, $classesID, $studentID, $this->data['section_id']);

		$this->data['classlevel'] = $this->db->query('select result_format FROM ' . set_schema_name() . 'classlevel WHERE classlevel_id=' . $this->data['classes']->classlevel_id)->row();

		if ($this->data['classlevel']->result_format == 'ACSEE') {
		    $division = $this->getAcseeDivision($studentID, $exam_id, $classesID);
		    $this->data['division'] = acseeDivision($division[0], $division[1]);
		} else if ($this->data['classlevel']->result_format == 'CSEE') {
		    $division = $this->getCseeDivision($studentID, $exam_id, $classesID);
		    $this->data['division'] = cseeDivision($division[0], $division[1]);
		}
		$this->data['points'] = $division[0];
		$this->data["subview"] = "exam/singlereport";
		$this->load->view('_layout_main', $this->data);
	    } else {
		$this->data["subview"] = "error";
		$this->load->view('_layout_main', $this->data);
	    }
	} else {
	    $this->data["subview"] = "error";
	    $this->load->view('_layout_main', $this->data);
	}
    }

    public function combined() {

	$language = $this->session->userdata('lang');
	$this->lang->load('exam_report', $language);
	$id = htmlentities(($this->uri->segment(3)));
//        $this->data['exams'] = $this->exam_m->get_exam();
	$this->data['classes'] = $this->classes_m->get_classes();
	$classesID = $this->input->post("classesID");
	if ($classesID != 0) {
	    $this->data['subjects'] = $this->subject_m->get_order_by_subject(array("classesID" => $classesID));
	} else {
	    $this->data['subjects'] = "empty";
	}
	$this->data['subjectID'] = 0;
	$this->data['students'] = array();
	$year = date("Y");

	if ($_POST && $classesID != 0) {
	    $classID = $this->input->get_post('classesID');
	    $academic_year_id = $this->input->get_post('academic_year_id');

	    unset($_POST['classesID']);
	    unset($_POST['academic_year_id']);
	    unset($_POST['all']);

	    $examID = $this->input->get_post('examID');
	    $subjectID = $this->input->get_post('subjectID');
	    $year = date("Y");

	    $exams = [];

	    foreach ($_POST as $key => $value) {
		if ((int) $value <= 0) {
		    unset($_POST[$key]);
		}
		if (preg_match('/percentage/', $key)) {
		    unset($_POST[$key]);
		}
	    }
	
	    $combine_exams_array = array_flip($_POST);
	    if (!array_filter($_POST)) {
		$this->session->set_flashdata('error', 'You need to select at least one exam');
		redirect(base_url("exam/combined"));
	    }
	    /*
	     * ----------------------------------------------------------
	     * Check the total marks from a certain exam and combine them
	     * with a specified percentage
	     * ----------------------------------------------------------
	     */
//	    $exam_pecentage = 0.5;
//	    $marks = $this->mark_m->get_order_by_mark(array("classesID" => $classID, "year" => $year));
//	    foreach ($marks as $mark) {
//		$subject[$mark->subject][$mark->studentID]+=$mark->mark * 0.5;
//	    }


	    $this->data['eattendances'] = $this->eattendance_m->get_order_by_eattendance(array("examID" => $examID, "classesID" => $classID, "subjectID" => $subjectID, 'year' => $year));
	    $this->data['students'] = $this->student_m->get_order_by_student(array("classesID" => $classID));
	    //if (count($this->data['students'])) {

	    if ($this->data['students']) {
		$sections = $this->section_m->get_order_by_section(array("classesID" => $classesID));
		$this->data['sections'] = $sections;
		foreach ($sections as $key => $section) {
		    $this->data['allsection'][$section->section] = $this->examreport_m->combinesql($classesID, $combine_exams_array, $section->sectionID,$academic_year_id);
		}
	    } else {
		$this->data['students'] = NULL;
	    }

	    $this->data['combine_exams'] = $this->examreport_m->combinesql($classesID, $combine_exams_array,NULL,$academic_year_id);

	    $this->data['examID'] = $examID;
	    $this->data['classesID'] = $classesID;
	    $this->data['subjectID'] = $subjectID;
	    $data = array_flip($combine_exams_array);
	    $url_ids = array();
	    $out = '';

	    foreach ($data as $key => $value) {
		if ((int) $value > 0) {
		    $out .=$value != '' ? ' "examID"=' . $value . ' or ' : '';
		    $value != '' ? array_push($url_ids, $value) : '';
		}
	    }
	    $where = rtrim($out, ' or ');
	    $title_sql = 'SELECT * FROM ' . set_schema_name() . 'exam WHERE ' . $where;
	    $this->data['exams_title'] = $this->db->query($title_sql)->result();
	    $report_sent = $this->db->query("SELECT DISTINCT combined_exams FROM " . set_schema_name() . 'exam_report WHERE "classesID"=' . $classesID)->result();
	    $report_sent_status = 0;

	    foreach ($report_sent as $sent_report) {
		$array1 = explode('_', $sent_report->combined_exams);

		$array2 = $url_ids;

		if ($array1 === array_intersect($array1, $array2) && $array2 === array_intersect($array2, $array1)) {
		    $report_sent_status = 1;
		} else {
		    
		}
	    }
	    $this->data['report_sent'] = $report_sent_status;
	    //print_r($this->data); exit;
	    $this->data["subview"] = "exam/combined";
	    $this->load->view('_layout_main', $this->data);
	} else {
	    $this->data["subview"] = "exam/combined";
	    $this->load->view('_layout_main', $this->data);
	}
    }

    public function singlecombined($student_id = NULL) {
	$language = $this->session->userdata('lang');
	$this->lang->load('exam_report', $language);
	$id = htmlentities(($this->uri->segment(3)));

	$this->data['tag'] = $this->input->get_post('tag');
	$data = explode('_', $this->input->get_post('exam'));
	$out = '';
	foreach ($data as $value) {
	    $out .= ' "examID"=' . $value . ' or ';
	}

	$where = rtrim($out, ' or ');

	$this->data['exams'] = $this->db->query('SELECT * FROM ' . set_schema_name() . 'exam WHERE ' . $where . ' ORDER BY date ASC ')->result();
	$student_id_report = $student_id == NULL ? $id : $student_id;
	$student = $this->student_m->get_student($student_id_report);


	$this->data["signature"] = $this->setting_m->get_signature();
	$this->data['subjects'] = $this->subject_m->get_subject_in_section($student->sectionID);
	$this->data['subjectID'] = 0;
	if ($this->data['tag'] == 'all') {
	    $this->data['all_students'] = $this->student_m->get_order_by_student(array("classesID" => $student->classesID));
	}
	$this->data['students'] = $this->student_m->get_order_by_student(array("classesID" => $student->classesID, 'sectionID' => $student->sectionID));

	$this->data['year'] = date("Y");
	$this->data["classes"] = $this->student_m->get_class($student->classesID);
	// $this->data['eattendances'] = $this->eattendance_m->get_order_by_eattendance(array("examID" => $examID, "classesID" => $classID, "subjectID" => $subjectID, 'year' => $year));
	//if (count($this->data['students'])) {

	$this->data["grades"] = $this->grade_m->get_order_by_grade(array('classlevel_id' => $this->data['classes']->classlevel_id));
	$this->data["marks"] = $this->mark_m->get_order_by_mark(array("studentID" => $student->studentID, "classesID" => $student->classesID));
	$this->data['level'] = $this->exam_m->get_result_format($student->studentID);

	$this->data['classesID'] = $student->classesID;
	$this->data['rank'] = $this->input->get_post('rank');
	;
	$this->data['student'] = $student;
	$this->data["class_teacher_signature"] = $this->teacher_m->get_teacher_signature($student->sectionID);
	$this->data["subview"] = "exam/singlecombined";
	$this->load->view('_layout_main', $this->data);
    }

    public function get_total_marks($student_id, $exam_id, $class_id, $subjectID = null) {
	//total marks for that student in all subjects

	if ($subjectID == '0') {
	    $sql = 'select total as total_mark,rank from  ' . set_schema_name() . 'mark_grade where "examID"=' . $exam_id . ' AND "classesID"=' . $class_id . ' AND "studentID"=' . $student_id;
	} else {
	    $sql = 'select sum(mark) as total_mark from ' . set_schema_name() . 'mark where "examID"=' . $exam_id . ' AND "classesID"=' . $class_id . ' AND "studentID"=' . $student_id . ' AND "subjectID"=' . $subjectID;
	}

	return $this->db->query($sql)->row();
    }

    public function get_mark_by_section($student_id, $exam_id, $class_id, $subjectID, $sectionID) {
	$sql = 'select sum(mark) as  total_mark, avg(mark) as average from  ' . set_schema_name() . 'mark where "examID"=' . $exam_id . ' AND "classesID"=' . $class_id . '  AND "subjectID"=' . $subjectID . ' AND "studentID" IN (select "studentID" from ' . set_schema_name() . 'student WHERE "sectionID"=' . $sectionID . ')';
	return $this->db->query($sql)->row();
    }

    function getCseeDivision($student_id, $exam_id, $class_id) {
	$class = $this->classes_m->get_classes($class_id);
	$grades = $this->grade_m->get_order_by_grade(array('classlevel_id' => $class->classlevel_id));
//	$marks = $this->mark_m->get_order_by_mark(array('studentID' => $student_id, 'examID' => $exam_id, 'classesID' => $class_id));
	$marks = $this->db->query('select a.*,s.subject_code, s.is_counted, s.is_penalty from ' . set_schema_name() . 'mark a join ' . set_schema_name() . 'subject s ON s."subjectID"=a."subjectID" where a."studentID"=' . $student_id . ' AND a."examID"=' . $exam_id . ' AND a."classesID"=' . $class_id . ' LIMIT 7')->result();

	$sql = 'select a.*,s.* from ' . set_schema_name() . 'mark a join ' . set_schema_name() . 'subject s ON s."subjectID"=a."subjectID" where a."studentID"=' . $student_id . ' AND a."examID"=' . $exam_id . ' AND a."classesID"=' . $class_id . ' AND s.is_penalty=1';
	$penalty_marks = $this->db->query($sql)->result();

	$total_point = 0;
	$penalty_point = 0;
	$failed = 0;

	foreach ($penalty_marks as $mark) {
	    /**
	     * if this subject is NOT counted in division and has got a penalty if user failed
	     */
	    //  $arr = [];
//	    foreach ($grades as $grade) {
//		if ($grade->gradefrom == 0) {
//		    if ($mark->mark < $grade->gradeupto) {
//			$penalty_point += $grade->point;
//		    }
//		}
//	    }
	    if ($mark->mark < $mark->pass_mark) {
		$failed = 1;
	    }
	    // $failed = $penalty_point > 0 ? 1 : 0;
	}

	foreach ($marks as $mark) {

	    foreach ($grades as $grade) {
		if ($grade->gradefrom <= $mark->mark && $grade->gradeupto >= $mark->mark) {
		    $total_point += $grade->point;
		}
	    }
	}
	return array($total_point, $failed);
    }

    /**
     *
     * @param type $studentID : Student ID number
     * @param type $examID    : Exam ID
     * @return type		: Array (Division, total points)
     */
    public function getDivision($studentID, $examID) {
	$level = $this->exam_m->get_result_format($studentID);
	if ($level->result_format == 'ACSEE') {
	    $divisionpoints = $this->getAcseeDivision($studentID, $examID, $level->classesID);
	    $division = acseeDivision($divisionpoints[0], $divisionpoints[1]);
	    $points = $divisionpoints[0];
	} else if ($level->result_format == 'CSEE') {
	    $divisionpoints = $this->getCseeDivision($studentID, $examID, $level->classesID);
	    $division = cseeDivision($divisionpoints[0], $divisionpoints[1]);
	    $points = $divisionpoints[0];
	} else {
	    $division = 'NULL';
	    $points = 'NULL';
	}
	return array($division, $points);
    }

    public function getAcseeDivision($student_id, $exam_id, $class_id) {
	$class = $this->classes_m->get_classes($class_id);
	$grades = $this->grade_m->get_order_by_grade(array('classlevel_id' => $class->classlevel_id));
//	$marks = $this->mark_m->get_order_by_mark(array('studentID' => $student_id, 'examID' => $exam_id, 'classesID' => $class_id));
	$marks = $this->db->query('select a.*,s.subject_code, s.is_counted, s.is_penalty,s.pass_mark from ' . set_schema_name() . 'mark a join ' . set_schema_name() . 'subject s ON s."subjectID"=a."subjectID" where a."studentID"=' . $student_id . ' AND a."examID"=' . $exam_id . ' AND a."classesID"=' . $class_id)->result();

	$total_point = 0;
	$penalty_point = 0;
	$failed = 0;
	foreach ($marks as $mark) {

	    if ($mark->is_counted == 0 && $mark->is_penalty == 1) {
		/**
		 * if this subject is NOT counted in division and has got a penalty is user failed
		 */
//		$arr = [];
//		foreach ($grades as $grade) {
//		    array_push($arr, $grade->point);
//		    if ($grade->gradefrom <= $mark->mark && $grade->gradeupto >= $mark->mark) {
//			$penalty_point += $grade->point;
//		    }
//		}
		if ($mark->mark < $mark->pass_mark) {
		    $failed = 1;
		}
		//$failed = $penalty_point < max($arr) ? 0 : 1;
		/**
		 * if special point is greater than 1, then this user pass well the exam otherwise count penalty
		 */
	    } else if ($mark->is_counted == 1) {
		foreach ($grades as $grade) {
		    if ($grade->gradefrom <= $mark->mark && $grade->gradeupto >= $mark->mark) {
			$total_point += $grade->point;
		    }
		}
	    }
	}
	return array($total_point, $failed);
    }

    public function get_student_position($student_id, $exam_id, $subject_id) {
	$sql = 'select a.dense_rank, b.exam, c.subject from  ' . set_schema_name() . 'mark_ranking a JOIN ' . set_schema_name() . 'exam b on b."examID"=a."examID" join ' . set_schema_name() . 'subject c ON c."subjectID"=a."subjectID"  where a."examID"=' . $exam_id . ' AND  a."studentID"=' . $student_id . ' AND a."subjectID"=' . $subject_id;
	return $this->db->query($sql)->row();
    }

    public function get_student_position_persubject($student_id, $examID, $subject_id, $classesID, $sectionID = null) {
	if ($sectionID == NULL) {

	    $sql = 'select subject, mark, rank FROM (
select a."examID", a."subject", a."classesID", a."studentID", a.mark, rank() over (partition by a."subjectID" order by a.mark desc) from ' . set_schema_name() . 'mark a where a."examID" = ' . $examID . ' and a."classesID" =' . $classesID . ' and "subjectID" = ' . $subject_id . '  and a.mark is not null
) a where "studentID"=' . $student_id;
	} else {
	    $sql = 'select subject, mark, rank FROM ( select subject, a."examID", a."classesID", a."studentID", a.mark, rank() over (partition by a."subjectID" order by a.mark desc) from ' . set_schema_name() . 'mark a where a."examID" = ' . $examID . ' and a."classesID" = ' . $classesID . ' and "subjectID" =' . $subject_id . ' and "studentID" in (select "studentID" from ' . set_schema_name() . 'student where "sectionID" = ' . $sectionID . ') and a.mark is not null) a where "studentID"=' . $student_id;
	}
	return $this->db->query($sql)->row();
    }

    public function get_rank_per_all_subjects($examID, $classesID, $studentID, $sectionID = null) {
	if ($sectionID == NULL) {
	    $sql = 'SELECT sum,rank,average FROM (
select "examID", "classesID", "studentID", avg(mark) as average , sum(mark), rank() over (order by avg(mark) desc) from ' . set_schema_name() . 'mark where "examID" = ' . $examID . ' and "classesID" = ' . $classesID . ' AND "subjectID" IN (select "subjectID" from ' . set_schema_name() . 'subject where is_counted=1) and mark is not null group by "examID", "classesID", "studentID"  ) a WHERE a."studentID"=' . $studentID . '';
	} else {
	    $sql = 'SELECT sum,rank, average FROM (
select a."examID", a."classesID", a."studentID", avg(a.mark) as average ,sum(a.mark), rank() over (order by avg(a.mark) desc) from ' . set_schema_name() . 'mark a where a."examID" = ' . $examID . ' and a."classesID" = ' . $classesID . ' and a."studentID" in (select "studentID" from ' . set_schema_name() . 'student where "sectionID" = ' . $sectionID . ')  AND a."subjectID" IN (select "subjectID" from ' . set_schema_name() . 'subject where is_counted=1) and a.mark is not null AND a.mark !=0 group by a."examID", a."classesID", a."studentID") a WHERE a."studentID"=' . $studentID . '';
	}
	return $total = $this->db->query($sql)->row();
    }

    public function get_by_section($sectionID) {
	return $this->student_m->get_order_by_student(array("sectionID" => $sectionID));
    }

    public function send_mail() {
	$usertype = $this->session->userdata("usertype");
	if ($usertype == "Admin") {
	    $id = $this->input->post('id');
	    if ((int) $id) {
		$this->data['parentes'] = $this->parentes_m->get_parentes($id);
		if ($this->data['parentes']) {
		    $html = $this->singlereport();
		    $this->load->library('html2pdf');
		    $this->html2pdf->folder('uploads/report');
		    $this->html2pdf->filename('Report.pdf');
		    $this->html2pdf->paper('a4', 'portrait');
		    $this->data['panel_title'] = $this->lang->line('panel_title');
		    $this->html2pdf->html($html);
		    $this->html2pdf->create('save');

		    if ($path = $this->html2pdf->create('save')) {
			$this->load->library('email');
			$this->email->set_mailtype("html");
			$this->email->from($this->data["siteinfos"]->email, $this->data['siteinfos']->sname);
			$this->email->to($this->input->post('to'));
			$this->email->subject($this->input->post('subject'));
			$this->email->message($this->input->post('message'));
			$this->email->attach($path);
			if ($this->email->send()) {
			    $this->session->set_flashdata('success', $this->lang->line('mail_success'));
			} else {
			    $this->session->set_flashdata('error', $this->lang->line('mail_error'));
			}
		    }
		} else {
		    $this->data["subview"] = "error";
		    $this->load->view('_layout_main', $this->data);
		}
	    } else {
		$this->data["subview"] = "error";
		$this->load->view('_layout_main', $this->data);
	    }
	} else {
	    $this->data["subview"] = "error";
	    $this->load->view('_layout_main', $this->data);
	}
    }

    public function print_preview() {
	$id = htmlentities(($this->uri->segment(3)));
	$url = htmlentities(($this->uri->segment(4)));

	if ((int) $id) {
	    $usertype = $this->session->userdata('usertype');
	    if ($usertype == "Admin" || strtolower($usertype) == 'parent') {
		$this->load->library('html2pdf');
		$this->html2pdf->folder('./assets/pdfs/');
		$this->html2pdf->filename('Report.pdf');
		$this->html2pdf->paper('a4', 'portrait');


		$this->data['panel_title'] = $this->lang->line('panel_title');
		$html = $this->load->view('exam/singlereport', $this->singlereport(), true);
		$this->html2pdf->html($html);
		$this->html2pdf->create();
	    } else {
		$this->data["subview"] = "error";
		$this->load->view('_layout_main', $this->data);
	    }
	} else {
	    $this->data["subview"] = "error";
	    $this->load->view('_layout_main', $this->data);
	}
    }

    function send_report() {
	$usertype = $this->session->userdata('usertype');
	$username = $this->session->userdata('username');
	$password = $this->hash($this->input->post('password'));
	$user = $this->db->get_where('user', array("username" => $username, "password" => $password))->row();
	if (!empty($user)) {
	    if ($usertype == "Admin") {
		$exams = explode('_', $this->input->get_post('exam'));
		foreach ($exams as $value) {
		    $combine_exams_array['exam' . $value] = $value;
		}
		$combine_exams = $this->examreport_m->combinesql($this->input->get_post('class'), array_flip($combine_exams_array));
		foreach ($combine_exams as $val) {
		    $this->db->insert('exam_report', array(
			'classesID' => $this->input->get_post('class'),
			'combined_exams' => $this->input->get_post('exam'),
			'name' => $this->input->get_post('report_name'),
			'rank' => $val['rank'],
			'student_id' => $val['studentID']
		    ));
		}

		echo json_encode(array(
		    'status' => 'success',
		    'message' => 'Report generated and sent to all parents in this class'));
	    } else {
		echo json_encode(array(
		    'status' => 'warning',
		    'message' => 'Only Admin can send final reports to all parentss'));
	    }
	} else {
	    echo json_encode(array(
		'status' => 'warning',
		'message' => 'Password do not match with administrator password'));
	}
    }

    public function printall_combined() {
	$language = $this->session->userdata('lang');
	$this->lang->load('exam_report', $language);
	$id = htmlentities(($this->uri->segment(3)));
	$classesID = $this->input->get_post('class_id');
	$sectionID = $this->input->get_post('section_id');

	$data = explode('_', $this->input->get_post('exam'));
	$out = '';
	foreach ($data as $value) {
	    $out .= ' "examID"=' . $value . ' or ';
	}

	$where = rtrim($out, ' or ');

	$this->data['exams'] = $this->db->query('SELECT * FROM ' . set_schema_name() . 'exam WHERE ' . $where . '  ORDER BY date ASC')->result();

	$examarray = array();
	foreach ($this->data['exams'] as $value) {
	    $examarray[$value->examID] = $value->exam;
	}

	$this->data["signature"] = $this->setting_m->get_signature();
	$this->data['subjects'] = $this->subject_m->get_subject_in_section($sectionID);
	$this->data['students'] = $this->examreport_m->combinesql($classesID, $examarray, $sectionID);
	$this->data["classes"] = $this->student_m->get_class($classesID);
	$this->data["grades"] = $this->grade_m->get_order_by_grade(array('classlevel_id' => $this->data['classes']->classlevel_id));

	foreach ($this->data['students'] as $student_info) {
	    $student = (object) $student_info;

	    $this->data['subjectID'] = 0;

	    // $this->data['eattendances'] = $this->eattendance_m->get_order_by_eattendance(array("examID" => $examID, "classesID" => $classID, "subjectID" => $subjectID, 'year' => $year));
	    //if (count($this->data['students'])) {

	    $this->data["marks"][$student->studentID] = $this->mark_m->get_order_by_mark(array("studentID" => $student->studentID, "classesID" => $classesID));
	    $this->data['level'] = $this->exam_m->get_result_format($student->studentID);

	    $this->data['classesID'] = $classesID;
	    $student_id = $student->studentID;
	}
	$info = $this->student_m->get_student($student_id);
	$this->data["class_teacher_signature"] = $this->teacher_m->get_teacher_signature($info->sectionID);
	$this->data['year'] = $info->year;
	$this->data['section'] = $info->section;
	$this->data['sectionID'] = $info->sectionID;
	$this->load->view('examreport/print_combined', $this->data);
    }

    public function printall() {
	//should pass either class ID or SectionID with examID
	$id = htmlentities(($this->uri->segment(3))); //examID
	$classesID = $this->input->get_post('class_id');
	$sectionID = $this->input->get_post('section_id');

	if (empty($classesID) && empty($sectionID)) {
	    die('Wrong url supplied');
	} else {
	    $exam_id = $id;
	    $this->data["exams"] = $this->exam_m->get_exam($exam_id);
	    $this->data["classes"] = $this->student_m->get_class($classesID);
	    $this->data["signature"] = $this->setting_m->get_signature();

	    $where_student = $sectionID == '' ? array("classesID" => $classesID) : array("classesID" => $classesID, 'sectionID' => $sectionID);
	    $this->data["grades"] = $this->grade_m->get_order_by_grade(array('classlevel_id' => $this->data['classes']->classlevel_id));
	    $this->data["class_teacher_signature"] = $this->teacher_m->get_teacher_signature($sectionID);

	    $this->data['students'] = $this->student_m->get_order_by_student($where_student);
	    foreach ($this->data['students'] as $student) {

		$this->data["student"][$student->studentID] = $this->student_m->get_student($student->studentID);




		$this->data["marks"][$student->studentID] = $this->db->query('SELECT * FROM ' . set_schema_name() . 'mark WHERE "subjectID" in (SELECT subject_id FROM ' . set_schema_name() . 'subject_section WHERE section_id=' . $sectionID . ') AND "studentID"=' . $student->studentID . ' and "classesID"=' . $student->classesID . ' AND "examID"=' . $exam_id . ' AND mark !=0 AND mark is not NULL ')->result();


		$this->data['report'][$student->studentID] = $this->get_rank_per_all_subjects($exam_id, $classesID, $student->studentID, $sectionID);

		$this->data['level'] = $classlevel = $this->db->query('select result_format FROM ' . set_schema_name() . 'classlevel WHERE classlevel_id=' . $this->data['classes']->classlevel_id)->row();

		if ($classlevel->result_format == 'ACSEE') {
		    $division = $this->getAcseeDivision($student->studentID, $exam_id, $classesID);
		    $this->data['division'][$student->studentID] = acseeDivision($division[0], $division[1]);
		} else if ($classlevel->result_format == 'CSEE') {
		    $division = $this->getCseeDivision($student->studentID, $exam_id, $classesID);
		    $this->data['division'][$student->studentID] = cseeDivision($division[0], $division[1]);
		}
		$this->data['points'][$student->studentID] = $division[0];
	    }
	}

	$this->load->view('examreport/print', $this->data);
    }

    public function loadReports($student_id) {
	$language = $this->session->userdata('lang');
	$this->lang->load('exam_report', $language);



	$data = explode('_', $this->input->get_post('exam'));
	$out = '';
	foreach ($data as $value) {
	    $out .= ' "examID"=' . $value . ' or ';
	}

	$where = rtrim($out, ' or ');

	$this->data['exams'] = $this->db->query('SELECT * FROM ' . set_schema_name() . 'exam WHERE ' . $where)->result();
	$student = $this->student_m->get_student($student_id);

	$this->data['subjects'] = $this->subject_m->get_order_by_subject(array("classesID" => $student->classesID));
	$this->data['subjectID'] = 0;
	$this->data['students'] = $this->student_m->get_order_by_student(array("classesID" => $student->classesID));
	;
	$this->data['year'] = date("Y");
	$this->data["classes"] = $this->student_m->get_class($student->classesID);
	// $this->data['eattendances'] = $this->eattendance_m->get_order_by_eattendance(array("examID" => $examID, "classesID" => $classID, "subjectID" => $subjectID, 'year' => $year));
	//if (count($this->data['students'])) {

	$this->data["grades"] = $this->grade_m->get_order_by_grade(array('classlevel_id' => $this->data['classes']->classlevel_id));
	$this->data["marks"] = $this->mark_m->get_order_by_mark(array("studentID" => $student->studentID, "classesID" => $student->classesID));
	$this->data['level'] = $this->exam_m->get_result_format($student->studentID);

	$this->data['classesID'] = $student->classesID;
	$this->data['rank'] = $this->input->get_post('rank');
	;
	$this->data['student'] = $student;

	$this->data["subview"] = "examreport/print_all";
	$this->load->view('_layout_main', $this->data);
    }

    public function hash($string) {
	return hash("sha512", $string . config_item("encryption_key"));
    }

}

/* End of file exam.php */
/* Location: .//D/xampp/htdocs/school/mvc/controllers/exam.php */