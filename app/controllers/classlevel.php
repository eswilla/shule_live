<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Classlevel extends Admin_Controller {

    /**
     * -----------------------------------------
     * 
     * ******* Address****************
     * INETS COMPANY LIMITED
     * P.O BOX 32258, DAR ES SALAAM
     * TANZANIA
     * 
     * 
     * *******Office Location *********
     * 11th block, Bima Road, Mikocheni B, Kinondoni, Dar es salaam
     * 
     * 
     * ********Contacts***************
     * Email: <info@inetstz.com>
     * Website: <www.inetstz.com>
     * Mobile: <+255 655 406 004>
     * Tel:    <+255 22 278 0228>
     * -----------------------------------------
     */
    function __construct() {
	parent::__construct();
	$language = $this->session->userdata('lang');
	$this->lang->load('classlevel', $language);
    }

    public function index() {
	$usertype = $this->session->userdata("usertype");
	if ($usertype == "Admin") {
	    $this->data['classlevels'] = $this->classlevel_m->get_order_by_classlevel();
	    $this->data["subview"] = "classlevel/index";
	    $this->load->view('_layout_main', $this->data);
	} else {
	    $this->data["subview"] = "error";
	    $this->load->view('_layout_main', $this->data);
	}
    }

    protected function rules() {
	$rules = array(
	    array(
		'field' => 'name',
		'label' => $this->lang->line("name"),
		'rules' => 'trim|required|xss_clean|max_length[60]|callback_unique_classlevel'
	    ),
	    array(
		'field' => 'span_number',
		'label' => $this->lang->line("span_number"),
		'rules' => 'trim|required|numeric|max_length[11]|xss_clean'
	    ),
	    array(
		'field' => 'start_date',
		'label' => $this->lang->line("start_date"),
		'rules' => 'trim|required|max_length[20]|xss_clean'
	    ),
	    array(
		'field' => 'end_date',
		'label' => $this->lang->line("end_date"),
		'rules' => 'trim|required|max_length[20]|xss_clean'
	    ),
	    array(
		'field' => 'note',
		'label' => $this->lang->line("note"),
		'rules' => 'trim|max_length[200]|xss_clean'
	    )
	);
	return $rules;
    }

    public function add() {
	$usertype = $this->session->userdata("usertype");
	if ($usertype == "Admin") {
	    if ($_POST) {
		$rules = $this->rules();
		$this->form_validation->set_rules($rules);
		if ($this->form_validation->run() == FALSE) {
		    $this->data["subview"] = "classlevel/add";
		    $this->load->view('_layout_main', $this->data);
		} else {
		    $array = array(
			"name" => $this->input->post("name"),
			"start_date" => date("Y-m-d", strtotime($this->input->post("start_date"))),
			"end_date" => date("Y-m-d", strtotime($this->input->post("end_date"))),
			"span_number" => $this->input->post("span_number"),
			"note" => $this->input->post("note"),
			"result_format" => $this->input->post("result_format"),
		    );

		    $this->classlevel_m->insert_classlevel($array);
		    //$this->db->insert('classlevel', $_POST);
		    $this->session->set_flashdata('success', $this->lang->line('menu_success'));
		    redirect(base_url("classlevel/index"));
		}
	    } else {
		$this->data["subview"] = "classlevel/add";
		$this->load->view('_layout_main', $this->data);
	    }
	} else {
	    $this->data["subview"] = "error";
	    $this->load->view('_layout_main', $this->data);
	}
    }

    public function edit() {
	$usertype = $this->session->userdata("usertype");
	if ($usertype == "Admin") {
	    $id = htmlentities(($this->uri->segment(3)));
	    if ((int) $id) {
		$this->data['classlevel'] = $this->classlevel_m->get_classlevel($id);
		if ($this->data['classlevel']) {
		    if ($_POST) {
			$rules = $this->rules();
			$this->form_validation->set_rules($rules);
			if ($this->form_validation->run() == FALSE) {
			    $this->data["subview"] = "classlevel/edit";
			    $this->load->view('_layout_main', $this->data);
			} else {			   

			    $this->classlevel_m->update_classlevel($_POST, $id);
			    $this->session->set_flashdata('success', $this->lang->line('menu_success'));
			    redirect(base_url("classlevel/index"));
			}
		    } else {
			$this->data["subview"] = "classlevel/edit";
			$this->load->view('_layout_main', $this->data);
		    }
		} else {
		    $this->data["subview"] = "error";
		    $this->load->view('_layout_main', $this->data);
		}
	    } else {
		$this->data["subview"] = "error";
		$this->load->view('_layout_main', $this->data);
	    }
	} else {
	    $this->data["subview"] = "error";
	    $this->load->view('_layout_main', $this->data);
	}
    }

    public function delete() {
	$usertype = $this->session->userdata("usertype");
	if ($usertype == "Admin") {
	    $id = htmlentities(($this->uri->segment(3)));
	    if ((int) $id) {
		$this->classlevel_m->delete_classlevel($id);
		$this->session->set_flashdata('success', $this->lang->line('menu_success'));
		redirect(base_url("classlevel/index"));
	    } else {
		redirect(base_url("classlevel/index"));
	    }
	} else {
	    $this->data["subview"] = "error";
	    $this->load->view('_layout_main', $this->data);
	}
    }

    public function unique_classlevel() {
	$id = htmlentities(($this->uri->segment(3)));
	if ((int) $id) {
	    $classlevel = $this->classlevel_m->get_order_by_classlevel(array("name" => $this->input->post("name"), "classlevel_id !=" => $id));
	    if (count($classlevel)) {
		$this->form_validation->set_message("unique_classlevel", "%s already exists");
		return FALSE;
	    }
	    return TRUE;
	} else {
	    $classlevel = $this->classlevel_m->get_order_by_classlevel(array("name" => $this->input->post("name")));

	    if (count($classlevel)) {
		$this->form_validation->set_message("unique_classlevel", "%s already exists");
		return FALSE;
	    }
	    return TRUE;
	}
    }

    public function unique_point() {
	$id = htmlentities(($this->uri->segment(3)));
	if ((int) $id) {
	    $classlevel = $this->classlevel_m->get_order_by_classlevel(array("point" => $this->input->post("point"), "classlevel_id !=" => $id));
	    if (count($classlevel)) {
		$this->form_validation->set_message("unique_point", "%s already exists");
		return FALSE;
	    }
	    return TRUE;
	} else {
	    $classlevel = $this->classlevel_m->get_order_by_classlevel(array("point" => $this->input->post("point")));

	    if (count($classlevel)) {
		$this->form_validation->set_message("unique_point", "%s already exists");
		return FALSE;
	    }
	    return TRUE;
	}
    }

    public function unique_classlevelfrom() {
	$id = htmlentities(($this->uri->segment(3)));
	if ((int) $id) {
	    $classlevel = $this->classlevel_m->get_order_by_classlevel(array("classlevelfrom" => $this->input->post("classlevelfrom"), "classlevelID !=" => $id));
	    if (count($classlevel)) {
		$this->form_validation->set_message("unique_classlevelfrom", "%s already exists");
		return FALSE;
	    }
	    return TRUE;
	} else {
	    $classlevel = $this->classlevel_m->get_order_by_classlevel(array("classlevelfrom" => $this->input->post("classlevelfrom")));

	    if (count($classlevel)) {
		$this->form_validation->set_message("unique_classlevelfrom", "%s already exists");
		return FALSE;
	    }
	    return TRUE;
	}
    }

    public function unique_classlevelupto() {
	$id = htmlentities(($this->uri->segment(3)));
	if ((int) $id) {
	    $grade = $this->grade_m->get_order_by_grade(array("gradeupto" => $this->input->post("gradeupto"), "gradeID !=" => $id));
	    if (count($grade)) {
		$this->form_validation->set_message("unique_gradeupto", "%s already exists");
		return FALSE;
	    }
	    return TRUE;
	} else {
	    $grade = $this->grade_m->get_order_by_grade(array("gradeupto" => $this->input->post("gradeupto")));

	    if (count($grade)) {
		$this->form_validation->set_message("unique_gradeupto", "%s already exists");
		return FALSE;
	    }
	    return TRUE;
	}
    }

}
