<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Vendor extends Admin_Controller {

    /**
     * -----------------------------------------
     * 
     * ******* Address****************
     * INETS COMPANY LIMITED
     * P.O BOX 32258, DAR ES SALAAM
     * TANZANIA
     * 
     * 
     * *******Office Location *********
     * 11th block, Bima Road, Mikocheni B, Kinondoni, Dar es salaam
     * 
     * 
     * ********Contacts***************
     * Email: <info@inetstz.com>
     * Website: <www.inetstz.com>
     * Mobile: <+255 655 406 004>
     * Tel:    <+255 22 278 0228>
     * -----------------------------------------
     */
    function __construct() {
	parent::__construct();
	$language = $this->session->userdata('lang');
	$this->lang->load('vendor', $language);
    }

    public function index() {
	$id = htmlentities($this->uri->segment(3));
	if ((int) $id) {
	    $this->data['set'] = $id;
	    $this->data['classes'] = $this->student_m->get_classes();
	    $this->data['students'] = $this->student_m->get_order_by_student(array('classesID' => $id));

	    $this->data['students'] = NULL;

	    $this->data["subview"] = "student/index";
	    $this->load->view('_layout_main', $this->data);
	} else {
	    $this->data['vendors'] = $this->db->query('SELECT * FROM ' . set_schema_name() . 'vendor ')->result();
	    $this->data["subview"] = "vendor/index";
	    $this->load->view('_layout_main', $this->data);
	}
    }

    protected function rules() {
	$rules = array(
	    array(
		'field' => 'email',
		'label' => $this->lang->line("student_email"),
		'rules' => 'trim|required|max_length[40]|valid_email|xss_clean|callback_unique_email'
	    ),
	    array(
		'field' => 'phone_number',
		'label' => $this->lang->line("phone_number"),
		'rules' => 'trim|required|numeric|max_length[11]|xss_clean|callback_valid_number'
	    ),
	    array(
		'field' => 'telephone_number',
		'label' => $this->lang->line("telephone_number"),
		'rules' => 'trim|required|numeric|max_length[11]|xss_clean|callback_valid_number'
	    ),
	    array(
		'field' => 'name',
		'label' => $this->lang->line("name"),
		'rules' => 'trim|required|xss_clean|max_length[60]'
	    ),
	    array(
		'field' => 'location',
		'label' => $this->lang->line("location"),
		'rules' => 'trim|required|xss_clean|max_length[200]'
	    ),
	    array(
		'field' => 'country',
		'label' => $this->lang->line("country"),
		'rules' => 'trim|required|xss_clean|max_length[250]'
	    ),
	    array(
		'field' => 'city',
		'label' => $this->lang->line("city"),
		'rules' => 'trim|required|xss_clean|max_length[120]'
	    ),
	    array(
		'field' => 'bank_branch',
		'label' => $this->lang->line("bank_branch"),
		'rules' => 'trim|required|xss_clean|max_length[60]'
	    ),
	    array(
		'field' => 'bank_name',
		'label' => $this->lang->line("bank_name"),
		'rules' => 'trim|required|xss_clean|max_length[60]'
	    ),
	    array(
		'field' => 'account_number',
		'label' => $this->lang->line("account_number"),
		'rules' => 'trim|required|xss_clean|max_length[60]'
	    ),
	    array(
		'field' => 'contact_person_name',
		'label' => $this->lang->line("contact_person_name"),
		'rules' => 'trim|required|xss_clean|max_length[60]'
	    ),
	    array(
		'field' => 'contact_person_email',
		'label' => $this->lang->line("contact_person_email"),
		'rules' => 'trim|required|max_length[40]|valid_email|xss_clean|callback_unique_email'
	    ),
	);
	return $rules;
    }

    public function add() {

	$usertype = $this->session->userdata("usertype");
	if ($usertype == "Admin") {
	    if ($_POST) {
		$rules = $this->rules();
		$this->form_validation->set_rules($rules);
		if ($this->form_validation->run() == FALSE) {
		    $this->data["subview"] = "vendor/add";
		    $this->load->view('_layout_main', $this->data);
		} else {
		    $array = array(
			"classes" => $this->input->post("classes"),
			"classes_numeric" => $this->input->post("classes_numeric"),
			"teacherID" => $this->input->post("teacherID"),
			"note" => $this->input->post("note")
		    );
		    $this->vendor_m->insert_vendor($_POST);
		    $this->session->set_flashdata('success', $this->lang->line('menu_success'));
		    // $this->send_sms($teacher->phone, 'Hello ' . $teacher->name . ', you have been added as a Teacher of ' . $this->input->post("classes"));

		    redirect(base_url("vendor/index"));
		}
	    } else {
		$this->data["subview"] = "vendor/add";
		$this->load->view('_layout_main', $this->data);
	    }
	} else {
	    $this->data["subview"] = "error";
	    $this->load->view('_layout_main', $this->data);
	}
    }

    public function edit() {

	$usertype = $this->session->userdata("usertype");
	if ($usertype == "Admin") {
	    $id = htmlentities(($this->uri->segment(3)));
	    if ((int) $id) {
		$this->data['vendor'] = $this->vendor_m->get_vendor($id);
		if ($this->data['vendor']) {
		    if ($_POST) {
			$rules = $this->rules();
			$this->form_validation->set_rules($rules);
			if ($this->form_validation->run() == FALSE) {
			    $this->data["subview"] = "vendor/edit";
			    $this->load->view('_layout_main', $this->data);
			} else {
			    $this->vendor_m->update_vendor($_POST, $id);
			    $this->session->set_flashdata('success', $this->lang->line('menu_success'));
			    redirect(base_url("vendor/index"));
			}
		    } else {
			$this->data["subview"] = "vendor/edit";
			$this->load->view('_layout_main', $this->data);
		    }
		} else {
		    $this->data["subview"] = "error";
		    $this->load->view('_layout_main', $this->data);
		}
	    } else {
		$this->data["subview"] = "error";
		$this->load->view('_layout_main', $this->data);
	    }
	} else {
	    $this->data["subview"] = "error";
	    $this->load->view('_layout_main', $this->data);
	}
    }

    public function delete() {
	$usertype = $this->session->userdata("usertype");
	if ($usertype == "Admin") {
	    $id = htmlentities(($this->uri->segment(3)));
	    if ((int) $id) {
		$this->vendor_m->delete_vendor($id);
		$this->session->set_flashdata('success', $this->lang->line('menu_success'));
		redirect(base_url("vendor/index"));
	    } else {
		redirect(base_url("vendor/index"));
	    }
	} else {
	    $this->data["subview"] = "error";
	    $this->load->view('_layout_main', $this->data);
	}
    }

    public function unique_classes() {
	$id = htmlentities(($this->uri->segment(3)));
	if ((int) $id) {
	    $classes = $this->classes_m->get_order_by_classes(array("classes" => $this->input->post("classes"), "classesID !=" => $id));
	    if (count($classes)) {
		$this->form_validation->set_message("unique_classes", "%s already exists");
		return FALSE;
	    }
	    return TRUE;
	} else {
	    $classes = $this->classes_m->get_order_by_classes(array("classes" => $this->input->post("classes")));

	    if (count($classes)) {
		$this->form_validation->set_message("unique_classes", "%s already exists");
		return FALSE;
	    }
	    return TRUE;
	}
    }

    public function view() {
	$id = htmlentities(($this->uri->segment(3)));
	if ((int) $id) {
	    $this->data["vendor"] = $this->vendor_m->get_vendor($id);
	    if ($this->data["vendor"]) {
		$this->data["subview"] = "vendor/view";
		$this->load->view('_layout_main', $this->data);
	    } else {
		$this->data["subview"] = "error";
		$this->load->view('_layout_main', $this->data);
	    }
	} else {
	    $this->data["subview"] = "error";
	    $this->load->view('_layout_main', $this->data);
	}
    }

    function valid_number() {
	if ($this->input->post('classes_numeric') < 0) {
	    $this->form_validation->set_message("valid_number", "%s is invalid number");
	    return FALSE;
	}
	return TRUE;
    }

}

/* End of file class.php */
/* Location: .//D/xampp/htdocs/school/mvc/controllers/class.php */