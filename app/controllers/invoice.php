<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Invoice extends Admin_Controller {

    /**
     * -----------------------------------------
     * 
     * ******* Address****************
     * INETS COMPANY LIMITED
     * P.O BOX 32258, DAR ES SALAAM
     * TANZANIA
     * 
     * 
     * *******Office Location *********
     * 11th block, Bima Road, Mikocheni B, Kinondoni, Dar es salaam
     * 
     * 
     * ********Contacts***************
     * Email: <info@inetstz.com>
     * Website: <www.inetstz.com>
     * Mobile: <+255 655 406 004>
     * Tel:    <+255 22 278 0228>
     * -----------------------------------------
     */
    function __construct() {
        parent::__construct();
        $language = $this->session->userdata('lang');
        $this->lang->load('invoice', $language);


        //require_once(APPPATH . "libraries/Omnipay/vendor/autoload.php");
    }

    public function index() {
        $usertype = $this->session->userdata("usertype");
        $this->data['classes'] = $this->invoice_m->get_classes();
        $id = htmlentities($this->uri->segment(3));
        if ($usertype == "Admin" || $usertype == "Accountant") {

            if ((int) $id) {
                $this->data['set'] = $id;
                $this->data['invoices'] = $this->invoice_m->get_invoice_by_class($id);
                $this->data["subview"] = "invoice/index";
                $this->load->view('_layout_main', $this->data);
            } else {
                $this->data['invoices'] = array();
                $this->data['set'] = 0;
                $this->data["subview"] = "invoice/index";
                $this->load->view('_layout_main', $this->data);
            }
        } elseif ($usertype == "Student") {
            if ((int) $id) {
                $username = $this->session->userdata("username");
                $student = $this->student_m->get_single_student(array("username" => $username));
                $this->data['invoices'] = $this->invoice_m->get_invoice_bystudentID($student->studentID, $id);
                $this->data["subview"] = "invoice/index";
                $this->load->view('_layout_main', $this->data);
            } else {
                $this->data['invoices'] = array();
                $this->data["subview"] = "invoice/index";
                $this->load->view('_layout_main', $this->data);
            }
        } elseif ($usertype == "Parent") {
            $username = $this->session->userdata("username");
            $parent = $this->parentes_m->get_single_parentes(array('username' => $username));
            $this->data['students'] = $this->student_m->get_order_by_student(array('parentID' => $parent->parentID));
            $id = htmlentities(($this->uri->segment(3)));
            if ((int) $id) {
                $checkstudent = $this->student_m->get_single_student(array('studentID' => $id));
                if (count($checkstudent)) {
                    if ($checkstudent->parentID == $parent->parentID) {
                        $classesID = $checkstudent->classesID;
                        $this->data['set'] = $id;
                        $this->data['invoices'] = $this->invoice_m->get_order_by_invoice(array('studentID' => $id));
                        $this->data["subview"] = "invoice/index_parent";
                        $this->load->view('_layout_main', $this->data);
                    } else {
                        $this->data["subview"] = "error";
                        $this->load->view('_layout_main', $this->data);
                    }
                } else {
                    $this->data["subview"] = "error";
                    $this->load->view('_layout_main', $this->data);
                }
            } else {
                $this->data["subview"] = "invoice/search_parent";
                $this->load->view('_layout_main', $this->data);
            }
        } else {
            $this->data["subview"] = "error";
            $this->load->view('_layout_main', $this->data);
        }
    }

    public function bulk_index() {
        $usertype = $this->session->userdata("usertype");
        $this->data['classes'] = $this->invoice_m->get_classes();
        if ($usertype == "Admin" || $usertype == "Accountant") {
            $id = htmlentities($this->uri->segment(3));
            if ((int) $id) {
                $this->data['set'] = $id;
                $this->data['invoices'] = $this->invoice_m->get_invoice_by_class($id);
                $this->data['fees'] = $this->invoice_m->get_distinctinvoice_by_class($id);
                $this->data['classesID'] = $id;
                $this->data["subview"] = "bulk_invoice/bulk_index";
                $this->load->view('_layout_main', $this->data);
            } else {
                $this->data['invoices'] = array();
                $this->data['set'] = 0;
                $this->data["subview"] = "bulk_invoice/bulk_index";
                $this->load->view('_layout_main', $this->data);
            }
        } elseif ($usertype == "Student") {
            $username = $this->session->userdata("username");
            $student = $this->student_m->get_single_student(array("username" => $username));
            $this->data['invoices'] = $this->invoice_m->get_order_by_invoice(array('studentID' => $student->studentID));
            $this->data["subview"] = "invoice/index";
            $this->load->view('_layout_main', $this->data);
        } elseif ($usertype == "Parent") {
            $username = $this->session->userdata("username");
            $parent = $this->parentes_m->get_single_parentes(array('username' => $username));
            $this->data['students'] = $this->student_m->get_order_by_student(array('parentID' => $parent->parentID));
            $id = htmlentities(($this->uri->segment(3)));
            if ((int) $id) {
                $checkstudent = $this->student_m->get_single_student(array('studentID' => $id));
                if (count($checkstudent)) {
                    if ($checkstudent->parentID == $parent->parentID) {
                        $classesID = $checkstudent->classesID;
                        $this->data['set'] = $id;
                        $this->data['invoices'] = $this->invoice_m->get_order_by_invoice(array('studentID' => $id));
                        $this->data["subview"] = "invoice/index_parent";
                        $this->load->view('_layout_main', $this->data);
                    } else {
                        $this->data["subview"] = "error";
                        $this->load->view('_layout_main', $this->data);
                    }
                } else {
                    $this->data["subview"] = "error";
                    $this->load->view('_layout_main', $this->data);
                }
            } else {
                $this->data["subview"] = "invoice/search_parent";
                $this->load->view('_layout_main', $this->data);
            }
        } else {
            $this->data["subview"] = "error";
            $this->load->view('_layout_main', $this->data);
        }
    }

    public function invoice_list() {
        $classID = $this->input->post('id');
        if ((int) $classID) {
            $string = base_url("invoice/index/$classID");
            echo $string;
        } else {
            redirect(base_url("invoice/index"));
        }
    }

    public function bulk_list() {
        $classID = $this->input->post('id');
        if ((int) $classID) {
            $string = base_url("invoice/bulk_index/$classID");
            echo $string;
        } else {
            redirect(base_url("invoice/bulk_index"));
        }
    }

    protected function rules() {
        $rules = array(
            array(
                'field' => 'classesID',
                'label' => $this->lang->line("invoice_classesID"),
                'rules' => 'trim|required|xss_clean|max_length[11]|numeric|callback_unique_classID'
            ),
            array(
                'field' => 'studentID',
                'label' => $this->lang->line("invoice_studentID"),
                'rules' => 'trim|required|xss_clean|max_length[11]|numeric|callback_unique_studentID'
            ),
            array(
                'field' => 'feetypeID',
                'label' => $this->lang->line("invoice_feetype"),
                'rules' => 'trim|required|xss_clean|max_length[128]'
            ),
//	    array(
//		'field' => 'amount',
//		'label' => $this->lang->line("invoice_amount"),
//		'rules' => 'trim|required|xss_clean|max_length[20]|numeric|callback_valid_number'
//	    ),
            array(
                'field' => 'date',
                'label' => $this->lang->line("invoice_date"),
                'rules' => 'trim|required|xss_clean|max_length[20]'
            ),
        );
        return $rules;
    }

    protected function payment_rules() {
        $rules = array(
            array(
                'field' => 'amount',
                'label' => $this->lang->line("invoice_amount"),
                'rules' => 'trim|required|xss_clean|max_length[11]|numeric|callback_valid_number'
            ),
            array(
                'field' => 'payment_method',
                'label' => $this->lang->line("invoice_paymentmethod"),
                'rules' => 'trim|required|xss_clean|max_length[11]|callback_unique_paymentmethod'
            )
        );
        return $rules;
    }

    public function createInvoiceNo() {
        $invoiceNo = rand(1, 999) . substr(str_shuffle('ABCDEFGHJKLMNPQRSTUVWXYZ'), 0, 3) . rand(1, 999);
        $data = $this->invoice_m->get_invoice($invoiceNo);
        if (!empty($data)) {
            return $this->createInvoiceNo();
        } else {
            return $invoiceNo;
        }
    }

    public function add() {
        $this->load->library("session");
        $usertype = $this->session->userdata("usertype");
        if ($usertype == "Admin" || $usertype == "Accountant") {
            $this->data['classes'] = $this->classes_m->get_classes();
            $this->data['feetypes'] = $this->feetype_m->get_feetype();
            $this->data['feetypecategory'] = $this->feetype_m->feetype_category_all();

            $classesID = $this->input->post("classesID");
            if ($classesID != 0) {
                $this->data['students'] = $this->student_m->get_order_by_student(array("classesID" => $classesID));
            } else {
                $this->data['students'] = "empty";
            }
            $this->data['studentID'] = 0;
            if ($_POST) {
                $this->data['studentID'] = $this->input->post('studentID');
                $rules = $this->rules();
                $this->form_validation->set_rules($rules);
                if ($this->form_validation->run() == FALSE) {
                    $this->data["subview"] = "invoice/add";
                    $this->load->view('_layout_main', $this->data);
                } else {
                    if ($this->input->post('studentID')) {
                        $classesID = $this->input->post('classesID');
                        $getclasses = $this->classes_m->get_classes($classesID);
                        $studentID = $this->input->post('studentID');
                        $getstudent = $this->student_m->get_student($studentID);
                        $amount = $this->input->post("amount");                    
                        $feetype_info = $this->feetype_m->feetype_by_id($this->input->post("feetypeID"),$this->input->post("feetype_categoryID"));
                          $feetype = array_shift($feetype_info);
                    
                        $data_check = array(
                            'classesID' => $classesID,
                            'classes' => $getclasses->classes,
                            'studentID' => $studentID,
                            'roll' => $getstudent->roll,
                            'feetypeID' => $feetype->feetypeID,
                            'amount' => $feetype->amount,
                            'year' => date('Y'));
                        $check = $this->invoice_m->get_single_invoice($data_check);
                        if (empty($check)) {
                            $invoice_no = $this->createInvoiceNo();
                            $array = array(
                                'classesID' => $classesID,
                                'classes' => $getclasses->classes,
                                'studentID' => $studentID,
                                'student' => $getstudent->name,
                                'roll' => $getstudent->roll,
                                'feetypeID' => $feetype->feetypeID,
                                'feetype' => $feetype->feetype,
                                'amount' => $feetype->amount,
                                'status' => 0,
                                'invoiceNO' => $invoice_no,
                                'date' => date("Y-m-d", strtotime($this->input->post("date"))),
                                'year' => date('Y')
                            );
                            $oldamount = $getstudent->totalamount;
                            $nowamount = $oldamount + $feetype->amount;
                            $this->student_m->update_student(array('totalamount' => $nowamount), $getstudent->studentID);

                            $returnID = $this->db->insert('invoice', $array);
                            $parent = $this->invoice_m->get_parent($studentID);
                            //send SMS notification to user

//                            $message = 'Hello ' . $parent->name . '. Invoice Number ' . $invoice_no . ' with amount ' . $feetype->amount . ' has been created for ' . $parent->sname . ', to pay for  ' . $feetype->feetype;
//                            $this->send_sms($parent->phone, $message);
                            $this->session->set_flashdata('success', $this->lang->line('menu_success'));
                            redirect(base_url("invoice/index/$classesID"));
                        } else {
                            $this->session->set_flashdata('error', $this->lang->line('invoice_exist'));
                            redirect(base_url("invoice/index/$classesID"));
                        }
                    } else {
                        $classesID = $this->input->post('classesID');
                        $getclasses = $this->classes_m->get_classes($classesID);

                        $getstudents = $this->student_m->get_order_by_student(array("classesID" => $classesID));

                        $amount = $this->input->post("amount");
                        $feetype_info = $this->feetype_m->feetype_by_id($this->input->post("feetypeID"),$this->input->post("feetype_categoryID"));
                        
                        $feetype = array_shift($feetype_info);
                        foreach ($getstudents as $getstudent) {

                            $invoiceNO = $this->createInvoiceNo();
                            $array = array(
                                'classesID' => $classesID,
                                'classes' => $getclasses->classes,
                                'studentID' => $getstudent->studentID,
                                'student' => $getstudent->name,
                                'roll' => $getstudent->roll,
                                'feetype' => $feetype->feetype,
                                'feetypeID' => $feetype->feetypeID,
                                'amount' => $feetype->amount,
                                'status' => 0,
                                'invoiceNO' => $invoiceNO,
                                'date' => date("Y-m-d", strtotime($this->input->post("date"))),
                                'year' => date('Y')
                            );
                               
                           
                            $oldamount = $getstudent->totalamount;
                            $nowamount = $oldamount + $feetype->amount;

                            $this->student_m->update_student(array('totalamount' => $nowamount), $getstudent->studentID);

                            $this->db->insert('invoice', $array);
                            $parent = $this->invoice_m->get_parent($getstudent->studentID);
                            //send SMS notification to user

                            $message = 'Hello ' . $parent->name . '. Invoice Number ' . $invoice_no . ' with amount ' . $feetype->amount . ' has been created for ' . $parent->sname . ', to pay for ' . $feetype->feetype;
                            $this->send_sms($parent->phone, $message);
                        }
                        $this->session->set_flashdata('success', $this->lang->line('menu_success'));
                        redirect(base_url("invoice/index"));
                    }
                }
            } else {
                $this->data["subview"] = "invoice/add";
                $this->load->view('_layout_main', $this->data);
            }
        } else {
            $this->data["subview"] = "error";
            $this->load->view('_layout_main', $this->data);
        }
    }

    protected function bulk_rules() {
        $rules = array(
            array(
                'field' => 'classesID',
                'label' => $this->lang->line("invoice_classesID"),
                'rules' => 'trim|required|xss_clean|max_length[11]|numeric|callback_unique_classID'
            ),
            array(
                'field' => 'feetype_categoryID',
                'label' => $this->lang->line("invoice_feetype_categoryID"),
                'rules' => 'trim|required|xss_clean|max_length[11]|numeric'
            ),
            array(
                'field' => 'feetypeID',
                'label' => $this->lang->line("invoice_feetype"),
                'rules' => 'trim|required|xss_clean|max_length[128]'
            ),
//	    array(
//		'field' => 'amount',
//		'label' => $this->lang->line("invoice_amount"),
//		'rules' => 'trim|required|xss_clean|max_length[20]|numeric|callback_valid_number'
//	    ),
            array(
                'field' => 'date',
                'label' => $this->lang->line("invoice_date"),
                'rules' => 'trim|required|xss_clean|max_length[20]'
            ),
        );
        return $rules;
    }

    public function bulk_add() {
        $this->load->library("session");
        $usertype = $this->session->userdata("usertype");
        if ($usertype == "Admin" || $usertype == "Accountant") {
            $this->data['classes'] = $this->classes_m->get_classes();
            $this->data['feetypes'] = $this->feetype_m->get_feetype();

            $this->data['feetypecategory'] = $this->feetype_m->feetype_category_all();

            if ($_POST) {
                $classesID = $this->input->post('classesID');
                $rules = $this->bulk_rules();
                $this->form_validation->set_rules($rules);
                if ($this->form_validation->run() == FALSE) {
                    $this->data["subview"] = "bulk_invoice/bulk_add";
                    $this->load->view('_layout_main', $this->data);
                } else {
                    $getclasses = $this->classes_m->get_classes($classesID);
                    $getstudents = $this->student_m->get_order_by_student(array("classesID" => $classesID));
                    $amount = $this->input->post("amount");
                    $feetype_info = $this->feetype_m->get_order_by_feetype(array('feetypeID' => $this->input->post("feetypeID"), 'feetype_categoryID' => $this->input->post("feetype_categoryID")));
                    $feetype = array_shift($feetype_info);
                    if (empty($feetype)) {
                        $this->session->set_flashdata('error', $this->lang->line('invoice_exist'));
                        redirect(base_url("invoice/bulk_add"));
                    }
                    $data_check = array(
                        'classesID' => $classesID,
                        'classes' => $getclasses->classes,
                        'feetypeID' => $feetype->feetypeID,
                        'amount' => $feetype->amount,
                        'year' => date('Y'));
                    $check = $this->invoice_m->get_single_invoice($data_check);
                    if (empty($check)) {
                        foreach ($getstudents as $getstudent) {

                            $invoiceNO = $this->createInvoiceNo();

                            $array = array(
                                'classesID' => $classesID,
                                'classes' => $getclasses->classes,
                                'studentID' => $getstudent->studentID,
                                'student' => $getstudent->name,
                                'roll' => $getstudent->roll,
                                'feetype' => $feetype->feetype,
                                'feetypeID' => $feetype->feetypeID,
                                'amount' => $feetype->amount,
                                'status' => 0,
                                'invoiceNO' => $invoiceNO,
                                'date' => date("Y-m-d", strtotime($this->input->post("date"))),
                                'year' => date('Y')
                            );
                         
                            $oldamount = $getstudent->totalamount;
                            $nowamount = $oldamount + $feetype->amount;

                            $this->student_m->update_student(array('totalamount' => $nowamount), $getstudent->studentID);
                            $this->db->insert('invoice', $array);
                            //insert in SMS table
                            $parent = $this->invoice_m->get_parent($getstudent->studentID);

                            $message = 'Hello ' . $parent->name . '. Invoice Number ' . $invoiceNO . ' with amount ' . $feetype->amount . ' has been created for ' . $parent->sname . ', to pay for ' . $feetype->feetype;
                            $this->send_sms($parent->phone, $message);
                        }
                    } else {

                        $this->session->set_flashdata('error', $this->lang->line('invoice_exist'));
                        redirect(base_url("invoice/bulk_index/$classesID"));
                    }
                    $this->session->set_flashdata('success', $this->lang->line('menu_success'));
                    redirect(base_url("invoice/bulk_index/$classesID"));
                }
            } else {

                $this->data["subview"] = "bulk_invoice/bulk_add";
                $this->load->view('_layout_main', $this->data);
            }
        } else {
            $this->data["subview"] = "error";
            $this->load->view('_layout_main', $this->data);
        }
    }

    public function edit() {
        $usertype = $this->session->userdata("usertype");
        if ($usertype == "Admin" || $usertype == "Accountant") {
            $id = htmlentities(($this->uri->segment(3)));
            if ((int) $id) {
                $this->data['invoice'] = $this->invoice_m->get_invoice($id);
                $this->data['classes'] = $this->classes_m->get_classes();
                $this->data['feetypes'] = $this->feetype_m->get_feetype();
                if ($this->data['invoice']) {

                    if ($this->data['invoice']->classesID != 0) {
                        $this->data['students'] = $this->student_m->get_order_by_student(array("classesID" => $this->data['invoice']->classesID));
                    } else {
                        $this->data['students'] = "empty";
                    }
                    $this->data['studentID'] = $this->data['invoice']->studentID;

                    if ($_POST) {
                        $this->data['studentID'] = $this->input->post('studentID');
                        $rules = $this->rules();
                        $this->form_validation->set_rules($rules);
                        if ($this->form_validation->run() == FALSE) {
                            $this->data["subview"] = "invoice/edit";
                            $this->load->view('_layout_main', $this->data);
                        } else {

                            $status = 0;
                            $oldstudent = $this->student_m->get_student($this->data['invoice']->studentID);
                            $osoldamount = $oldstudent->totalamount;
                            $oldnowamount = ($osoldamount) - ($this->data['invoice']->amount);
                            $this->student_m->update_student(array('totalamount' => $oldnowamount), $oldstudent->studentID);

                            $classesID = $this->input->post('classesID');
                            $getclasses = $this->classes_m->get_classes($classesID);
                            $studentID = $this->input->post('studentID');
                            $getstudent = $this->student_m->get_student($studentID);
                            $amount = $this->input->post("amount");

                            if (empty($this->data['invoice']->paidamount)) {
                                $status = 0;
                            } elseif ($this->data['invoice']->paidamount == $amount) {
                                $status = 2;
                            } else {
                                $status = 1;
                            }
                            $feetype_info = $this->feetype_m->get_order_by_feetype(array('feetypeID' => $this->input->post("feetypeID")));
                            $feetype = array_shift($feetype_info);
                            $array = array(
                                'classesID' => $classesID,
                                'classes' => $getclasses->classes,
                                'studentID' => $studentID,
                                'student' => $getstudent->name,
                                'roll' => $getstudent->roll,
                                'feetypeID' => $feetype->feetypeID,
                                'feetype' => $feetype->feetype,
                                'amount' => $amount,
                                'status' => $status,
                            );

                            $oldamount = $getstudent->totalamount;
                            $nowamount = $oldamount + $amount;

                            $this->student_m->update_student(array('totalamount' => $nowamount), $getstudent->studentID);

                            $this->invoice_m->update_invoice($array, $id);
                            $this->session->set_flashdata('success', $this->lang->line('menu_success'));
                            redirect(base_url("invoice/index"));
                        }
                    } else {
                        $this->data["subview"] = "invoice/edit";
                        $this->load->view('_layout_main', $this->data);
                    }
                } else {
                    $this->data["subview"] = "error";
                    $this->load->view('_layout_main', $this->data);
                }
            } else {
                $this->data["subview"] = "error";
                $this->load->view('_layout_main', $this->data);
            }
        } else {
            $this->data["subview"] = "error";
            $this->load->view('_layout_main', $this->data);
        }
    }

    public function delete() {
        $usertype = $this->session->userdata("usertype");
        if ($usertype == "Admin" || $usertype == "Accountant") {
            $id = htmlentities(($this->uri->segment(3)));
            if ((int) $id) {
                $this->data['invoice'] = $this->invoice_m->get_invoice($id);
                if ($this->data['invoice']) {
                    $oldstudent = $this->student_m->get_student($this->data['invoice']->studentID);
                    $osoldamount = $oldstudent->totalamount;
                    $oldnowamount = ($osoldamount) - ($this->data['invoice']->amount);
                    $this->student_m->update_student(array('totalamount' => $oldnowamount), $oldstudent->studentID);
                    $this->invoice_m->delete_invoice($id);
                    $this->session->set_flashdata('success', $this->lang->line('menu_success'));
                    redirect(base_url("invoice/index/" . $oldstudent->classesID));
                } else {
                    redirect(base_url('invoice/index'));
                }
            } else {
                $this->data["subview"] = "error";
                $this->load->view('_layout_main', $this->data);
            }
        } else {
            $this->data["subview"] = "error";
            $this->load->view('_layout_main', $this->data);
        }
    }

    public function view() {
        $usertype = $this->session->userdata("usertype");
        if ($usertype == "Admin" || $usertype == "Accountant") {
            $id = htmlentities(($this->uri->segment(3)));
            if ((int) $id) {
                $this->data["invoice"] = $this->invoice_m->get_invoice_byid($id);
                if ($this->data["invoice"]) {
                    $this->data["student"] = $this->student_m->get_student($this->data["invoice"]->studentID);
                    //$this->data["student_invoice"] = $this->invoice_m->get_combined_invoice_by_student($this->data["invoice"]->studentID);


                    $this->data["subview"] = "invoice/view";
                    $this->load->view('_layout_main', $this->data);
                } else {
                 $this->session->set_flashdata('error', 'This Invoice has issues Please contact your administrator');
                    redirect(base_url("invoice/index"));
                }
            } else {
               

                $this->data["subview"] = "error";
                $this->load->view('_layout_main', $this->data);
            }
        } elseif ($usertype == "Student") {
            $id = htmlentities(($this->uri->segment(3)));
            if ((int) $id) {
                $username = $this->session->userdata("username");
                $getstudent = $this->student_m->get_single_student(array("username" => $username));

                $this->data["invoice"] = $this->invoice_m->get_invoice_byid($id);
                if ($this->data['invoice'] && ($this->data['invoice']->studentID == $getstudent->studentID)) {
                    $this->data["student"] = $this->student_m->get_student($this->data["invoice"]->studentID);
                    $this->data["subview"] = "invoice/view";
                    $this->load->view('_layout_main', $this->data);
                } else {
                    $this->data["subview"] = "error";
                    $this->load->view('_layout_main', $this->data);
                }
            } else {
                $this->data["subview"] = "error";
                $this->load->view('_layout_main', $this->data);
            }
        } elseif ($usertype == "Parent") {
            $id = htmlentities(($this->uri->segment(3)));
            if ((int) $id) {
                $username = $this->session->userdata("username");
                $parent = $this->student_m->get_parent_info($username);
                $this->data["invoice"] = $this->invoice_m->get_invoice_byid($id);
                if ($this->data['invoice']) {
                    $getstudent = $this->student_m->get_single_student(array("studentID" => $this->data['invoice']->studentID));
                    if ($this->data['invoice'] && ($parent->parentID == $getstudent->parentID)) {
                        $this->data["student"] = $this->student_m->get_student($this->data["invoice"]->studentID);
                        $this->data["subview"] = "invoice/view";
                        $this->load->view('_layout_main', $this->data);
                    } else {
                        $this->data["subview"] = "error";
                        $this->load->view('_layout_main', $this->data);
                    }
                } else {
                    $this->data["subview"] = "error";
                    $this->load->view('_layout_main', $this->data);
                }
            } else {
                $this->data["subview"] = "error";
                $this->load->view('_layout_main', $this->data);
            }
        } else {
            $this->data["subview"] = "error";
            $this->load->view('_layout_main', $this->data);
        }
    }

    public function bulk_view() {
        $usertype = $this->session->userdata("usertype");
        if ($usertype == "Admin" || $usertype == "Accountant") {
            $id = htmlentities(($this->uri->segment(3)));
            $feetypeid = htmlentities(($this->uri->segment(4)));
            if ((int) $id) {
                $this->data["invoices"] = $this->invoice_m->get_allinvoice_by_class($id, $feetypeid);
                if ($this->data["invoices"]) {
                    $this->data["feetypeID"] = $feetypeid;
                    $this->data["classesID"] = $id;
                    //$this->data["student_invoice"] = $this->invoice_m->get_combined_invoice_by_student($this->data["invoice"]->studentID);


                    $this->data["subview"] = "bulk_invoice/bulk_view";
                    $this->load->view('_layout_main', $this->data);
                } else {
                       $this->session->set_flashdata('error', 'There is an error in generating invoices');
                    redirect(base_url("invoice/bulk_index/$id"));
                }
            } else {
                $this->data["subview"] = "error";
                $this->load->view('_layout_main', $this->data);
            }
        } elseif ($usertype == "Student") {
            $id = htmlentities(($this->uri->segment(3)));
            if ((int) $id) {
                $username = $this->session->userdata("username");
                $getstudent = $this->student_m->get_single_student(array("username" => $username));
                $this->data["invoice"] = $this->invoice_m->get_invoice($id);
                if ($this->data['invoice'] && ($this->data['invoice']->studentID == $getstudent->studentID)) {
                    //$this->data["student"] = $this->student_m->get_student($this->data["invoice"]->studentID);
                    $this->data["student"] = array();
                    $this->data["subview"] = "invoice/view";
                    $this->load->view('_layout_main', $this->data);
                } else {
                    $this->data["subview"] = "error";
                    $this->load->view('_layout_main', $this->data);
                }
            } else {
                $this->data["subview"] = "error";
                $this->load->view('_layout_main', $this->data);
            }
        } elseif ($usertype == "Parent") {
            $id = htmlentities(($this->uri->segment(3)));
            if ((int) $id) {
                $username = $this->session->userdata("username");
                $parent = $this->student_m->get_parent_info($username);
                $this->data["invoice"] = $this->invoice_m->get_single_invoice(array('invoiceID' => $id));
                if ($this->data['invoice']) {
                    $getstudent = $this->student_m->get_single_student(array("studentID" => $this->data['invoice']->studentID));
                    if ($this->data['invoice'] && ($parent->parentID == $getstudent->parentID)) {
                        $this->data["student"] = $this->student_m->get_student($this->data["invoice"]->studentID);
                        $this->data["subview"] = "invoice/view";
                        $this->load->view('_layout_main', $this->data);
                    } else {
                        $this->data["subview"] = "error";
                        $this->load->view('_layout_main', $this->data);
                    }
                } else {
                    $this->data["subview"] = "error";
                    $this->load->view('_layout_main', $this->data);
                }
            } else {
                $this->data["subview"] = "error";
                $this->load->view('_layout_main', $this->data);
            }
        } else {
            $this->data["subview"] = "error";
            $this->load->view('_layout_main', $this->data);
        }
    }

    public function get_user_info() {
        $usertype = $this->session->userdata("usertype");
        $username = $this->session->userdata("username");
        if ($usertype == "Admin" || $usertype == "Accountant") {
            $user = $this->user_m->get_single_user(array("username" => $username));
        } elseif ($usertype == "Student") {

            $user = $this->student_m->get_single_student(array("username" => $username));
        } elseif ($usertype == "Parent") {
            $user = $this->parentes_m->get_single_parentes(array('username' => $username));
        }
        return $user;
    }

    public function method() {
        $method = htmlentities(($this->uri->segment(3)));
        $invoice_id = $this->input->get_post('iid');
        $this->data["invoice"] = $this->invoice_m->get_invoice($invoice_id);
        /**
         * -----------------------------------------------
         * Parameters to be used globally in payment page
         * ----------------------------------------------
         */
        $this->data['amount'] = $this->data["invoice"]->amount;

        //our bank account
        $this->data['CRDB_BANK_ACCOUNT'] = '014668989879877';

        //will depend on bank agrement and the way we will define our payment
        $this->data['TITLE'] = '<b>ShuleSoft</b>';
        $this->data['bn_number'] = '<b>3007500</b>';
        $this->data['invoice'] = $this->data["invoice"]->invoiceNO;
        $this->data['user'] = $this->get_user_info();
        //call a respective page
        $this->data["subview"] = "invoice/payment/" . $method;
        $this->load->view('_layout_main', $this->data);
    }

    public function print_preview() {
        $usertype = $this->session->userdata("usertype");
        if ($usertype == "Admin" || $usertype == "Accountant") {
            $id = htmlentities(($this->uri->segment(3)));
            if ((int) $id) {
                $this->data["invoice"] = $this->invoice_m->get_invoice($id);
                if ($this->data["invoice"]) {
                    $this->data["student"] = $this->student_m->get_student($this->data["invoice"]->studentID);

                    $this->load->library('html2pdf');
                    $this->html2pdf->folder('./assets/pdfs/');
                    $this->html2pdf->filename('Report.pdf');
                    $this->html2pdf->paper('a4', 'portrait');
                    $this->data['panel_title'] = $this->lang->line('panel_title');

                    $html = $this->load->view('invoice/print_preview', $this->data, true);
                    $this->html2pdf->html($html);
                    $this->html2pdf->create();
                } else {
                    $this->data["subview"] = "error";
                    $this->load->view('_layout_main', $this->data);
                }
            } else {
                $this->data["subview"] = "error";
                $this->load->view('_layout_main', $this->data);
            }
        }
    }

    public function bulk_print_preview() {
        $usertype = $this->session->userdata("usertype");
        if ($usertype == "Admin" || $usertype == "Accountant") {
            $id = htmlentities(($this->uri->segment(3)));
            $feetypeid = htmlentities(($this->uri->segment(4)));
            if ((int) $id) {
                $this->data["invoices"] = $this->invoice_m->get_allinvoice_by_class($id, $feetypeid);
                if ($this->data["invoices"]) {
                    $this->data["feetypeID"] = $feetypeid;
                    $this->data["classesID"] = $id;
                    $this->load->library('html2pdf');
                    $this->html2pdf->folder('./assets/pdfs/');
                    $this->html2pdf->filename('Report.pdf');
                    $this->html2pdf->paper('a4', 'portrait');
                    $this->data['panel_title'] = $this->lang->line('panel_title');
                    $html = $this->load->view('bulk_invoice/print_preview', $this->data, true);
                    $this->html2pdf->html($html);
                    $this->html2pdf->create();
                } else {
                    $this->data["subview"] = "error";
                    $this->load->view('_layout_main', $this->data);
                }
            } else {
                $this->data["subview"] = "error";
                $this->load->view('_layout_main', $this->data);
            }
        }
    }

    public function send_mail() {
        $usertype = $this->session->userdata("usertype");
        if ($usertype == "Admin" || $usertype == "Accountant") {
            $id = $this->input->post('id');
            if ((int) $id) {
                $this->data["invoice"] = $this->invoice_m->get_invoice($id);
                if ($this->data["invoice"]) {
                    $this->data["student"] = $this->student_m->get_student($this->data["invoice"]->studentID);

                    $this->load->library('html2pdf');
                    $this->html2pdf->folder('./assets/pdfs/');
                    $this->html2pdf->filename('Report.pdf');
                    $this->html2pdf->paper('a4', 'portrait');
                    $this->data['panel_title'] = $this->lang->line('panel_title');
                    $html = $this->load->view('invoice/print_preview', $this->data, true);
                    $this->html2pdf->html($html);
                    $this->html2pdf->create('save');

                    if ($path = $this->html2pdf->create('save')) {
                        $this->load->library('email');
                        $this->email->set_mailtype("html");
                        $this->email->from($this->data["siteinfos"]->email, $this->data['siteinfos']->sname);
                        $this->email->to($this->input->post('to'));
                        $this->email->subject($this->input->post('subject'));
                        $this->email->message($this->input->post('message'));
                        $this->email->attach($path);
                        if ($this->email->send()) {
                            $this->session->set_flashdata('success', $this->lang->line('mail_success'));
                        } else {
                            $this->session->set_flashdata('error', $this->lang->line('mail_error'));
                        }
                    }
                } else {
                    $this->data["subview"] = "error";
                    $this->load->view('_layout_main', $this->data);
                }
            } else {
                $this->data["subview"] = "error";
                $this->load->view('_layout_main', $this->data);
            }
        } else {
            $this->data["subview"] = "error";
            $this->load->view('_layout_main', $this->data);
        }
    }

    public function payment() {
        $usertype = $this->session->userdata("usertype");
        if ($usertype == "Admin" || $usertype == "Accountant") {
            $id = htmlentities(($this->uri->segment(3)));
            if ((int) $id) {
                $this->data['invoice'] = $this->invoice_m->get_invoice($id);

                if ($this->data['invoice']) {
                    if (($this->data['invoice']->paidamount != $this->data['invoice']->amount) && ($this->data['invoice']->status == 0 || $this->data['invoice']->status == 1)) {
                        if ($_POST) {
                            $rules = $this->payment_rules();
                            $this->form_validation->set_rules($rules);
                            if ($this->form_validation->run() == FALSE) {
                                $this->data["subview"] = "invoice/payment";
                                $this->load->view('_layout_main', $this->data);
                            } else {

                                $payable_amount = $this->input->post('amount') + $this->data['invoice']->paidamount;
                                if ($payable_amount > $this->data['invoice']->amount) {
                                    $this->session->set_flashdata('error', 'Payment amount is much than invoice amount');
                                    redirect(base_url("invoice/payment/$id"));
                                } else {
                                    $this->post_data = $this->input->post();
                                    if ($this->input->post('payment_method') == 'Paypal') {
                                        $this->post_data['id'] = $this->uri->segment(3);
                                        $this->invoice_data = $this->invoice_m->get_invoice($this->post_data['id']);
                                        $this->Paypal();
                                    } elseif ($this->input->post('payment_method') == 'Cash') {
                                        $status = 0;
                                        if ($payable_amount == $this->data['invoice']->amount) {
                                            $status = 2;
                                        } else {
                                            $status = 1;
                                        }

                                        $username = $this->session->userdata('username');
                                        $dbuserID = 0;
                                        $dbusertype = '';
                                        $dbuname = '';
                                        if ($usertype == "Admin") {
                                            $user = $this->user_m->get_username_row("setting", array("username" => $username));
                                            $dbuserID = $user->settingID;
                                            $dbusertype = $user->usertype;
                                            $dbuname = $user->name;
                                        } elseif ($usertype == "Accountant") {
                                            $user = $this->user_m->get_username_row("user", array("username" => $username));
                                            $dbuserID = $user->userID;
                                            $dbusertype = $user->usertype;
                                            $dbuname = $user->name;
                                        }

                                        $nowpaymenttype = '';
                                        if (empty($this->data['invoice']->paymenttype)) {
                                            $nowpaymenttype = 'Cash';
                                        } else {
                                            if ($this->data['invoice']->paymenttype == 'Cash') {
                                                $nowpaymenttype = 'Cash';
                                            } else {
                                                $exp = explode(',', $this->data['invoice']->paymenttype);
                                                if (!in_array('Cash', $exp)) {
                                                    $nowpaymenttype = $this->data['invoice']->paymenttype . ',' . 'Cash';
                                                } else {
                                                    $nowpaymenttype = $this->data['invoice']->paymenttype;
                                                }
                                            }
                                        }

                                        $array = array(
                                            "paidamount" => $payable_amount,
                                            "status" => $status,
                                            "paymenttype" => $nowpaymenttype,
                                            "paiddate" => date('Y-m-d'),
                                            "userID" => $dbuserID,
                                            "usertype" => $dbusertype,
                                            'uname' => $dbuname
                                        );

                                        $payment_array = array(
                                            "invoiceID" => $id,
                                            "studentID" => $this->data['invoice']->studentID,
                                            "paymentamount" => $this->input->post('amount'),
                                            "paymenttype" => $this->input->post('payment_method'),
                                            "paymentdate" => date('Y-m-d'),
                                            "paymentmonth" => date('M'),
                                            "paymentyear" => date('Y'),
                                            "transactionID" => $this->input->post('number'),
                                            'userID' => $dbuserID,
                                            'approved' => 1,
                                            'approved_date' => 'now()',
                                            'approved_user_id' => $dbuserID
                                        );

                                        $this->payment_m->insert_payment($payment_array);

                                        $studentID = $this->data['invoice']->studentID;
                                        $getstudent = $this->student_m->get_student($studentID);
                                        $nowamount = ($getstudent->paidamount) + ($this->input->post('amount'));
                                        $this->student_m->update_student(array('paidamount' => $nowamount), $studentID);

                                        $this->invoice_m->update_invoice($array, $id);
                                        $this->session->set_flashdata('success', $this->lang->line('menu_success'));
                                        redirect(base_url("invoice/view/$id"));
                                    } elseif ($this->input->post('payment_method') == 'Cheque') {
                                        $status = 0;
                                        if ($payable_amount == $this->data['invoice']->amount) {
                                            $status = 2;
                                        } else {
                                            $status = 1;
                                        }

                                        $username = $this->session->userdata('username');
                                        $dbuserID = 0;
                                        $dbusertype = '';
                                        $dbuname = '';
                                        if ($usertype == "Admin") {
                                            $user = $this->user_m->get_username_row("setting", array("username" => $username));
                                            $dbuserID = $user->settingID;
                                            $dbusertype = $user->usertype;
                                            $dbuname = $user->name;
                                        } elseif ($usertype == "Accountant") {
                                            $user = $this->user_m->get_username_row("user", array("username" => $username));
                                            $dbuserID = $user->userID;
                                            $dbusertype = $user->usertype;
                                            $dbuname = $user->name;
                                        }

                                        $nowpaymenttype = '';
                                        if (empty($this->data['invoice']->paymenttype)) {
                                            $nowpaymenttype = 'Cheque';
                                        } else {
                                            if ($this->data['invoice']->paymenttype == 'Cheque') {
                                                $nowpaymenttype = 'Cheque';
                                            } else {
                                                $exp = explode(',', $this->data['invoice']->paymenttype);
                                                if (!in_array('Cheque', $exp)) {
                                                    $nowpaymenttype = $this->data['invoice']->paymenttype . ',' . 'Cheque';
                                                } else {
                                                    $nowpaymenttype = $this->data['invoice']->paymenttype;
                                                }
                                            }
                                        }

                                        $array = array(
                                            "paidamount" => $payable_amount,
                                            "status" => $status,
                                            "paymenttype" => $nowpaymenttype,
                                            "paiddate" => date('Y-m-d'),
                                            "userID" => $dbuserID,
                                            "usertype" => $dbusertype,
                                            'uname' => $dbuname
                                        );

                                        $payment_array = array(
                                            "invoiceID" => $id,
                                            "studentID" => $this->data['invoice']->studentID,
                                            "paymentamount" => $this->input->post('amount'),
                                            "paymenttype" => $this->input->post('payment_method'),
                                            "paymentdate" => date('Y-m-d'),
                                            "paymentmonth" => date('M'),
                                            "paymentyear" => date('Y'),
                                            "transactionID" => $this->input->post('number'),
                                            'userID' => $dbuserID,
                                            'approved' => 1,
                                            'approved_date' => 'now()',
                                            'approved_user_id' => $dbuserID
                                        );

                                        $this->payment_m->insert_payment($payment_array);

                                        $studentID = $this->data['invoice']->studentID;
                                        $getstudent = $this->student_m->get_student($studentID);
                                        $nowamount = ($getstudent->paidamount) + ($this->input->post('amount'));
                                        $this->student_m->update_student(array('paidamount' => $nowamount), $studentID);

                                        $this->invoice_m->update_invoice($array, $id);
                                        $this->session->set_flashdata('success', $this->lang->line('menu_success'));
                                        redirect(base_url("invoice/view/$id"));
                                    } else {
                                        $this->data["subview"] = "invoice/payment";
                                        $this->load->view('_layout_main', $this->data);
                                    }
                                }
                            }
                        } else {
                            $this->data["subview"] = "invoice/payment";
                            $this->load->view('_layout_main', $this->data);
                        }
                    } else {
                        $this->data["subview"] = "error";
                        $this->load->view('_layout_main', $this->data);
                    }
                } else {
                    $this->data["subview"] = "error";
                    $this->load->view('_layout_main', $this->data);
                }
            } else {
                $this->data["subview"] = "error";
                $this->load->view('_layout_main', $this->data);
            }
        } elseif ($usertype == "Student") {
            $id = htmlentities(($this->uri->segment(3)));
            if ((int) $id) {

                $username = $this->session->userdata("username");
                $getstudent = $this->student_m->get_single_student(array("username" => $username));
                $this->data["invoice"] = $this->invoice_m->get_invoice($id);

                if ($this->data['invoice'] && ($this->data['invoice']->studentID == $getstudent->studentID)) {
                    if (($this->data['invoice']->paidamount != $this->data['invoice']->amount) && ($this->data['invoice']->status == 0 || $this->data['invoice']->status == 1)) {

                        /**
                         * ------------------------------------------
                         * Load payment interface to show options for 
                         * user to make payments for the invoice
                         * ------------------------------------------
                         */
                        $this->data["subview"] = "invoice/payment";
                        $this->load->view('_layout_main', $this->data);
                    } else {
                        $this->data["subview"] = "error";
                        $this->load->view('_layout_main', $this->data);
                    }
                } else {
                    $this->data["subview"] = "error";
                    $this->load->view('_layout_main', $this->data);
                }
            } else {
                $this->data["subview"] = "error";
                $this->load->view('_layout_main', $this->data);
            }
        } elseif ($usertype == "Parent") {
            $id = htmlentities(($this->uri->segment(3)));
            if ((int) $id) {
                $this->data['invoice'] = $this->invoice_m->get_invoice($id);
                $username = $this->session->userdata("username");
                $this->data["invoice"] = $this->invoice_m->get_invoice($id);

                if ($this->data["invoice"]) {
                    $getstudent = $this->student_m->get_single_student(array("studentID" => $this->data['invoice']->studentID));
                    if ($this->data['invoice'] && ($this->data['invoice']->studentID == $getstudent->studentID)) {
                        if (($this->data['invoice']->paidamount != $this->data['invoice']->amount) && ($this->data['invoice']->status == 0 || $this->data['invoice']->status == 1)) {
                            if ($_POST) {
                                $rules = $this->payment_rules();
                                unset($rules[1]);
                                $this->form_validation->set_rules($rules);
                                if ($this->form_validation->run() == FALSE) {
                                    $this->data["subview"] = "invoice/payment";
                                    $this->load->view('_layout_main', $this->data);
                                } else {
                                    $payable_amount = $this->input->post('amount') + $this->data['invoice']->paidamount;
                                    if ($payable_amount > $this->data['invoice']->amount) {
                                        $this->session->set_flashdata('error', 'Payment amount is much than invoice amount');
                                        redirect(base_url("invoice/payment/$id"));
                                    } else {
                                        $this->post_data = $this->input->post();
                                        $this->post_data['id'] = $id;
                                        $this->invoice_data = $this->invoice_m->get_invoice($id);
                                        $this->Paypal();
                                    }
                                }
                            } else {
                                $this->data["subview"] = "invoice/payment";
                                $this->load->view('_layout_main', $this->data);
                            }
                        } else {
                            $this->data["subview"] = "error";
                            $this->load->view('_layout_main', $this->data);
                        }
                    } else {
                        $this->data["subview"] = "error";
                        $this->load->view('_layout_main', $this->data);
                    }
                } else {
                    $this->data["subview"] = "error";
                    $this->load->view('_layout_main', $this->data);
                }
            } else {
                $this->data["subview"] = "error";
                $this->load->view('_layout_main', $this->data);
            }
        } else {
            $this->data["subview"] = "error";
            $this->load->view('_layout_main', $this->data);
        }
    }

    /* Paypal payment start */

    public function Paypal() {
        $api_config = array();
        $get_configs = $this->payment_settings_m->get_order_by_config();
        foreach ($get_configs as $key => $get_key) {
            $api_config[$get_key->config_key] = $get_key->value;
        }
        $this->data['set_key'] = $api_config;
        if ($api_config['paypal_api_username'] == "" || $api_config['paypal_api_password'] == "" || $api_config['paypal_api_signature'] == "") {
            $this->session->set_flashdata('error', 'Paypal settings not available');
            redirect($_SERVER['HTTP_REFERER']);
        } else {
            $this->item_data = $this->post_data;
            $this->invoice_info = (array) $this->invoice_data;

            $params = array(
                'cancelUrl' => base_url('invoice/getSuccessPayment'),
                'returnUrl' => base_url('invoice/getSuccessPayment'),
                'invoice_id' => $this->item_data['id'],
                'name' => $this->invoice_info['student'],
                'description' => $this->invoice_info['feetype'],
                'amount' => number_format(floatval($this->item_data['amount']), 2),
                'currency' => $this->data["siteinfos"]->currency_code,
            );
            $this->session->set_userdata("params", $params);
            $gateway = Omnipay::create('PayPal_Express');
            $gateway->setUsername($api_config['paypal_api_username']);
            $gateway->setPassword($api_config['paypal_api_password']);
            $gateway->setSignature($api_config['paypal_api_signature']);

            $gateway->setTestMode($api_config['paypal_demo']);

            $response = $gateway->purchase($params)->send();

            if ($response->isSuccessful()) {
                // payment was successful: update database 
            } elseif ($response->isRedirect()) {
                $response->redirect();
            } else {
                // payment failed: display message to customer
                echo $response->getMessage();
            }
        }
        /* omnipay Paypal end */
    }

    public function getSuccessPayment() {
        $userID = $this->userID();
        $api_config = array();
        $get_configs = $this->payment_settings_m->get_order_by_config();
        foreach ($get_configs as $key => $get_key) {
            $api_config[$get_key->config_key] = $get_key->value;
        }
        $this->data['set_key'] = $api_config;

        $gateway = Omnipay::create('PayPal_Express');
        $gateway->setUsername($api_config['paypal_api_username']);
        $gateway->setPassword($api_config['paypal_api_password']);
        $gateway->setSignature($api_config['paypal_api_signature']);

        $gateway->setTestMode($api_config['paypal_demo']);

        $params = $this->session->userdata('params');
        $response = $gateway->completePurchase($params)->send();
        $paypalResponse = $response->getData(); // this is the raw response object
        $purchaseId = $_GET['PayerID'];
        $this->data['invoice'] = $this->invoice_m->get_invoice($params['invoice_id']);
        $recent_paidamount = $params['amount'] + $this->data['invoice']->paidamount;
        if (isset($paypalResponse['PAYMENTINFO_0_ACK']) && $paypalResponse['PAYMENTINFO_0_ACK'] === 'Success') {
            // Response
            if ($purchaseId) {

                $status = 0;
                if ($recent_paidamount == $this->data['invoice']->amount) {
                    $status = 2;
                } else {
                    $status = 1;
                }

                $usertype = $this->session->userdata("usertype");
                $username = $this->session->userdata('username');
                $dbuserID = 0;
                $dbusertype = '';
                $dbuname = '';
                if ($usertype == "Admin") {
                    $user = $this->user_m->get_username_row("setting", array("username" => $username));
                    $dbuserID = $user->settingID;
                    $dbusertype = $user->usertype;
                    $dbuname = $user->name;
                } elseif ($usertype == "Accountant") {
                    $user = $this->user_m->get_username_row("user", array("username" => $username));
                    $dbuserID = $user->userID;
                    $dbusertype = $user->usertype;
                    $dbuname = $user->name;
                } elseif ($usertype == "Student") {
                    $user = $this->user_m->get_username_row("student", array("username" => $username));
                    $dbuserID = $user->studentID;
                    $dbusertype = $user->usertype;
                    $dbuname = $user->name;
                } elseif ($usertype == "Parent") {
                    $user = $this->user_m->get_username_row("parent", array("username" => $username));
                    $dbuserID = $user->parentID;
                    $dbusertype = $user->usertype;
                    $dbuname = $user->name;
                }

                $nowpaymenttype = '';
                if (empty($this->data['invoice']->paymenttype)) {
                    $nowpaymenttype = 'Paypal';
                } else {
                    if ($this->data['invoice']->paymenttype == 'Paypal') {
                        $nowpaymenttype = 'Paypal';
                    } else {
                        $exp = explode(',', $this->data['invoice']->paymenttype);
                        if (!in_array('Paypal', $exp)) {
                            $nowpaymenttype = $this->data['invoice']->paymenttype . ',' . 'Paypal';
                        } else {
                            $nowpaymenttype = $this->data['invoice']->paymenttype;
                        }
                    }
                }

                $array = array(
                    "paidamount" => $recent_paidamount,
                    "status" => $status,
                    "paymenttype" => $nowpaymenttype,
                    "paiddate" => date('Y-m-d'),
                    "userID" => $dbuserID,
                    "usertype" => $dbusertype,
                    'uname' => $dbuname
                );

                $payment_array = array(
                    "invoiceID" => $params['invoice_id'],
                    "studentID" => $this->data['invoice']->studentID,
                    "paymentamount" => $params['amount'],
                    "paymenttype" => 'Paypal',
                    "paymentdate" => date('Y-m-d'),
                    "paymentmonth" => date('M'),
                    "paymentyear" => date('Y'),
                    "transactionID" => $this->input->post('number'),
                    'userID' => $dbuserID
                );

                $this->payment_m->insert_payment($payment_array);

                $studentID = $this->data['invoice']->studentID;
                $getstudent = $this->student_m->get_student($studentID);
                $nowamount = ($getstudent->paidamount) + ($params['amount']);
                $this->student_m->update_student(array('paidamount' => $nowamount), $studentID);

                $this->invoice_m->update_invoice($array, $params['invoice_id']);
                $this->session->set_flashdata('success', $this->lang->line('menu_success'));
            } else {
                $this->session->set_flashdata('error', 'Payer id not found!');
            }
            redirect(base_url("invoice/view/" . $params['invoice_id']));
        } else {

            //Failed transaction
            $this->session->set_flashdata('error', 'Payment not success!');
            redirect(base_url("invoice/view/" . $params['invoice_id']));
        }
    }

    /* Paypal payment end */

    function call_all_student() {
        $classesID = $this->input->post('id');
        $status = $this->input->post('status');
        if ((int) $classesID) {
            if ($status == 'parent') {
                echo "<option value='" . 0 . "'>" . $this->lang->line('invoice_select_student') . "</option>";
                $parents = $this->student_m->get_parent_info($this->session->userdata('username'));
                //print_r($parents);
                $students = $this->student_m->get_order_by_student(array('classesID' => $classesID, 'parentID' => $parents->parentID));
                foreach ($students as $key => $student) {
                    echo "<option value='" . $student->studentID . "'>" . $student->name . "</option>";
                }
            } else {
                echo "<option value='" . 0 . "'>" . $this->lang->line('invoice_select_student') . "</option>";
                $students = $this->student_m->get_order_by_student(array('classesID' => $classesID));
                foreach ($students as $key => $student) {
                    echo "<option value='" . $student->studentID . "'>" . $student->name . "</option>";
                }
            }
        } else {
            echo "<option value='" . 0 . "'>" . $this->lang->line('invoice_select_student') . "</option>";
        }
    }

    function feetypecall() {
        $feetype = $this->input->post('feetype');
        if ($feetype) {
            $allfeetypes = $this->feetype_m->allfeetype($feetype);

            foreach ($allfeetypes as $allfeetype) {
                echo "<li id='" . $allfeetype->feetypeID . "'>" . $allfeetype->feetype . "</li>";
            }
        }
    }

    function unique_classID() {
        if ($this->input->post('classesID') == 0) {
            $this->form_validation->set_message("unique_classID", "The %s field is required");
            return FALSE;
        }
        return TRUE;
    }

    function valid_number() {
        if ($this->input->post('amount') && $this->input->post('amount') < 0) {
            $this->form_validation->set_message("valid_number", "%s is invalid number");
            return FALSE;
        }
        return TRUE;
    }

    function unique_paymentmethod() {
        if ($this->input->post('payment_method') === '0') {
            $this->form_validation->set_message("unique_paymentmethod", "The %s field is required");
            return FALSE;
        } elseif ($this->input->post('payment_method') === 'Paypal') {
            $api_config = array();
            $get_configs = $this->payment_settings_m->get_order_by_config();
            foreach ($get_configs as $key => $get_key) {
                $api_config[$get_key->config_key] = $get_key->value;
            }
            if ($api_config['paypal_api_username'] == "" || $api_config['paypal_api_password'] == "" || $api_config['paypal_api_signature'] == "") {
                $this->form_validation->set_message("unique_paymentmethod", "Paypal settings required");
                return FALSE;
            }
        }
        return TRUE;
    }

    public function student_list() {
        $studentID = $this->input->post('id');
        if ((int) $studentID) {
            $string = base_url("invoice/index/$studentID");
            echo $string;
        } else {
            redirect(base_url("invoice/index"));
        }
    }

    public function userID() {
        $usertype = $this->session->userdata('usertype');
        $username = $this->session->userdata('username');
        if ($usertype == "Admin") {
            $table = "setting";
            $tableID = "settingID";
        } elseif ($usertype == "Accountant" || $usertype == "Librarian") {
            $table = "user";
            $tableID = "userID";
        } else {
            $table = strtolower($usertype);
            $tableID = $table . "ID";
        }
        $query = $this->db->get_where($table, array('username' => $username));
        $userID = $query->row()->$tableID;
        return $userID;
    }

    public function bankslip($invoiceID) {
        $this->data["subview"] = "invoice/bankslip";
        $this->data['invoiceID'] = $invoiceID;
        $this->data['banknames'] = $this->db->query('select distinct bank_name from ' . set_schema_name() . 'bank_account')->result();
        $this->data['invoice'] = $this->db->get_where('invoice', array('invoiceID' => $invoiceID))->row();

        if (!empty($_FILES) && !empty($_POST)) {

            //Lets check if this transaction ID exist or not
            $pdata = $this->db->get_where('payment', array('transactionID' => $this->input->post('transaction_id')))->row();
            if (!empty($pdata)) {
                $this->data["transaction_id"] = "This transaction ID exist";
                $this->session->set_flashdata('error', $this->data["transaction_id"]);
                return $this->load->view('_layout_main', $this->data);
            }
            if ($this->input->post('amount') > $this->data['invoice']->amount) {
                $this->data["amount_error"] = "This amount you specify is greater than invoiced amount. Please contact school accountant if you have pay more than what you are required";
                $this->session->set_flashdata('error', $this->data["amount_error"]);
                return $this->load->view('_layout_main', $this->data);
            }

            $file_name = $_FILES["image"]['name'];
            $file_name_rename = rand(1, 100000000000);
            $explode = explode('.', $file_name);

            $new_file = $file_name_rename . '.' . $explode[1];
//		$config['upload_path'] = "./uploads/bankslip";
//		$config['allowed_types'] = "gif|jpg|png|jpeg";
//		$config['file_name'] = $new_file;
//		$config['max_size'] = '1024';
//		$config['max_width'] = '3000';
//		$config['max_height'] = '3000';
//		$array['photo'] = $new_file;
            $upload_path = 'uploads/bankslip/' . $new_file;
            move_uploaded_file($_FILES['image']['tmp_name'], $upload_path);
            //$this->load->library('upload', $config);
//		if (!$this->upload->do_upload("image")) {
//		    $this->data["image"] = $this->upload->display_errors();
//		    $this->load->view('_layout_main', $this->data);
//		} else {
//		    $data = array("upload_data" => $this->upload->data());
//		}


            $payable_amount = $this->input->post('amount');

            $payment_array = array(
                "invoiceID" => $invoiceID,
                "studentID" => $this->data['invoice']->studentID,
                "paymentamount" => $payable_amount,
                "paymenttype" => $this->input->post('bank_name'),
                "paymentdate" => date('Y-m-d'),
                "paymentmonth" => date('M'),
                "paymentyear" => date('Y'),
                "transactionID" => $this->input->post('transaction_id'),
                'userID' => $this->userID(),
                'slipfile' => $new_file
            );
            $this->payment_m->insert_payment($payment_array);

            $usertype = $this->session->userdata('usertype');
            $username = $this->session->userdata('username');

            $total_paid = $payable_amount + $this->data['invoice']->paidamount;

            $array = array(
                "paidamount" => $total_paid,
                "status" => ($total_paid == $this->data['invoice']->amount) ? 2 : 1,
                "paymenttype" => $this->input->post('bank_name'),
                "paiddate" => date('Y-m-d'),
                "userID" => $this->userID(),
                "usertype" => $usertype,
                'uname' => $username
            );
            $this->invoice_m->update_invoice($array, $invoiceID);

            $this->session->set_flashdata('success', $this->lang->line('menu_success'));
            redirect(base_url("invoice/view/$invoiceID"));
        } else {
            $this->load->view('_layout_main', $this->data);
        }
    }

    public function payment_history() {
        $usertype = $this->session->userdata("usertype");
        $type = htmlentities(($this->uri->segment(4)));

        if ($type != null && ($usertype == 'Admin' || $usertype == 'Accountant')) {
            $classesID = htmlentities(($this->uri->segment(3)));
            $this->data['payment'] = array();
            if ($type < 3) {
                $this->data['payment'] = $this->invoice_m->get_payment_bystatus($classesID, $type);
            }
            if ($type == 3) {
                $this->data['payment'] = $this->invoice_m->get_paymentinfo($classesID);
            }
        }
        if ($type != null && ($usertype == 'Parent' || $usertype == 'Student')) {
            $name = $this->session->userdata['name'];
            $this->data['payment'] = $this->db->query('SELECT * FROM ' . set_schema_name() . 'payment_history where parent_name=\'' . $name . '\' OR student_name=\'' . $name . "'")->result();
        }
        $parents = $this->student_m->get_parent_info($this->session->userdata('username'));
        $this->data['classes'] = $this->invoice_m->get_classes();
        $this->data['subview'] = 'invoice/payment_history';
        $this->load->view('_layout_main', $this->data);
    }

    public function payment_data($type = null) {
        $classID = $this->input->post('classesID');
        $status = $this->input->post('id');

        if ($classID != 0) {
            $string = base_url("invoice/payment_history/$classID/$status");
            echo $string;
        } else {
            
        }
    }

    public function bankslip_view($id = 22) {
        $this->data['invoice'] = $this->db->get_where('payment_history', array('paymentID' => $id))->row();
        $this->data['subview'] = 'invoice/view_bankslip';
        $this->load->view('_layout_main', $this->data);
    }

    public function payment_accept() {
        $this->invoice_m->update_invoice(array('status' => $this->input->get_post('status')), $this->input->get_post('invoiceID'));
        $payment_id = $this->input->get_post('paymentID');
        $user_id = $this->userID();
        $data = array(
            'approved' => $this->input->get_post('approved'),
            "approved_date" => "now()",
            "approved_user_id" => $user_id
        );
        $this->db->set($data);
        $this->db->where('paymentID', $payment_id);
        $this->db->update('payment');
        //create receipt
        $referenceNo = 'SS.' . strtotime(date('h:m:s')) . '.' . date('Y') . '.RC.' . strtotime(date('M'));
        $receipt_data = array(
            'paymentID' => $payment_id,
            'code' => $referenceNo
        );
        $this->db->insert('receipt', $receipt_data);
        $school = $this->session->CI->data['siteinfos'];
        //send SMS to a parent
        $parent = $this->db->query('select p.phone, p.name FROM ' . set_schema_name() . 'parent p JOIN ' . set_schema_name() . 'payment_history h ON h.parent_name=p.name WHERE h."paymentID"=' . $payment_id)->row();
        $status = $this->input->get_post('approved') == 1 ? ' accepted' : ' rejected';
        $message = 'Hello ' . $parent->name . '. Your payment has been ' . $status . ' successfully by ' . $school->sname . '. Your receipt  reference No : ' . $referenceNo;
        $this->send_sms($parent->phone, $message);
        echo 'success';
    }

    public function receipt() {
        $usertype = $this->session->userdata("usertype");
        $classID = htmlentities(($this->uri->segment(4)));
        $id = htmlentities(($this->uri->segment(3)));
        if ((int) $classID) {
            $this->data["receipt"] = $this->invoice_m->get_join_receipts($classID, $id);
            $this->data["receipt_info"] = $this->invoice_m->get_join_receipts($classID, $id);
            if ($this->data["receipt"]) {
//		$this->load->library('html2pdf');
//		$this->html2pdf->folder('./assets/pdfs/');
//		$this->html2pdf->filename('Report.pdf');
//		$this->html2pdf->paper('a4', 'portrait');
//		$html = $this->load->view('invoice/payment/receipt', $this->data, true);
//		$this->html2pdf->html($html);
//		$this->html2pdf->create();
                $this->data["subview"] = "invoice/payment/bulk_receipt";
                $this->load->view('_layout_main', $this->data);
                //return $this->load->view('invoice/payment/receipt', $this->data);
            } else {
                 $this->session->set_flashdata('error', 'No one has Paid yet');
                 redirect(base_url("invoice/bulk_index/$classID"));
               // $this->data["subview"] = "error";
                //$this->load->view('_layout_main', $this->data);
            }
        } elseif ((int) $id && (int) $classID == '') {

            $this->data["receipt"] = $this->user_m->get_username_row('payment_history', array('paymentID' => $id));
            $this->data["receipt_info"] = $this->user_m->get_username_row('receipt', array('paymentID' => $id));
            if ($this->data["receipt"]) {

//		$this->load->library('html2pdf');
//		$this->html2pdf->folder('./assets/pdfs/');
//		$this->html2pdf->filename('Report.pdf');
//		$this->html2pdf->paper('a4', 'portrait');
//		$html = $this->load->view('invoice/payment/receipt', $this->data, true);
//		$this->html2pdf->html($html);
//		$this->html2pdf->create();
                $this->data["subview"] = "invoice/payment/receipt";
                $this->load->view('_layout_main', $this->data);
                //return $this->load->view('invoice/payment/receipt', $this->data);
            } else {
                $this->data["subview"] = "error";
                $this->load->view('_layout_main', $this->data);
            }
        } else {
            $this->data["subview"] = "error";
            $this->load->view('_layout_main', $this->data);
        }
    }

}
