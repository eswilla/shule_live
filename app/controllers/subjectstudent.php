<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
/**
 * Description of subjectstudent
 *
 *  -----------------------------------------------------
 *  Copyright: INETS COMPANY LIMITED
 *  Website: www.inetstz.com
 *  Email: info@inetstz.com
 *  -----------------------------------------------------
 * @author Ephraim Swilla
 */
Class subjectstudent extends Admin_Controller
{

    /**
     * -----------------------------------------
     *
     * ******* Address****************
     * INETS COMPANY LIMITED
     * P.O BOX 32258, DAR ES SALAAM
     * TANZANIA
     *
     *
     * *******Office Location *********
     * 11th block, Bima Road, Mikocheni B, Kinondoni, Dar es salaam
     *
     *
     * ********Contacts***************
     * Email: <info@inetstz.com>
     * Website: <www.inetstz.com>
     * Mobile: <+255 655 406 004>
     * Tel:    <+255 22 278 0228>
     * -----------------------------------------
     */
    function __construct()
    {
        parent::__construct();
        $language = $this->session->userdata('lang');
        $this->lang->load('subject', $language);
	$this->load->model("subject_student_m");
    }
    
    
     public function view() {
	$language = $this->session->userdata('lang');
	$this->lang->load('student', $language);
	
	
	$subjectID = $this->input->get_post('subject_id');
	$class_id = $this->input->get_post('class_id');
	$this->data['set'] = $subjectID;

	$this->data['subject_name']=$this->subject_m->get_subject($subjectID)->subject;
	//lets first check if this subject is option subject 
	$sections = $this->section_m->get_order_by_section(array("classesID" => $class_id));
	$this->data['sections'] = $sections;
	foreach ($sections as $key => $section) {
	    $this->data['allsection'][$section->section] = $this->subject_student_m->get_students_not_subscribe($class_id, $section->sectionID, $subjectID);
	}


	//Then get all students from that class who study that subject
	$this->data['subject_students'] = $this->db->query('SELECT b.*,a.* FROM ' . set_schema_name() . 'subject_student a JOIN ' . set_schema_name() . 'student_info b ON a.student_id=b."studentID" WHERE a.subject_id=' . $subjectID . '')->result();

	$this->load->view('subject/subject_subscriber', $this->data);
    }

    public function add() {
	$student_ids = explode(',', $this->input->get_post('student_ids'));
	$subject_id = $this->input->get_post('subject_id');
	$data = [];
	foreach ($student_ids as $student_id) {
	    $arr = array('student_id' => $student_id, 'subject_id' => $subject_id);
	    array_push($data, $arr);
	}
	$this->db->insert_batch('subject_student', $data);
	$this->session->set_flashdata('success', $this->lang->line('menu_success'));
	echo 'Student Subscribed successfully';
    }
    
    public function delete() {
	$subject_student_id = $this->input->get_post('id');
	$this->subject_student_m->delete_subject_student($subject_student_id);
	echo 'Student Removed successfully';
    }

}