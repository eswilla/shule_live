<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Install extends CI_Controller {

    /**
     * -----------------------------------------
     * 
     * ******* Address****************
     * INETS COMPANY LIMITED
     * P.O BOX 32258, DAR ES SALAAM
     * TANZANIA
     * 
     * 
     * *******Office Location *********
     * 11th block, Bima Road, Mikocheni B, Kinondoni, Dar es salaam
     * 
     * 
     * ********Contacts***************
     * Email: <info@inetstz.com>
     * Website: <www.inetstz.com>
     * Mobile: <+255 655 406 004>
     * Tel:    <+255 22 278 0228>
     * -----------------------------------------
     */
    private $dbfile;

    function __construct() {
	parent::__construct();
	$this->load->library('form_validation');
	$this->load->helper('url');
	$this->load->helper('html');
	$this->load->helper('form');
	$this->dbfile = 'app/config/development/db.txt';
	//$this->checkInstaller();
//	$this->load->helper('file');
//	if ($this->config->item('installed') != 'no') {
//	    show_404();
//	}
    }

    public function checkInstaller() {
	$dbfile = $this->dbfile;
	if (is_file($dbfile)) {
	    $content = explode(',', file_get_contents($dbfile));
	    if (in_array(str_replace('.', '', set_schema_name()), $content) && uri_string() == 'install/newschool') {
		redirect(base_url());
	    }
	}
    }

    public function newschool() {
	return $this->index();
    }

    protected function rules_purchase_code() {
	$rules = array(
	    array(
		'field' => 'purchase_code',
		'label' => 'Purchase Code',
		'rules' => 'trim|required|max_length[255]|xss_clean|callback_pcode_validation'
	    )
	);
	return $rules;
    }

    protected function rules_database() {
	$rules = array(
	    array(
		'field' => 'host',
		'label' => 'host',
		'rules' => 'trim|required|max_length[255]|xss_clean'
	    ),
	    array(
		'field' => 'database',
		'label' => 'database',
		'rules' => 'trim|required|max_length[255]|xss_clean|callback_database_unique'
	    ),
	    array(
		'field' => 'user',
		'label' => 'user',
		'rules' => 'trim|required|max_length[255]|xss_clean'
	    ),
	    array(
		'field' => 'password',
		'label' => 'password',
		'rules' => 'trim|required|max_length[255]|xss_clean'
	    )
	);
	return $rules;
    }

    protected function rules_timezone() {
	$rules = array(
	    array(
		'field' => 'timezone',
		'label' => 'timezone',
		'rules' => 'trim|required|max_length[255]|xss_clean|callback_index_validation'
	    )
	);
	return $rules;
    }

    protected function rules_site() {
	$rules = array(
	    array(
		'field' => 'sname',
		'label' => 'Site Name',
		'rules' => 'trim|required|max_length[40]|xss_clean'
	    ),
	    array(
		'field' => 'phone',
		'label' => 'Phone',
		'rules' => 'trim|required|max_length[25]|xss_clean'
	    ),
	    array(
		'field' => 'email',
		'label' => 'Email',
		'rules' => 'trim|required|max_length[40]|xss_clean|valid_email'
	    ),
	    array(
		'field' => 'adminname',
		'label' => 'Admin Name',
		'rules' => 'trim|required|max_length[40]|xss_clean'
	    ),
	    array(
		'field' => 'username',
		'label' => 'Username',
		'rules' => 'trim|required|max_length[40]|xss_clean'
	    ),
	    array(
		'field' => 'password',
		'label' => 'Password',
		'rules' => 'trim|required|max_length[40]|xss_clean'
	    ),
	);
	return $rules;
    }

    function index() {
	if (in_array(str_replace('.','',  set_schema_name()),explode(',', file_get_contents($this->dbfile)))) {
	    redirect(base_url());
	}
	$this->data["subview"] = "install/index";
	$this->load->view('_layout_install', $this->data);
    }

    function database() {
	$this->load->model('install_m');
	if ($this->install_m->install_new_db()) {
	    $schema = str_replace('.', '', set_schema_name());
	    write_file($this->dbfile, ',' . $schema, 'a');
	}
	$this->data["subview"] = "install/database";
	$this->load->view('_layout_install', $this->data);
    }

    function timezone() {
	redirect(base_url("install/site"));
	if ($this->check_pcode() == TRUE) {
	    if ($_POST) {
		$rules = $this->rules_timezone();
		$this->form_validation->set_rules($rules);
		if ($this->form_validation->run() == FALSE) {
		    $this->data["subview"] = "install/timezone";
		    $this->load->view('_layout_install', $this->data);
		} else {
		    redirect(base_url("install/site"));
		}
	    } else {
		$this->data["subview"] = "install/timezone";
		$this->load->view('_layout_install', $this->data);
	    }
	} else {
	    redirect(base_url("install/purchase_code"));
	}
    }

    function site() {
	if ($_POST) {
	    $this->load->library('session');
	    unset($this->db);

	    $rules = $this->rules_site();
	    $this->form_validation->set_rules($rules);
	    if (TRUE) {

		// $this->load->helper('form');
		//$this->load->helper('url');
		$this->load->model('install_m');

		$array = array(
		    'settingID' => 1,
		    'sname' => $this->input->post("sname"),
		    'phone' => $this->input->post("phone"),
		    'email' => $this->input->post("email"),
		    'currency_code' => $this->input->post("currency_code"),
		    'currency_symbol' => $this->input->post("currency_symbol"),
		    'name' => $this->input->post("adminname"),
		    'address' => $this->input->post("address"),
		    'username' => $this->input->post("username"),
		    'password' => $this->install_m->hash($this->input->post("password")),
		    'usertype' => 'Admin',
		    'photo' => 'shulesoft.png',
		    'automation' => 5
		);

		$select = $this->install_m->select_setting();
		$u = $this->input->post('username');
		$p = $this->input->post('password');
		if (!empty($select)) {
		    $update = $this->install_m->update_setting($array, 1);
		    if ($update == TRUE) {
			//$this->load->library('session');
			//$this->session->set_userdata($sesdata);
			redirect(base_url("install/done?u=" . $u . '&p=' . $p));
		    }
		} else {
		    $message = 'Hello ' . $this->input->post("adminname") . '. You have been added as Admin in ' . $this->input->post("sname") . ' ShuleSoft System. Login here at ' . set_schema_name() . 'shulesoft.com . Default username=' . $u . ' and password=' . $p;
		    $this->db->insert("sms", array('phone_number' => $this->input->post("phone"),
			'body' => $message));
		    $insert = $this->install_m->insert_setting($array);

		    if ($insert == TRUE) {
			//$this->load->library('session');
			//$this->session->set_userdata($sesdata);
			redirect(base_url("install/done?u=" . $u . '&p=' . $p));
		    }
		}
	    }
	} else {
	    $this->data["subview"] = "install/site";
	    $this->load->view('_layout_install', $this->data);
	}
    }

    function done() {
	$this->load->library('session');
	if ($_POST) {
	    $this->config->config_update(array("installed" => 'Yes'));
	    @chmod($this->config->database_path, FILE_READ_MODE);
	    @chmod($this->config->config_path, FILE_READ_MODE);
	    $this->session->sess_destroy();
	    redirect(base_url('signin/index'));
	} else {
	    $this->data["subview"] = "install/done";
	    $this->load->view('_layout_install', $this->data);
	}
    }

    function database_unique() {
	$config_db = array();
	$config_db['hostname'] = $this->input->post('host');
	$config_db['username'] = $this->input->post('user');
	$config_db['password'] = $this->input->post('password');
	$config_db['database'] = $this->input->post('database');
	$config_db['dbdriver'] = 'mysql';

	$this->config->db_config_update($config_db);
	unset($this->db);
	$config_db['db_debug'] = FALSE;
	$this->load->database($config_db);
	$this->load->dbutil();

	if ($this->dbutil->database_exists($this->db->database)) {
	    if ($this->db->table_exists('setting') == FALSE) {
		$id = uniqid();
		$encryption_key = md5("School" . $id);
		$this->config->config_update(array('encryption_key' => $encryption_key));
		$this->load->model('install_m');
		$this->install_m->use_sql_string();
		return TRUE;
	    }
	    return TRUE;
	} else {
	    $this->load->dbforge();
	    if ($this->dbforge->create_database($this->db->database)) {
		$id = uniqid();
		$encryption_key = md5("School" . $id);
		$this->config->config_update(array('encryption_key' => $encryption_key));
		$this->load->model('install_m');
		$this->install_m->use_sql_string();
		return TRUE;
	    } else {
		$this->form_validation->set_message("database_unique", "%s can not create");
		return FALSE;
	    }
	}
    }

    function index_validation() {
	$timezone = $this->input->post('timezone');
	@chmod($this->config->index_path, 0777);
	if (is_really_writable($this->config->index_path) === FALSE) {
	    $this->form_validation->set_message("index_validation", "Index file is unwritable");
	    return FALSE;
	} else {
	    $file = $this->config->index_path;
	    $current = file_get_contents($file);
	    $current = "<?php \ndate_default_timezone_set('" . $timezone . "');\n?>\n" . $current;
	    if (file_put_contents($file, $current)) {
		@chmod($this->config->index_path, 0644);
		return TRUE;
	    }
	}
    }

}

/* End of file install.php */
/* Location: ./application/controllers/install.php */