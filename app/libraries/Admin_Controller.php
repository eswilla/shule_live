<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Admin_Controller extends MY_Controller {

    /**
     * -----------------------------------------
     * 
     * ******* Address****************
     * INETS COMPANY LIMITED
     * P.O BOX 32258, DAR ES SALAAM
     * TANZANIA
     * 
     * 
     * *******Office Location *********
     * 11th block, Bima Road, Mikocheni B, Kinondoni, Dar es salaam
     * 
     * 
     * ********Contacts***************
     * Email: <info@inetstz.com>
     * Website: <www.inetstz.com>
     * Mobile: <+255 655 406 004>
     * Tel:    <+255 22 278 0228>
     * -----------------------------------------
     */
    function __construct() {

	parent::__construct();

	$this->load->library("session");

	$this->data["siteinfos"] = $this->db->query('select * from ' . set_schema_name() . 'setting')->row();

	//it is mandatory since each user has specific ID

	$this->load->helper('language');
	$this->load->helper('date');
	$this->load->helper('form');
	$this->load->library('form_validation');

	/* Alert System Start......... */
	$this->load->model("notice_m");
	$this->load->model("alert_m");
	$this->data['all'] = array();
	$this->data['alert'] = array();

	$this->load->model('user_m');
//	$notices = $this->notice_m->get_notice();
//
//	$i = 0;
//	if (count($notices) > 0) {
//
//	    foreach ($notices as $notice) {
//		$this->data['all'][] = $this->user_m->get_username('alert', array("noticeID" => $notice->noticeID, "username" => "'" . $this->session->userdata("username") . "'"));
//		if (count($this->data['all'][$i]) == 0) {
//		    $this->data['alert'][] = $notice;
//		}
//		$i++;
//	    }
//	}
	$this->data['alert'];
	/* Alert System End......... */
	/* message counter */
//	$email = $this->session->userdata('email');
//	$usertype = $this->session->userdata('usertype');
//	//$userID = $this->userID();
//	$this->load->model('message_m');
//	$this->data['unread'] = $this->message_m->get_order_by_message(array('email' => "'" . $email . "'", 'receiverType' => "'" . $usertype . "'", 'to_status' => 0, 'read_status' => 0));
////	/* message counter end */

	$language = $this->session->userdata('lang');
	$this->lang->load('topbar_menu', $language);

	$exception_uris = array(
	    "signin/index",
	    "background/index",
	    "help/index",
	    "payment/api",
	    "help",
	    "signin/signout",
	    "admission/index",
	    "admission/addStudent",
	    "admission/addParent",
	    "admission/status",
	    "termsandprivacy/index",
	    "install/newschool",
	    "background/test"
	);

	if (in_array(uri_string(), $exception_uris) == FALSE) {
	    // $this->load->model('signin_m');
	    if ((bool) $this->session->userdata("loggedin") == FALSE) {
		redirect(base_url("signin/index"));
	    }
	}

	//get user pic
	$id = $this->session->userdata("id");
	if( (int) $id >0) {
	    $user = $this->user_m->get_username_row('users', array("id" => $id));
	    $this->data['user_info'] = $this->user_m->get_username_row($user->table, array("username" => $user->username));
	}
    }

    public function send_email($email, $subject, $message) {
	return $this->db->insert("email", array('body' => is_array($message) ? $message['message'] : $message, 'subject' => $subject, 'email' => $email));
    }

    public function send_sms($phone_number, $message, $message_type = true) {
	$status = $this->db->insert("sms", array('phone_number' => $phone_number, 'body' => $message));
	if ($status) {
	    $data = json_encode(array('success' => 1, 'message' => 'sent'));
	} else {
	    $data = json_encode(array('success' => 0, 'message' => 'sent'));
	}
	return $data;
    }

    public function get_setting() {
	return $this->setting_m->get_setting(1);
    }

    public function sendSMS() {
	
    }

    public function sendEmail($email, $subject, $message, $to_name = NULL) {
	$siteinfo = $this->site_m->get_site(1);
	$headers = 'From: ' . $siteinfo->sname . ' <noreply@shulesoft.com>\r\n';
	//$headers .= "Reply-To: noreply@shulesoft.com\n";
	$headers .= "X-Mailer: PHP/" . phpversion() . "\n";
	$headers .= "MIME-Version: 1.0\n";
	$headers .= "Content-Type: text/html; charset=iso-8859-1";
	$array = array(
	    "src" => base_url('uploads/images/' . $siteinfo->photo),
	    'width' => '59px',
	    'height' => '59px',
	    'class' => 'img-rounded',
	    'style' => 'height: auto; display: block; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; color: #666666; font-size: 16px;'
	);
	$img_url = img($array);

	$content = '<html>
    <head>
	<title>' . $subject . '</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width">
	<style type="text/css">

	    /* CLIENT-SPECIFIC STYLES */
	    #outlook a{padding:0;} /* Force Outlook to provide a "view in browser" message */
	    .ReadMsgBody{width:100%;} .ExternalClass{width:100%;} /* Force Hotmail to display emails at full width */
	    .ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div {line-height: 100%;} /* Force Hotmail to display normal line spacing */
	    body, table, td, a{-webkit-text-size-adjust:100%; -ms-text-size-adjust:100%;} /* Prevent WebKit and Windows mobile changing default text sizes */
	    table, td{mso-table-lspace:0pt; mso-table-rspace:0pt;} /* Remove spacing between tables in Outlook 2007 and up */
	    img{-ms-interpolation-mode:bicubic;} /* Allow smoother rendering of resized image in Internet Explorer */

	    /* iOS BLUE LINKS */

	    .ios-links a {color:#db4c3f !important; text-decoration: none !important;}


	    /* RESET STYLES */
	    body{margin:0; padding:0;}
	    img{border:0; height:auto; line-height:100%; outline:none; text-decoration:none;}
	    table{border-collapse:collapse !important;}
	    body{height:100% !important; margin:0; padding:0; width:100% !important;}


	    /* FONT SMOOTHING */
	    body {
		-webkit-font-smoothing: antialiased;
		-moz-osx-font-smoothing: grayscale;
	    }


	    /* MOBILE STYLES */
	    @media screen and (max-width: 630px) {

		/* RESPONSIVE ELEMENTS */
		img[class="responsive-img"]{
		    max-width: 100% !important;
		    height: auto !important;
		}

		table[class="responsive-button"]{
		    width: 100% !important;
		}

		table[class="responsive-table"]{
		    width: 100% !important;
		}

		/* RESPONSIVE VISIBILITY */
		img[class="mobile-hide"]{
		    display: none !important;
		}

		td[class="mobile-hide"]{
		    display: none !important;
		}

		/* UTILITY CLASSES FOR ADJUSTING PADDING ON MOBILE */
		td[class="no-padding"]{
		    padding: 0 !important;
		}

		/* COMPONENTS */

		/* HEADING A */
		td[class="heading-a-title"]{
		    font-size: 23px !important;
		}

		td[class="heading-a-subtitle"]{
		    font-size: 20px !important;
		}

	    }

	</style>
    </head>



    <body style="margin: 0; padding: 0;">



	<!-- PREHEADER TEXT -->
	<div style="display: none; font-size: 1px; color: #474747; line-height: 1px; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; max-height: 0px; max-width: 0px; opacity:0; overflow:hidden;">

	</div>

	<!-- HEADER -->
	<table border="0" cellpadding="0" cellspacing="0" width="100%" style="table-layout: fixed;">
	    <tr>
		<td bgcolor="#ffffff" align="center" style="padding: 0 15px 55px 15px;">
		    <table border="0" cellpadding="0" cellspacing="0" width="480" class="responsive-table">

			<tr>
			    <td style="border-top: 4px solid #00acac; padding-bottom: 25px" colspan="2"></td>
			</tr>
		    </table>
		</td>
	    </tr>
	</table>



	<!-- GRID CONTAINER -->
	<table border="0" cellpadding="0" cellspacing="0" width="100%" style="table-layout: fixed;">
	    <tr>
		<td bgcolor="#ffffff" align="center" style="padding: 0 15px 0 15px;">
		    <table border="0" cellpadding="0" cellspacing="0" width="480" class="responsive-table">



			<!-- GRID ROW -->
			<tr>
			    <td>




				<!-- GRID CELL -->
				<table cellpadding="0" cellspacing="0" border="0" width="100%" align="center" class="responsive-table">
				    <tr>
					<td style="padding: 20px 0 20px 0;">

					    <!-- GRID CELL INNER -->
					    <table cellpadding="0" cellspacing="0" border="0" width="100%">

						<!-- CONTENT -->

						<tr>
						    <!-- PARAGRAPH -->
						    <td align="left" style="padding-bottom: 25px; color: #474747; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; font-size: 15px; font-weight: normal; line-height: 24px">
							' . $message . ' 

						    </td>
						</tr>        

						<tr>
						    <!-- SPACER A -->
						    <td style="padding-bottom: 25px" class="spacer-a"></td>
						</tr>
						<tr>
						    <td align="center">

							<!-- BUTTON -->
							<table border="0" cellspacing="0" cellpadding="0" class="responsive-button">
							    <tr>
								<td align="center" style="-webkit-border-radius: 4px; -moz-border-radius: 4px; border-radius: 4px;" bgcolor="#00acac">
								    <a href="' . $siteinfo->name . '.shulesoft.com" target="_blank" style="font-size: 15px; line-height: 17px; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; font-weight: 500; color: #ffffff; text-decoration: none; -webkit-border-radius: 4px; -moz-border-radius: 4px; border-radius: 4px; padding: 9px 24px; border: 1px solid #00acac; display: block;"><!--[if gte mso 9]>&nbsp;<![endif]-->Account Login<!--[if gte mso 9]>&nbsp;<![endif]--></a>
								</td>
							    </tr>
							</table>

						    </td>
						</tr>


						<tr>

						    <!-- SPACER A -->
						    <td style="padding-bottom: 25px" class="spacer-a"></td>

						</tr>


						<tr>

						    <!-- SPACER A -->
						    <td style="padding-bottom: 25px" class="spacer-a"></td>

						</tr>




						<tr>
						    <!-- DIVIDER -->
						    <td style="border-top: 1px solid #e4e4e4; padding: 0 15px 30px 15px;">

						    </td>
						</tr>



						<tr>
						    <!-- PARAGRAPH -->
						    <td align="center" style="padding-bottom: 25px; color: #474747; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; font-size: 15px; font-weight: normal; line-height: 24px">


							<b>ShuleSoft</b><br>
							Don\'t forget the best


						    </td>
						</tr>

					    </table>

					</td>
				    </tr>
				</table>



			    </td>
			</tr>



		    </table>
		</td>
	    </tr>
	</table>




	<!--FOOTER -->
	<table border = "0" cellpadding = "0" cellspacing = "0" width = "100%" style = "table-layout: fixed;">
	    <tr>
		<td bgcolor = "#ffffff" align = "center" style = "padding: 25px 15px 25px 15px;">
		    <table border = "0" cellpadding = "0" cellspacing = "0" width = "480" class = "responsive-table">

			<!--CONTENT -->
			<tr>
			    <!--SOCIAL COLUMN -->
			    <td bgcolor = "#ffffff" align = "center">
				<table border = "0" cellpadding = "0" cellspacing = "0">
				    <tr>
					<td align = "center" style = "padding-right: 20px">
					    <a href = "https://www.facebook.com/shulesoft" target = "_blank">
						<img alt = "Facebook" src = "https://s3.amazonaws.com/f.cl.ly/items/0g20230l0J3J023t2Q0i/icon_facebook.png" width = "18" height = "18" style = "height: auto; display: block; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; color: #666666; font-size: 10px;" border = "0">
					    </a>
					</td>
					<td align = "center" style = "padding-right: 20px">
					    <a href = "https://twitter.com/shulesoft" target = "_blank">
						<img alt = "Twitter" src = "https://s3.amazonaws.com/f.cl.ly/items/09063i313T0M0k3Q2911/icon_twitter.png" width = "18" height = "18" style = "height: auto; display: block; font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; color: #666666; font-size: 10px;" border = "0">
					    </a>
					</td>
				    </tr>
				</table>
			    </td>
			</tr>
		    </table>
		</td>
	    </tr>
	</table>
    </body>
</html>';

	return mail($email, $subject, $content, $headers);
    }

    function validate_phone($phone) {
	$this->load->model("user_m");
	$user = $this->user_m->get_username_row('users', array('phone' => $phone));
	if (count($user)) {
	    $this->form_validation->set_message("unique_email", "%s already exists");
	    $array['permition'][$i] = 'no';
	    return FALSE;
	} else {
	    $array['permition'][$i] = 'yes';
	    return TRUE;
	}
    }

    function date_valid($date) {
	if (strlen($date) < 10) {
	    $this->form_validation->set_message("date_valid", "%s is not valid dd-mm-yyyy");
	    return FALSE;
	} else {
	    $arr = explode("-", $date);
	    $dd = $arr[0];
	    $mm = $arr[1];
	    $yyyy = $arr[2];
	    if (checkdate($mm, $dd, $yyyy)) {
		return TRUE;
	    } else {
		$this->form_validation->set_message("date_valid", "%s is not valid dd-mm-yyyy");
		return FALSE;
	    }
	}
    }

    // public function userID() {
    // 	$usertype = $this->session->userdata('usertype');
    // 	$username = $this->session->userdata('username');
    // 	if ($usertype=="Admin") {
    // 		$table = "setting";
    // 		$tableID = "settingID";
    // 	} elseif($usertype=="Accountant" || $usertype=="Librarian") {
    // 		$table = "user";
    // 		$tableID = "userID";
    // 	} else {
    // 		$table = strtolower($usertype);
    // 		$tableID = $table."ID";
    // 	}				
    // 	$query = $this->db->get_where($table, array('username' => $username));
    // 	$userID = $query->row()->$tableID;
    // 	return $userID;
    // }
    public function uploadExcel() {
	$this->load->library('Excel');
	try {
	    // it will be your file name that you are posting with a form or c
	    //an pass static name $_FILES["file"]["name"]
	    $folder = "./uploads/media/";
	    is_dir($folder) ? '' : mkdir($folder, 0777);
	    $name = $_FILES['file']['name'];
	    $ext = end((explode(".", $name)));
	    $file = time() . '.' . $ext;
	    $path = $folder . $file;
	    $move = move_uploaded_file($_FILES['file']['tmp_name'], $path);
	    if (!$move) {
		die('upload Error');
	    } else {
		$objPHPExcel = PHPExcel_IOFactory::load($path);
	    }
	} catch (Exception $e) {
	    $this->resp->success = FALSE;
	    $this->resp->msg = 'Error Uploading file';
	    echo json_encode($this->resp);
	}

	$sheet = $objPHPExcel->getSheet(0);
	$highestRow = $sheet->getHighestRow();
	$highestColumn = $sheet->getHighestColumn();
	$headings = $sheet->rangeToArray('A1:' . $highestColumn . 1, NULL, TRUE, FALSE);

	$data = array();
	for ($row = 2; $row <= $highestRow; $row++) {
	    //  Read a row of data into an array
	    $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, TRUE, FALSE);
	    $rowData[0] = array_combine($headings[0], $rowData[0]);
	    $data = array_merge_recursive($data, $rowData);
	}
	return $data;
    }

    public function exportExcel($data_array, $title, $file_name = 'file') {
	//load our new PHPExcel library
	$this->load->library('excel');
	//activate worksheet number 1
	$this->excel->setActiveSheetIndex(0);
	//name the worksheet
	$this->excel->getActiveSheet()->setTitle('Users list');

	//set the title values	
	$this->excel->getActiveSheet()->fromArray($title, NULL, 'A2');

	// read data to active sheet
	$this->excel->getActiveSheet()->fromArray($data_array);

	$filename = $file_name . '.xls'; //save our workbook as this file name

	header('Content-Type: application/vnd.ms-excel'); //mime type

	header('Content-Disposition: attachment;
	filename = "' . $filename . '"'); //tell browser what's the file name

	header('Cache-Control: max-age=0'); //no cache
	//save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
	//if you want to save it as .XLSX Excel 2007 format

	$objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');

	//force user to download the Excel file without writing it to server's HD
	$objWriter->save('php://output');
    }

    public function uploadImage($path = "./uploads/images/") {
	$name = $_FILES['image']['name'];
	$ext = end((explode(".", $name)));
	if (!in_array($ext, array("gif", "jpg", "png", "jpge"))) {
	    return FALSE;
	}
	$file = time() . '.' . $ext;
	$path = $path . $file;
	$move = move_uploaded_file($_FILES['image']['tmp_name'], $path);
	if (!$move) {
	    return FALSE;
	} else {
	    //save in the database
	    return $file;
	}
    }

}

/* End of file Admin_Controller.php */
/* Location: .//D/xampp/htdocs/school/mvc/libraries/Admin_Controller.php */