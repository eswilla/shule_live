<?php

class Migration_create_database extends CI_Migration {

    public function up() {


	$schema = set_schema_name();

	$this->db->trans_start();

	/*	 * ****** Start : Creating Database Structure ******* */

	$this->dbforge->add_field([
	    'id' => [
		'type' => 'smallint',
		'auto_increment' => TRUE,
	    ],
	    'name' => [
		'type' => 'VARCHAR',
		'constraint' => '100',
		"null" => FALSE,
		'unique' => TRUE,
	    ],
	    "created_at timestamp(0) without time zone NOT NULL DEFAULT ('now'::text)::timestamp(0) without time zone",
	    'updated_at' => [
		'type' => 'timestamp(0) without time zone',
		"null" => true,
	    ],
	]);
	$this->dbforge->add_key('id', TRUE);
	$this->dbforge->create_table('user_type');

	$this->db->query("insert into {$schema}user_type(name) values ('Parent')");
	$this->db->query("insert into {$schema}user_type(name) values ('User')");
	$this->db->query("insert into {$schema}user_type(name) values ('Student')");

	$this->dbforge->add_field([
	    'id' => [
		'type' => 'smallint',
		'auto_increment' => TRUE,
	    ],
	    'name' => [
		'type' => 'VARCHAR',
		'constraint' => '100',
		"null" => FALSE,
		'unique' => TRUE,
	    ],
	    "created_at timestamp(0) without time zone NOT NULL DEFAULT ('now'::text)::timestamp(0) without time zone",
	    'updated_at' => [
		'type' => 'timestamp(0) without time zone',
		"null" => true,
	    ],
	]);
	$this->dbforge->add_key('id', TRUE);
	$this->dbforge->create_table('profession');

	$this->dbforge->add_field([
	    'id' => [
		'type' => 'smallint',
		'auto_increment' => TRUE,
	    ],
	    'name' => [
		'type' => 'VARCHAR',
		'constraint' => '100',
		"null" => FALSE,
		'unique' => TRUE,
	    ],
	    "created_at timestamp(0) without time zone NOT NULL DEFAULT ('now'::text)::timestamp(0) without time zone",
	    'updated_at' => [
		'type' => 'timestamp(0) without time zone',
		"null" => true,
	    ],
	]);
	$this->dbforge->add_key('id', TRUE);
	$this->dbforge->create_table('religion');

	$this->dbforge->add_field([
	    'id' => [
		'type' => 'bigint',
		'auto_increment' => TRUE,
	    ],
	    'username' => [
		'type' => 'VARCHAR',
		'constraint' => '30',
		"null" => FALSE,
		'unique' => TRUE,
	    ],
	    'password' => [
		'type' => 'VARCHAR',
		'constraint' => '300',
		"null" => FALSE,
	    ],
	    'firstname' => [
		'type' => 'VARCHAR',
		'constraint' => '30',
		"null" => FALSE,
	    ],
	    'othernames' => [
		'type' => 'VARCHAR',
		'constraint' => '30',
		"null" => FALSE,
	    ],
	    'email' => [
		'type' => 'VARCHAR',
		'constraint' => '30',
	    ],
	    'phone' => [
		'type' => 'VARCHAR',
		'constraint' => '30',
	    ],
	    'address' => [
		'type' => 'VARCHAR',
		'constraint' => '50',
	    ],
	    'photo' => [
		'type' => 'VARCHAR',
		'constraint' => '50',
	    ],
	    'profession_id' => [
		'type' => 'smallint',
	    ],
	    'user_type_id' => [
		'type' => 'smallint',
		"null" => FALSE,
		'default' => 1,
	    ],
	    "created_at timestamp(0) without time zone NOT NULL DEFAULT ('now'::text)::timestamp(0) without time zone",
	    'updated_at' => [
		'type' => 'timestamp(0) without time zone',
		"null" => true,
	    ],
	    "FOREIGN KEY (profession_id) REFERENCES {$schema}profession(id) on update restrict on delete restrict",
	    "FOREIGN KEY (user_type_id) REFERENCES {$schema}user_type(id) on update restrict on delete restrict",
	]);
	$this->dbforge->add_key('id', TRUE);
	$this->dbforge->create_table('parent');

	$this->dbforge->add_field([
	    'id' => [
		'type' => 'bigint',
		'auto_increment' => TRUE,
	    ],
	    'firstname' => [
		'type' => 'VARCHAR',
		'constraint' => '30',
		"null" => FALSE,
	    ],
	    'othernames' => [
		'type' => 'VARCHAR',
		'constraint' => '30',
		"null" => FALSE,
	    ],
	    'dob' => [
		'type' => 'date',
	    ],
	    'gender' => [
		'type' => 'char(1)',
		"null" => FALSE,
	    ],
	    'email' => [
		'type' => 'VARCHAR',
		'constraint' => '30',
	    ],
	    'photo' => [
		'type' => 'VARCHAR',
		'constraint' => '50',
	    ],
	    'username' => [
		'type' => 'VARCHAR',
		'constraint' => '30',
		"null" => FALSE,
		'unique' => TRUE,
	    ],
	    'password' => [
		'type' => 'VARCHAR',
		'constraint' => '300',
		"null" => FALSE,
	    ],
	    'jod' => [
		'type' => 'date',
	    ],
	    'religion_id' => [
		'type' => 'smallint',
	    ],
	    'user_type_id' => [
		'type' => 'smallint',
		"null" => FALSE,
		'default' => 2,
	    ],
	    "created_at timestamp(0) without time zone NOT NULL DEFAULT ('now'::text)::timestamp(0) without time zone",
	    'updated_at' => [
		'type' => 'timestamp(0) without time zone',
		"null" => true,
	    ],
	    "FOREIGN KEY (religion_id) REFERENCES {$schema}religion(id) on update restrict on delete restrict",
	    "FOREIGN KEY (user_type_id) REFERENCES {$schema}user_type(id) on update restrict on delete restrict",
	]);
	$this->dbforge->add_key('id', TRUE);
	$this->dbforge->create_table('user');
	$this->db->query("comment on column {$schema}user.jod is 'Joining date'");

	$this->dbforge->add_field([
	    'id' => [
		'type' => 'smallint',
		'auto_increment' => TRUE,
	    ],
	    'name' => [
		'type' => 'VARCHAR',
		'constraint' => '100',
		"null" => FALSE,
		'unique' => TRUE,
	    ],
	    "created_at timestamp(0) without time zone NOT NULL DEFAULT ('now'::text)::timestamp(0) without time zone",
	    'updated_at' => [
		'type' => 'timestamp(0) without time zone',
		"null" => true,
	    ],
	]);
	$this->dbforge->add_key('id', TRUE);
	$this->dbforge->create_table('class_level');
	$this->db->query("comment on table {$schema}class_level is 'Class level example Primary School, O-level, A-level School, '");


	$this->dbforge->add_field([
	    'id' => [
		'type' => 'smallint',
		'auto_increment' => TRUE,
	    ],
	    'name' => [
		'type' => 'VARCHAR',
		'constraint' => '100',
		"null" => FALSE,
		'unique' => TRUE,
	    ],
	    'numeric' => [
		'type' => 'VARCHAR',
		'constraint' => '10',
		'unique' => TRUE,
	    ],
	    'name' => [
		'type' => 'text',
	    ],
	    'class_level_id' => [
		'type' => 'smallint',
		"null" => FALSE,
	    ],
	    "created_at timestamp(0) without time zone NOT NULL DEFAULT ('now'::text)::timestamp(0) without time zone",
	    'updated_at' => [
		'type' => 'timestamp(0) without time zone',
		"null" => true,
	    ],
	    "FOREIGN KEY (class_level_id) REFERENCES {$schema}class_level(id) on update restrict on delete restrict",
	]);
	$this->dbforge->add_key('id', TRUE);
	$this->dbforge->create_table('class');


	$this->dbforge->add_field([
	    'id' => [
		'type' => 'smallint',
		'auto_increment' => TRUE,
	    ],
	    'name' => [
		'type' => 'VARCHAR',
		'constraint' => '100',
		"null" => FALSE,
		'unique' => TRUE,
	    ],
	    "created_at timestamp(0) without time zone NOT NULL DEFAULT ('now'::text)::timestamp(0) without time zone",
	    'updated_at' => [
		'type' => 'timestamp(0) without time zone',
		"null" => true,
	    ],
	]);
	$this->dbforge->add_key('id', TRUE);
	$this->dbforge->create_table('section_category');
	$this->db->query("comment on table {$schema}section_category is 'Example Science Section, Arts Section'");


	$this->dbforge->add_field([
	    'id' => [
		'type' => 'smallint',
		'auto_increment' => TRUE,
	    ],
	    'name' => [
		'type' => 'VARCHAR',
		'constraint' => '100',
		"null" => FALSE,
		'unique' => TRUE,
	    ],
	    'note' => [
		'type' => 'text',
	    ],
	    'extra' => [
		'type' => 'text',
	    ],
	    'section_category_id' => [
		'type' => 'smallint',
		"null" => FALSE,
	    ],
	    'class_id' => [
		'type' => 'smallint',
		"null" => FALSE,
	    ],
	    "created_at timestamp(0) without time zone NOT NULL DEFAULT ('now'::text)::timestamp(0) without time zone",
	    'updated_at' => [
		'type' => 'timestamp(0) without time zone',
		"null" => true,
	    ],
	    "FOREIGN KEY (section_category_id) REFERENCES {$schema}section_category(id) on update restrict on delete restrict",
	    "FOREIGN KEY (class_id) REFERENCES {$schema}class(id) on update restrict on delete restrict",
	]);
	$this->dbforge->add_key('id', TRUE);
	$this->dbforge->create_table('section');

	$this->dbforge->add_field([
	    'id' => [
		'type' => 'smallint',
		'auto_increment' => TRUE,
	    ],
	    'section_id' => [
		'type' => 'smallint',
		"null" => FALSE,
	    ],
	    'user_id' => [
		'type' => 'bigint',
		"null" => FALSE,
	    ],
	    "created_at timestamp(0) without time zone NOT NULL DEFAULT ('now'::text)::timestamp(0) without time zone",
	    'updated_at' => [
		'type' => 'timestamp(0) without time zone',
		"null" => true,
	    ],
	    "FOREIGN KEY (section_id) REFERENCES {$schema}section(id) on update restrict on delete restrict",
	    "FOREIGN KEY (user_id) REFERENCES {$schema}user(id) on update restrict on delete restrict",
	]);
	$this->dbforge->add_key('id', TRUE);
	$this->dbforge->create_table('section_user');
	$this->db->query("comment on table {$schema}section_user is 'Store the teacher who is responsible for a given section'");


	$this->dbforge->add_field([
	    'id' => [
		'type' => 'integer',
		'auto_increment' => TRUE,
	    ],
	    'route' => [
		'type' => 'VARCHAR',
		'constraint' => '200',
		"null" => FALSE,
		'unique' => TRUE,
	    ],
	    'fare' => [
		'type' => 'numeric(6,2)',
		"null" => FALSE,
	    ],
	    'note' => [
		'type' => 'text',
	    ],
	    "created_at timestamp(0) without time zone NOT NULL DEFAULT ('now'::text)::timestamp(0) without time zone",
	    'updated_at' => [
		'type' => 'timestamp(0) without time zone',
		"null" => true,
	    ],
	]);
	$this->dbforge->add_key('id', TRUE);
	$this->dbforge->create_table('transport');

	$this->dbforge->add_field([
	    'id' => [
		'type' => 'smallint',
		'auto_increment' => TRUE,
	    ],
	    'name' => [
		'type' => 'VARCHAR',
		'constraint' => '100',
		"null" => FALSE,
		'unique' => TRUE,
	    ],
	    "created_at timestamp(0) without time zone NOT NULL DEFAULT ('now'::text)::timestamp(0) without time zone",
	    'updated_at' => [
		'type' => 'timestamp(0) without time zone',
		"null" => true,
	    ],
	]);
	$this->dbforge->add_key('id', TRUE);
	$this->dbforge->create_table('hostel_type');
	$this->db->query("comment on table {$schema}hostel_type is 'Type of hostels, Boys or Girls or Boys and Girls etc'");

	$this->dbforge->add_field([
	    'id' => [
		'type' => 'smallint',
		'auto_increment' => TRUE,
	    ],
	    'name' => [
		'type' => 'VARCHAR',
		'constraint' => '200',
		"null" => FALSE,
		'unique' => TRUE,
	    ],
	    'hostel_type_id' => [
		'type' => 'smallint',
		"null" => FALSE,
	    ],
	    'address' => [
		'type' => 'text',
	    ],
	    'note' => [
		'type' => 'text',
	    ],
	    "created_at timestamp(0) without time zone NOT NULL DEFAULT ('now'::text)::timestamp(0) without time zone",
	    'updated_at' => [
		'type' => 'timestamp(0) without time zone',
		"null" => true,
	    ],
	    "FOREIGN KEY (hostel_type_id) REFERENCES {$schema}hostel_type(id) on update restrict on delete restrict",
	]);
	$this->dbforge->add_key('id', TRUE);
	$this->dbforge->create_table('hostel');


	$this->dbforge->add_field([
	    'id' => [
		'type' => 'bigint',
		'auto_increment' => TRUE,
	    ],
	    'firstname' => [
		'type' => 'VARCHAR',
		'constraint' => '30',
		"null" => FALSE,
	    ],
	    'othernames' => [
		'type' => 'VARCHAR',
		'constraint' => '30',
		"null" => FALSE,
	    ],
	    'dob' => [
		'type' => 'date',
		"null" => FALSE,
	    ],
	    'gender' => [
		'type' => 'char(1)',
		"null" => FALSE,
	    ],
	    'phone' => [
		'type' => 'varchar',
		'constraint' => '30',
	    ],
	    'address' => [
		'type' => 'text',
	    ],
	    'username' => [
		'type' => 'VARCHAR',
		'constraint' => '30',
		'unique' => TRUE,
	    ],
	    'password' => [
		'type' => 'VARCHAR',
		'constraint' => '300',
	    ],
	    'roll' => [
		'type' => 'VARCHAR',
		'constraint' => '30',
		"null" => FALSE,
	    ],
	    'photo' => [
		'type' => 'VARCHAR',
		'constraint' => '30',
		"null" => FALSE,
	    ],
	    'status' => [
		'type' => 'char',
		'constraint' => '1',
		"null" => FALSE,
		"default" => '1',
	    ],
	    'section_id' => [
		'type' => 'smallint',
		"null" => FALSE,
	    ],
	    'user_type_id' => [
		'type' => 'smallint',
		"null" => FALSE,
		'default' => 3,
	    ],
	    'hostel_id' => [
		'type' => 'smallint',
	    ],
	    "created_at timestamp(0) without time zone NOT NULL DEFAULT ('now'::text)::timestamp(0) without time zone",
	    'updated_at' => [
		'type' => 'timestamp(0) without time zone',
		"null" => true,
	    ],
	    "FOREIGN KEY (section_id) REFERENCES {$schema}section(id) on update restrict on delete restrict",
	    "FOREIGN KEY (user_type_id) REFERENCES {$schema}user_type(id) on update restrict on delete restrict",
	    "FOREIGN KEY (hostel_id) REFERENCES {$schema}hostel(id) on update restrict on delete restrict",
	]);
	$this->dbforge->add_key('id', TRUE);
	$this->dbforge->create_table('student');
	$this->db->query("comment on column {$schema}student.status is 'Indicate the status of the student whether is active in school of out of school'");

	$this->dbforge->add_field([
	    'id' => [
		'type' => 'bigint',
		'auto_increment' => TRUE,
	    ],
	    'student_id' => [
		'type' => 'bigint',
		"null" => FALSE,
	    ],
	    'transport_id' => [
		'type' => 'integer',
		"null" => FALSE,
	    ],
	    "created_at timestamp(0) without time zone NOT NULL DEFAULT ('now'::text)::timestamp(0) without time zone",
	    'updated_at' => [
		'type' => 'timestamp(0) without time zone',
		"null" => true,
	    ],
	    "FOREIGN KEY (student_id) REFERENCES {$schema}student(id) on update restrict on delete restrict",
	    "FOREIGN KEY (transport_id) REFERENCES {$schema}transport(id) on update restrict on delete restrict",
	]);
	$this->dbforge->add_key('id', TRUE);
	$this->dbforge->create_table('student_transport');

	$this->dbforge->add_field([
	    'id' => [
		'type' => 'bigint',
		'auto_increment' => TRUE,
	    ],
	    'parent_id' => [
		'type' => 'bigint',
		"null" => FALSE,
	    ],
	    'student_id' => [
		'type' => 'integer',
		"null" => FALSE,
	    ],
	    "created_at timestamp(0) without time zone NOT NULL DEFAULT ('now'::text)::timestamp(0) without time zone",
	    'updated_at' => [
		'type' => 'timestamp(0) without time zone',
		"null" => true,
	    ],
	    "FOREIGN KEY (parent_id) REFERENCES {$schema}parent(id) on update restrict on delete restrict",
	    "FOREIGN KEY (student_id) REFERENCES {$schema}student(id) on update restrict on delete restrict",
	]);
	$this->dbforge->add_key('id', TRUE);
	$this->dbforge->create_table('parent_student');

	$this->dbforge->add_field([
	    'id' => [
		'type' => 'smallint',
		'auto_increment' => TRUE,
	    ],
	    'name' => [
		'type' => 'VARCHAR',
		'constraint' => '100',
		"null" => FALSE,
		'unique' => TRUE,
	    ],
	    'class_level_id' => [
		'type' => 'smallint',
		"null" => FALSE,
	    ],
	    "created_at timestamp(0) without time zone NOT NULL DEFAULT ('now'::text)::timestamp(0) without time zone",
	    'updated_at' => [
		'type' => 'timestamp(0) without time zone',
		"null" => true,
	    ],
	    "FOREIGN KEY (class_level_id) REFERENCES {$schema}class_level(id) on update restrict on delete restrict",
	]);
	$this->dbforge->add_key('id', TRUE);
	$this->dbforge->create_table('accademic_year');

	$this->dbforge->add_field([
	    'id' => [
		'type' => 'bigint',
		'auto_increment' => TRUE,
	    ],
	    'user_id' => [
		'type' => 'bigint',
		"null" => FALSE,
	    ],
	    'accademic_year_id' => [
		'type' => 'smallint',
		"null" => FALSE,
	    ],
	    "created_at timestamp(0) without time zone NOT NULL DEFAULT ('now'::text)::timestamp(0) without time zone",
	    'updated_at' => [
		'type' => 'timestamp(0) without time zone',
		"null" => true,
	    ],
	    "FOREIGN KEY (user_id) REFERENCES {$schema}user(id) on update restrict on delete restrict",
	    "FOREIGN KEY (accademic_year_id) REFERENCES {$schema}accademic_year(id) on update restrict on delete restrict",
	]);
	$this->dbforge->add_key('id', TRUE);
	$this->dbforge->create_table('teacher_attendance');

	$this->dbforge->add_field([
	    'id' => [
		'type' => 'smallint',
		'auto_increment' => TRUE,
	    ],
	    'name' => [
		'type' => 'VARCHAR',
		'constraint' => '100',
		"null" => FALSE,
		'unique' => TRUE,
	    ],
	    'class_level_id' => [
		'type' => 'smallint',
		"null" => FALSE,
	    ],
	    "created_at timestamp(0) without time zone NOT NULL DEFAULT ('now'::text)::timestamp(0) without time zone",
	    'updated_at' => [
		'type' => 'timestamp(0) without time zone',
		"null" => true,
	    ],
	    "FOREIGN KEY (class_level_id) REFERENCES {$schema}class_level(id) on update restrict on delete restrict",
	]);
	$this->dbforge->add_key('id', TRUE);
	$this->dbforge->create_table('semister');

	$this->dbforge->add_field([
	    'id' => [
		'type' => 'smallint',
		'auto_increment' => TRUE,
	    ],
	    'name' => [
		'type' => 'VARCHAR',
		'constraint' => '100',
		"null" => FALSE,
		'unique' => TRUE,
	    ],
	    'note' => [
		'type' => 'text',
	    ],
	    'semister_id' => [
		'type' => 'smallint',
		"null" => FALSE,
	    ],
	    "created_at timestamp(0) without time zone NOT NULL DEFAULT ('now'::text)::timestamp(0) without time zone",
	    'updated_at' => [
		'type' => 'timestamp(0) without time zone',
		"null" => true,
	    ],
	    "FOREIGN KEY (semister_id) REFERENCES {$schema}semister(id) on update restrict on delete restrict",
	]);
	$this->dbforge->add_key('id', TRUE);
	$this->dbforge->create_table('exam');

	$this->dbforge->add_field([
	    'id' => [
		'type' => 'smallint',
		'auto_increment' => TRUE,
	    ],
	    'name' => [
		'type' => 'VARCHAR',
		'constraint' => '100',
		"null" => FALSE,
		'unique' => TRUE,
	    ],
	    'code' => [
		'type' => 'VARCHAR',
		'constraint' => '20',
		'unique' => TRUE,
	    ],
	    'author' => [
		'type' => 'VARCHAR',
		'constraint' => '150',
	    ],
	    'status' => [
		'type' => 'char',
		'constraint' => '1',
		"null" => FALSE,
		'default' => '1',
	    ],
	    'class_id' => [
		'type' => 'smallint',
	    ],
	    "created_at timestamp(0) without time zone NOT NULL DEFAULT ('now'::text)::timestamp(0) without time zone",
	    'updated_at' => [
		'type' => 'timestamp(0) without time zone',
		"null" => true,
	    ],
	    "FOREIGN KEY (class_id) REFERENCES {$schema}class(id) on update restrict on delete restrict",
	]);
	$this->dbforge->add_key('id', TRUE);
	$this->dbforge->create_table('subject');
	$this->db->query("comment on column {$schema}subject.status is 'Indicate the status of the subject 1 - Mandatory / 0 - Optional'");

	$this->dbforge->add_field([
	    'id' => [
		'type' => 'bigint',
		'auto_increment' => TRUE,
	    ],
	    'section_id' => [
		'type' => 'smallint',
	    ],
	    'subject_id' => [
		'type' => 'smallint',
	    ],
	    "created_at timestamp(0) without time zone NOT NULL DEFAULT ('now'::text)::timestamp(0) without time zone",
	    'updated_at' => [
		'type' => 'timestamp(0) without time zone',
		"null" => true,
	    ],
	    "FOREIGN KEY (section_id) REFERENCES {$schema}section(id) on update restrict on delete restrict",
	    "FOREIGN KEY (subject_id) REFERENCES {$schema}subject(id) on update restrict on delete restrict",
	]);
	$this->dbforge->add_key('id', TRUE);
	$this->dbforge->create_table('subject_section');
	$this->db->query("comment on table {$schema}subject_section is 'Store the number of compulsory subjects in a certain section '");

	$this->dbforge->add_field([
	    'id' => [
		'type' => 'bigint',
		'auto_increment' => TRUE,
	    ],
	    'subject_id' => [
		'type' => 'smallint',
	    ],
	    'user_id' => [
		'type' => 'smallint',
	    ],
	    "created_at timestamp(0) without time zone NOT NULL DEFAULT ('now'::text)::timestamp(0) without time zone",
	    'updated_at' => [
		'type' => 'timestamp(0) without time zone',
		"null" => true,
	    ],
	    "FOREIGN KEY (subject_id) REFERENCES {$schema}subject(id) on update restrict on delete restrict",
	    "FOREIGN KEY (user_id) REFERENCES {$schema}user(id) on update restrict on delete restrict",
	]);
	$this->dbforge->add_key('id', TRUE);
	$this->dbforge->create_table('subject_user');
	$this->db->query("comment on table {$schema}subject_user is ' Store the teacher who is responsible for /teaching the certain subject '");

	$this->dbforge->add_field([
	    'id' => [
		'type' => 'bigint',
		'auto_increment' => TRUE,
	    ],
	    'name' => [
		'type' => 'VARCHAR',
		'constraint' => '200',
	    ],
	    'exam_date' => [
		'type' => 'date',
		"null" => FALSE,
	    ],
	    'start_time' => [
		'type' => 'timestamp(0) without time zone',
		"null" => FALSE,
	    ],
	    'finish_time' => [
		'type' => 'timestamp(0) without time zone',
		"null" => FALSE,
	    ],
	    'room' => [
		'type' => 'VARCHAR',
		'constraint' => '200',
		"null" => FALSE,
	    ],
	    'exam_id' => [
		'type' => 'smallint',
		"null" => FALSE,
	    ],
	    'section_id' => [
		'type' => 'smallint',
		"null" => FALSE,
	    ],
	    'subject_id' => [
		'type' => 'smallint',
		"null" => FALSE,
	    ],
	    'accademic_year_id' => [
		'type' => 'smallint',
		"null" => FALSE,
	    ],
	    "created_at timestamp(0) without time zone NOT NULL DEFAULT ('now'::text)::timestamp(0) without time zone",
	    'updated_at' => [
		'type' => 'timestamp(0) without time zone',
		"null" => true,
	    ],
	    "FOREIGN KEY (exam_id) REFERENCES {$schema}exam(id) on update restrict on delete restrict",
	    "FOREIGN KEY (section_id) REFERENCES {$schema}section(id) on update restrict on delete restrict",
	    "FOREIGN KEY (subject_id) REFERENCES {$schema}subject(id) on update restrict on delete restrict",
	    "FOREIGN KEY (accademic_year_id) REFERENCES {$schema}accademic_year(id) on update restrict on delete restrict",
	]);
	$this->dbforge->add_key('id', TRUE);
	$this->dbforge->create_table('exam_schedule');

	$this->dbforge->add_field([
	    'id' => [
		'type' => 'bigint',
		'auto_increment' => TRUE,
	    ],
	    'note' => [
		'type' => 'text',
	    ],
	    'student_id' => [
		'type' => 'bigint',
		"null" => FALSE,
	    ],
	    'exam_schedule_id' => [
		'type' => 'bigint',
		"null" => FALSE,
	    ],
	    'accademic_year_id' => [
		'type' => 'smallint',
		"null" => FALSE,
	    ],
	    "created_at timestamp(0) without time zone NOT NULL DEFAULT ('now'::text)::timestamp(0) without time zone",
	    'updated_at' => [
		'type' => 'timestamp(0) without time zone',
		"null" => true,
	    ],
	    "FOREIGN KEY (student_id) REFERENCES {$schema}student(id) on update restrict on delete restrict",
	    "FOREIGN KEY (exam_schedule_id) REFERENCES {$schema}exam_schedule(id) on update restrict on delete restrict",
	    "FOREIGN KEY (accademic_year_id) REFERENCES {$schema}accademic_year(id) on update restrict on delete restrict",
	]);
	$this->dbforge->add_key('id', TRUE);
	$this->dbforge->create_table('exam_attendance');
	$this->db->query("comment on table {$schema}exam_attendance is ' Store the attendance of student in the scheduled exam '");

	$this->dbforge->add_field([
	    'id' => [
		'type' => 'bigint',
		'auto_increment' => TRUE,
	    ],
	    'subject_id' => [
		'type' => 'smallint',
	    ],
	    'class_id' => [
		'type' => 'smallint',
	    ],
	    "created_at timestamp(0) without time zone NOT NULL DEFAULT ('now'::text)::timestamp(0) without time zone",
	    'updated_at' => [
		'type' => 'timestamp(0) without time zone',
		"null" => true,
	    ],
	    "FOREIGN KEY (subject_id) REFERENCES {$schema}subject(id) on update restrict on delete restrict",
	    "FOREIGN KEY (class_id) REFERENCES {$schema}class(id) on update restrict on delete restrict",
	]);
	$this->dbforge->add_key('id', TRUE);
	$this->dbforge->create_table('subject_class');
	$this->db->query("comment on table {$schema}subject_class is ' Store all compulsory subjects in a certain class '");

	$this->dbforge->add_field([
	    'id' => [
		'type' => 'bigint',
		'auto_increment' => TRUE,
	    ],
	    'subject_id' => [
		'type' => 'smallint',
	    ],
	    'student_id' => [
		'type' => 'bigint',
	    ],
	    "created_at timestamp(0) without time zone NOT NULL DEFAULT ('now'::text)::timestamp(0) without time zone",
	    'updated_at' => [
		'type' => 'timestamp(0) without time zone',
		"null" => true,
	    ],
	    "FOREIGN KEY (subject_id) REFERENCES {$schema}subject(id) on update restrict on delete restrict",
	    "FOREIGN KEY (student_id) REFERENCES {$schema}student(id) on update restrict on delete restrict",
	]);
	$this->dbforge->add_key('id', TRUE);
	$this->dbforge->create_table('subject_student');
	$this->db->query("comment on table {$schema}subject_student is ' Store all the optional subjects which a student take '");

	$this->dbforge->add_field([
	    'id' => [
		'type' => 'smallint',
		'auto_increment' => TRUE,
	    ],
	    'day' => [
		'type' => 'VARCHAR',
		'constraint' => '200',
		"null" => FALSE,
		'unique' => TRUE,
	    ],
	    'room' => [
		'type' => 'VARCHAR',
		'constraint' => '200',
		"null" => FALSE,
	    ],
	    'start_time' => [
		'type' => 'date',
		"null" => FALSE,
	    ],
	    'end_time' => [
		'type' => 'date',
		"null" => FALSE,
	    ],
	    'section_id' => [
		'type' => 'smallint',
		"null" => FALSE,
	    ],
	    'subject_id' => [
		'type' => 'smallint',
		"null" => FALSE,
	    ],
	    'accademic_year_id' => [
		'type' => 'smallint',
		"null" => FALSE,
	    ],
	    "created_at timestamp(0) without time zone NOT NULL DEFAULT ('now'::text)::timestamp(0) without time zone",
	    'updated_at' => [
		'type' => 'timestamp(0) without time zone',
		"null" => true,
	    ],
	    "FOREIGN KEY (section_id) REFERENCES {$schema}section(id) on update restrict on delete restrict",
	    "FOREIGN KEY (subject_id) REFERENCES {$schema}subject(id) on update restrict on delete restrict",
	    "FOREIGN KEY (accademic_year_id) REFERENCES {$schema}accademic_year(id) on update restrict on delete restrict",
	]);
	$this->dbforge->add_key('id', TRUE);
	$this->dbforge->create_table('routine');

	$this->dbforge->add_field([
	    'id' => [
		'type' => 'bigint',
		'auto_increment' => TRUE,
	    ],
	    'class_id' => [
		'type' => 'smallint',
	    ],
	    'student_id' => [
		'type' => 'bigint',
	    ],
	    'user_id' => [
		'type' => 'bigint',
	    ],
	    "created_at timestamp(0) without time zone NOT NULL DEFAULT ('now'::text)::timestamp(0) without time zone",
	    'updated_at' => [
		'type' => 'timestamp(0) without time zone',
		"null" => true,
	    ],
	    "FOREIGN KEY (student_id) REFERENCES {$schema}student(id) on update restrict on delete restrict",
	    "FOREIGN KEY (user_id) REFERENCES {$schema}user(id) on update restrict on delete restrict",
	    "FOREIGN KEY (class_id) REFERENCES {$schema}class(id) on update restrict on delete restrict",
	]);
	$this->dbforge->add_key('id', TRUE);
	$this->dbforge->create_table('library_member');

	$this->dbforge->add_field([
	    'id' => [
		'type' => 'bigint',
		'auto_increment' => TRUE,
	    ],
	    'name' => [
		'type' => 'VARCHAR',
		'constraint' => '100',
		"null" => FALSE,
		'unique' => TRUE,
	    ],
	    'author' => [
		'type' => 'VARCHAR',
		'constraint' => '70',
	    ],
	    'quantity' => [
		'type' => 'int',
		"null" => FALSE,
	    ],
	    'books_taken' => [
		'type' => 'int',
	    ],
	    'rack' => [
		'type' => 'VARCHAR',
		'constraint' => '10',
	    ],
	    'edition' => [
		'type' => 'VARCHAR',
		'constraint' => '15',
	    ],
	    'book_for' => [
		'type' => 'char',
		'constraint' => '1',
		'default' => '1',
	    ],
	    'subject_id' => [
		'type' => 'smallint',
	    ],
	    "created_at timestamp(0) without time zone NOT NULL DEFAULT ('now'::text)::timestamp(0) without time zone",
	    'updated_at' => [
		'type' => 'timestamp(0) without time zone',
		"null" => true,
	    ],
	    "FOREIGN KEY (subject_id) REFERENCES {$schema}subject(id) on update restrict on delete restrict",
	]);
	$this->dbforge->add_key('id', TRUE);
	$this->dbforge->create_table('book');
	$this->db->query("comment on column {$schema}book.book_for is ' Store where the book is for student/teacher, 1 - Student 2 - Teacher '");

	$this->dbforge->add_field([
	    'id' => [
		'type' => 'bigint',
		'auto_increment' => TRUE,
	    ],
	    'note' => [
		'type' => 'text',
	    ],
	    'fine' => [
		'type' => 'numeric(6,2)',
	    ],
	    'issue_date' => [
		'type' => 'date',
		"null" => FALSE,
	    ],
	    'due_date' => [
		'type' => 'date',
		"null" => FALSE,
	    ],
	    'return_date' => [
		'type' => 'date',
	    ],
	    'student_id' => [
		'type' => 'bigint',
	    ],
	    'user_id' => [
		'type' => 'bigint',
	    ],
	    'book_id' => [
		'type' => 'bigint',
		"null" => FALSE,
	    ],
	    "created_at timestamp(0) without time zone NOT NULL DEFAULT ('now'::text)::timestamp(0) without time zone",
	    'updated_at' => [
		'type' => 'timestamp(0) without time zone',
		"null" => true,
	    ],
	    "FOREIGN KEY (student_id) REFERENCES {$schema}student(id) on update restrict on delete restrict",
	    "FOREIGN KEY (user_id) REFERENCES {$schema}user(id) on update restrict on delete restrict",
	    "FOREIGN KEY (book_id) REFERENCES {$schema}book(id) on update restrict on delete restrict",
	]);
	$this->dbforge->add_key('id', TRUE);
	$this->dbforge->create_table('issue');


	$this->dbforge->add_field([
	    'id' => [
		'type' => 'smallint',
		'auto_increment' => TRUE,
	    ],
	    'name' => [
		'type' => 'VARCHAR',
		'constraint' => '100',
		"null" => FALSE,
		'unique' => TRUE,
	    ],
	    'plate_number' => [
		'type' => 'VARCHAR',
		'constraint' => '20',
	    ],
	    'mark' => [
		'type' => 'VARCHAR',
		'constraint' => '100',
	    ],
	    "created_at timestamp(0) without time zone NOT NULL DEFAULT ('now'::text)::timestamp(0) without time zone",
	    'updated_at' => [
		'type' => 'timestamp(0) without time zone',
		"null" => true,
	    ],
	]);
	$this->dbforge->add_key('id', TRUE);
	$this->dbforge->create_table('vehicle');

	$this->dbforge->add_field([
	    'id' => [
		'type' => 'smallint',
		'auto_increment' => TRUE,
	    ],
	    'transport_id' => [
		'type' => 'smallint',
		"null" => FALSE,
	    ],
	    'vehicle_id' => [
		'type' => 'smallint',
		"null" => FALSE,
	    ],
	    "created_at timestamp(0) without time zone NOT NULL DEFAULT ('now'::text)::timestamp(0) without time zone",
	    'updated_at' => [
		'type' => 'timestamp(0) without time zone',
		"null" => true,
	    ],
	    "FOREIGN KEY (transport_id) REFERENCES {$schema}transport(id) on update restrict on delete restrict",
	    "FOREIGN KEY (vehicle_id) REFERENCES {$schema}vehicle(id) on update restrict on delete restrict",
	]);
	$this->dbforge->add_key('id', TRUE);
	$this->dbforge->create_table('transport_vehicle');

	$this->dbforge->add_field([
	    'id' => [
		'type' => 'smallint',
		'auto_increment' => TRUE,
	    ],
	    'name' => [
		'type' => 'VARCHAR',
		'constraint' => '100',
		"null" => FALSE,
		'unique' => TRUE,
	    ],
	    "created_at timestamp(0) without time zone NOT NULL DEFAULT ('now'::text)::timestamp(0) without time zone",
	    'updated_at' => [
		'type' => 'timestamp(0) without time zone',
		"null" => true,
	    ],
	]);
	$this->dbforge->add_key('id', TRUE);
	$this->dbforge->create_table('payment_type');
	$this->db->query("comment on table {$schema}payment_type is ' Type of payment issued example MPESA, TIGOPESA, BANK, etc '");

	$this->dbforge->add_field([
	    'id' => [
		'type' => 'smallint',
		'auto_increment' => TRUE,
	    ],
	    'name' => [
		'type' => 'VARCHAR',
		'constraint' => '100',
		"null" => FALSE,
		'unique' => TRUE,
	    ],
	    'amount' => [
		'type' => 'numeric(12, 2)',
		"null" => FALSE,
	    ],
	    'is_repetitive' => [
		'type' => 'char',
		'constraint' => '1',
		"null" => FALSE,
		'default' => '0',
	    ],
	    "created_at timestamp(0) without time zone NOT NULL DEFAULT ('now'::text)::timestamp(0) without time zone",
	    'updated_at' => [
		'type' => 'timestamp(0) without time zone',
		"null" => true,
	    ],
	]);
	$this->dbforge->add_key('id', TRUE);
	$this->dbforge->create_table('fee_type');
	$this->db->query("comment on column {$schema}fee_type.is_repetitive is ' set whether this amount is repetitive or not, 1 - YES, 0 - NO '");

	$this->dbforge->add_field([
	    'id' => [
		'type' => 'smallint',
		'auto_increment' => TRUE,
	    ],
	    'name' => [
		'type' => 'VARCHAR',
		'constraint' => '100',
		"null" => FALSE,
		'unique' => TRUE,
	    ],
	    'amount_orig' => [
		'type' => 'numeric(12, 2)',
		"null" => FALSE,
	    ],
	    'amount_new' => [
		'type' => 'numeric(12, 2)',
		"null" => FALSE,
	    ],
	    'start_date' => [
		'type' => 'date',
		"null" => FALSE,
	    ],
	    'end_date' => [
		'type' => 'date',
		"null" => FALSE,
	    ],
	    'fee_type_id' => [
		'type' => 'smallint',
		"null" => FALSE,
	    ],
	    "created_at timestamp(0) without time zone NOT NULL DEFAULT ('now'::text)::timestamp(0) without time zone",
	    'updated_at' => [
		'type' => 'timestamp(0) without time zone',
		"null" => true,
	    ],
	    "FOREIGN KEY (fee_type_id) REFERENCES {$schema}fee_type(id) on update restrict on delete restrict",
	]);
	$this->dbforge->add_key('id', TRUE);
	$this->dbforge->create_table('fee_type_audit');

	$this->dbforge->add_field([
	    'id' => [
		'type' => 'smallint',
		'auto_increment' => TRUE,
	    ],
	    'note' => [
		'type' => 'text',
	    ],
	    'start_date' => [
		'type' => 'date',
		"null" => FALSE,
	    ],
	    'end_date' => [
		'type' => 'date',
		"null" => FALSE,
	    ],
	    'fee_type_id' => [
		'type' => 'smallint',
		"null" => FALSE,
	    ],
	    'accademic_year_id' => [
		'type' => 'smallint',
		"null" => FALSE,
	    ],
	    "created_at timestamp(0) without time zone NOT NULL DEFAULT ('now'::text)::timestamp(0) without time zone",
	    'updated_at' => [
		'type' => 'timestamp(0) without time zone',
		"null" => true,
	    ],
	    "FOREIGN KEY (fee_type_id) REFERENCES {$schema}fee_type(id) on update restrict on delete restrict",
	    "FOREIGN KEY (accademic_year_id) REFERENCES {$schema}accademic_year(id) on update restrict on delete restrict",
	]);
	$this->dbforge->add_key('id', TRUE);
	$this->dbforge->create_table('accademic_fee_type');
	$this->db->query("comment on table {$schema}accademic_fee_type is ' show in each accademic year a specific fee type must be paid within a certain period plus other specific information'");

	$this->dbforge->add_field([
	    'id' => [
		'type' => 'bigint',
		'auto_increment' => TRUE,
	    ],
	    'amount' => [
		'type' => 'numeric(12,2)',
		"null" => FALSE,
	    ],
	    'number' => [
		'type' => 'varchar',
		'constraint' => '15',
		"null" => FALSE,
	    ],
	    'student_id' => [
		'type' => 'bigint',
		"null" => FALSE,
	    ],
	    'user_id' => [
		'type' => 'bigint',
		"null" => FALSE,
	    ],
	    'accademic_year_id' => [
		'type' => 'smallint',
		"null" => FALSE,
	    ],
	    "created_at timestamp(0) without time zone NOT NULL DEFAULT ('now'::text)::timestamp(0) without time zone",
	    'updated_at' => [
		'type' => 'timestamp(0) without time zone',
		"null" => true,
	    ],
	    "FOREIGN KEY (student_id) REFERENCES {$schema}student(id) on update restrict on delete restrict",
	    "FOREIGN KEY (user_id) REFERENCES {$schema}user(id) on update restrict on delete restrict",
	    "FOREIGN KEY (accademic_year_id) REFERENCES {$schema}accademic_year(id) on update restrict on delete restrict",
	]);
	$this->dbforge->add_key('id', TRUE);
	$this->dbforge->create_table('invoice');

	$this->dbforge->add_field([
	    'id' => [
		'type' => 'smallint',
		'auto_increment' => TRUE,
	    ],
	    'invoice_id' => [
		'type' => 'bigint',
		"null" => FALSE,
	    ],
	    'fee_type_id' => [
		'type' => 'smallint',
		"null" => FALSE,
	    ],
	    "created_at timestamp(0) without time zone NOT NULL DEFAULT ('now'::text)::timestamp(0) without time zone",
	    'updated_at' => [
		'type' => 'timestamp(0) without time zone',
		"null" => true,
	    ],
	    "FOREIGN KEY (invoice_id) REFERENCES {$schema}invoice(id) on update restrict on delete restrict",
	    "FOREIGN KEY (fee_type_id) REFERENCES {$schema}fee_type(id) on update restrict on delete restrict",
	]);
	$this->dbforge->add_key('id', TRUE);
	$this->dbforge->create_table('invoice_fee');

	$this->dbforge->add_field([
	    'id' => [
		'type' => 'smallint',
		'auto_increment' => TRUE,
	    ],
	    'amount' => [
		'type' => 'numeric(12,2)',
		"null" => FALSE,
	    ],
	    'receipt' => [
		'type' => 'varchar',
		'constraint' => '30',
		"null" => FALSE,
		'unique' => TRUE,
	    ],
	    'pay_date' => [
		'type' => 'date',
		"null" => FALSE,
	    ],
	    'invoice_id' => [
		'type' => 'bigint',
		"null" => FALSE,
	    ],
	    'payment_type_id' => [
		'type' => 'smallint',
		"null" => FALSE,
	    ],
	    "created_at timestamp(0) without time zone NOT NULL DEFAULT ('now'::text)::timestamp(0) without time zone",
	    'updated_at' => [
		'type' => 'timestamp(0) without time zone',
		"null" => true,
	    ],
	    "FOREIGN KEY (invoice_id) REFERENCES {$schema}invoice(id) on update restrict on delete restrict",
	    "FOREIGN KEY (payment_type_id) REFERENCES {$schema}payment_type(id) on update restrict on delete restrict",
	]);
	$this->dbforge->add_key('id', TRUE);
	$this->dbforge->create_table('payment');


	$this->dbforge->add_field([
	    'id' => [
		'type' => 'bigint',
		'auto_increment' => TRUE,
	    ],
	    'name' => [
		'type' => 'VARCHAR',
		'constraint' => '100',
		"null" => FALSE,
		'unique' => TRUE,
	    ],
	    'amount' => [
		'type' => 'numeric(12,2)',
		"null" => FALSE,
	    ],
	    'note' => [
		'type' => 'text',
	    ],
	    'expense_date' => [
		'type' => 'date',
		"null" => FALSE,
	    ],
	    'user_id' => [
		'type' => 'bigint',
		"null" => FALSE,
	    ],
	    'accademic_year_id' => [
		'type' => 'smallint',
		"null" => FALSE,
	    ],
	    "created_at timestamp(0) without time zone NOT NULL DEFAULT ('now'::text)::timestamp(0) without time zone",
	    'updated_at' => [
		'type' => 'timestamp(0) without time zone',
		"null" => true,
	    ],
	    "FOREIGN KEY (user_id) REFERENCES {$schema}user(id) on update restrict on delete restrict",
	    "FOREIGN KEY (accademic_year_id) REFERENCES {$schema}accademic_year(id) on update restrict on delete restrict",
	]);
	$this->dbforge->add_key('id', TRUE);
	$this->dbforge->create_table('expense');

	$this->dbforge->add_field([
	    'id' => [
		'type' => 'bigint',
		'auto_increment' => TRUE,
	    ],
	    'student_id' => [
		'type' => 'bigint',
		"null" => FALSE,
	    ],
	    'accademic_year_id' => [
		'type' => 'smallint',
		"null" => FALSE,
	    ],
	    'section_id' => [
		'type' => 'smallint',
		"null" => FALSE,
	    ],
	    "created_at timestamp(0) without time zone NOT NULL DEFAULT ('now'::text)::timestamp(0) without time zone",
	    'updated_at' => [
		'type' => 'timestamp(0) without time zone',
		"null" => true,
	    ],
	    "FOREIGN KEY (student_id) REFERENCES {$schema}student(id) on update restrict on delete restrict",
	    "FOREIGN KEY (accademic_year_id) REFERENCES {$schema}accademic_year(id) on update restrict on delete restrict",
	    "FOREIGN KEY (section_id) REFERENCES {$schema}section(id) on update restrict on delete restrict",
	]);
	$this->dbforge->add_key('id', TRUE);
	$this->dbforge->create_table('student_archive');


	$this->dbforge->add_field([
	    'id' => [
		'type' => 'smallint',
		'auto_increment' => TRUE,
	    ],
	    'pass_mark' => [
		'type' => 'int',
		"null" => FALSE,
	    ],
	    'subject_id' => [
		'type' => 'smallint',
		"null" => FALSE,
	    ],
	    "created_at timestamp(0) without time zone NOT NULL DEFAULT ('now'::text)::timestamp(0) without time zone",
	    'updated_at' => [
		'type' => 'timestamp(0) without time zone',
		"null" => true,
	    ],
	    "FOREIGN KEY (subject_id) REFERENCES {$schema}subject(id) on update restrict on delete restrict",
	]);
	$this->dbforge->add_key('id', TRUE);
	$this->dbforge->create_table('promotion_subject');

	$this->dbforge->add_field([
	    'id' => [
		'type' => 'smallint',
		'auto_increment' => TRUE,
	    ],
	    'name' => [
		'type' => 'VARCHAR',
		'constraint' => '100',
		"null" => FALSE,
		'unique' => TRUE,
	    ],
	    'folder_name' => [
		'type' => 'VARCHAR',
		'constraint' => '100',
	    ],
	    'user_id' => [
		'type' => 'bigint',
		"null" => FALSE,
	    ],
	    "created_at timestamp(0) without time zone NOT NULL DEFAULT ('now'::text)::timestamp(0) without time zone",
	    'updated_at' => [
		'type' => 'timestamp(0) without time zone',
		"null" => true,
	    ],
	    "FOREIGN KEY (user_id) REFERENCES {$schema}user(id) on update restrict on delete restrict",
	]);
	$this->dbforge->add_key('id', TRUE);
	$this->dbforge->create_table('media_category');

	$this->dbforge->add_field([
	    'id' => [
		'type' => 'bigint',
		'auto_increment' => TRUE,
	    ],
	    'file_name' => [
		'type' => 'VARCHAR',
		'constraint' => '100',
		"null" => FALSE,
		'unique' => TRUE,
	    ],
	    'file_name_display' => [
		'type' => 'VARCHAR',
		'constraint' => '100',
	    ],
	    'media_category_id' => [
		'type' => 'smallint',
		"null" => FALSE,
	    ],
	    'user_id' => [
		'type' => 'bigint',
		"null" => FALSE,
	    ],
	    "created_at timestamp(0) without time zone NOT NULL DEFAULT ('now'::text)::timestamp(0) without time zone",
	    'updated_at' => [
		'type' => 'timestamp(0) without time zone',
		"null" => true,
	    ],
	    "FOREIGN KEY (user_id) REFERENCES {$schema}user(id) on update restrict on delete restrict",
	    "FOREIGN KEY (media_category_id) REFERENCES {$schema}media_category(id) on update restrict on delete restrict",
	]);
	$this->dbforge->add_key('id', TRUE);
	$this->dbforge->create_table('media');

	$this->dbforge->add_field([
	    'id' => [
		'type' => 'bigint',
		'auto_increment' => TRUE,
	    ],
	    'is_public' => [
		'type' => 'char',
		'constraint' => '1',
		"null" => FALSE,
		'default' => '0',
	    ],
	    'type' => [
		'type' => 'char',
		'constraint' => '1',
		"null" => FALSE,
		'default' => '1',
	    ],
	    'media_id' => [
		'type' => 'bigint',
		"null" => FALSE,
	    ],
	    'class_id' => [
		'type' => 'bigint',
		"null" => FALSE,
	    ],
	    "created_at timestamp(0) without time zone NOT NULL DEFAULT ('now'::text)::timestamp(0) without time zone",
	    'updated_at' => [
		'type' => 'timestamp(0) without time zone',
		"null" => true,
	    ],
	    "FOREIGN KEY (class_id) REFERENCES {$schema}class(id) on update restrict on delete restrict",
	    "FOREIGN KEY (media_id) REFERENCES {$schema}media(id) on update restrict on delete restrict",
	]);
	$this->dbforge->add_key('id', TRUE);
	$this->dbforge->create_table('media_share');
	$this->db->query("comment on column {$schema}media_share.is_public is ' show whether this media is public or not 1 - YES, 0 - NO'");
	$this->db->query("comment on column {$schema}media_share.type is ' 1 - File, 2 - Folder '");

	$this->dbforge->add_field([
	    'id' => [
		'type' => 'bigint',
		'auto_increment' => TRUE,
	    ],
	    'class_id' => [
		'type' => 'smallint',
		"null" => FALSE,
	    ],
	    'user_id' => [
		'type' => 'bigint',
		"null" => FALSE,
	    ],
	    "created_at timestamp(0) without time zone NOT NULL DEFAULT ('now'::text)::timestamp(0) without time zone",
	    'updated_at' => [
		'type' => 'timestamp(0) without time zone',
		"null" => true,
	    ],
	    "FOREIGN KEY (class_id) REFERENCES {$schema}class(id) on update restrict on delete restrict",
	    "FOREIGN KEY (user_id) REFERENCES {$schema}user(id) on update restrict on delete restrict",
	]);
	$this->dbforge->add_key('id', TRUE);
	$this->dbforge->create_table('class_teacher');

	$this->dbforge->add_field([
	    'id' => [
		'type' => 'bigint',
		'auto_increment' => TRUE,
	    ],
	    'student_id' => [
		'type' => 'bigint',
		"null" => FALSE,
	    ],
	    'accademic_year_id' => [
		'type' => 'smallint',
		"null" => FALSE,
	    ],
	    "created_at timestamp(0) without time zone NOT NULL DEFAULT ('now'::text)::timestamp(0) without time zone",
	    'updated_at' => [
		'type' => 'timestamp(0) without time zone',
		"null" => true,
	    ],
	    "FOREIGN KEY (student_id) REFERENCES {$schema}student(id) on update restrict on delete restrict",
	    "FOREIGN KEY (accademic_year_id) REFERENCES {$schema}accademic_year(id) on update restrict on delete restrict",
	]);
	$this->dbforge->add_key('id', TRUE);
	$this->dbforge->create_table('class_attendance');

	$this->dbforge->add_field([
	    'id' => [
		'type' => 'bigint',
		'auto_increment' => TRUE,
	    ],
	    'mark' => [
		'type' => 'int',
		"null" => FALSE,
	    ],
	    'subject_id' => [
		'type' => 'smallint',
		"null" => FALSE,
	    ],
	    'exam_id' => [
		'type' => 'smallint',
		"null" => FALSE,
	    ],
	    'student_id' => [
		'type' => 'bigint',
		"null" => FALSE,
	    ],
	    'accademic_year_id' => [
		'type' => 'smallint',
		"null" => FALSE,
	    ],
	    "created_at timestamp(0) without time zone NOT NULL DEFAULT ('now'::text)::timestamp(0) without time zone",
	    'updated_at' => [
		'type' => 'timestamp(0) without time zone',
		"null" => true,
	    ],
	    "FOREIGN KEY (student_id) REFERENCES {$schema}student(id) on update restrict on delete restrict",
	    "FOREIGN KEY (accademic_year_id) REFERENCES {$schema}accademic_year(id) on update restrict on delete restrict",
	    "FOREIGN KEY (exam_id) REFERENCES {$schema}exam(id) on update restrict on delete restrict",
	    "FOREIGN KEY (subject_id) REFERENCES {$schema}subject(id) on update restrict on delete restrict",
	]);
	$this->dbforge->add_key('id', TRUE);
	$this->dbforge->create_table('mark');


	$this->dbforge->add_field([
	    'id' => [
		'type' => 'bigint',
		'auto_increment' => TRUE,
	    ],
	    'title' => [
		'type' => 'varchar',
		'constraint' => '30',
		"null" => FALSE,
	    ],
	    'notice' => [
		'type' => 'text',
	    ],
	    'start_date' => [
		'type' => 'date',
		"null" => FALSE,
	    ],
	    'end_date' => [
		'type' => 'date',
		"null" => FALSE,
	    ],
	    'user_id' => [
		'type' => 'bigint',
		"null" => FALSE,
	    ],
	    'accademic_year_id' => [
		'type' => 'smallint',
		"null" => FALSE,
	    ],
	    "created_at timestamp(0) without time zone NOT NULL DEFAULT ('now'::text)::timestamp(0) without time zone",
	    'updated_at' => [
		'type' => 'timestamp(0) without time zone',
		"null" => true,
	    ],
	    "FOREIGN KEY (user_id) REFERENCES {$schema}user(id) on update restrict on delete restrict",
	    "FOREIGN KEY (accademic_year_id) REFERENCES {$schema}accademic_year(id) on update restrict on delete restrict",
	]);
	$this->dbforge->add_key('id', TRUE);
	$this->dbforge->create_table('alert');

	$this->dbforge->add_field([
	    'id' => [
		'type' => 'smallint',
		'auto_increment' => TRUE,
	    ],
	    'name' => [
		'type' => 'varchar',
		'constraint' => '30',
		"null" => FALSE,
		"unique" => TRUE,
	    ],
	    'point' => [
		'type' => 'varchar',
		'constraint' => '10',
	    ],
	    'from' => [
		'type' => 'int',
		"null" => FALSE,
	    ],
	    'to' => [
		'type' => 'int',
		"null" => FALSE,
	    ],
	    'note' => [
		'type' => 'text',
	    ],
	    'overall_note' => [
		'type' => 'text',
	    ],
	    'class_level_id' => [
		'type' => 'smallint',
		"null" => FALSE,
	    ],
	    "created_at timestamp(0) without time zone NOT NULL DEFAULT ('now'::text)::timestamp(0) without time zone",
	    'updated_at' => [
		'type' => 'timestamp(0) without time zone',
		"null" => true,
	    ],
	    "FOREIGN KEY (class_level_id) REFERENCES {$schema}class_level(id) on update restrict on delete restrict",
	]);
	$this->dbforge->add_key('id', TRUE);
	$this->dbforge->create_table('grade');


	$this->dbforge->add_field([
	    'id' => [
		'type' => 'smallint',
		'auto_increment' => TRUE,
	    ],
	    'sname' => [
		'type' => 'varchar',
		'constraint' => '30',
		"null" => FALSE,
		"unique" => TRUE,
	    ],
	    'name' => [
		'type' => 'varchar',
		'constraint' => '30',
	    ],
	    'phone' => [
		'type' => 'varchar',
		'constraint' => '30',
	    ],
	    'address' => [
		'type' => 'text',
	    ],
	    'email' => [
		'type' => 'varchar',
		'constraint' => '30',
	    ],
	    'automation' => [
		'type' => 'varchar',
		'constraint' => '30',
	    ],
	    'currency_code' => [
		'type' => 'varchar',
		'constraint' => '5',
	    ],
	    'currency_symbol' => [
		'type' => 'varchar',
		'constraint' => '5',
	    ],
	    'footer' => [
		'type' => 'varchar',
		'constraint' => '5',
	    ],
	    'photo' => [
		'type' => 'varchar',
		'constraint' => '50',
	    ],
	    'purchase_code' => [
		'type' => 'varchar',
		'constraint' => '5',
	    ],
	    'api_key' => [
		'type' => 'varchar',
		'constraint' => '50',
	    ],
	    'api_secret' => [
		'type' => 'varchar',
		'constraint' => '30',
	    ],
	]);
	$this->dbforge->add_key('id', TRUE);
	$this->dbforge->create_table('setting');

	$this->dbforge->add_field([
	    'id' => [
		'type' => 'bigint',
		'auto_increment' => TRUE,
	    ],
	    'subject' => [
		'type' => 'varchar',
		'constraint' => '150',
		"null" => FALSE,
	    ],
	    'message' => [
		'type' => 'text',
		"null" => FALSE,
	    ],
	    'attach' => [
		'type' => 'varchar',
		'constraint' => '150',
	    ],
	    'attach_file_name' => [
		'type' => 'varchar',
		'constraint' => '200',
	    ],
	    'sender_id' => [
		'type' => 'smallint',
		"null" => FALSE,
	    ],
	    'receiver_id' => [
		'type' => 'smallint',
		"null" => FALSE,
	    ],
	    "created_at timestamp(0) without time zone NOT NULL DEFAULT ('now'::text)::timestamp(0) without time zone",
	    'updated_at' => [
		'type' => 'timestamp(0) without time zone',
		"null" => true,
	    ],
	    "FOREIGN KEY (sender_id) REFERENCES {$schema}user(id) on update restrict on delete restrict",
	    "FOREIGN KEY (receiver_id) REFERENCES {$schema}user(id) on update restrict on delete restrict",
	]);
	$this->dbforge->add_key('id', TRUE);
	$this->dbforge->create_table('message');


	$this->dbforge->add_field([
	    'id' => [
		'type' => 'bigint',
		'auto_increment' => TRUE,
	    ],
	    'body' => [
		'type' => 'text',
		"null" => FALSE,
	    ],
	    'status' => [
		'type' => 'char',
		'constraint' => '1',
		"null" => FALSE,
		'default' => '0',
	    ],
	    'return_code' => [
		'type' => 'char',
		'constraint' => '5',
	    ],
	    'user_id' => [
		'type' => 'bigint',
		"null" => FALSE,
	    ],
	    'user_type_id' => [
		'type' => 'smallint',
		"null" => FALSE,
	    ],
	    "created_at timestamp(0) without time zone NOT NULL DEFAULT ('now'::text)::timestamp(0) without time zone",
	    'updated_at' => [
		'type' => 'timestamp(0) without time zone',
		"null" => true,
	    ],
	    "FOREIGN KEY (user_id) REFERENCES {$schema}user(id) on update restrict on delete restrict",
	    "FOREIGN KEY (user_type_id) REFERENCES {$schema}user_type(id) on update restrict on delete restrict",
	]);
	$this->dbforge->add_key('id', TRUE);
	$this->dbforge->create_table('sms');

	$this->dbforge->add_field([
	    'id' => [
		'type' => 'bigint',
		'auto_increment' => TRUE,
	    ],
	    'ip_address' => [
		'type' => 'varchar',
		'constraint' => '30',
		"null" => FALSE,
	    ],
	    'user_agent' => [
		'type' => 'varchar',
		'constraint' => '30',
		"null" => FALSE,
	    ],
	    'last_activity' => [
		'type' => 'char',
		'constraint' => '5',
	    ],
	    'user_date' => [
		'type' => 'date',
		"null" => FALSE,
	    ],
	    "created_at timestamp(0) without time zone NOT NULL DEFAULT ('now'::text)::timestamp(0) without time zone",
	]);
	$this->dbforge->add_key('id', TRUE);
	$this->dbforge->create_table('school_session');


	$this->dbforge->add_field([
	    'id' => [
		'type' => 'smallint',
		'auto_increment' => TRUE,
	    ],
	    'types' => [
		'type' => 'varchar',
		'constraint' => '300',
		"null" => FALSE,
	    ],
	    'fields_names' => [
		'type' => 'varchar',
		'constraint' => '300',
		"null" => FALSE,
	    ],
	    'field_values' => [
		'type' => 'text',
	    ],
	    'extra' => [
		'type' => 'text',
	    ],
	    "created_at timestamp(0) without time zone NOT NULL DEFAULT ('now'::text)::timestamp(0) without time zone",
	]);
	$this->dbforge->add_key('id', TRUE);
	$this->dbforge->create_table('sms_setting');

	$this->dbforge->add_field([
	    'id' => [
		'type' => 'smallint',
		'auto_increment' => TRUE,
	    ],
	    'key_id' => [
		'type' => 'varchar',
		'constraint' => '300',
		"null" => FALSE,
	    ],
	    'email' => [
		'type' => 'varchar',
		'constraint' => '30',
		"null" => FALSE,
	    ],
	    "created_at timestamp(0) without time zone NOT NULL DEFAULT ('now'::text)::timestamp(0) without time zone",
	]);
	$this->dbforge->add_key('id', TRUE);
	$this->dbforge->create_table('reset');

	$this->dbforge->add_field([
	    'id' => [
		'type' => 'smallint',
		'auto_increment' => TRUE,
	    ],
	    'body' => [
		'type' => 'text',
		"null" => FALSE,
	    ],
	    'status' => [
		'type' => 'char',
		'constraint' => '1',
		'default' => '0',
	    ],
	    'message_id' => [
		'type' => 'smallint',
		"null" => FALSE,
	    ],
	    "created_at timestamp(0) without time zone NOT NULL DEFAULT ('now'::text)::timestamp(0) without time zone",
	    "FOREIGN KEY (message_id) REFERENCES {$schema}message(id) on update restrict on delete restrict",
	]);
	$this->dbforge->add_key('id', TRUE);
	$this->dbforge->create_table('reply_message');

	$this->dbforge->add_field([
	    'id' => [
		'type' => 'smallint',
		'auto_increment' => TRUE,
	    ],
	    'type' => [
		'type' => 'varchar',
		'constraint' => '100',
		"null" => FALSE,
	    ],
	    'config_keyvalue' => [
		'type' => 'varchar',
		'constraint' => '100',
		"null" => FALSE,
	    ],
	    "created_at timestamp(0) without time zone NOT NULL DEFAULT ('now'::text)::timestamp(0) without time zone",
	]);
	$this->dbforge->add_key('id', TRUE);
	$this->dbforge->create_table('ini_config');

	$this->dbforge->add_field([
	    'id' => [
		'type' => 'smallint',
		'auto_increment' => TRUE,
	    ],
	    'name' => [
		'type' => 'varchar',
		'constraint' => '30',
		"null" => FALSE,
		'unique' => true,
	    ],
	    "created_at timestamp(0) without time zone NOT NULL DEFAULT ('now'::text)::timestamp(0) without time zone",
	]);
	$this->dbforge->add_key('id', TRUE);
	$this->dbforge->create_table('permission_group');

	$this->dbforge->add_field([
	    'id' => [
		'type' => 'smallint',
		'auto_increment' => TRUE,
	    ],
	    'name' => [
		'type' => 'varchar',
		'constraint' => '30',
		"null" => FALSE,
		'unique' => true,
	    ],
	    'display_name' => [
		'type' => 'varchar',
		'constraint' => '30',
		"null" => FALSE,
		'unique' => true,
	    ],
	    'description' => [
		'type' => 'text',
	    ],
	    'is_super' => [
		'type' => 'char',
		'constraint' => '1',
		'default' => '0',
		'null' => false,
	    ],
	    'permission_group_id' => [
		'type' => 'smallint',
		"null" => FALSE,
	    ],
	    "created_at timestamp(0) without time zone NOT NULL DEFAULT ('now'::text)::timestamp(0) without time zone",
	    "FOREIGN KEY (permission_group_id) REFERENCES {$schema}permission_group(id) on update restrict on delete restrict",
	]);
	$this->dbforge->add_key('id', TRUE);
	$this->dbforge->create_table('permission');


	$this->dbforge->add_field([
	    'id' => [
		'type' => 'smallint',
		'auto_increment' => TRUE,
	    ],
	    'name' => [
		'type' => 'varchar',
		'constraint' => '30',
		"null" => FALSE,
		'unique' => true,
	    ],
	    'sys_role' => [
		'type' => 'char',
		'constraint' => '1',
		"null" => FALSE,
		'default' => '0',
	    ],
	    'is_super' => [
		'type' => 'char',
		'constraint' => '1',
		"null" => FALSE,
		'default' => '0',
	    ],
	    "created_at timestamp(0) without time zone NOT NULL DEFAULT ('now'::text)::timestamp(0) without time zone",
	    'updated_at' => [
		'type' => 'timestamp(0) without time zone',
		"null" => true,
	    ],
	]);
	$this->dbforge->add_key('id', TRUE);
	$this->dbforge->create_table('role');

	$this->dbforge->add_field([
	    'id' => [
		'type' => 'bigint',
		'auto_increment' => TRUE,
	    ],
	    'role_id' => [
		'type' => 'smallint',
		"null" => FALSE,
	    ],
	    'permission_id' => [
		'type' => 'smallint',
		"null" => FALSE,
	    ],
	    "created_at timestamp(0) without time zone NOT NULL DEFAULT ('now'::text)::timestamp(0) without time zone",
	    'updated_at' => [
		'type' => 'timestamp(0) without time zone',
		"null" => true,
	    ],
	    "FOREIGN KEY (role_id) REFERENCES {$schema}role(id) on update restrict on delete restrict",
	    "FOREIGN KEY (permission_id) REFERENCES {$schema}permission(id) on update restrict on delete restrict",
	]);
	$this->dbforge->add_key('id', TRUE);
	$this->dbforge->create_table('role_permission');

	$this->dbforge->add_field([
	    'id' => [
		'type' => 'smallint',
		'auto_increment' => TRUE,
	    ],
	    'user_id' => [
		'type' => 'bigint',
		"null" => FALSE,
	    ],
	    'role_id' => [
		'type' => 'smallint',
		"null" => FALSE,
	    ],
	    "created_at timestamp(0) without time zone NOT NULL DEFAULT ('now'::text)::timestamp(0) without time zone",
	    'updated_at' => [
		'type' => 'timestamp(0) without time zone',
		"null" => true,
	    ],
	    "FOREIGN KEY (user_id) REFERENCES {$schema}user(id) on update restrict on delete restrict",
	    "FOREIGN KEY (role_id) REFERENCES {$schema}role(id) on update restrict on delete restrict",
	]);
	$this->dbforge->add_key('id', TRUE);
	$this->dbforge->create_table('user_role');

	/*	 * ****** End : Creating Database Structure ******* */


	/*	 * ****** Start : Database Seeding ******* */
	$this->db->query("insert into {$schema}profession(name) values ('Teacher')");
	$this->db->query("insert into {$schema}profession(name) values ('Nurse')");
	$this->db->query("insert into {$schema}profession(name) values ('Engineer')");
	$this->db->query("insert into {$schema}profession(name) values ('Doctor')");
	$this->db->query("insert into {$schema}religion(name) values ('Islam')");
	$this->db->query("insert into {$schema}religion(name) values ('Christian')");
	$this->db->query("insert into {$schema}class_level(name) values ('Primary-level')");
	$this->db->query("insert into {$schema}class_level(name) values ('Ordinary-level')");
	$this->db->query("insert into {$schema}class_level(name) values ('Advanced-level')");
	$this->db->query("insert into {$schema}hostel_type(name) values ('Boys')");
	$this->db->query("insert into {$schema}hostel_type(name) values ('Girls')");
	$this->db->query("insert into {$schema}hostel_type(name) values ('Both')");
	$this->db->query("insert into {$schema}payment_type(name) values ('Mobile')");
	$this->db->query("insert into {$schema}payment_type(name) values ('Bank')");
	$this->db->query("insert into {$schema}payment_type(name) values ('Cash')");
	/*	 * ****** End : Database Seeding ******* */

	$this->db->trans_complete();
    }

    public function down() {

	$this->db->trans_start();

	$this->dbforge->drop_table('user_role');
	$this->dbforge->drop_table('role_permission');
	$this->dbforge->drop_table('role');
	$this->dbforge->drop_table('permission');
	$this->dbforge->drop_table('permission_group');
	$this->dbforge->drop_table('ini_config');
	$this->dbforge->drop_table('reply_message');
	$this->dbforge->drop_table('reset');
	$this->dbforge->drop_table('sms_setting');
	$this->dbforge->drop_table('school_session');
	$this->dbforge->drop_table('sms');
	$this->dbforge->drop_table('message');
	$this->dbforge->drop_table('setting');
	$this->dbforge->drop_table('grade');
	$this->dbforge->drop_table('alert');
	$this->dbforge->drop_table('mark');
	$this->dbforge->drop_table('class_attendance');
	$this->dbforge->drop_table('class_teacher');
	$this->dbforge->drop_table('media_share');
	$this->dbforge->drop_table('media');
	$this->dbforge->drop_table('media_category');
	$this->dbforge->drop_table('promotion_subject');
	$this->dbforge->drop_table('student_archive');
	$this->dbforge->drop_table('expense');
	$this->dbforge->drop_table('payment');
	$this->dbforge->drop_table('invoice_fee');
	$this->dbforge->drop_table('invoice');
	$this->dbforge->drop_table('accademic_fee_type');
	$this->dbforge->drop_table('fee_type_audit');
	$this->dbforge->drop_table('fee_type');
	$this->dbforge->drop_table('payment_type');
	$this->dbforge->drop_table('transport_vehicle');
	$this->dbforge->drop_table('vehicle');
	$this->dbforge->drop_table('issue');
	$this->dbforge->drop_table('book');
	$this->dbforge->drop_table('library_member');
	$this->dbforge->drop_table('routine');
	$this->dbforge->drop_table('subject_student');
	$this->dbforge->drop_table('subject_class');
	$this->dbforge->drop_table('exam_attendance');
	$this->dbforge->drop_table('exam_schedule');
	$this->dbforge->drop_table('subject_user');
	$this->dbforge->drop_table('subject_section');
	$this->dbforge->drop_table('subject');
	$this->dbforge->drop_table('exam');
	$this->dbforge->drop_table('semister');
	$this->dbforge->drop_table('teacher_attendance');
	$this->dbforge->drop_table('accademic_year');
	$this->dbforge->drop_table('parent_student');
	$this->dbforge->drop_table('student_transport');
	$this->dbforge->drop_table('student');
	$this->dbforge->drop_table('hostel');
	$this->dbforge->drop_table('hostel_type');
	$this->dbforge->drop_table('transport');
	$this->dbforge->drop_table('section_user');
	$this->dbforge->drop_table('section');
	$this->dbforge->drop_table('section_category');
	$this->dbforge->drop_table('class');
	$this->dbforge->drop_table('class_level');
	$this->dbforge->drop_table('user');
	$this->dbforge->drop_table('parent');
	$this->dbforge->drop_table('religion');
	$this->dbforge->drop_table('profession');
	$this->dbforge->drop_table('user_type');

	$this->db->trans_complete();
    }

}
