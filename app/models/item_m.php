<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Item_m extends MY_Model {

	protected $_table_name = 'item';
	protected $_primary_key = 'item_id';
	protected $_primary_filter = 'intval';
	protected $_order_by = "item_id asc";

	function __construct() {
		parent::__construct();
	}


	function get_item($array=NULL, $signal=FALSE) {
		$query = parent::get($array, $signal);
		return $query;
	}

	function get_order_by_items($array=NULL) {
		$query = parent::get_order_by($array);
		return $query;
	}

	function insert_item($array) {
		$error = parent::insert($array);
		return $error;
	}

	function update_item($data, $id = NULL) {
		parent::update($data, $id);
		return $id;
	}

	public function delete_item($id){
		parent::delete($id);
	}

	function get_order_by_numeric_classes() {
		$this->db->select('*')->from('classes')->order_by('classes_numeric asc');
		$query = $this->db->get();
		return $query->result();
	}
}

/* End of file classes_m.php */
/* Location: .//D/xampp/htdocs/school/mvc/models/classes_m.php */