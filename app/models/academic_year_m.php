<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Description of academic_year_m
 *
 *  -----------------------------------------------------
 *  Copyright: INETS COMPANY LIMITED
 *  Website: www.inetstz.com
 *  Email: info@inetstz.com
 *  -----------------------------------------------------
 * @author Ephraim Swilla
 */
class Academic_year_m extends MY_Model {

    protected $_table_name = 'academic_year';
    protected $_primary_key = 'id';
    protected $_primary_filter = 'intval';
    protected $_order_by = "id asc";

    function __construct() {
	parent::__construct();
    }

    function get_next_academic($current_academic_id) {
	return $this->db->query('select * FROM '.  set_schema_name().'academic_year WHERE status=2 AND class_level_id IN (select class_level_id FROM '.  set_schema_name().'academic_year WHERE id='.$current_academic_id.' ) ')->row();
    }
    
     function get_current_year($class_level_id) {
	return $this->db->query('select * FROM '.  set_schema_name().'academic_year WHERE class_level_id='.$class_level_id.' AND  end_date > now() AND start_date  < now()')->row();
    }

    function get_single_year($array) {
	$query = parent::get_single($array);
	return $query;
    }

    function get_all_years($array = NULL, $signal = FALSE) {
	$query = parent::get($array, $signal);
	return $query;
    }

    function get_order_by_academic($array = NULL) {
	$query = parent::get_order_by($array);
	return $query;
    }

    function insert_academic($array) {
	$error = parent::insert($array);
	return TRUE;
    }

    function update_academic($data, $id = NULL) {
	parent::update($data, $id);
	return $id;
    }

    public function delete_academic($id) {
	parent::delete($id);
    }

}
