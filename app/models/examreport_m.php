<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Description of examreport_m
 *
 *  -----------------------------------------------------
 *  Copyright: INETS COMPANY LIMITED
 *  Website: www.inetstz.com
 *  Email: info@inetstz.com
 *  -----------------------------------------------------
 * @author Ephraim Swilla
 */
class Examreport_m extends MY_Model {

    protected $_table_name = 'exam_report';
    protected $_primary_key = 'exam_report_id';
    protected $_primary_filter = 'intval';
    protected $_order_by = "exam_report_id asc";

    function __construct() {
	parent::__construct();
    }

    function get_exam_report($array = NULL, $signal = FALSE) {
	$query = parent::get($array, $signal);
	return $query;
    }

    function get_order_by_exam($array = NULL) {
	$query = parent::get_order_by($array);
	return $query;
    }

    function insert_exam($array) {
	$error = parent::insert($array);
	return TRUE;
    }

    function update_exam($data, $id = NULL) {
	parent::update($data, $id);
	return $id;
    }

    function get_examreport_by_class($id) {
	return $this->db->query('SELECT DISTINCT combined_exams, name , "classesID", to_char(created_at, \'YYYY-MM-DD\') as regdate FROM  ' . set_schema_name() . 'exam_report WHERE "classesID"=' . $id)->result();
    }

    public function delete_exam($id) {
	parent::delete($id);
    }

    public function get_current_year($classesID, $sectionID = null) {

	return $this->db->query('select * FROM ' . set_schema_name() . 'academic_year WHERE class_level_id IN (select classlevel_id FROM ' . set_schema_name() . 'classes WHERE "classesID"=' . $classesID . ' ) AND  end_date > now() AND start_date  < now()')->row();
    }


    public function combinesql($classesID, $exams, $sectionID = NULL, $academicyear_id = NULL) {
	$initial_letter = 97;
	$academic_year_id = $academicyear_id == NULL ? $this->get_current_year($classesID)->id : $academicyear_id;
	$string = '';
	$join_query = '';
	$adding_sum = '';
	$col = '';
	$total_exams = array();
	$i = 0;
	$current_year = $this->get_current_year($classesID);
	
	foreach ($exams as $key => $value) {
	    $value = preg_replace('/[^a-z0-9]/', '', strtolower($value));
	    if ((int) $key > 0) {
		array_push($total_exams, $value);
		$string = "$string" . chr($initial_letter) . '."' . $value . '", ';

		if ($sectionID == NULL) {
		    $section = "";
		} else {
		    /**
		     * this is the logic
		     * If this student view results in the same academic year, then we should fetch section
		     * from the existing year.
		     * If we fetch results from the previous year, then we need to search section from previous
		     * year in student_archive so we should get the correct result
		     */
		    if ($current_year->id == $academicyear_id) {
			//this is the current year
			$section = 'and a."studentID" IN (SELECT "studentID" FROM ' . set_schema_name() . 'student WHERE "sectionID"=' . $sectionID . ')';
		    } else {
			//this is different year from current academic year
			 $section = 'and  CASE WHEN a.academic_year_id='.$current_year->id.' THEN a."studentID" IN (SELECT student_id FROM ' . set_schema_name() . 'student_archive WHERE section_id=' . $sectionID . ') ELSE a."studentID" IN (SELECT "studentID" FROM ' . set_schema_name() . 'student WHERE "sectionID"=' . $sectionID . ') END '; 
			
		    }
		}

		$join_query .= '(select a."studentID", s.name, s.section, s.roll, s.photo, round((sum(a.mark)::numeric/Coalesce(nullif((select count(*) FROM ' . set_schema_name() . 'subject_count WHERE student_id=a."studentID" ),0),1)::numeric), 1) as "' . $value . '", a."examID" from ' . set_schema_name() . 'mark a join ' . set_schema_name() . 'exam b on a."examID" = b."examID" JOIN ' . set_schema_name() . 'student s ON s."studentID"=a."studentID" where a."examID" = ' . $key . ' AND a."subjectID" IN (SELECT subject_id FROM ' . set_schema_name() . 'subject_count WHERE is_counted=1) and a."academic_year_id"=' . $academic_year_id . '  ' . $section . '  group by a."examID", a."classesID", s.section, a."studentID",s.name, s.roll,s.photo) as ' . chr($initial_letter);


		$join_query .= $initial_letter == 97 ? '' : ' on a."studentID" = ' . chr($initial_letter) . '."studentID"';
		$initial_letter++;
		$join_query .= ' join ';

		$adding_sum .= 'Coalesce("' . $value . '",0) + ';
		$col .= "\"" . $value . "\"::int || ',' || ";
		$i++;
	    }
	}
	//echo $col; 
	$query_string = rtrim($string, ', ');
	$adding_sum_sql = rtrim($adding_sum, ' + ');

	$sql = 'with tempa as (
			    select a."studentID",a.roll, a.name, a.section, a.photo, ' . $query_string . ' from

			    ' . rtrim($join_query, ' join ') . '),
		    tempb as (
				select *, ' . $adding_sum_sql . ' as total, '
		. ' round(((' . $adding_sum_sql . ')::numeric / (' . count($total_exams) . ')::numeric), 2)  as average from tempa
		    )
			    select *, rank() over (order by average desc) as rank from tempb ';
	//echo $sql; exit;
	return $this->db->query($sql)->result_array();
    }

}

/* End of file exam_m.php */
/* Location: .//D/xampp/htdocs/school/mvc/models/exam_m.php */