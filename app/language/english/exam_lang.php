<?php

/* List Language  */
$lang['panel_title'] = "Exam";
$lang['add_title'] = "Add a exam";
$lang['slno'] = "#";
$lang['exam_name'] = "Exam Name";
$lang['exam_date'] = "Date";
$lang['exam_class']="Class";
$lang['exam_section']="Section";
$lang['exam_select_section']="Select Section";
$lang['exam_note'] = "Note";
$lang['exam_all'] = "All";
$lang['exam_academic_year']='Academic Year';
$lang['exam_select_year']='Select Year';
$lang['set_year']='Set accademic year first';


$lang['action'] = "Action";
$lang['view'] = 'View';
$lang['edit'] = 'Edit';
$lang['delete'] = 'Delete';

/* Add Language */

$lang['add_exam'] = 'Add Exam';
$lang['update_exam'] = 'Update Exam';

/* List Language  */
$lang['exam'] = "Exams";
$lang['add_exam'] = "Add Exam";
$lang['add_title'] = 'Add a mark';
$lang['slno'] = "#";
$lang['mark_exam'] = "Exam";
$lang['mark_classes'] = "Class";
$lang['mark_student'] = "Student";
$lang['mark_subject'] = "Subject";
$lang['mark_photo'] = "Photo";
$lang['mark_name'] = "Name";
$lang['mark_roll'] = "Roll";
$lang['mark_phone'] = "Phone";
$lang['mark_dob'] = "Date of Birth";
$lang['mark_sex'] = "Gender";
$lang['mark_religion'] = "Religion";
$lang['mark_email'] = "Email";
$lang['mark_address'] = "Address";
$lang['mark_username'] = "Username";

$lang['mark_subject'] = "Subject";
$lang['mark_mark'] = "Mark";
$lang['mark_point'] = "Point";
$lang['mark_grade'] = "Grade";


$lang['mark_select_classes'] = "Select Class";
$lang['mark_select_exam'] = "Select Exam";
$lang['mark_select_subject'] = "Select Subject";
$lang['mark_select_student'] = "Select Student";
$lang['mark_success'] = "Success";
$lang['personal_information'] = "Personal Information";
$lang['mark_information'] = "Mark Information";
$lang['action'] = "Action";
$lang['view'] = 'View';
$lang['pdf_preview'] = 'PDF Preview';
$lang['print'] = 'Print';
$lang["mail"] = "Send Pdf to Mail";

// /* Add Language */
$lang['add_mark'] = 'Mark';
$lang['add_sub_mark'] = 'Add Mark';

$lang['to'] = 'To';
$lang['subject'] = 'Subject';
$lang['message'] = 'Message';
$lang['send'] = 'Send';
$lang['mail_to'] = "The To field is required.";
$lang['mail_valid'] = "The To field must contain a valid email address.";
$lang['mail_subject'] = "The Subject field is required.";
$lang['mail_success'] = 'Email send successfully!';
$lang['mail_error'] = 'oops! Email could not be sent!';