<?php

/* List Language  */
$lang['panel_title'] = "Mwanafunzi";
$lang['add_title'] = "Ongeza Mwanafunzi";
$lang['slno'] = "#";
$lang['student_photo'] = "Picha";
$lang['student_name'] = "Jina";
$lang['student_email'] = "Barua pepe";
$lang['student_dob'] = "Tarehe ya Kuzaliwa";
$lang['student_sex'] = "Jinsia";
$lang['student_sex_male'] = "Kiume";
$lang['student_sex_female'] = "Kike";
$lang['student_religion'] = "Dini";
$lang['student_phone'] = "Namba ya Simu";
$lang['student_address'] = "Anuani";
$lang['student_classes'] = "Darasa";
$lang['student_roll'] = "Namba ya Udahili";
$lang['student_username'] = "Jina la Kutumia";
$lang['student_password'] = "Neno Siri";
$lang['student_select_class'] = "Chagua Darasa";
$lang['student_guargian'] = "Mlezi";

/* Parent */
$lang['parent_guargian_name'] = "Mlezi";
$lang['parent_father_name'] = "Jina la baba";
$lang['parent_mother_name'] = "Jina la mama";
$lang['parent_father_profession'] = "Kazi ya Baba";
$lang['parent_mother_profession'] = "Kazi ya Mama";
$lang['parent_email'] = "Barua pepe";
$lang['parent_phone'] = "Namba ya Simu";
$lang['parent_address'] = "Anuani";
$lang['parent_username'] = "Jina la Kutumia";
$lang['parent_error'] = "Bado wazazi hwajaongezwa Tafadhari ongeza taarifa zao.";

$lang['action'] = "Hatua";
$lang['view'] = 'Tazama';
$lang['edit'] = 'Hariri';
$lang['delete'] = 'Futa';
$lang['parent'] = 'Ongea Mzazi';
$lang['pdf_preview'] = 'Onesha PDF ya Awali';
$lang['idcard'] = 'Namba ya Kitambulisho';
$lang['print'] = 'Chapa';
$lang["mail"] = "Tuma PDF kwa Barua pepe";

$lang['to'] = 'Kwenda';
$lang['subject'] = 'Kichwa cha Ujumbe';
$lang['message'] = 'Ujumbe';
$lang['send'] = 'Tuma';
$lang['mail_to'] = "Sehemu ya kwenda inahitajika";
$lang['mail_valid'] = "Sehemu ya kwenda lazima iwe na barua pepe halali";
$lang['mail_subject'] = "Sehemu ya somo inahitajika";
$lang['mail_success'] = 'Barua pepe imefanikiwa kutumwa';
$lang['mail_error'] = 'Barua pepe imeshindwa kutuma';

/* Add Language */
$lang['personal_information'] = "Taarifa Binafsi";
$lang['parents_information'] = "Taarifa ya mzazi";
$lang['add_student'] = 'Ongeza mwanfunzi';
$lang['add_parent'] = 'Ongeza mzazi';
$lang['update_student'] = 'Sasisha Mwanafunzi';

$lang['student_section'] = 'Mkondo';
$lang['student_select_section'] = 'Chagua Mkondo';

$lang['student_all_students'] = 'Wanafunzi wote';




