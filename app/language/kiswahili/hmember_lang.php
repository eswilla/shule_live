<?php

/* List Language  */
$lang['panel_title'] = "mwanachama";
$lang['panel_title_profile'] = "Profile";
$lang['slno'] = "#";
$lang['hmember_photo'] = "Picha";
$lang['hmember_name'] = "Jina";
$lang['hmember_section'] = "Mkondo";
$lang['hmember_roll'] = "Namba ya Udahili";
$lang['hmember_email'] = "Barua pepe";
$lang['hmember_phone'] = "Namba ya Simu ya Mkononi";
$lang['hmember_tfee'] = "Ada ya Bweni";
$lang['hmember_classes'] = "Darasa";
$lang['hmember_select_class'] = "Chagua Darasa";
$lang['hmember_hname'] = "Jina la Bweni";
$lang['hmember_class_type'] = "Aina ya Darasa";
$lang['hmember_htype'] = "Aina";
$lang['hmember_select_class_type'] = "Chagua Aina ya Darasa";
$lang['hmember_select_hostel_name'] = "Chagua Darasa";

$lang['hmember_hname'] = "Jina la Bweni";
$lang['hmember_joindate'] = "Tarehe ya Kujiunga";
$lang['hmember_dob'] = "Tarehe ya Kuzaliwa";
$lang['hmember_sex'] = "jinsia";
$lang['hmember_religion'] = "Dini";
$lang['hmember_address'] = "anuani";
$lang['hmember_hostel_address'] = "anuani ya bweni";
$lang['hmember_message'] = "Haujaongezwa.";

$lang['action'] = "Hatua";
$lang['view'] = 'Tazama';
$lang['edit'] = 'Hariri';
$lang['hmember'] = 'Bweni';
$lang['delete'] = 'Futa';
$lang['pdf_preview'] = 'Onesha PDF ya awali';
$lang['print'] = 'Chapa';
$lang["mail"] = "Tuma PDF kwa Barua pepe";

$lang['personal_information'] = "Taarifa Binafsi";

$lang["add_hmember"] = "Ongeza mwanachama";
$lang["update_hmember"] = "Sasisha mwanachama";

$lang['to'] = 'Kwenda';
$lang['subject'] = 'Kichwa cah Ujumbe';
$lang['message'] = 'Ujumbe';
$lang['send'] = 'Tuma';
$lang['mail_to'] = "Uwanja wa kutuma unahitajika";
$lang['mail_valid'] = "uwanja wa kuti=uma lazima uew na barua pepe halali";
$lang['mail_subject'] = "Uwanja wa somo unahitajika";
$lang['mail_success'] = 'Barua pepe imetumwa kwa mafanikio!';
$lang['mail_error'] = 'Barua pepe haijatumwa';