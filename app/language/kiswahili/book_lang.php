<?php


/* List Language  */
$lang['panel_title'] = "Vitabu";
$lang['add_title'] = "Ongeza Kitabu";
$lang['slno'] = "#";
$lang['book_name'] = "Jina";
$lang['book_subject_code'] = "Namba ya somo";
$lang['book_author'] = "Mwandishi";
$lang['book_price'] = "Bei";
$lang['book_class'] = "Darasa";
$lang['book_edition'] = "Toleo";
$lang['book_for'] = "Kitabu kwa ajili ya";
$lang['book_quantity'] = "Kiasi";
$lang['book_rack_no'] = "Rack No";
$lang['book_status'] = "Hali";
$lang['book_available'] = "Kinapatikana";
$lang['book_unavailable'] = "Haipatikani";

$lang['action'] = "Hatua";
$lang['view'] = 'Tazama';
$lang['edit'] = 'Hariri';
$lang['delete'] = 'Futa';

/* Add Language */

$lang['add_book'] = 'Ongeza Kitabu';
$lang['update_book'] = 'Sasisha Kitabu';

