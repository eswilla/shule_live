<?php


/* List Language  */
$lang['panel_title'] = "Aina ya Ada";
$lang['add_title'] = "Ongeza Aina ya Ada";
$lang['slno'] = "#";
$lang['feetype_name'] = "Aina ya Ada";
$lang['feetype_amount'] = "Kiasi";
$lang['feetype_is_repeative'] = "Ni Marudio";
$lang['feetype_is_repeative_yes'] = "Ndio";
$lang['feetype_is_repeative_no'] = "Hapana";
$lang['feetype_startdate'] = "Tarehe ya Kuanza";
$lang['feetype_enddate'] = "Tarehe ya Kumaliza";
$lang['feetype_note'] = "Kumbuka";
$lang['action'] = "Hatua";

$lang['edit'] = 'Hariri';
$lang['delete'] = 'Futa';

/* Add Language */
$lang['add_feetype'] = 'Ongeza Ada';
$lang['update_feetype'] = 'Sasisha Aina ya Ada';