<?php

/* List Language  */
$lang['panel_title'] = "Kuazima";
$lang['panel_title2'] = "Tozo";
$lang['add_title'] = "Azimisha Kitabu";
$lang['slno'] = "#";
$lang['issue_id'] = "ID";
$lang['issue_book'] = "Kitabu";
$lang['issue_author'] = "Mwandishi";
$lang['issue_student'] = "Mwanafunzi";
$lang['issue_subject_code'] = "Namba ya Somo";
$lang['issue_serial_no'] = " Namba ya Chapisho"; 
$lang['issue_issue_date'] = "Tarehe ya Kuazima";
$lang['issue_due_date'] = "Tarehe ya Kuazimishwa";
$lang['issue_return_date'] = "Tarehe ya kurudisha"; 
$lang['issue_select_student'] = "Chagua Mwanafunzi";
$lang['issue_fine'] = "Tozo";
$lang['issue_search'] = "Tafuta";
$lang['issue_book_name']='Jina la Kitabu';
$lang['issue_note'] = "Notisi";
$lang['action'] = "Hatua";


$lang['issue_message'] = "Hakuna cha Kuazima";

/* Fine lang Start */
$lang['issue_day'] = "Siku";
$lang['issue_month'] = "Mwezi";
$lang['issue_year'] = "Mwaka";
$lang['issue_select_day'] = "Chagua Siku";
$lang['issue_select_month'] = "Chagua Mwezi";
$lang['issue_select_year'] = "Chagua Mwaka";
$lang['issue_total'] = "Jumla : ";
$lang['issue_print'] = "Chapa";
$lang['issue_printpreview'] = "Chapa ya onesho la awali";

$lang['issue_book_information'] = "Taarifa za kitabu";

/* Fine lang End */


$lang['view'] = 'Tazama';
$lang['return'] = 'Rudisha';
$lang['edit'] = 'Hariri';
$lang['delete'] = 'Futa';

/* Add Language */

$lang['add_issue'] = 'Ongeza uazimishaji';
$lang['add_fine'] = 'Tozo';
$lang['update_issue'] = 'Sasisha  Uazimishaji';