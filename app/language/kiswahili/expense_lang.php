<?php

/* List Language  */
$lang['panel_title'] = "Gharama";
$lang['add_title'] = "Ongeza Gharama";
$lang['slno'] = "#";
$lang['expense_expense'] = "Jina";
$lang['expense_date'] = "Tarehe";
$lang['expense_amount'] = "Kiasi";
$lang['expense_note'] = "Kumbuka";
$lang['expense_uname'] = "Mtumiaji";
$lang['expense_total'] = "Jumla";
$lang['action'] = "Hatua";

// $lang['view'] = 'View';
$lang['edit'] = 'Hariri';
$lang['delete'] = 'Futa';

/* Add Language */

$lang['add_expense'] = 'Ongeza Gharama';
$lang['update_expense'] = 'Sasisha Gharama';