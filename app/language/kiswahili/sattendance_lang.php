<?php

/* List Language  */
$lang['panel_title'] = "Mahudhurio ya Mwanafunzi";
$lang['panel_title2'] = "Mahudhurio";
$lang['add_title'] = "Ongeza mahudhurio ya Mwanafunzi";
$lang['slno'] = "#";
$lang['attendance_classes'] = "Darasa";
$lang['attendance_student'] = "Mwanafunzi";
$lang['attendance_date'] = "Tarehe";
$lang['attendance_photo'] = "Picha";
$lang['attendance_name'] = "Jina";
$lang['attendance_section'] = "Mkondo";
$lang['attendance_roll'] = "Namba ya Udahili";
$lang['attendance_phone'] = "Namba ya Simu";
$lang['attendance_dob'] = "Tarehe ya Kuzaliwa";
$lang['attendance_sex'] = "Jinsia";
$lang['attendance_religion'] = "Dini";
$lang['attendance_email'] = "Barua pepe";
$lang['attendance_address'] = "Anuani";
$lang['attendance_username'] = "Jina la Kutumia";
$lang['attendance_all_students'] = 'Wanafunzi Wote';


$lang['attendance_select_classes'] = "Chagua Darasa";
$lang['attendance_select_student'] = "Chagua Mwanafunzi";
$lang['personal_information'] = "Taarifa Binafsi";
$lang['attendance_information'] = "Taarifa za mahudhurio";
$lang['action'] = "Hatua";
$lang['view'] = 'Tazama';
$lang['pdf_preview'] = 'Onesha PDF ya awali';
$lang['print'] = 'Chapa';
$lang["mail"] = "Tuma PDF kwa barua pepe";

// /* Add Language */

$lang['add_attendance'] = 'Mahudhurio';
$lang['add_all_attendance'] = 'Ongeza Mahudhurio yote';

$lang['attendance_jan'] = "Januari";
$lang['attendance_feb'] = "Februari";
$lang['attendance_mar'] = "Machi";
$lang['attendance_apr'] = "Aprili";
$lang['attendance_may'] = "Mei";
$lang['attendance_june'] = "Juni";
$lang['attendance_jul'] = "Julai";
$lang['attendance_aug'] = "Agosti";
$lang['attendance_sep'] = "Septemba";
$lang['attendance_oct'] = "Oktoba";
$lang['attendance_nov'] = "Novemba";
$lang['attendance_dec'] = "Desemba";

$lang['attendance_1'] = "1";
$lang['attendance_2'] = "2";
$lang['attendance_3'] = "3";
$lang['attendance_4'] = "4";
$lang['attendance_5'] = "5";
$lang['attendance_6'] = "6";
$lang['attendance_7'] = "7";
$lang['attendance_8'] = "8";
$lang['attendance_9'] = "9";
$lang['attendance_10'] = "10";
$lang['attendance_11'] = "11";
$lang['attendance_12'] = "12";
$lang['attendance_13'] = "13";
$lang['attendance_14'] = "14";
$lang['attendance_15'] = "15";
$lang['attendance_16'] = "16";
$lang['attendance_17'] = "17";
$lang['attendance_18'] = "18";
$lang['attendance_19'] = "19";
$lang['attendance_20'] = "20";
$lang['attendance_21'] = "21";
$lang['attendance_22'] = "22";
$lang['attendance_23'] = "23";
$lang['attendance_24'] = "24";
$lang['attendance_25'] = "25";
$lang['attendance_26'] = "26";
$lang['attendance_27'] = "27";
$lang['attendance_28'] = "28";
$lang['attendance_29'] = "29";
$lang['attendance_30'] = "30";
$lang['attendance_31'] = "31";

$lang['to'] = 'Kwenda';
$lang['subject'] = 'Kichwa cha Ujumbe';
$lang['message'] = 'Ujumbe';
$lang['send'] = 'Tuma';
$lang['mail_to'] = "Sehemu ya kwenda inahitajika";
$lang['mail_valid'] = "Sehemu ya kwenda lazima iwe na barua pepe halali";
$lang['mail_subject'] = "Sehemu ya somo inahitajika";
$lang['mail_success'] = 'Barua pepe imefanikiwa kutumwa';
$lang['mail_error'] = 'Barua pepe imeshindwa kutuma';
$lang['report_information']='Ripoti ya Mitihani';
