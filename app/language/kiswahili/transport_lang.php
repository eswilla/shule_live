<?php

/* List Language  */
$lang['panel_title'] = "Usafiri";
$lang['add_title'] = "Ongeza Usafiri";
$lang['slno'] = "#";
$lang['transport_route'] = "Jina la Njia";
$lang['transport_vehicle'] = "Idadi ya magari";
$lang['transport_fare'] = "Nauli ya Njia";
$lang['transport_note'] = "Kumbuka";

$lang['action'] = "Hatua";
$lang['view'] = 'Tazama';
$lang['edit'] = 'hariri';
$lang['delete'] = 'Futa';

/* Add Language */

$lang['add_transport'] = 'Ongeza Usafiri';
$lang['update_transport'] = 'Sasisha Usafiri';