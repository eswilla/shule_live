<form role="form" method="post" class="form-horizontal">
			<h1 class="text-center">Reset Password</h1>


			<?php
			if ($form_validation == "No") {
			    
			} else {
			    if (count($form_validation)) {
				echo "<div class=\"alert alert-danger alert-dismissable\">
                            <i class=\"fa fa-warning\"></i>
                            <button aria-hidden=\"true\" data-dismiss=\"alert\" class=\"close\" type=\"button\">×</button>
                            $form_validation
                        </div>";
			    }
			}

			if ($this->session->flashdata('reset_send')) {
			    $message = $this->session->flashdata('reset_send');
			    echo "<div class=\"alert alert-success alert-dismissable\">
                        <i class=\"fa fa-warning\"></i>
                        <button aria-hidden=\"true\" data-dismiss=\"alert\" class=\"close\" type=\"button\">×</button>
                        $message
                    </div>";
			} else {
			    if ($this->session->flashdata('reset_error')) {
				$message = $this->session->flashdata('reset_error');
				echo "<div class=\"alert alert-danger alert-dismissable\">
	                        <i class=\"fa fa-warning\"></i>
	                        <button aria-hidden=\"true\" data-dismiss=\"alert\" class=\"close\" type=\"button\">×</button>
	                        $message
	                    </div>";
			    }
			}
			?>
			<div class="input-field">
			    <i class="material-icons prefix">email</i>
			    <input type="text" name="phone" id="inputEmail"  class="validate"/>
			    <label for="inputEmail">Phone</label>
			</div>

			<div class="col-sm-12">
			    <div class="form-group">
				<div class="form-group">
				    <label for="calculate">Calculate</label>
				</div>

				<div class="col-sm-12">
				    <div class="col-sm-3">
					<div class="form-group">
					    <input type="text" name="firstNumber" value="<?php echo $firstnumber; ?>">
					</div>
				    </div>
				    <div class="col-sm-1">
					<label for="plus">+</label>
				    </div>
				    <div class="col-sm-3">
					<div class="form-group">
					    <input type="text" name="secondNumber"  value="<?php echo $secondnumber; ?>" >
					</div>
				    </div>
				    <div class="col-sm-2">
					<label for="plus">=</label>
				    </div>
				    <div class="col-sm-3">
					<div class="form-group">
					    <input type="text" name="answer" placeholder="Answer">
					</div>
				    </div>
				</div>

			    </div>

			</div>
			<div>


			    <button class="btn btn-default  btn-block text-uppercase" type="submit" >Send</button>

			</div>
		    </form>