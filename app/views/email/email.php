<?php
/**
 * Description of user_feedback
 *
 *  -----------------------------------------------------
 *  Copyright: INETS COMPANY LIMITED
 *  Website: www.inetstz.com
 *  Email: info@inetstz.com
 *  -----------------------------------------------------
 * @author Ephraim Swilla
 */
?>
<!DOCTYPE HTML>
<html lang="en" xmlns:mc="http://www.w3.org/1999/xhtml">
<head>

    <!-- Meta information -->
    <meta charset="utf-8"/>
    <title>vet Feeds</title>
    <script>
        window.bgimg = "boxhead.html";
        window.bgcol = "ffffff";
        window.h1col = "ce616e";
        window.h2col = "222222";
        window.butcol = "75bec0";
        window.bodycol = "n/a";
        window.temname = "notify";
    </script>
</head>
<body scrolling="no">

<div id="tc_theme">

    <center>
        <div id="tc_central" c-style="bgcolor" onClick="closeAll();">

            <style type="text/css">

                div, p, a, li, td {
                    -webkit-text-size-adjust: none;
                }

                * {
                    -webkit-font-smoothing: antialiased;
                    -moz-osx-font-smoothing: grayscale;
                }

                .ReadMsgBody {
                    width: 100%;
                    background-color: #ffffff;
                }

                .ExternalClass {
                    width: 100%;
                    background-color: #ffffff;
                }

                /*body*/
                #tc_central {
                    width: 100%;
                    height: 100%;
                    background-color: #ffffff;
                    margin: 0;
                    padding: 0;
                    -webkit-font-smoothing: antialiased;
                }

                /*html*/
                #tc_central {
                    width: 100%;
                    background-color: #ffffff;
                }

                @font-face {
                    font-family: 'proxima_novalight';
                    src: url('templates/titan/font/proximanova-light-webfont.eot');
                    src: url('templates/titan/font/proximanova-light-webfontd41d.eot?#iefix') format('embedded-opentype'), url('templates/titan/font/proximanova-light-webfont.woff') format('woff'), url('templates/titan/font/proximanova-light-webfont.ttf') format('truetype');
                    font-weight: normal;
                    font-style: normal;
                }

                @font-face {
                    font-family: 'proxima_nova_rgregular';
                    src: url('templates/titan/font/proximanova-regular-webfont.eot');
                    src: url('templates/titan/font/proximanova-regular-webfontd41d.eot?#iefix') format('embedded-opentype'), url('templates/titan/font/proximanova-regular-webfont.woff') format('woff'), url('templates/titan/font/proximanova-regular-webfont.ttf') format('truetype');
                    font-weight: normal;
                    font-style: normal;
                }

                @font-face {
                    font-family: 'proxima_novasemibold';
                    src: url('templates/titan/font/proximanova-semibold-webfont.eot');
                    src: url('templates/titan/font/proximanova-semibold-webfontd41d.eot?#iefix') format('embedded-opentype'), url('templates/titan/font/proximanova-semibold-webfont.woff') format('woff'), url('templates/titan/font/proximanova-semibold-webfont.ttf') format('truetype');
                    font-weight: normal;
                    font-style: normal;
                }

                @font-face {
                    font-family: 'proxima_nova_rgbold';
                    src: url('templates/titan/font/proximanova-bold-webfont.eot');
                    src: url('templates/titan/font/proximanova-bold-webfontd41d.eot?#iefix') format('embedded-opentype'), url('templates/titan/font/proximanova-bold-webfont.woff') format('woff'), url('templates/titan/font/proximanova-bold-webfont.ttf') format('truetype');
                    font-weight: normal;
                    font-style: normal;
                }

                @font-face {
                    font-family: 'proxima_novablack';
                    src: url('templates/titan/font/proximanova-black-webfont.eot');
                    src: url('templates/titan/font/proximanova-black-webfontd41d.eot?#iefix') format('embedded-opentype'), url('templates/titan/font/proximanova-black-webfont.woff') format('woff'), url('templates/titan/font/proximanova-black-webfont.ttf') format('truetype');
                    font-weight: normal;
                    font-style: normal;
                }

                @font-face {
                    font-family: 'proxima_novathin';
                    src: url('templates/mason/font/proximanova-thin-webfont.eot');
                    src: url('templates/mason/font/proximanova-thin-webfontd41d.eot?#iefix') format('embedded-opentype'), url('templates/mason/font/proximanova-thin-webfont.woff') format('woff'), url('templates/mason/font/proximanova-thin-webfont.ttf') format('truetype');
                    font-weight: normal;
                    font-style: normal;
                }

                p {
                    padding: 0 !important;
                    margin-top: 0 !important;
                    margin-right: 0 !important;
                    margin-bottom: 0 !important;
                    margin-left: 0 !important;
                }

                .hover:hover {
                    opacity: 0.85;
                    filter: alpha(opacity=85);
                }

                .image77 img {
                    width: 77px;
                    height: auto;
                }

                .avatar125 img {
                    width: 125px;
                    height: auto;
                }

                .icon61 img {
                    width: 61px;
                    height: auto;
                }

                .logo img {
                    width: 75px;
                    height: auto;
                }

                .icon18 img {
                    width: 18px;
                    height: auto;
                }

            </style>

            <!-- @media only screen and (max-width: 640px)
                       {*/
                       -->
            <!--LARGEONEO
            <style type="text/css">
                    /*body*/ #tc_central{width:auto!important;}
                    table[class=full2] {width: 100%!important; clear: both; }
                    table[class=mobile2] {width: 100%!important; padding-left: 20px; padding-right: 20px; clear: both; }
                    table[class=fullCenter2] {width: 100%!important; text-align: center!important; clear: both; }
                    td[class=fullCenter2] {width: 100%!important; text-align: center!important; clear: both; }
                    td[class=pad15] {width: 100%!important; padding-left: 15px; padding-right: 15px; clear: both;}

            </style>
            CLARGEONE-->
            <!--

            @media only screen and (max-width: 479px)
                       {
                       -->
            <!--SMALLONEO
            <style type="text/css">
                    /*body*/ #tc_central{width:auto!important;}
                    table[class=full2] {width: 100%!important; clear: both; }
                    table[class=mobile2] {width: 100%!important; padding-left: 20px; padding-right: 20px; clear: both; }
                    table[class=fullCenter2] {width: 100%!important; text-align: center!important; clear: both; }
                    td[class=fullCenter2] {width: 100%!important; text-align: center!important; clear: both; }
                    table[class=full] {width: 100%!important; clear: both; }
                    table[class=mobile] {width: 100%!important; padding-left: 20px; padding-right: 20px; clear: both; }
                    table[class=fullCenter] {width: 100%!important; text-align: center!important; clear: both; }
                    td[class=fullCenter] {width: 100%!important; text-align: center!important; clear: both; }
                    td[class=pad15] {width: 100%!important; padding-left: 15px; padding-right: 15px; clear: both;}
                    .erase {display: none;}

                    }
            </style>
            CSMALLONE-->


            <div id="sort_them">
                <!-- Notification 1  -->
                <table width="100%" border="0" cellpadding="0" cellspacing="0" align="center" class="full"
                       bgcolor="#ccc" c-style="bgImage" object="drag-module">
                    <tr mc:repeatable>
                        <td align="center"
                            style="background-image: url(templates/notify/images/not1_bg_image.jpg); -webkit-background-size: cover; -moz-background-size: cover; -o-background-size: cover; background-size: cover; background-position: center center; background-repeat: no-repeat; background-color: #bcbcbc;"
                            id="not1">
                            <div mc:hideable>

                                <!-- Mobile Wrapper -->
                                <table width="100%" border="0" cellpadding="0" cellspacing="0" align="center"
                                       class="mobile">
                                    <tr>
                                        <td width="100%" height="100" align="center">

                                            <div class="sortable_inner">
                                                <table width="400" border="0" cellpadding="0" cellspacing="0"
                                                       align="center" class="mobile" object="drag-module-small">
                                                    <tr>
                                                        <td width="100%" valign="middle">

                                                            <table width="265" border="0" cellpadding="0"
                                                                   cellspacing="0" align="center"
                                                                   style="text-align: center; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;"
                                                                   class="fullCenter">
                                                                <tr>
                                                                    <td width="100%" height="50"></td>
                                                                </tr>
                                                            </table>

                                                        </td>
                                                    </tr>
                                                </table>
                                                <table width="400" border="0" cellpadding="0" cellspacing="0"
                                                       align="center" class="mobile" object="drag-module-small">
                                                    <tr>
                                                        <td width="100%" valign="middle" align="center">

                                                            <table width="265" border="0" cellpadding="0"
                                                                   cellspacing="0" align="center"
                                                                   style="text-align: center; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;"
                                                                   class="fullCenter">
                                                                <tr>
                                                                    <td width="100%" height="50"></td>
                                                                </tr>
                                                            </table>

                                                        </td>
                                                    </tr>
                                                </table>

                                                <table width="400" border="0" cellpadding="0" cellspacing="0"
                                                       align="center" class="mobile" bgcolor="#25b89a" c-style="blueBG"
                                                       object="drag-module-small">
                                                    <tr>
                                                        <td width="100%" valign="middle" align="center">

                                                            <table width="265" border="0" cellpadding="0"
                                                                   cellspacing="0" align="center"
                                                                   style="text-align: center; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;"
                                                                   class="fullCenter">
                                                                <tr>
                                                                    <td width="100%" height="50"></td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                                <!-- Start Top -->
                                                <table width="400" border="0" cellpadding="0" cellspacing="0"
                                                       align="center" class="mobile" bgcolor="#25b89a" c-style="blueBG"
                                                       object="drag-module-small">
                                                    <tr>
                                                        <td width="100%" valign="middle" align="center">

                                                            <!-- Header Text -->
                                                            <table width="300" border="0" cellpadding="0"
                                                                   cellspacing="0" align="center"
                                                                   style="text-align: center; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;"
                                                                   class="fullCenter">
                                                                <tr>
                                                                    <td align="center" valign="middle" width="100%"
                                                                        style="text-align: center; font-family: Helvetica, Arial, sans-serif; font-size: 40px; color: #ffffff; line-height: 44px; font-weight: 100;"
                                                                        t-style="whiteText" class="fullCenter"
                                                                        mc:edit="1">
                                                                        <p object="text-editable">
                                                                            <!--[if !mso]><!--><span
                                                                                style="font-family: 'proxima_novathin', Helvetica; font-weight: normal;"><!--<![endif]--><singleline>
                                                                                    ShuleSoft
                                                                                </singleline><!--[if !mso]><!--></span>
                                                                            <!--<![endif]--></p>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td width="100%" height="5"></td>
                                                                </tr>
                                                            </table>

                                                        </td>
                                                    </tr>
                                                </table><!-- End Top -->

                                                <table width="400" border="0" cellpadding="0" cellspacing="0"
                                                       align="center" class="mobile" bgcolor="#25b89a" c-style="blueBG"
                                                       object="drag-module-small">
                                                    <tr>
                                                        <td width="100%" valign="middle" align="center">

                                                            <table width="300" border="0" cellpadding="0"
                                                                   cellspacing="0" align="center"
                                                                   style="text-align: center; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;"
                                                                   class="fullCenter">
                                                                <tr>
                                                                    <td width="100%" height="15"></td>
                                                                </tr>
                                                                <tr>
                                                                    <td valign="middle" width="100%"
                                                                        style="text-align: center; font-family: Helvetica, Arial, sans-serif; font-size: 31px; color: #ffffff; line-height: 38px; font-weight: bold; text-transform: uppercase;"
                                                                        t-style="whiteText" class="fullCenter"
                                                                        mc:edit="2">
                                                                        <p object="text-editable">
                                                                            <!--[if !mso]><!--><span
                                                                                style="font-family: 'proxima_novablack', Helvetica; font-weight: normal;"><!--<![endif]--><singleline>

                                                                                </singleline><!--[if !mso]><!--></span>
                                                                            <!--<![endif]--></p>
                                                                    </td>
                                                                </tr>
                                                            </table>

                                                        </td>
                                                    </tr>
                                                </table><!-- End Top -->

                                                <table width="400" border="0" cellpadding="0" cellspacing="0"
                                                       align="center" class="mobile" bgcolor="#25b89a" c-style="blueBG"
                                                       object="drag-module-small">
                                                    <tr>
                                                        <td width="100%" valign="middle" align="center">

                                                            <table width="265" border="0" cellpadding="0"
                                                                   cellspacing="0" align="center"
                                                                   style="text-align: center; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;"
                                                                   class="fullCenter">
                                                                <tr>
                                                                    <td width="100%" height="10"></td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>

                                                <table width="400" border="0" cellpadding="0" cellspacing="0"
                                                       align="center" class="mobile" bgcolor="#ffffff" c-style="whiteBG"
                                                       object="drag-module-small">
                                                    <tr>
                                                        <td width="100%" valign="middle" align="center">

                                                            <table width="265" border="0" cellpadding="0"
                                                                   cellspacing="0" align="center"
                                                                   style="text-align: center; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;"
                                                                   class="fullCenter">
                                                                <tr>
                                                                    <td width="100%" height="50"></td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>

                                                <!-- Start Second -->
                                                <table width="400" border="0" cellpadding="0" cellspacing="0"
                                                       align="center" class="mobile" bgcolor="#ffffff" c-style="whiteBG"
                                                       object="drag-module-small">
                                                    <tr>
                                                        <td width="100%" valign="middle" align="center">

                                                            <!-- Header Text -->
                                                            <table width="265" border="0" cellpadding="0"
                                                                   cellspacing="0" align="center"
                                                                   style="text-align: center; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;"
                                                                   class="fullCenter">
                                                                <tr>
                                                                    <td align="center" valign="middle" width="100%"
                                                                        style="text-align: left; font-family: Helvetica, Arial, sans-serif; font-size: 36px; color: #212121; line-height: 44px; font-weight: 100;"
                                                                        t-style="headline" class="fullCenter"
                                                                        mc:edit="3">
                                                                        <p object="text-editable">
                                                                            <!--[if !mso]><!--><span
                                                                                style="font-family: 'proxima_novathin', Helvetica; font-weight: normal;"><!--<![endif]--><singleline>
                                                                                    Hello,
                                                                                </singleline><!--[if !mso]><!--></span>
                                                                            <!--<![endif]--></p>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>

                                                <table width="400" border="0" cellpadding="0" cellspacing="0"
                                                       align="center" class="mobile" bgcolor="#ffffff" c-style="whiteBG"
                                                       object="drag-module-small">
                                                    <tr>
                                                        <td width="100%" valign="middle" align="center">

                                                            <table width="265" border="0" cellpadding="0"
                                                                   cellspacing="0" align="center"
                                                                   style="text-align: center; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;"
                                                                   class="fullCenter">
                                                                <tr>
                                                                    <td width="100%" height="20"></td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>

                                                <!-- Start Second -->
                                                <table width="400" border="0" cellpadding="0" cellspacing="0"
                                                       align="center" class="mobile" bgcolor="#ffffff" c-style="whiteBG"
                                                       object="drag-module-small">
                                                    <tr>
                                                        <td width="100%" valign="middle" align="center">

                                                            <!-- Header Text -->
                                                            <table width="265" border="0" cellpadding="0"
                                                                   cellspacing="0" align="center"
                                                                   style="text-align: center; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;"
                                                                   class="fullCenter">
                                                                <tr>
                                                                    <td valign="middle" width="100%"
                                                                        style="text-align: center; font-family: Helvetica, Arial, sans-serif; font-size: 14px; color: #515151; line-height: 24px;"
                                                                        t-style="textColor" class="fullCenter"
                                                                        mc:edit="4">
                                                                        <p object="text-editable">
                                                                            <!--[if !mso]><!--><span
                                                                                style="font-family: 'proxima_nova_rgregular', Helvetica; font-weight: normal;"><!--<![endif]--><singleline>
                                                                                   <?= $message?>
                                                                                </singleline><!--[if !mso]><!--></span>
                                                                            <!--<![endif]--></p>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>

                                                <table width="400" border="0" cellpadding="0" cellspacing="0"
                                                       align="center" class="mobile" bgcolor="#ffffff" c-style="whiteBG"
                                                       object="drag-module-small">
                                                    <tr>
                                                        <td width="100%" valign="middle" align="center">

                                                            <table width="265" border="0" cellpadding="0"
                                                                   cellspacing="0" align="center"
                                                                   style="text-align: center; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;"
                                                                   class="fullCenter">
                                                                <tr>
                                                                    <td width="100%" height="50"></td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>

                                                <table width="400" border="0" cellpadding="0" cellspacing="0"
                                                       align="center" class="mobile" bgcolor="#ffffff" c-style="whiteBG"
                                                       object="drag-module-small">
                                                    <tr>
                                                        <td width="100%" valign="middle" align="center">

                                                            <table width="265" border="0" cellpadding="0"
                                                                   cellspacing="0" align="center"
                                                                   style="text-align: center; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;"
                                                                   class="fullCenter">

                                                                <tr>
                                                                    <td valign="middle" width="100%" class="icon18"
                                                                        align="center">


                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td width="100%" height="1"></td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>

                                                <table width="400" border="0" cellpadding="0" cellspacing="0"
                                                       align="center" class="mobile" bgcolor="#ffffff" c-style="whiteBG"
                                                       object="drag-module-small">
                                                    <tr>
                                                        <td width="100%" valign="middle" align="center">

                                                            <table width="265" border="0" cellpadding="0"
                                                                   cellspacing="0" align="center"
                                                                   style="text-align: center; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;"
                                                                   class="fullCenter">
                                                                <tr>
                                                                    <td valign="middle" width="100%" class="icon18"
                                                                        align="center">

                                                                        <!-- Input 2 -->
                                                                        <table width="265" border="0" cellpadding="0"
                                                                               cellspacing="0" align="left"
                                                                               style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;"
                                                                               class="full">
                                                                            <tr>
                                                                                <td width="44" height="44"
                                                                                    bgcolor="#25b89a" c-style="blueBG"
                                                                                    style="text-align: center;"><span
                                                                                        object="image-editable"></span></td>
                                                                                <td width="20" height="44"
                                                                                    bgcolor="#f5f7f7"
                                                                                    c-style="greyBG"></td>
                                                                                <td width="181" height="44"
                                                                                    bgcolor="#f5f7f7" c-style="greyBG"
                                                                                    style="text-align: left; font-family: Helvetica, Arial, sans-serif; font-size: 14px; color: #25b89a; line-height: 24px;"
                                                                                    mc:edit="6">
                                                                                    <p object="text-editable">
                                                                                        <!--[if !mso]><!--><span
                                                                                            style="font-family: 'proxima_nova_rgregular', Helvetica; font-weight: normal;"
                                                                                            t-style="blueText"><!--<![endif]--><singleline>
                                                                                                Best Regards,
                                                                                            </singleline>
                                                                                            <!--[if !mso]><!--></span>
                                                                                        <!--<![endif]--></p>
                                                                                </td>
                                                                                <td width="20" height="44"
                                                                                    bgcolor="#f5f7f7"
                                                                                    c-style="greyBG"></td>
                                                                            </tr>
                                                                        </table><!-- End Space -->

                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td width="100%" height="1"></td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>

                                                <table width="400" border="0" cellpadding="0" cellspacing="0"
                                                       align="center" class="mobile" bgcolor="#ffffff" c-style="whiteBG"
                                                       object="drag-module-small">
                                                    <tr>
                                                        <td width="100%" valign="middle" align="center">

                                                            <table width="400" border="0" cellpadding="0"
                                                                   cellspacing="0" align="center"
                                                                   style="text-align: center; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;"
                                                                   class="fullCenter">
                                                                <tr>
                                                                    <td width="100%" height="20"></td>
                                                                </tr>
                                                                <!----------------- Button Center ----------------->
                                                                <tr>
                                                                    <td>
                                                                        <table width="100%" border="0" cellpadding="0"
                                                                               cellspacing="0" align="center">
                                                                            <tr>
                                                                                <td align="center" height="45"
                                                                                    c-style="blueBG" bgcolor="#25b89a"
                                                                                    style="padding-left: 22px; padding-right: 22px; font-weight: bold; font-family: Helvetica, Arial, sans-serif; color: #ffffff; text-transform: uppercase;"
                                                                                    t-style="whiteText" mc:edit="7">
                                                                                    <multiline><!--[if !mso]><!--><span
                                                                                            style="font-family: 'proxima_novablack', Helvetica; font-weight: normal;"><!--<![endif]-->
																<a href="#"
                                                                   style="color: #ffffff; font-size:18px; text-decoration: none; line-height:34px; width:100%;"
                                                                   t-style="whiteText"> ShuleSoft Team</a>
                                                                                            <!--[if !mso]><!--></span>
                                                                                        <!--<![endif]--></multiline>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                <!----------------- End Button Center ----------------->
                                                            </table>

                                                        </td>
                                                    </tr>
                                                </table>

                                                <table width="400" border="0" cellpadding="0" cellspacing="0"
                                                       align="center" class="mobile" bgcolor="#ffffff" c-style="whiteBG"
                                                       object="drag-module-small">
                                                    <tr>
                                                        <td width="100%" valign="middle" align="center">

                                                            <table width="265" border="0" cellpadding="0"
                                                                   cellspacing="0" align="center"
                                                                   style="text-align: center; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;"
                                                                   class="fullCenter">
                                                                <tr>
                                                                    <td width="100%" height="50"></td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table><!-- End Second -->

                                                <table width="400" border="0" cellpadding="0" cellspacing="0"
                                                       align="center" class="mobile" object="drag-module-small">
                                                    <tr>
                                                        <td width="100%" valign="middle" align="center">

                                                            <table width="265" border="0" cellpadding="0"
                                                                   cellspacing="0" align="center"
                                                                   style="text-align: center; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;"
                                                                   class="fullCenter">
                                                                <tr>
                                                                    <td width="100%" height="40"></td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>

                                                <!-- CopyRight -->
                                                <table width="400" border="0" cellpadding="0" cellspacing="0"
                                                       align="center" class="full" object="drag-module-small">
                                                    <tr>
                                                        <td align="center" valign="middle" width="100%"
                                                            style="text-align: center; font-family: Helvetica, Arial, sans-serif; font-size: 13px; color: #000000; line-height: 24px;"
                                                            t-style="copyright" class="fullCenter" mc:edit="8">
                                                            <p object="text-editable"><!--[if !mso]><!--><span
                                                                    style="font-family: 'proxima_nova_rgregular', Helvetica; font-weight: normal;"><!--<![endif]--> &copy; Copyright 2016 INETS Co Ltd<br>
                                                                    <!--<![endif]--></span><!--[if !mso]><!-->

                                                                <!--[if !mso]><!--><span
                                                                    style="font-family: 'proxima_nova_rgregular', Helvetica;"><!--<![endif]-->
                                                                    <!--subscribe-->
<!--                                                                    <a href="#"-->
<!--                                                                                       style="text-decoration: none; color: #000000;"-->
<!--                                                                                       t-style="copyright"-->
<!--                                                                                       object="link-editable">Unsubscribe</a>-->
                                                                    <!--unsub--><!--[if !mso]><!--></span>
                                                                <!--<![endif]--></p>
                                                        </td>
                                                    </tr>
                                                </table><!-- End CopyRight -->

                                                <table width="400" border="0" cellpadding="0" cellspacing="0"
                                                       align="center" class="mobile" object="drag-module-small">
                                                    <tr>
                                                        <td align="center" width="100%" valign="middle">

                                                            <table width="265" border="0" cellpadding="0"
                                                                   cellspacing="0" align="center"
                                                                   style="text-align: center; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;"
                                                                   class="fullCenter">
                                                                <tr>
                                                                    <td width="100%" height="60"></td>
                                                                </tr>
                                                                <tr>
                                                                    <td width="100%" height="1"></td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>

                                        </td>
                                    </tr>
                                </table>

                            </div>
                        </td>
                    </tr>
                </table><!-- End Notification 1 -->

            </div>
        </div>
    </center>
</div>
</body>
</html>