<?php echo doctype("html5"); ?>
<html class="white-bg-login" lang="en">
    <head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
	<title>Sign in</title>

	<!-- bootstrap 3.0.2 -->
	<link href="<?php echo base_url('assets/bootstrap/bootstrap.min.css'); ?>" rel="stylesheet"  type="text/css">
	<!-- font Awesome -->
	<link href="<?php echo base_url('assets/fonts/font-awesome.css'); ?>" rel="stylesheet"  type="text/css">
	<!-- Style -->
	<link href="<?php echo base_url('assets/custom.css'); ?>" rel="stylesheet"  type="text/css">
	<link href="<?php echo base_url('assets/materialize.min.css'); ?>" rel="stylesheet"  type="text/css">
	<link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    </head>

    <body style="background:#F7F7F7;">

	<div id="wrapper">

	    <div id="login" class=" form">
		<section class="login_content">


		    <?php $this->load->view($subview); ?>
		    <div class="clearfix"></div>
		    <br/>
		    <div>
			<h1 class="text-center"><i class="fa fa-paw" style="font-size: 26px;"></i> ShuleSoft</h1>

			<p class="text-center">©2016 All Rights Reserved.</p>

		    </div>
		    <div class="text-capitalize  text-center">
			<?php
			echo anchor('termsandprivacy/index', 'Terms and Privacy');
			?>

		    </div>
		    <!--			</div>-->


		    <p align='right'> 
			<a href="http://www.inetstz.com" target="_blank"><img src="<?php echo base_url('/assets/images/inets.png'); ?>"  height="40" title="Owned by Inets Company Limited"/></a>
		    </p>
		</section>
	    </div>
	</div>



	<script type="text/javascript" src="<?php echo base_url('assets/shulesoft/jquery.js'); ?>"></script>
	<script type="text/javascript" src="<?php echo base_url('assets/bootstrap/bootstrap.min.js'); ?>"></script>
	<script type="text/javascript" src="<?php echo base_url('assets/materialize.min.js'); ?>"></script>
    </body>
</html>