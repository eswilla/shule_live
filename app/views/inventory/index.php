
<div class="box">
    <div class="box-header">
        <h3 class="box-title"><i class="fa icon-basket"></i> <?= $this->lang->line('panel_title') ?></h3>


        <ol class="breadcrumb">
            <li><a href="<?= base_url("dashboard/index") ?>"><i class="fa fa-laptop"></i> <?= $this->lang->line('menu_dashboard') ?></a></li>
            <li class="active"><?= $this->lang->line('menu_inventory') ?></li>
        </ol>
    </div><!-- /.box-header -->
    <!-- form start -->
    <div class="box-body">
        <div class="row">
            <div class="col-sm-12">

		<?php
		$usertype = $this->session->userdata("usertype");
		?>
		<h5 class="page-header">
		    <a class="btn btn-success" href="<?php echo base_url('inventory/add') ?>">
			<i class="fa fa-plus"></i> 
			<?= $this->lang->line('add_title') ?>
		    </a>
		</h5>

		<div class="col-sm-12">

		    <div class="nav-tabs-custom">



			<div class="tab-content">
			    <div id="all" class="tab-pane active">
				<div id="hide-table">
				    <table id="example1" class="table table-striped table-bordered table-hover dataTable no-footer">
					<thead>
					    <tr>
						<th class="col-sm-2"><?= $this->lang->line('slno') ?></th>
						<th class="col-sm-2"><?= $this->lang->line('item_name') ?></th>
						<th class="col-sm-2"><?= $this->lang->line('batch_number') ?></th>
						<th class="col-sm-2"><?= $this->lang->line('available_quantity') ?></th>
						<th class="col-sm-2"><?= $this->lang->line('price') ?></th>

						<th class="col-sm-2"><?= $this->lang->line('contact_person_name') ?></th>

						<th class="col-sm-2"></th>
					    </tr>
					</thead>
					<tbody>
					    <?php
					    if (count($items)) {
						$i = 1;
						foreach ($items as $item) {
						    ?>
						    <tr>
							<td data-title="<?= $this->lang->line('slno') ?>">
							    <?php echo $i; ?>
							</td>

							<td data-title="<?= $this->lang->line('name') ?>">
							    <?php
							    echo $item->name
							    ?>
							</td>
							<td data-title="<?= $this->lang->line('batch_number') ?>">
							    <?php echo $item->batch_number; ?>
							</td>

							<td data-title="<?= $this->lang->line('quantity') ?>">
							    <?php echo $item->quantity; ?>
							</td>
							<td><?= $item->price ?></td>
							<td data-title="<?= $this->lang->line('contact_person_name') ?>">
							    <?php echo $item->contact_person_name; ?>
							</td>
							<td data-title="<?= $this->lang->line('action') ?>">
							    <?php
							    echo btn_view('inventory/index/' . $item->item_id . "/" . $set, $this->lang->line('view'));
							    echo btn_edit('inventory/edit/' . $item->item_id . "/" . $set, $this->lang->line('edit'));
							    echo btn_delete('inventory/delete/' . $item->item_id . "/" . $set, $this->lang->line('delete'));
							    ?>
							</td>
						    </tr>
	<?php
	$i++;
    }
}
?>
					</tbody>
				    </table>
				</div>

			    </div>
			</div>
		    </div> <!-- nav-tabs-custom -->
		</div>

            </div> <!-- col-sm-12 -->
        </div><!-- row -->
    </div><!-- Body -->
</div><!-- /.box -->

<script type="text/javascript">
    $('#classesID').change(function () {
	var classesID = $(this).val();
	if (classesID == 0) {
	    $('#hide-table').hide();
	    $('.nav-tabs-custom').hide();
	} else {
	    $.ajax({
		type: 'POST',
		url: "<?= base_url('student/student_list') ?>",
		data: "id=" + classesID,
		dataType: "html",
		success: function (data) {
		    window.location.href = data;
		}
	    });
	}
    });
</script>