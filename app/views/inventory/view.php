
<div class="well">
    <div class="row">
	<div class="col-sm-6">
	    <button class="btn-cs btn-sm-cs" onclick="javascript:printDiv('printablediv')"><span class="fa fa-print"></span> <?= $this->lang->line('print') ?> </button>


	    <?php echo btn_sm_edit('inventory/edit/' . $item->item_id . "/" . $set, $this->lang->line('edit'))
	    ?>                                    
	</div>
	<div class="col-sm-6">
	    <ol class="breadcrumb">
		<li><a href="<?= base_url("dashboard/index") ?>"><i class="fa fa-laptop"></i> <?= $this->lang->line('menu_dashboard') ?></a></li>
		<li><a href="<?= base_url("inventory/view/$item->item_id") ?>"><?= $this->lang->line('menu_vendor') ?></a></li>
		<li class="active"><?= $this->lang->line('view') ?></li>
	    </ol>
	</div>
    </div>

</div>



<div id="printablediv">
    <section class="panel">

	<div class="profile-view-head">
	    <h1><?= $item->name ?></h1>
	    <p><?= $item->comments ?></p>

	</div>
	<div class="panel-body profile-view-dis">
	    <h1><?= $this->lang->line("vendor_information") ?></h1>
	    <div class="row">
		<div class="profile-view-tab">
		    <p><span><?= $this->lang->line("name") ?> </span>: <?= $item->name ?></p>
		</div>
		<div class="profile-view-tab">
		    <p><span><?= $this->lang->line("batch_number") ?> </span>: <?= $item->batch_number ?></p>
		</div>
		
		<div class="profile-view-tab">
		    <p><span><?= $this->lang->line("date_purchased") ?> </span>: <?= date("d M Y", strtotime($item->date_purchased)) ?></p>
		</div>
		<div class="profile-view-tab">
		    <p><span><?= $this->lang->line("created_at") ?> </span>: <?= date("d M Y", strtotime($item->created_at)) ?></p>
		</div>
		<div class="profile-view-tab">
		    <p><span><?= $this->lang->line("price") ?> </span>: <?= $item->price ?></p>
		</div>
		<div class="profile-view-tab">
		    <p><span><?= $this->lang->line("quantity") ?> </span>: <?= $item->quantity ?></p>
		</div>
		<div class="profile-view-tab">
		    <p><span><?= $this->lang->line("status") ?> </span>: <?php
			if ($item->status == 1) {
			    echo 'New';
			} else if ($item->status == 2) {
			    echo 'Used';
			} else {
			    echo 'Obsolete';
			}
			?></p>
		</div>
		
		<div class="profile-view-tab">
		    <p><span><?= $this->lang->line("depreciation") ?> </span>: <?= $item->depreciation ?></p>
		</div>
		
	
	    </div>

	    <h1><?= $this->lang->line("other_information") ?></h1>

	    <div class="row">
		<div class="profile-view-tab">
		    <p><span><?= $this->lang->line("contact_person_name") ?> </span>: <?= $item->contact_person_name ?></p>
		</div>
		<div class="profile-view-tab">
		    <p><span><?= $this->lang->line("contact_person_number") ?> </span>: <?= $item->contact_person_number ?></p>
		</div>
		
		<div class="profile-view-tab">
		    <p><span></span>: <a href="<?= base_url("vendor/view/$item->vendor_id") ?>" class="btn btn-info"><?= $this->lang->line("view_vendor_indo") ?></a></p>
		</div>


	    </div>


	</div>
    </section>
</div>

<script language="javascript" type="text/javascript">
    function printDiv(divID) {
	//Get the HTML of div
	var divElements = document.getElementById(divID).innerHTML;
	//Get the HTML of whole page
	var oldPage = document.body.innerHTML;

	//Reset the page's HTML with div's HTML only
	document.body.innerHTML =
		"<html><head><title></title></head><body>" +
		divElements + "</body>";

	//Print Page
	window.print();

	//Restore orignal HTML
	document.body.innerHTML = oldPage;
    }
    function closeWindow() {
	location.reload();
    }
</script>
