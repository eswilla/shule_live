
<div class="box">
    <div class="box-header">
        <h3 class="box-title"><i class="fa icon-invoice"></i> <?=$this->lang->line('account_title')?></h3>

        <ol class="breadcrumb">
            <li><a href="<?=base_url("dashboard/index")?>"><i class="fa fa-laptop"></i> <?=$this->lang->line('menu_dashboard')?></a></li>
            <li><a href="<?=base_url("invoice/index")?>"><?=$this->lang->line('menu_invoice')?></a></li>
            <li class="active"><?=$this->lang->line('menu_add')?> <?=$this->lang->line('menu_invoice')?></li>
        </ol>
    </div><!-- /.box-header -->
    <!-- form start -->
    <div class="box-body">
        <div class="row">
            <div class="col-sm-8">
                <form class="form-horizontal" role="form" method="post">
   <?php 
   if($type==2){
       
   }else {
                        if(form_error('from_date')) 
                            echo "<div class='form-group has-error' >";
                        else     
                            echo "<div class='form-group'>";
                    ?>
                        <label for="from_date" class="col-sm-2 control-label">
                         From
                        </label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" id="from_date" name="from_date" value="<?=set_value('from_date')?>" >
                        </div>
                        <span class="col-sm-4 control-label">
                            <?php echo form_error('from_date'); ?>
                        </span>
                    </div>
   <?php }?>
                    <?php 
                        if(form_error('to_date')) 
                            echo "<div class='form-group has-error'>";
                        else     
                            echo "<div class='form-group' >";
                    ?>
                        <label for="to_date" class="col-sm-2 control-label">
                           To
                        </label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" id="to_date" name="to_date" value="<?=set_value('to_date')?>" >
                        </div>
                        <span class="col-sm-4 control-label">
                            <?php echo form_error('to_date'); ?>
                        </span>
                    </div>

                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-8">
                            <input type="submit" class="btn btn-success" value="Submit" >
                        </div>
                    </div>

                </form>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
$(document).ready(function(){

    $("#from_date").click(function(){
        $("#from_date").pickadate({

            selectYears: 50,
            selectMonths: true,
            max:new Date()
        });
    });

});
$(document).ready(function(){
    $("#to_date").click(function(){
        $("#to_date").pickadate({

            selectYears: 50,
            selectMonths: true,
            max:new Date()
        });
    });

});
</script>