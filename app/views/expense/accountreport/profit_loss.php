
<?php
    $usertype = $this->session->userdata("usertype");
    if ($usertype == "Admin" || $usertype == "Accountant" || $usertype == "Student" || $usertype == "Parent") {
	?>
	<?php if ($usertype == "Admin" || $usertype == "Accountant") { ?>
	    <div class="well">
	        <div class="row">

	    	<div class="col-sm-6">
	    	    <button class="btn-cs btn-sm-cs" onclick="javascript:printDiv('printablediv')"><span class="fa fa-print"></span> <?= $this->lang->line('print') ?> </button>
		               
	    	</div>

	    	<div class="col-sm-6">
	    	    <ol class="breadcrumb">
	    		<li><a href="<?= base_url("dashboard/index") ?>"><i class="fa fa-laptop"></i> <?= $this->lang->line('menu_dashboard') ?></a></li>
	    		<li><a href="<?= base_url("invoice/index") ?>"><?= $this->lang->line('menu_invoice') ?></a></li>
	    		<li class="active"><?= $this->lang->line('view') ?></li>
	    	    </ol>
	    	</div>
	        </div>
	    </div>
	<?php } ?>
	<div id="printablediv">
          
                
          
	    <section class="content invoice" >
		<!-- title row -->
		<div class="row">
                    <div class="col-xs-1"></div>
		    <div class="col-xs-11">
			<h2 class="page-header">
			    <?php
			    if ($siteinfos->photo) {
				$array = array(
				    "src" => base_url('uploads/images/' . $siteinfos->photo),
				    'width' => '25px',
				    'height' => '25px',
				    'class' => 'img-circle'
				);
				echo img($array);
			    }
			    ?>
			</h2>
		    </div><!-- /.col -->
		</div>
		<!-- info row -->
                <div class="row invoice-info" style="margin-left: 3%">
		    <div class="col-sm-4 invoice-col">

			<?php echo $this->lang->line("invoice_from"); ?>
			<address>
			    <strong><?= $siteinfos->sname ?></strong><br>
			    <?= $siteinfos->address ?><br>
			    <?= $this->lang->line("invoice_phone") . " : " . $siteinfos->phone ?><br>
			    <?= $this->lang->line("invoice_email") . " : " . $siteinfos->email ?><br>
			</address>


		    </div><!-- /.col -->
		    <div class="col-sm-4 invoice-col">
		

		    </div><!-- /.col -->
		    <div class="col-sm-4 invoice-col">
			<b>INCOME STATEMENT</b><br>
                        Period:<?php echo  date("d M Y", strtotime($from_date)) . '<br/>';?>to &nbsp <?php echo  date("d M Y", strtotime($to_date)) . '<br/>';?>
		    </div><!-- /.col -->
		</div><!-- /.row -->

		<!-- Table row -->
		<br />
                <div class="row" style="margin-left: 3%">
		    <div class="col-xs-12" id="hide-table">                    
			<table class=" table">
                             <thead>                            
                              <tr>
				    <th class="col-lg-4">ACCOUNT</th>
				    <th class="col-lg-4">  </th>
				    <th class="col-lg-4">AMOUNT(TZS)</th>
				</tr>       
                         
				
			    </thead>
			    <tbody>
                                <?php
$expense_value=$this->db->query('select a.*,b."name" from ' . set_schema_name() . 'expense a JOIN ' . set_schema_name() . 'financial_category b ON a."categoryID"=b."categoryID" WHERE a."create_date" between '."'$from_date'".' AND '."'$to_date'".' and a."categoryID"=2')->result(); 
?>
                              <?php
         echo '<tr  class="info"><td>'.$expense_value[0]->name.'</td><td> </td><td></td></tr>'; 
                                $sum_revenue=0; $i=0;
                                foreach ($expense_value as $value) {                               
                                 $sum_revenue=$sum_revenue+$value->amount;?>                            
				<tr>
                                    <td data-title="<?=$value->expense?>">
					<?php echo $value->expense ?>
				    </td>
				    <td data-title="<?= $this->lang->line('slno') ?>">			
				    </td>				   
                                    <td data-title="<?=$value->amount?>" >&nbsp;
					<?php echo $value->amount; ?>
				    </td>
				</tr>
                                  <?php }?>
                                
                                <?php
                         $feetype=$this->db->query('select "feetypeID",feetype from ' . set_schema_name() . 'feetype')->result();  
                      
                       
                         foreach ($feetype as $fee) {
                          
                             ?>
                                <?php
                          
                $fee_total=$this->db->query('select sum(amount::float) as total from ' . set_schema_name() . 'invoice where "feetypeID"='.$fee->feetypeID.'')->row();
              
                
                $sum_revenue=$sum_revenue + $fee_total->total
                     ?>
                     <tr>
                            
                                    <td data-title="<?=$fee->feetype?>">
					<?php echo $fee->feetype?>
				    </td>
				    <td data-title="<?= $this->lang->line('slno') ?>">			
				    </td>				   
                                    <td data-title="<?=$fee->total?>" >&nbsp;
					<?php echo $fee_total->total; ?>
				    </td>
				</tr>           
                                
                                
                         <?php } ?>
                                
               <tr><td>Total <?=$expense_value[0]->name ?></td> <td></td><td>&nbsp<?=$sum_revenue?></td></tr>
                                
                                
                                                          <?php
$expense_operational=$this->db->query('select a.*,b."name" from ' . set_schema_name() . 'expense a JOIN ' . set_schema_name() . 'financial_category b ON a."categoryID"=b."categoryID" WHERE a."create_date" between '."'$from_date'".' AND '."'$to_date'".' and a."categoryID"=3')->result(); ?>
                              <?php
                           echo '<tr  class="info"><td>'.$expense_operational[0]->name.'</td><td> </td><td></td></tr>'; 
                                $sum_operational=0; 
                                foreach ($expense_operational as $value) {
                           
                                    
                                 $sum_operational=$sum_operational+$value->amount;
                               ?>                            
				<tr>
                                    <td data-title="<?=$value->expense?>">
					<?php echo $value->expense ?>
				    </td>
				    <td data-title="<?= $this->lang->line('slno') ?>">			
				    </td>				   
                                    <td data-title="<?=$value->amount?>">&nbsp;
					<?php echo $value->amount; ?>
				    </td>
				</tr>
                                  <?php }?>
                                <tr><td>Total <?=$expense_operational[0]->name ?></td> <td></td><td>&nbsp<?=$sum_operational?></td></tr>
                                
       <tbody>
                               
                                <tr><td>Operating Income</td><td>      </td><td>&nbsp<?=$sum_revenue-$sum_operational;?></td></tr>
			    </tbody>                                          
                                                          <?php
$expense_general=$this->db->query('select a.*,b."name" from ' . set_schema_name() . 'expense a JOIN ' . set_schema_name() . 'financial_category b ON a."categoryID"=b."categoryID" WHERE a."create_date" between '."'$from_date'".' AND '."'$to_date'".' and a."categoryID"=4')->result(); ?>
                              <?php
                           echo '<tr  class="info"><td> General & administrative Expenses</td><td> </td><td></td></tr>'; 
                                $sum_general=0; 
                                foreach ($expense_general as $value) {
                           
                                    
                                 $sum_operational=$sum_general+$value->amount;
                               ?>                            
				<tr>
                                    <td data-title="<?=$value->expense?>">
					<?php echo $value->expense ?>
				    </td>
				    <td data-title="<?= $this->lang->line('slno') ?>">			
				    </td>				   
                                    <td data-title="<?=$value->amount?>">&nbsp;
					<?php echo $value->amount; ?>
				    </td>
				</tr>
                                  <?php }?>
                                <tr><td>Total General & administrative Expenses</td> <td></td><td>&nbsp<?=$sum_general?></td></tr>
                                
                             <table class="table">
                           
			
			</table>   
                                
			    </tbody>
			</table>
                         
		    </div>
                    
                      <div class="col-xs-12" id="hide-table">
                                     
			<table class="table">
                           
			    <tbody>
                               
                                <tr><td>Profit/(Loss)</td><td>      </td><td>&nbsp<?=$sum_revenue-($sum_general+$sum_operational);?></td></tr>
			    </tbody>
			</table>
                      
		    </div>
                    
                    
                    
		</div><!-- /.row -->
	    </section><!-- /.content -->
	</div>

 
	<!-- email modal starts here -->
	<form class="form-horizontal" role="form" action="<?= base_url('teacher/send_mail'); ?>" method="post">
	    <div class="modal fade" id="mail">
		<div class="modal-dialog">
		    <div class="modal-content">
			<div class="modal-header">
			    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
			    <h4 class="modal-title"><?= $this->lang->line('mail') ?></h4>
			</div>
			<div class="modal-body">

	<?php
	if (form_error('to'))
	    echo "<div class='form-group has-error' >";
	else
	    echo "<div class='form-group' >";
	?>
			    <label for="to" class="col-sm-2 control-label">
			    <?= $this->lang->line("to") ?>
			    </label>
			    <div class="col-sm-6">
				<input type="email" class="form-control" id="to" name="to" value="<?= set_value('to') ?>" >
			    </div>
			    <span class="col-sm-4 control-label" id="to_error">
			    </span>
			</div>

	<?php
	if (form_error('subject'))
	    echo "<div class='form-group has-error' >";
	else
	    echo "<div class='form-group' >";
	?>
			<label for="subject" class="col-sm-2 control-label">
			<?= $this->lang->line("subject") ?>
			</label>
			<div class="col-sm-6">
			    <input type="text" class="form-control" id="subject" name="subject" value="<?= set_value('subject') ?>" >
			</div>
			<span class="col-sm-4 control-label" id="subject_error">
			</span>

		    </div>

	<?php
	if (form_error('message'))
	    echo "<div class='form-group has-error' >";
	else
	    echo "<div class='form-group' >";
	?>
		    <label for="message" class="col-sm-2 control-label">
		    <?= $this->lang->line("message") ?>
		    </label>
		    <div class="col-sm-6">
			<textarea class="form-control" id="message" name="message" style="resize: vertical;" value="<?= set_value('message') ?>" ></textarea>
		    </div>
		</div>


	    </div>
	    <div class="modal-footer">
		<button type="button" class="btn btn-default" style="margin-bottom:0px;" data-dismiss="modal"><?= $this->lang->line('close') ?></button>
		<input type="button" id="send_pdf" class="btn btn-success" value="<?= $this->lang->line("send") ?>" />
	    </div>
	</div>
	</div>
	</div>
	</form>
	<!-- email end here -->
	<script language="javascript" type="text/javascript">
	    function printDiv(divID) {
		//Get the HTML of div
		var divElements = document.getElementById(divID).innerHTML;
		//Get the HTML of whole page
		var oldPage = document.body.innerHTML;

		//Reset the page's HTML with div's HTML only
		document.body.innerHTML =
			"<html><head><title></title></head><body>" +
			divElements + "</body>";

		//Print Page
		window.print();

		//Restore orignal HTML
		document.body.innerHTML = oldPage;
	    }
	    function closeWindow() {
		location.reload();
	    }

	 

	</script>
    <?php }

?>
