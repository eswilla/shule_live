<?php
$user = empty($usertype) ? 'Administrator' : $usertype;
?>

<!DOCTYPE html>
<html>
    <head>
        <title>ShuleSoft Terms & Privacy</title>
	<?php
	$css = base_url() . "/assets/manual_assets/";
	$js = base_url() . "/assets/manual_assets/";
	?>
        <meta charset="utf-8">
	<!--        <link rel="canonical" href="http://www.templatemonster.com/help/quick-start-guide/website-templates/responsive-website-templates-v1-1/index_en.html"/> -->
        <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/3.2.1/css/font-awesome.min.css" media="all" rel="stylesheet" type="text/css"> 
        <link rel="stylesheet" href="<?= $css ?>css/bootstrap.css" media="screen">
        <link rel="stylesheet" href="<?= $css ?>css/responsive.css" media="screen">
        <link rel="stylesheet" href="<?= $css ?>css/prettify.css"  media="screen">
        <link rel="stylesheet" href="<?= $css ?>css/prettyPhoto.css"  media="screen">
        <link rel="stylesheet" href="<?= $css ?>css/idea.css"  media="screen">
        <link rel="stylesheet" href="<?= $css ?>css/style.css" media="screen">
        <link rel="stylesheet" href="<?= $css ?>css/contact-form.css" media="screen">
        <script src="<?= $js ?>js/jquery.js"></script>
        <script src="<?= $js ?>js/jquery.scrollTo.js"></script>
        <script src="<?= $js ?>js/jquery-migrate-1.1.0.js"></script>
        <script src="<?= $js ?>js/prettify.js"></script>
        <script src="<?= $js ?>js/bootstrap-affix.js"></script>
        <script src="<?= $js ?>js/jquery.prettyPhoto.js"></script> 

        <script src="<?= $js ?>js/TMForm.js"></script>
        <script src="<?= $js ?>js/modal.js"></script>  
        <script src="<?= $js ?>js/bootstrap-filestyle.js"></script>    

        <script src="<?= $js ?>js/sForm.js"></script>  
        <script src="<?= $js ?>js/jquery.countdown.min.js"></script>
        <script src="<?php echo $js ?>js/scripts.js"></script>
        <!--[if IE 8]>
        <link rel="stylesheet" href="css/ie.css" />
      <![endif]-->

        <!--[if lt IE 9]>
                    <div style='text-align:center'><a href="http://www.microsoft.com/windows/internet-explorer/default.aspx?ocid=ie6_countdown_bannercode"><img src="http://www.theie6countdown.com/img/upgrade.jpg"border="0"alt=""/></a></div>  
              <![endif]-->
        <!--[if lt IE 9]>
          <script src="js/html5shiv.js"></script>
        <![endif]-->
    </head>

    <body style="background:#F7F7F7;">  

        <!-- header
        ================================================== -->
        <header class="header" style=" float: left; background: #EDEDED; border-bottom: 1px solid #D9DEE4;
    margin-bottom: 10px;
    width: 100%;
    position: relative;">
            <div class="container">
                <div class="row">
                    <article class="span4"><a href="<?= base_url() ?>">ShuleSoft</a></article>
                    <article class="span6">
                        <h1>
			    <i class="fa fa-book"></i> 
			  Terms & Privacy
			</h1>
                    </article> 
                    <article class="span2">
			<!--                        <div id="languages" class="select-menu pull-right">
						    <div class="select-menu_icon">
							<b>En</b>
							<i class="icon-angle-down"></i>
						    </div>
						    <ul class="select-menu_list">
			
						    </ul>
						</div>-->
                        <div id="versions" class="select-menu pull-right">
                            <div class="select-menu_icon"><b>v1-0</b><i class="icon-angle-down"></i></div>
			    <!--                            <ul class="select-menu_list">
							    <li><a href="/help/quick-start-guide/website-templates/responsive-website-templates-v1-0/index_en.html"><span>v1-0</span></a></li>
							    <li class="active"><a href="/help/quick-start-guide/website-templates/responsive-website-templates-v1-1/index_en.html"><span>v1-1</span></a></li>
							    <li class="active"><a href="/help/quick-start-guide/website-templates/responsive-website-templates-v1-2/"><span>v1-2</span></a></li>
							</ul>-->
                        </div>
                    </article>   
                </div>    
            </div>
        </header>
        <div id="content"> 
            <div class="bg-content-top">
                <div class="container">

                    <!-- Docs nav
                        ================================================== -->
                    <div class="row">
                        <div class="span3"> 

                            <div id="nav_container">
                                <a href="javascript:;" id="affect_all">
                                    <span class="expand">
                                        <i class="icon-angle-down"></i>
                                    </span>            
                                    <span class="close">
                                        <i class="icon-angle-up"></i>
                                    </span>
                                </a>
                                <div class="clearfix"></div>    
                                <ul class="nav nav-list bs-docs-sidenav"  id="nav">
                                    <li class="nav-item item1">
                                        <dl class="slide-down">
                                            <dt><a href="#introduction" class="icon-info-sign">Introduction</a></dt>
                                            <dd></dd>
                                        </dl>   
                                    </li>
                                    <li class="nav-item item2">
                                        <dl class="slide-down">
                                            <dt><a href="#modules" class="icon-check">Information we Collect</a></dt>  
                                            <dd></dd>                       
                                        </dl> 
                                    </li>           
                                    <li class="nav-item item3">
                                        <dl class="slide-down">
                                            <dt><a href="#general-information" class="icon-play">Access</a></dt>
                                            <dd></dd>
                                        </dl> 
                                    </li> 
                                    
				    <li class="nav-item item5">
                                        <dl class="slide-down">
                                            <dt><a href="#attendance" title="attendance" class="icon-play">Security</a></dt>
                                            <dd></dd>
                                        </dl> 
                                    </li>
				    <li class="nav-item item6">
                                        <dl class="slide-down">
                                            <dt><a href="#Promotion" title="Promotion" class="icon-play">Changes to this Privacy Policy</a></dt>
                                            <dd></dd>
                                        </dl> 
                                    </li>
				    <li class="nav-item item7">
                                        <dl class="slide-down">
                                            <dt><a href="#mail_sms" title="Mail and SMS" class="icon-inbox">Your Acceptance of these Terms</a></dt>
                                            <dd></dd>
                                        </dl> 
                                    </li>
				    
                                </ul>
                            </div>
                        </div> 


                        <div class="span9">

                            <!-- box-content
                            ================================================== -->
                            <div class="box-content">
                                <!-- block-started
                                ================================================== -->
                                <section class="block-started"  id="introduction">
                                    <h2 class="item1"><i class="icon-info-sign"></i> Introduction <small>
					  </small></h2>
				    <p>
		This Privacy Policy governs the manner in which ShuleSoft collects, uses, maintains and discloses information collected from users (each, a "User") of the www.shulesoft.com  website ("Site") and all its affiliates (“sub-domains”). </p>
	    <p> We believe your business is no one else’s. Your privacy is important to you and to us. So we will protect the information you share with us. To protect your privacy, ShuleSoft follows different principles in accordance with world-wide practices for customer privacy and data protection.</p>
	    <ul>
		<li>	We will not sell or give away your name, mail address, phone number, email address, credit card number or any other information to anyone.</li>
		<li>We will use state-of-the-art security measures to protect your information from unauthorized users.</li>
	    </ul>

                                </section>

                                <section class="block-templates" id="modules">  
                                    <h2 class="item2"><i class="icon-check"></i>Information we Collect</h2>

                                   <p>ShuleSoft and its sub-domains may collect the following information from and about you:
		Personal Information</p>
				   <p> "Personal Information" for purposes of this Privacy Policy is the information that identifies you as an individual or relates to an identifiable person, such as name, postal address (including billing addresses), telephone number, email address, credit or debit card number, or profile picture.</p>
				   <h4>Personal Information from You</h4>
				   <p> We collect Personal Information when, for example, you are registered for or subscribe to certain products or services; create a profile; take a survey; visit our site and its sub-domains; use an interactive feature (such as Chat or Email); participate in a community forum; or contact customer service.</p>
				   <h4>Personal Information We Receive from Third Parties</h4>
				   <p> We may receive Personal Information about you from third parties, such as social media services, commercially available sources, ShuleSoft sub-domains, business partners, and, if applicable to you, the third party provider of your subscription. The Personal Information we receive from third parties may include, for example, your name, contact information, information about your transactions, purchase history, or relationships with various product and service providers, and your use of certain applications. For example, if you access a ShuleSoft site and/or its sub-domains through a social media service or connect a ShuleSoft site and/or its sub-domains to a social media service, the information we collect may also include your user name associated with that social media service, any information or content the social media service has the right to share with us, such as your profile picture, email address or friends list, and any information you have made public in connection with that social media service. When you access the ShuleSoft site and/or its sub-domains through social media services or when you connect a ShuleSoft site and/or its sub-domains to social media services, you are authorizing ShuleSoft to collect, store, and use such information and content in accordance with this Privacy Policy.</p>
                                </section>

                                <section class="block-templates" id="general-information">  
                                    <h2 class="item3"><i class="icon-play"></i>ShuleSoft Access</h2>


                                    <p>The following are shulesoft access level. </p>

				    <section id="features">
					
	    We will provide you with the means to ensure that your personal information is correct and current. You may review and update this information at any time  at www.shulesoft.com  and/or its sub-domains. There, you can:
	    <ol>
		<li>View and edit personal information you have already given us.</li>
		<li>Tell us whether you want us to send you marketing information.</li>
		<li>Once you register, you won’t need to do it again. Wherever you go on www.shulesoft.com  your information stays with you. </li>
	    </ol>
				    </section>
                                </section>


                                <section class="block-templates" id="attendance">
                                    <h2 class="item5"><i class="icon-play-circle"></i> Security Measures</h2>
				    <section id="attendance">
   ShuleSoft has taken strong measures to protect the security of your personal information and to ensure that your choices for its intended use are honored. We take strong precautions to protect your data from loss, misuse, unauthorized access or disclosure, alteration, or destruction.
	    Your personal information is never shared outside the company without your permission, except under conditions explained above. Inside the company, data is stored in password-controlled servers with limited access. 
	    You also have a significant role in protecting your information. No one can see or edit your personal information without knowing your user name and password, so do not share these with others.
				    </section>
				</section>

				<section class="block-templates" id="Promotion">
				    <h2 class="item6"> <i class="icon-play-circle"></i> Changes to this Privacy Policy</h2>	
				    <section>
					ShuleSoft has the discretion to update this privacy policy at any time. When we do, we will revise the updated date at the bottom of this page. We encourage Users to frequently check this page for any changes to stay informed about how we are helping to protect the personal information we collect. You acknowledge and agree that it is your responsibility to review this privacy policy periodically and become aware of modifications.
				    </section>
				</section>


				<section class="block-templates" id="mail_sms">
				    <h2 class="item7"><i class="icon-inbox"></i>  Your Acceptance of these Terms</h2>
				    <section>

					
	    By using this Site and/or its sub-domains, you signify your acceptance of this policy and terms of service. If you do not agree to this policy, please do not use our Site and/or its sub-domains. Your continued use of the Site following the posting of changes to this policy will be deemed your acceptance of those changes.
				    </section>
				</section>
				
				
                            </div>
                        </div>
                    </div>

                </div>
		<footer id="conclusion">
		    <div class="container">        
			@ <a href="http://www.inetstz.com" target="_blank">ShuleSoft By INETS Company Limited </a>
		    </div>
		</footer>
            </div>
        </div>
        <!-- Footer
         ================================================== -->


        <div id="back-top">
            <a href="#"><i class="icon-double-angle-up"></i></a>
        </div>
<!-- <script src="js/bootstrap-scrollspy.js"></script>  -->
	<!--        <div class="language-modal">
		    <div class="modal_bg"></div>
		    <div class="modal">
			<div class="modal-header">
			    <span class="modal_remove pull-right"><i class="icon-remove"></i></span>
			    <h3>Choose language</h3>
			</div>
			<div class="modal-body">
			    <ul id="modal_languages" class="nav nav-list bs-docs-sidenav"></ul>
			</div>
		    </div>
		</div>-->
    </body> 
</html>
