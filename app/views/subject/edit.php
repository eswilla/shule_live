
<div class="box">
    <div class="box-header">
        <h3 class="box-title"><i class="fa icon-subject"></i> <?= $this->lang->line('panel_title') ?></h3>

        <ol class="breadcrumb">
            <li><a href="<?= base_url("dashboard/index") ?>"><i class="fa fa-laptop"></i> <?= $this->lang->line('menu_dashboard') ?></a></li>
            <li><a href="<?= base_url("subject/index/$set") ?>"><?= $this->lang->line('menu_subject') ?></a></li>
            <li class="active"><?= $this->lang->line('menu_edit') ?> <?= $this->lang->line('menu_subject') ?></li>
        </ol>
    </div><!-- /.box-header -->
    <!-- form start -->
    <div class="box-body">
        <div class="row">
            <div class="col-sm-8">
                <form class="form-horizontal" role="form" method="post">
		    <?php
		    if (form_error('classesID'))
			echo "<div class='form-group has-error' >";
		    else
			echo "<div class='form-group' >";
		    ?>
		    <label for="classesID" class="col-sm-2 control-label">
			<?= $this->lang->line("subject_class_name") ?>
		    </label>
		    <div class="col-sm-6">

			<?php
			$array = array();
			$array[0] = $this->lang->line("subject_select_class");
			foreach ($classes as $classa) {
			    $array[$classa->classesID] = $classa->classes;
			}
			echo form_dropdown("classesID", $array, set_value("classesID", $subject->classesID), "id='classesID' class='form-control'");
			?>
		    </div>
		    <span class="col-sm-4 control-label">
			<?php echo form_error('classesID'); ?>
		    </span>
	    </div>

	    <?php
	    if (form_error('sectionID'))
		echo "<div class='form-group has-error' >";
	    else
		echo "<div class='form-group' >";
	    ?>
	    <label for="classesID" class="col-sm-2 control-label">
		<?= $this->lang->line("subject_select_section") ?>
	    </label>
	    <div class="col-sm-6">
		<?php
		if (!empty($sections)) {
		    echo "<input type=\"checkbox\" id='checkboxs' onclick='check_all()'>", $this->lang->line("select_all"), "</option>";
		    $arr = [];
		    foreach ($subject_sections as $subject_section) {
			array_push($arr, $subject_section->section_id);
		    }
		    foreach ($sections as $value) {
			$checked = in_array($value->sectionID, $arr) ? 'checked' : '';
			echo "&nbsp;&nbsp; <input type=\"checkbox\" class='check'  name=\"sectionID[]\"  " . $checked . "  value=\"$value->sectionID\">", $value->section, "";
		    }
		} else {
		    echo '<span id="sectionID">';

		    echo '</span>';
		}
		?>
	    </div>
	    <span class="col-sm-4 control-label">
		<?php echo form_error('sectionID'); ?>
	    </span>
	</div>
	<?php
	if (form_error('teacherID'))
	    echo "<div class='form-group has-error' >";
	else
	    echo "<div class='form-group' >";
	?>
	<label for="teacherID" class="col-sm-2 control-label">
	    <?= $this->lang->line("subject_teacher_name") ?>
	</label>
	<div class="col-sm-6">
	    <?php
	    $array = array();
	    $array[0] = $this->lang->line("subject_select_teacher");
	    foreach ($teachers as $teacher) {
		$array[$teacher->teacherID] = $teacher->name;
	    }
	    echo form_dropdown("teacherID", $array, set_value("teacherID", $subject->teacherID), "id='teacherID' class='form-control'");
	    ?>
	</div>
	<span class="col-sm-4 control-label">
	    <?php echo form_error('teacherID'); ?>
	</span>
    </div>

    <?php
    if (form_error('subject'))
	echo "<div class='form-group has-error' >";
    else
	echo "<div class='form-group' >";
    ?>
    <label for="subject" class="col-sm-2 control-label">
	<?= $this->lang->line("subject_name") ?>
    </label>
    <div class="col-sm-6">
	<input type="text" class="form-control" id="subject" name="subject" value="<?= set_value('subject', $subject->subject) ?>" >
    </div>
    <span class="col-sm-4 control-label">
	<?php echo form_error('subject'); ?>
    </span>
</div>

<?php
if (form_error('subject_author'))
    echo "<div class='form-group has-error' >";
else
    echo "<div class='form-group' >";
?>
<label for="subject_author" class="col-sm-2 control-label">
    <?= $this->lang->line("subject_author") ?>
</label>
<div class="col-sm-6">

    <select name="subject_author" id="subject_author" class="form-control">
	<option value="Core">Core</option>
	<option value="Option">Option</option>
    </select>
</div>
<span class="col-sm-4 control-label">
    <?php echo form_error('subject_author'); ?>
</span>

</div>

<?php
if (form_error('subject_code'))
    echo "<div class='form-group has-error' >";
else
    echo "<div class='form-group' >";
?>
<label for="subject_code" class="col-sm-2 control-label">
    <?= $this->lang->line("subject_code") ?>
</label>
<div class="col-sm-6">
    <input type="text" class="form-control" id="subject_code" name="subject_code" value="<?= set_value('subject_code', $subject->subject_code) ?>" >
</div>
<span class="col-sm-4 control-label">
    <?php echo form_error('subject_code'); ?>
</span>
</div>
<a class="btn btn-app" onclick="$('#advanced_settings').toggle()">
    <i class="fa fa-edit"></i> Advanced
</a>
<div id="advanced_settings" style="display: none;">
    <?php
    if (form_error('is_counted'))
	echo "<div class='form-group has-error' >";
    else
	echo "<div class='form-group' >";
    ?>
    <label for="is_counted" class="col-sm-2 control-label">
	Is Counted ?
    </label>
    <div class="col-sm-6">
	YES <input type="radio" id="is_counted" name="is_counted" value="1" <?= $subject->is_counted == 1 ? 'checked="checked"' : '' ?>>
	&nbsp;&nbsp; NO <input type="radio"  id="is_counted" name="is_counted" value="0" <?= ($subject->is_counted == 0 || $subject->is_counted == '') ? 'checked="checked"' : '' ?>>
    </div>
    <span class="col-sm-4 control-label">
	<?php echo form_error('is_counted'); ?>
    </span>
</div>


<?php
if (form_error('is_penalty'))
    echo "<div class='form-group has-error' >";
else
    echo "<div class='form-group' >";
?>
<label for="is_penalty" class="col-sm-2 control-label">
    Has penalty ?
</label>
<div class="col-sm-6">
    YES <input type="radio" class="is_penalty" name="is_penalty" value="1" <?= $subject->is_penalty == 1 ? 'checked="checked"' : '' ?>>
    &nbsp;&nbsp; NO <input type="radio"  class="is_penalty" name="is_penalty" value="0" <?= ($subject->is_penalty == 0 || $subject->is_penalty == '') ? 'checked="checked"' : '' ?>>
</div>
<span class="col-sm-4 control-label">
    <?php echo form_error('is_penalty'); ?>
</span>
</div>
<div id="penalty_pass_mark" <?= $subject->is_penalty == 1 ? '' : 'style="display: none;"' ?>>
    <?php
    if (form_error('pass_mark'))
	echo "<div class='form-group has-error' >";
    else
	echo "<div class='form-group' >";
    ?>
    <label for="subject_code" class="col-sm-2 control-label">
	Penalty Pass Mark
    </label>
    <div class="col-sm-6">
	<input type="number" class="form-control" id="pass_mark" name="pass_mark" value="<?= set_value('pass_mark', $subject->pass_mark) ?>" >
    </div>
    <span class="col-sm-4 control-label">
	<?php echo form_error('pass_mark'); ?>
    </span>
</div>
</div>
</div>
<div class="form-group">
    <div class="col-sm-offset-2 col-sm-8">
	<input type="submit" class="btn btn-success" id="submit_button" value="<?= $this->lang->line("update_subject") ?>" >
    </div>
</div>
</form>

</div> <!-- col-sm-8 -->
</div><!-- row -->
</div><!-- Body -->
</div><!-- /.box -->




<script>
    $('.is_penalty').change(function () {
	var result = $('.is_penalty:checked').map(function () {
	    return this.value;
	}).get();
	if (result == '1') {
	    $('#penalty_pass_mark').show();
	    var pass_mark = $('#pass_mark').val();
	    if (pass_mark == '') {
		$('#submit_button').attr('disabled', true);
	    }
	} else {
	    $('#penalty_pass_mark').hide();
	    $('#submit_button').attr('disabled', false);
	}
    });

    $('#pass_mark').keyup(function () {
	var val = $(this).val();
	if (val !== '') {
	    $('#submit_button').attr('disabled', false);
	}else{
	    $('#submit_button').attr('disabled', true);
	}
    });

    $('#classesID').change(function (event) {
	var classesID = $(this).val();
	if (classesID === '0') {
	    $('#classesID').val(0);
	} else {
	    $.ajax({
		type: 'POST',
		url: "<?= base_url('subject/sectioncall') ?>",
		data: "id=" + classesID,
		dataType: "html",
		success: function (data) {
		    $('#sectionID').html(data);
		}
	    });
	}
    });

    function check_all() {
	$('.check').prop('checked', true);
    }
    ;
</script>

