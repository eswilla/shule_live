
<div class="box">
    <div class="box-header">
        <h3 class="box-title"><i class="fa icon-feetype"></i> <?=$this->lang->line('panel_title')?></h3>

        <ol class="breadcrumb">
            <li><a href="<?=base_url("dashboard/index")?>"><i class="fa fa-laptop"></i> <?=$this->lang->line('menu_dashboard')?></a></li>
            <li><a href="<?=base_url("feetype/index")?>"><?=$this->lang->line('menu_feetype')?></a></li>
            <li class="active"><?=$this->lang->line('menu_add')?> <?=$this->lang->line('menu_feetype')?></li>
        </ol>
    </div><!-- /.box-header -->
    <!-- form start -->
    <div class="box-body">
        <div class="row">
            <div class="col-sm-8">
                <form class="form-horizontal" role="form" method="post">

                    <?php 
                        if(form_error('feetype')) 
                            echo "<div class='form-group has-error' >";
                        else     
                            echo "<div class='form-group' >";
                    ?>
                        <label for="feetype" class="col-sm-2 control-label">
                            <?=$this->lang->line("feetype_name")?>
                        </label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" id="feetype" name="feetype" value="<?=set_value('feetype')?>" >
                        </div>
                        <span class="col-sm-4 control-label">
                            <?php echo form_error('feetype'); ?>
                        </span>
                    </div>
            
                 
                        <?php 
                        if(form_error('bank_account_id')) 
                            echo "<div class='form-group has-error' >";
                        else     
                            echo "<div class='form-group' >";
                    ?>
                        <label for="feetpeAccount" class="col-sm-2 control-label">
                            <?=$this->lang->line("feetype_account")?>
                        </label>
                        <div class="col-sm-6">

                            <?php
                                $array = array('0' => $this->lang->line("feetpype_select_account"));
                                foreach ($account_numbers as $bank_account) {
                                    $array[$bank_account->bank_account_id] = $bank_account->account_number;
                                }
                                echo form_dropdown("bank_account_id", $array, set_value("bank_account_id"), "id='bank_account_id' class='form-control'");
                            ?>
                        </div>
                        <span class="col-sm-4 control-label">
                            <?php echo form_error('bank_account_id'); ?>
                        </span>
                    </div>  
        
        
                                <?php 
                        if(form_error('feetypecategory')) 
                            echo "<div class='form-group has-error' >";
                        else     
                            echo "<div class='form-group' >";
                    ?>
                        <label for="feetypecategory" class="col-sm-2 control-label">
                            <?=$this->lang->line("feetypecategory")?>
                        </label>
                        <div class="col-sm-6">

                            <?php
                                $array = array('0' => $this->lang->line("feetpype_select_category"));
                                foreach ($feetype_category as $fee_category) {
                                    $array[$fee_category->feetype_categoryID] = $fee_category->feetype_name;
                                }
                                echo form_dropdown("feetype_categoryID", $array, set_value("feetype_categoryID"), "id='feetype_categoryID' class='form-control'"); ?>
                            
                        </div>
                        <span class="col-sm-4 control-label">
                            <?php echo form_error('feetypecategory'); ?>
                        </span>
                    </div> 
        
            

	    <?php 
                        if(form_error('amount')) 
                            echo "<div class='form-group has-error' >";
                        else     
                            echo "<div class='form-group' >";
                    ?>
                        <label for="amount" class="col-sm-2 control-label">
                            <?=$this->lang->line("feetype_amount")?>
                        </label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" id="feetype" name="amount" value="<?=set_value('amount')?>" >
                        </div>
                        <span class="col-sm-4 control-label">
                            <?php echo form_error('amount'); ?>
                        </span>
                    </div>
	
	
	<?php 
                        if(form_error('is_repeative')) 
                            echo "<div class='form-group has-error' >";
                        else     
                            echo "<div class='form-group' >";
                    ?>
                        <label for="feetype" class="col-sm-2 control-label">
                            <?=$this->lang->line("feetype_is_repeative")?>
                        </label>
                        <div class="col-sm-6">
                            <input type="radio" class="radio-inline" id="feetype" name="is_repeative" value="1" ><?=$this->lang->line("feetype_is_repeative_yes")?>
			    <input type="radio" class="radio-inline" id="feetype" name="is_repeative" value="0" ><?=$this->lang->line("feetype_is_repeative_no")?>
                        </div>
                        <span class="col-sm-4 control-label">
                            <?php echo form_error('is_repeative'); ?>
                        </span>
                    </div>
    
    <?php 
                        if(form_error('startdate')) 
                            echo "<div class='form-group has-error' >";
                        else     
                            echo "<div class='form-group' >";
                    ?>
                        <label for="feetype" class="col-sm-2 control-label">
                            <?=$this->lang->line("feetype_startdate")?>
                        </label>
                        <div class="col-sm-6">
                             <input type="text" class="form-control" id="startdate" name="startdate" value="<?=set_value('startdate')?>" >                   	    
                        </div>
                        <span class="col-sm-4 control-label">
                            <?php echo form_error('startdate'); ?>
                        </span>
                    </div>
    
<?php 
                        if(form_error('enddate')) 
                            echo "<div class='form-group has-error' >";
                        else     
                            echo "<div class='form-group' >";
                    ?>
                        <label for="feetype" class="col-sm-2 control-label">
                            <?=$this->lang->line("feetype_enddate")?>
                        </label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" id="enddate" name="enddate" value="<?=set_value('enddate')?>" >                   	    
                        </div>
                        <span class="col-sm-4 control-label">
                            <?php echo form_error('enddate'); ?>
                        </span>
                    </div>
    
    
                    <?php 
                        if(form_error('note')) 
                            echo "<div class='form-group has-error' >";
                        else     
                            echo "<div class='form-group' >";
                    ?>
                        <label for="note" class="col-sm-2 control-label">
                            <?=$this->lang->line("feetype_note")?>
                        </label>
                        <div class="col-sm-6">
                            <textarea class="form-control" style="resize:none;" id="note" name="note"><?=set_value('note')?></textarea>
                        </div>
                        <span class="col-sm-4 control-label">
                            <?php echo form_error('note'); ?>
                        </span>
                    </div>

                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-8">
                            <input type="submit" class="btn btn-success" value="<?=$this->lang->line("add_feetype")?>" >
                        </div>
                    </div>

                </form>

            </div>
        </div>
    </div>
</div>
<script type="text/javascript">

    $(document).ready(function(){

        $("#startdate").click(function(){
            $("#startdate").pickadate({

                selectYears: 50,
                selectMonths: true,
                max:new Date("2018")
            });
        });

    });

    $(document).ready(function(){

        $("#enddate").click(function(){
            $("#enddate").pickadate({

                selectYears: 50,
                selectMonths: true,
                max:new Date("2018")
            });
        });

    });

</script>