<div class="history">
<div class="page-header">
    <h2 class="page-title" key="h1">Payment History</h2>  
</div>
<?php
if (empty($history)) { ?>
   <div class="alert alert-warning" key="npy">You have no payment history</div>
<?php } else {
    ?>
    <section class="widget">
        <div class="content container">
    	<div class="mt">
    	    <div class="row">
    		<div class="col-md-12 space20">
    		    <div class="btn-group pull-right">
    			<button data-toggle="dropdown" class="notranslate btn btn-green dropdown-toggle btn-squared">
			    <span class="trl" key="h2"> Export</span> <i class="fa fa-angle-down"></i>
    			</button>
    			<ul class="dropdown-menu dropdown-light pull-right">
    			    <li>
    				<a href="#" class="export-xml" data-table="#sample-table-2" key="h3"> Save as XML </a>
    			    </li>
    			    <li>
    				<a href="#" class="export-excel" data-table="#sample-table-2" key="h4"> Export to Excel </a>
    			    </li>
    			</ul>
    		    </div>
    		</div>
    	    </div>
    	    <div id="datatable-table_wrapper" class="dataTables_wrapper form-inline no-footer">
    		<table id="sample-table-2" class="table table-striped table-bordered table-hover table-full-width">
    		    <thead>
    			<tr role="row">
			    <th class="trl" key="h6">Invoice</th>
    			    <th class="trl" key="h7">Payer Name</th>
    			    <th class="trl" key="h8">Amount</th>
    			    <th class="trl" key="h9">Invoice Date</th>
    			    <th class="trl" key="h10">Payment Date </th>
    			    <th class="trl" key="h11">Receipt Number</th>
    			</tr>
    		    </thead>
    		    <tbody>
			    <?php
			    $i = 1;
			    foreach ($history as $payment) {
				?>

				<tr role="row" class="<?= $i % 2 == 0 ? 'even' : 'odd' ?>">
				    <td><?= $payment->invoice ?></td>
				    <td class="hidden-xs"><?= $payment->payer ?></td>
				    <td class="hidden-xs"><?= strtoupper($payment->currency) ?><?= $payment->amount ?></td>
				    <td class="hidden-xs"> <?= date('d  F Y ', strtotime($payment->book_date)) ?></td>
				    <td class="hidden-xs"><?= $payment->receipt_date ?></td>
				    <td class="width-150"><?= $payment->receipt_code ?></td>
				</tr>

				<?php
				$i++;
			    }
			    ?>
    		    </tbody>
    		</table></div>
    	</div>
        </div>
    </section>
    <script src="<?= site_url(); ?>assets/js/table-export.js"></script>
    <script>
        jQuery(document).ready(function () {
    	TableExport.init();
        });
    </script>
<?php } ?>
</div>