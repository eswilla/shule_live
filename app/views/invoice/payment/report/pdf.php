<?php

if (!empty($data)) {
    ?>
    <div class="row">
        <div class="col-md-12">
    	<!-- start: DYNAMIC TABLE PANEL -->
	<div id="table_report">
    	<div class="page-header">
    	    <h3>Payment Status Report</h3>
    	    <small><?= $method ?> Status Report </small>
    	</div>
	    <style>
		thead tr th, tr td{ border-left: 1px solid #CCC;}
	    </style>
    	<table  class="table table-hover">
	    <thead>
    		<tr>
    		    <th>Date</th>
    		    <th>Payer Name</th>
    		    <th>Business Name</th>
    		    <th>Amount</th>
    		    <th>Invoice </th>
    		    <th>Bank Name</th>
    		    <th>Mobile Transaction ID</th>
    		    <th>Bank Transaction ID</th>
    		    <th>Receipt</th>
    		    <th>Status</th>
    		</tr>
    	    </thead>
    	 
		    <?php
		    $total = 0;
		    foreach ($data as $value) {
			$total+=$value->amount;
			?>
			<tr>
			    <td><?= strtotime($value->date); ?></td>
			    <td><?= $value->client_name; ?></td>
			    <td><?= $value->name; ?></td>
			    <td><?= $value->amount; ?></td>
			    <td><?= $value->invoice; ?></td>
			    <td><?= $value->bank_name; ?></td>
			    <td><?= $value->mobile_transaction_id; ?></td>
			    <td><?= $value->bank_transaction_id; ?></td>
			    <td><?= $value->code; ?></td>
			    <td><?= $value->status; ?></td>
			</tr>`
		    <?php } ?>
    	   
    	</table>
	    </div>
    	<!-- end: DYNAMIC TABLE PANEL -->
        </div>
    </div>

    <?php
} else {
    echo '<div class="alert alert-block alert-info fade in">
    <h4 class="alert-heading">Information</h4>
    <p>There is no records for this category</p>
</div>';
}
?>

