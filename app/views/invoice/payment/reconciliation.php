<div class="page-header">
    <h1>Payment status <small>banks and mobile money payments status</small></h1>
</div>
<div class="row">
    <div class="col-md-6">
	<div class="col">
	    <script src="<?= base_url() ?>assets/js/highCharts/highcharts.js"></script>
	    <script src="<?= base_url() ?>assets/js/highCharts/modules/exporting.js"></script>

	    <script type="text/javascript">
		$(function () {
		    $('#container').highcharts({
			chart: {
			    plotBackgroundColor: null,
			    plotBorderWidth: 1, 
			    plotShadow: false,
			    events: {
				click: function (e) {
				    console.log(e);
				}
			    }
			},
			title: {
			    text: 'Payment Status'
			},
			tooltip: {
			    pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
			},
			plotOptions: {
			    pie: {
				allowPointSelect: true,
				cursor: 'pointer',
				dataLabels: {
				    enabled: true,
				    format: '<b>{point.name}</b>: {point.percentage:.1f} %',
				    style: {
					color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
				    }
				},
				point: {
				    events: {
					click: function () {
					    /*console.log(this);
					    alert('Category: ' + this.x + ', value: ' + this.y);*/

					    loadAjax('payment', 'get_payment_table_report', 'Payment Status Report', '#master', {'cat_name': this.name});
					}
				    }
				}
			    }
			},
			series: [{
				type: 'pie',
				name: 'Payment status',
				data: [
<?= $param ?>
				]
			    }]

		    });
		});


	    </script>

	    <div id="container" style="min-width: 310px; height: 400px; max-width: 600px; margin: 0 auto"></div>

	</div>
    </div>
    <div class="col-md-6">

	<div class="row">
	    <div class="col-md-12">
		<!-- start: DYNAMIC TABLE PANEL -->
		<div class="page-header">
		    <h3>Total Payment Summary</h3><small>
			For all payments done</small>
		</div>
		<div class="space12">
		    <div class="btn-group btn-group-sm">
			<a class="btn btn-default active" href="javascript:;">
			    Today
			</a>
		    </div>
		</div>

		<table class='table table-striped table-bordered table-hover table-full-width' id='sample_2'>
                    <thead>
                        <tr>
                            <th>CRDB</th>
                            <th>NMB</th>
                            <th class='no-sort hidden-xs'>BANK CARDS</th>
                            <th class='hidden-xs'>MOBILE PAYMENTS</th>
                        </tr>
                    </thead>
                    <tbody>


			<tr>
                            <td><?=$payment['crdb']->total?></td>
                            <td><?=$payment['nmb']->total?></td>
                            <td><?=$payment['cards']->total?></td>
                            <td><?=$payment['mobile']->total?></td>
                        </tr>
		    </tbody>
                </table>
		<hr>
		<div id = "bn_name_app_review">

		</div>

		<!--            </div>
			</div>-->
		<!-- end: DYNAMIC TABLE PANEL -->
	    </div>
	</div>

    </div>
</div>
