<button data-style="expand-left" onclick="loadAjax('payment', 'apply', 'Business Name Payment', undefined, {'invoice': '<?= $invoice ?>'});" class="btn btn-default btn-squared">
    <span class="ladda-label"> Go back </span>
    <i class="fa fa-arrow-circle-left"></i>
    <span class="ladda-spinner"></span>
    <span class="ladda-spinner"></span></button>
<div class="row">
    <div class="col-md-6">
	<h4>Online payment with Debit or Credit Card</h4>
	<p align="center"><img src="<?= base_url() ?>assets/images/payments.png" alt="PayPal"></p>
	<form id="myCCForm" class="form-actions" action="<?= base_url() ?>payment/process_twocheckout" method="POST">
	    <!-- The $10 amount is set on the server side -->
	    <div class="form-group">
		<label class="col-md-3">Credit Card Number: </label>
		<input id="ccNo"  type="text" maxlength="20" autocomplete="off" value="" autofocus />
	    </div>
	    <div class="form-group">
		<label class="col-md-3">CVC: </label>
		<input id="cvv" type="text" maxlength="4" autocomplete="off" value=""/>
	    </div>
	    <div class="form-group">
		<label class="col-md-3">Expiry Date: </label>
		<select id="expMonth">
		    <option value="01">Jan</option>
		    <option value="02">Feb</option>
		    <option value="03">Mar</option>
		    <option value="04">Apr</option>
		    <option value="05">May</option>
		    <option value="06">Jun</option>
		    <option value="07">Jul</option>
		    <option value="08">Aug</option>
		    <option value="09">Sep</option>
		    <option value="10">Oct</option>
		    <option value="11">Nov</option>
		    <option value="12">Dec</option>
		</select>
		<select id="expYear">
		    <option value="13">2013</option>
		    <option value="14">2014</option>
		    <option value="15">2015</option>
		    <option value="16">2016</option>
		    <option value="17">2017</option>
		    <option value="18">2018</option>
		    <option value="19">2019</option>
		    <option value="20">2020</option>
		    <option value="21">2021</option>
		    <option value="22">2022</option>
		</select>
	    </div>
	    <label class="col-md-3"></label>
	    <button id="process-payment-btn" class="btn btn-primary"  type="button">Process Payment</button>
	</form>
	<div id="span_loader"></div>
    </div>
    <div class="col-md-6">
	<h4 class="heading">Payment Summary</h4>
	<table id="user" class="table table-bordered table-striped" style="clear: both">
	    <tbody>
		<tr>
		    <td class="column-left">Payment Amount:</td>
		    <td class="column-right">

			<?= $amountUSD = round($amount / EXCHANGE_RATE, 2); ?> USD
		    </td>
		</tr>
		<tr>
		    <td>Service Charge</td>
		    <td>
			<?= $service_charge = 0.05 * $amountUSD + 0.47; ?>
		    </td>
		</tr>
		<tr>
		    <td>Total Amount to Pay</td>
		    <td><?= $amountUSD + $service_charge ?></td>
		</tr>
		<tr>
		    <td>Payment For</td>
		    <td>
			<?php
			$data = "{'data': [" . $summary . "]}";
			$string = str_ireplace("'", '"', $data);
			$json = json_decode($string, TRUE);
			$payment_for = '';
			foreach ($json['data'] as $value) {
			    $payment_for.=$value['description'] . ' =' . round($value['amount'] / EXCHANGE_RATE, 2) . ' usd<br/>';
			}
			?>
			<?= $payment_for ?>
		    </td>
		</tr>
	    </tbody>
	</table>
    </div>
</div>
<script src="<?= base_url() ?>assets/js/2co.js"></script>
<script src="<?= base_url() ?>assets/js/direct.min.js"></script>
<script>
    /*Called when token created successfully.*/
    var successCallback = function (data) {
	var myForm = document.getElementById('myCCForm');

	/*Set the token as the value for the token input
         * Commented by Owden,enable progress bar
         * */
	//$('#span_loader').html('<div class="alert alert-info">Please wait...............</div>'+LOADER);
         $('#span_loader').html(LOADER);
	var token = data.response.token.token;
	/*console.log(token);
	 IMPORTANT: Here we call `submit()` on the form element directly instead of using jQuery to prevent and infinite token request loop.*/
	$.ajax({
	    url: $('#myCCForm').attr('action'),
	    type: $('#myCCForm').attr('method'),
	    data: 'token=' + token + '&invoice=<?= $invoice ?>',
	    success: function (response) {
		console.log(response);
		$('#span_loader').html('');
		$('#span_loader').html(response);
		/*lets know the status of the payment. if its okay
		then direct user to see a page with success, otherwise
		show error message and allow user to make payment later*/
	    },
	    error: function (xhr, err) {
		console.log(xhr);
		$('#span_loader').html('<div class="alert alert-danger">'+xhr.errorMsg+'</div>');
	    }
	});
	return false;
	/*myForm.submit();*/
    };

    /*Called when token creation fails.*/
    var errorCallback = function (data) {
	$('#span_loader').html('');
	console.log(data);
	if (data.errorCode === 200) {
	    tokenRequest();
	} else {
	    $('#span_loader').html('<div class="alert alert-danger">'+data.errorMsg+'</div>');
	}

    };

    var tokenRequest = function () {
	/*Setup token request arguments*/
<?php
$mode = 'production';
$PUBLISHABLE_KEY = $mode == 'sandbox' ?
	'0FDA6E4F-0A21-41E4-B446-C89FC00D6735' :
	'BEC71F48-E7C1-11E4-901D-5ECC3A5D4FFE';
?>
	var args = {
	    sellerId: "901273428", /*this is seller ID change dep on sand or live*/
	    publishableKey: "<?= $PUBLISHABLE_KEY ?>",
	    ccNo: $("#ccNo").val(),
	    cvv: $("#cvv").val(),
	    expMonth: $("#expMonth").val(),
	    expYear: $("#expYear").val()
	};

	/* Make the token request*/
	TCO.loadPubKey('<?= $mode ?>', function () {
	    TCO.requestToken(successCallback, errorCallback, args);
	});
    };

    $(function () {
	/*Pull in the public encryption key for our environment*/
	TCO.loadPubKey('<?= $mode ?>');
	$("#process-payment-btn").click(function (e) {
	    /*Call our token request function
             * Commented by Owden Godson
             * */
	    $('#span_loader').html(LOADER);
           
	    tokenRequest();
             $('#span_loader').html('');

	    /*Prevent form from submitting*/
	    return false;
	});
    });
</script>