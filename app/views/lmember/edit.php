

<div class="box">
    <div class="box-header">
        <h3 class="box-title"><i class="fa icon-member"></i> <?=$this->lang->line('panel_title')?></h3>

       
        <ol class="breadcrumb">
            <li><a href="<?=base_url("dashboard/index")?>"><i class="fa fa-laptop"></i> <?=$this->lang->line('menu_dashboard')?></a></li>
            <li><a href="<?=base_url("lmember/index/$set")?>"><?=$this->lang->line('menu_member')?></a></li>
            <li class="active"><?=$this->lang->line('menu_edit')?> <?=$this->lang->line('menu_member')?></li>
        </ol>
    </div><!-- /.box-header -->
    <!-- form start -->
    <div class="box-body">
        <div class="row">
            <div class="col-sm-8">

                <form class="form-horizontal" role="form" method="post" enctype="multipart/form-data">
                    <?php
                    if(form_error('id_number'))
                        echo "<div class='form-group has-error' >";
                    else
                        echo "<div class='form-group' >";
                    ?>
                    <label for="lid" class="col-sm-2 control-label">
                        <?=$this->lang->line("issue_id")?>
                    </label>
                    <div class="col-sm-6">
                        <input type="text" class="form-control" id="id_number" name="id_number" value="<?=set_value('id_number',$members->id_number)?>" >
                    </div>
                        <span class="col-sm-4 control-label">
                            <?php echo form_error('id_number'); ?>
                        </span>
            </div>

                                <?php 
                                    if(form_error('book'))
                                        echo "<div class='form-group has-error' >";
                                    else
                                        echo "<div class='form-group' >";
                                ?>
                                    <label for="book" class="col-sm-2 control-label">
                                        <?=$this->lang->line("issue_book")?>
                                    </label>
                                    <div class="col-sm-6">
            
                                        <input type="text" class="form-control" id="book" name="book" value="<?=set_value('book')?>" >
                                        <div class="book"><ul  class="result"></ul></div>
                                        
                                    </div>
            
        
                                    <span class="col-sm-4 control-label">
                                        <?php echo form_error('book'); ?>
                                    </span>
                                </div>


            <?php
            if (form_error('book'))
                echo "<div class='form-group has-error' >";
            else
                echo "<div class='form-group' >";
            ?>
            <label for="book" class="col-sm-2 control-label">
                <?= $this->lang->line("issue_book_name") ?> *
            </label>
            <div class="col-sm-6">
                <div class="select2-wrapper">

                    <?php
                    $array = array('' => '');
                    foreach ($books as $book) {
                        $array[$book->bookID] = $book->book;
                    }

                    echo form_dropdown("bookID", null, set_value('book',$members->book), " class='form-control book' id='bookID'");
                    ?>
                </div>
            </div>
	    <span class="col-sm-4 control-label">
		<?php echo form_error('book'); ?>
	    </span>
        </div>



        <?php
        if(form_error('due_date'))
            echo "<div class='form-group has-error' >";

        else
            echo "<div class='form-group' >";
        ?>
        <label for="due_date" class="col-sm-2 control-label">
            <?=$this->lang->line("issue_due_date")?>
        </label>
        <div class="col-sm-6">
            <input type="text" class="form-control" id="due_date" name="due_date" value="<?=set_value('due_date',$members->due_date)?>" >
        </div>
                        <span class="col-sm-4 control-label">
                            <?php echo form_error('due_date'); ?>
                        </span>
    </div>

    <?php
    if(form_error('fine'))
        echo "<div class='form-group has-error' >";
    else
        echo "<div class='form-group' >";
    ?>
    <label for="fine" class="col-sm-2 control-label">
        <?=$this->lang->line("issue_fine")?>
    </label>
    <div class="col-sm-6">
        <input type="text" class="form-control" id="fine" name="fine" value="<?=set_value($members->fine, '15')?>" >
    </div>
                        <span class="col-sm-4 control-label">
                            <?php echo form_error('fine'); ?>
                        </span>
</div>


<?php
if(form_error('note'))
    echo "<div class='form-group has-error' >";
else
    echo "<div class='form-group' >";
?>
<label for="note" class="col-sm-2 control-label">
    <?=$this->lang->line("issue_note")?>
</label>
<div class="col-sm-6">
    <input type="text" class="form-control" id="note" name="note" value="<?=set_value('note',$members->note)?>" >
</div>
<span class="col-sm-4 control-label">
                            <?php echo form_error('note'); ?>
                        </span>
</div>

<div class="form-group">
    <div class="col-sm-offset-2 col-sm-8">
        <input type="submit" class="btn btn-success" value="<?=$this->lang->line("add_issue")?>" >
    </div>
</div>
</form>

</div> <!-- col-sm-12 -->
        </div><!-- row -->
    </div><!-- Body -->
</div><!-- /.box -->
