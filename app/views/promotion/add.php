
<div class="box">
    <div class="box-header">
        <h3 class="box-title"><i class="fa icon-promotion"></i> <?= $this->lang->line('panel_title') ?></h3>

        <ol class="breadcrumb">
            <li><a href="<?= base_url("dashboard/index") ?>"><i class="fa fa-laptop"></i> <?= $this->lang->line('menu_dashboard') ?></a></li>
            <li><a href="<?= base_url("promotion/index") ?>"><?= $this->lang->line('menu_promotion') ?></a></li>
            <li class="active"><?= $this->lang->line('menu_add') ?> <?= $this->lang->line('menu_promotion') ?></li>
        </ol>
    </div><!-- /.box-header -->
    <!-- form start -->
    <div class="box-body">
        <div class="row">
            <div class="col-sm-12">


                <div class="col-sm-6 col-sm-offset-3 list-group">
                    <div class="list-group-item list-group-item-warning">

                        <form style="" class="form-horizontal" role="form" method="post">
                            <div class="form-group">
                                <label for="classesID" class="col-sm-2 col-sm-offset-2 control-label">
				    <?= $this->lang->line("promotion_classes") ?>
                                </label>
                                <div class="col-sm-6">
				    <?php
				    $array = array("0" => $this->lang->line("promotion_select_class"));
				    foreach ($classes as $classa) {
					$array[$classa->classesID] = $classa->classes;
				    }
				    echo form_dropdown("classesID", $array, set_value("classesID", $set), "id='classesID' class='form-control'");
				    ?>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>

                <div id="hide-table">
                    <table id="example1" class="table table-striped table-bordered table-hover dataTable no-footer">
                        <thead>
			    <tr>
				<th class="col-sm-2"><?= $this->lang->line('slno') ?></th>
				<th class="col-sm-2"><?= $this->lang->line('promotion_photo') ?></th>
				<th class="col-sm-2"><?= $this->lang->line('promotion_name') ?></th>
				<th class="col-sm-2"><?= $this->lang->line('promotion_roll') ?></th>
				<th class="col-sm-2"><?= $this->lang->line('promotion_section') ?></th>
				<th class="col-sm-2">Average</th>
				<th class="col-sm-1"><?= $this->lang->line('promotion_result') ?> Status </th>
				<th class="col-sm-2"><?php
				    if (in_array(2, $student_result)) {
					echo '<input type="checkbox" class="promotion btn btn-warning" disabled> ' . $this->lang->line('action');
				    } else {
					echo btn_attendance('', '', 'all_promotion', $this->lang->line('add_all_promotion')) . $this->lang->line('action');
				    }
				    ?></th>
			    </tr>
                        </thead>
                        <tbody>

			    <?php
			    if (count($students)) {
				$i = 1;
				foreach ($students as $student) {
				    $student = (object) $student;
				    ?>
			    <tr id="student<?=$student->studentID?>">
					<td data-title="<?= $this->lang->line('slno') ?>">
					    <?php echo $i; ?>
					</td>

					<td data-title="<?= $this->lang->line('promotion_photo') ?>">
					    <?php
					    $array = array(
						"src" => base_url('uploads/images/' . $student->photo),
						'width' => '35px',
						'height' => '35px',
						'class' => 'img-rounded'
					    );
					    echo img($array);
					    ?>
					</td>
					<td data-title="<?= $this->lang->line('promotion_name') ?>">
					    <?php echo $student->name; ?>
					</td>
					<td data-title="<?= $this->lang->line('promotion_roll') ?>">
					    <?php echo $student->roll; ?>
					</td>
					<td data-title="<?= $this->lang->line('promotion_section') ?>">
					    <?php echo $student->section; ?>
					</td>
					<td data-title="<?= $this->lang->line('promotion_section') ?>">
					    <?php echo $student->average; ?>
					</td>

					<td data-title="<?= $this->lang->line('promotion_result') ?>">
					    <?php
					    // dump($student_result[$student->studentID]);
					    if ($student_result[$student->studentID] == 1) {
						echo "<button class='btn btn-success btn-xs'>" . $this->lang->line('promotion_pass') . "</button>";
					    } elseif ($student_result[$student->studentID] == 0) {
						echo "<button class='btn btn-danger btn-xs'>" . $this->lang->line('promotion_fail') . "</button>";
					    } else {
						echo "<button class='btn btn-info btn-xs'>" . $this->lang->line('promotion_modarate') . "</button>";
					    }
					    ?>
					</td>



					<td data-title="<?= $this->lang->line('action') ?>">
					    <?php
					    if ($student_result[$student->studentID] == 1 ) {
						echo btn_promotion($student->studentID, 'promotion btn btn-warning', $this->lang->line('add_title'), $student->studentID);
						
					    }elseif ($student_result[$student->studentID] == 0) {
					 ?>
					    <button class='btn btn-warning btn-xs mrg' data-placement='top'  data-toggle='tooltip' onclick="promote_reasons(<?=$student->studentID?>)"  data-original-title="<?=$this->lang->line('add_title')?>"><i class='fa fa-plus'></i></button>
						 
					    <button class='btn btn-danger btn-xs mrg' data-placement='top' data-target='#promotion' data-toggle='modal' onclick="$('#student_id').val(<?=$student->studentID?>)" data-original-title='<?=$this->lang->line('delete') ?>'><i class='fa fa-trash-o'></i></button>
					<?php	
					    } else {
						echo '<input type="checkbox" class="promotion btn btn-warning" disabled>';
					    }
					    // echo  btn_promotion($student->studentID, 'promotion btn btn-warning', $this->lang->line('add_title'));
					    ?>
					</td>
				    </tr>
				    <?php
				    $i++;
				}
			    }
			    ?>
                        </tbody>
                    </table>
                </div>

                <div class="col-sm-3 col-sm-offset-9 list-group">

                    <input type="button" class="col-sm-12 btn btn-success" id="save" value="<?= $this->lang->line('add_promotion') ?>" >
                </div>

                <div id="dialog"></div>



            </div> <!-- col-sm-12 -->
        </div><!-- row -->
    </div><!-- Body -->
</div><!-- /.box -->

<!-- email modal starts here -->
<form class="form-horizontal" method="post">
    <div class="modal fade" id="promotion">
      <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title">Change Student Status</h4>
            </div>
            <div class="modal-body">
            
               <div class="">
			    <h2>Reasons</h2>
			    <ul class="list-inline prod_color">
				<li>
				    <p>Transfered to other School</p>
				    <div class="color bg-green">
					<input type="radio" name="prm" value="0"/>
				    </div>
				</li>
				<li>
				    <p>Repeat the same class</p>
				    <div class="color bg-blue">
					<input type="radio" name="prm" value="3"/>
				    </div>
				</li>

				<input type="hidden" name="student_id" id="student_id" value=""/>
				<input type="hidden" name="pass_mark" value="<?=$pass_mark?>"/>
			    </ul>
			    <span>This student will not be promoted to the next class</span>
			</div>        
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" style="margin-bottom:0px;" data-dismiss="modal"><?=$this->lang->line('close')?></button>
                <input type="submit" id="send_pdf" class="btn btn-success" value="Submit" />
            </div>
        </div>
      </div>
    </div>
</form>
<script type="text/javascript">
    $('#classesID').change(function () {
	var classesID = $(this).val();
	if (classesID == 0) {
	    $('#hide-table').hide();
	} else {
	    $.ajax({
		type: 'POST',
		url: "<?= base_url('promotion/promotion_list') ?>",
		data: "id=" + classesID,
		dataType: "html",
		success: function (data) {
		    window.location.href = data;
		}
	    });
	}
    });

    $('.all_promotion').click(function () {

	if ($(".all_promotion").prop('checked')) {
	    status = "checked";
	    $('.promotion').prop("checked", true);
	} else {
	    status = "unchecked";
	    $('.promotion').prop("checked", false);
	}
    });

    $('#save').click(function () {
	if ($('.promotion').filter(':checked').length == 0) {
	    toastr["error"]("<?= $this->lang->line('promotion_select_student') ?>")
	    toastr.options = {
		"closeButton": true,
		"debug": false,
		"newestOnTop": false,
		"progressBar": false,
		"positionClass": "toast-top-right",
		"preventDuplicates": false,
		"onclick": null,
		"showDuration": "500",
		"hideDuration": "500",
		"timeOut": "5000",
		"extendedTimeOut": "1000",
		"showEasing": "swing",
		"hideEasing": "linear",
		"showMethod": "fadeIn",
		"hideMethod": "fadeOut"
	    }
	} else {
	    var result = [];
	    var status = "";
	    var classesID = <?= $set ?>;

	    $('.promotion').each(function (index) {
		status = (this.checked ? $(this).attr('id') : 0);
		result.push(status);
	    });

	    $redirect = (window.location.href);
	    $.ajax({
		type: 'POST',
		url: "<?= base_url('promotion/promotion_to_next_class') ?>",
		data: "studentIDs=" + result + "&classesID=" + classesID,
		dataType: "html",
		success: function (data) {
		    window.location.replace($redirect);
		}
	    });

	}

    });
 
    promote_reasons = function (a) {
	    swal({
		title: "Promotion Reasons",
		text: "You are about to promote this student to the next class, but this student is below pass mark. Write reasons why you promote:",
		type: "input",
		showCancelButton: true,
		closeOnConfirm: false,
		animation: "slide-from-top"
	    },
	    function (inputValue) {

		if (inputValue === "") {
		    swal.showInputError("You need to write something!");
		    return false;
		}
		if (inputValue === false) {
		    return false;
		} else {
		    $.ajax({
			type: "POST",
			url: "<?= base_url('promotion/promotion_remark') ?>",
			data: "remark=" + inputValue + '&pass_mark=<?= $pass_mark ?>&status=1&from=<?= $this->session->userdata('academic_year_id'); ?>&student_id=' + a,
			dataType: "html",
			success: function (data) {
			    swal("Success!", "You wrote: " + data, "success");
			    $('#student'+a).html('');
			}
		    });
		}

	    });

//	    $.ajax({
//		type: "POST",
//		url: "<?= base_url('promotion/remove_promotion_remark') ?>",
//		data: 'from=<?= $this->session->userdata('academic_year_id'); ?>&student_id=' + a,
//		dataType: "html",
//		success: function (data) {
//		    swal("Success!", "You wrote: " + data, "success");
//		}
//	    });
	
    }
</script>