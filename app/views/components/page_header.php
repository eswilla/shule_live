<html>
    <head>
        <meta charset="UTF-8">
        <title>Shulesoft</title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        <meta name="theme-color" content="#00acac">
        <link rel="SHORTCUT ICON" href="<?= base_url("uploads/images/$siteinfos->photo") ?>" />
        <script type="text/javascript" src="<?php echo base_url('assets/shulesoft/jquery.js'); ?>"></script>
        <script type="text/javascript" src="<?php echo base_url('assets/jquery-ui/jquery-ui.js'); ?>"></script>
        <script type="text/javascript" src="<?php echo base_url('assets/timepicker/timepicker.js'); ?>"></script>
        <script type="text/javascript" src="<?php echo base_url('assets/toastr/toastr.min.js'); ?>"></script>
	    <script type="text/javascript" src="<?php echo base_url('assets/js/sweet-alert/sweetalert.min.js'); ?>"></script>
        <script type="text/javascript" src="<?php echo base_url('assets/datepicker/picker.date.js'); ?>"></script>
	<!--<link rel="stylesheet" href="--><?php //echo base_url('assets/materialize.min.css');  ?><!--">-->

        <link href="<?php echo base_url('assets/bootstrap/bootstrap.min.css'); ?>" rel="stylesheet">
        <link href="<?php echo base_url('assets/fonts/font-awesome.css'); ?>" rel="stylesheet">
        <link href="<?php echo base_url('assets/fonts/icomoon.css'); ?>" rel="stylesheet">
        <link href="<?php echo base_url('assets/datatables/dataTables.bootstrap.css'); ?>" rel="stylesheet">
        <link href="<?php echo base_url('assets/shulesoft/style.css'); ?>" rel="stylesheet">
        <link href="<?php echo base_url('assets/datepicker/datepicker.css'); ?>" rel="stylesheet">
        <link href="<?php echo base_url('assets/timepicker/timepicker.css'); ?>" rel="stylesheet">
        <link href="<?php echo base_url('assets/shulesoft/hidetable.css'); ?>" rel="stylesheet">
        <link href="<?php echo base_url('assets/shulesoft/shulesoft.css'); ?>" rel="stylesheet">
        <link href="<?php echo base_url('assets/shulesoft/responsive.css'); ?>" rel="stylesheet">
        <link href="<?php echo base_url('assets/fullcalendar/fullcalendar.css'); ?>" rel="stylesheet">
        <link href="<?php echo base_url('assets/editor/jquery-te-1.4.0.css'); ?>" rel="stylesheet">
        <link href="<?php echo base_url('assets/toastr/toastr.min.css'); ?>" rel="stylesheet">
        <link href="<?php echo base_url('assets/shulesoft/rid.css'); ?>" rel="stylesheet">
        <link href="<?php echo base_url('assets/jquery-ui/jquery-ui.css'); ?>" rel="stylesheet">
        <link href="<?php echo base_url('assets/shulesoft/jquery.mCustomScrollbar.min.css') ?>" rel="stylesheet">
	    <link href="<?php echo base_url('assets/css/sweet-alerts/sweetalert.css'); ?>" rel="stylesheet">
	
        <link rel="stylesheet" href="<?php echo base_url('assets/select2/css/select2.css'); ?>">
        <link rel="stylesheet" href="<?php echo base_url('assets/datepicker/themes/default.css'); ?>">
        <link rel="stylesheet" href="<?php echo base_url('assets/datepicker/themes/default.date.css'); ?>">
        <link rel="stylesheet" href="<?php echo base_url('assets/select2/css/select2-bootstrap.css'); ?>">
        <link rel="stylesheet" href="<?php echo base_url('assets/select2/css/gh-pages.css'); ?>">
        <link rel="stylesheet" href="<?php echo base_url('assets/jqueryUI/jqueryui.css'); ?>">
        <link rel="stylesheet" href="<?php echo base_url('assets/custom.css'); ?>">
        <link rel="stylesheet" href="<?= base_url("assets/css/documenter_style.css") ?>">
        <link href="<?php echo base_url()?>assets/css/pnotify/pnotify.css" rel="stylesheet">
        <link href="<?php echo base_url()?>assets/css/pnotify/pnotify.buttons.css" rel="stylesheet">
        <link href="<?php echo base_url()?>assets/css/pnotify/pnotify.nonblock.css" rel="stylesheet">
        <link href="<?php echo base_url()?>assets/css/loader.css" rel="stylesheet">
    </head>
    <body class="nav-md">
