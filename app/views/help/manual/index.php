<?php
$user = empty($usertype) ? 'Administrator' : $usertype;
?>

<!DOCTYPE html>
<html>
    <head>
        <title>ShuleSoft Manual</title>
	<?php
	$css = base_url() . "/assets/manual_assets/";
	$js = base_url() . "/assets/manual_assets/";
	?>
        <meta charset="utf-8">
	<!--        <link rel="canonical" href="http://www.templatemonster.com/help/quick-start-guide/website-templates/responsive-website-templates-v1-1/index_en.html"/> -->
        <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/3.2.1/css/font-awesome.min.css" media="all" rel="stylesheet" type="text/css"> 
        <link rel="stylesheet" href="<?= $css ?>css/bootstrap.css" media="screen">
        <link rel="stylesheet" href="<?= $css ?>css/responsive.css" media="screen">
        <link rel="stylesheet" href="<?= $css ?>css/prettify.css"  media="screen">
        <link rel="stylesheet" href="<?= $css ?>css/prettyPhoto.css"  media="screen">
        <link rel="stylesheet" href="<?= $css ?>css/idea.css"  media="screen">
        <link rel="stylesheet" href="<?= $css ?>css/style.css" media="screen">
        <link rel="stylesheet" href="<?= $css ?>css/contact-form.css" media="screen">
        <script src="<?= $js ?>js/jquery.js"></script>
        <script src="<?= $js ?>js/jquery.scrollTo.js"></script>
        <script src="<?= $js ?>js/jquery-migrate-1.1.0.js"></script>
        <script src="<?= $js ?>js/prettify.js"></script>
        <script src="<?= $js ?>js/bootstrap-affix.js"></script>
        <script src="<?= $js ?>js/jquery.prettyPhoto.js"></script> 

        <script src="<?= $js ?>js/TMForm.js"></script>
        <script src="<?= $js ?>js/modal.js"></script>  
        <script src="<?= $js ?>js/bootstrap-filestyle.js"></script>    

        <script src="<?= $js ?>js/sForm.js"></script>  
        <script src="<?= $js ?>js/jquery.countdown.min.js"></script>
        <script src="<?php echo $js ?>js/scripts.js"></script>
        <!--[if IE 8]>
        <link rel="stylesheet" href="css/ie.css" />
      <![endif]-->

        <!--[if lt IE 9]>
                    <div style='text-align:center'><a href="http://www.microsoft.com/windows/internet-explorer/default.aspx?ocid=ie6_countdown_bannercode"><img src="http://www.theie6countdown.com/img/upgrade.jpg"border="0"alt=""/></a></div>  
              <![endif]-->
        <!--[if lt IE 9]>
          <script src="js/html5shiv.js"></script>
        <![endif]-->
    </head>

    <body>  

        <!-- header
        ================================================== -->
        <header class="header">
            <div class="container">
                <div class="row">
                    <article class="span4"><a href="<?= base_url() ?>" target="_blank">Home</a></article>
                    <article class="span6">
                        <h1>
			    <i class="fa fa-book"></i> 
			    ShuleSoft System Manual
			</h1>
                    </article> 
                    <article class="span2">
			<!--<div id="languages" class="select-menu pull-right">
						    <div class="select-menu_icon">
							<b>En</b>
							<i class="icon-angle-down"></i>
						    </div>
						    <ul class="select-menu_list">
			
						    </ul>
						</div>-->
                        <div id="versions" class="select-menu pull-right">
                            <div class="select-menu_icon"><b>v1-0</b><i class="icon-angle-down"></i></div>
			    <!-- <ul class="select-menu_list">
							    <li><a href="/help/quick-start-guide/website-templates/responsive-website-templates-v1-0/index_en.html"><span>v1-0</span></a></li>
							    <li class="active"><a href="/help/quick-start-guide/website-templates/responsive-website-templates-v1-1/index_en.html"><span>v1-1</span></a></li>
							    <li class="active"><a href="/help/quick-start-guide/website-templates/responsive-website-templates-v1-2/"><span>v1-2</span></a></li>
							</ul>-->
                        </div>
                    </article>   
                </div>    
            </div>
        </header>
        <div id="content"> 
            <div class="bg-content-top">
                <div class="container">

                    <!-- Docs nav
                        ================================================== -->
                    <div class="row">
                        <div class="span3"> 

                            <div id="nav_container">
                                <a href="javascript:;" id="affect_all">
                                    <span class="expand">
                                        <i class="icon-angle-down"></i>
                                    </span>            
                                    <span class="close">
                                        <i class="icon-angle-up"></i>
                                    </span>
                                </a>
                                <div class="clearfix"></div>    
                                <ul class="nav nav-list bs-docs-sidenav"  id="nav">

                                        <li class="nav-item item1">
                                            <dl class="slide-down">
                                                <dt><a href="#introduction" class="icon-info-sign">Introduction</a></dt>
                                                <dd></dd>
                                            </dl>
                                        </li>
                                        <li class="nav-item item2">
                                            <dl class="slide-down">
                                                <dt><a href="#modules" class="icon-check"><?= $user ?> Modules</a></dt>
                                                <dd></dd>
                                            </dl>
                                        </li>
                                        <li class="nav-item item3">
                                            <dl class="slide-down">
                                                <dt><a href="#general-information" class="icon-play">Features</a></dt>
                                                <dd></dd>
                                            </dl>
                                        </li>


                                    <li class="nav-item item4">
                                        <dl class="slide-down">
                                            <dt><a href="#get-started" class="icon-cog">How To Start</a> <i class="icon-sample"></i></dt>
                                            <dd>
                                                <ul class="list">          
                                                    <li><a href="#add-teacher" title="Add Teacher">Add Teacher</a></li>
                                                    <li><a href="#add-class" title="Add Class">Add Class</a></li>
                                                    <li><a href="#add-section" title="Add Section">Add Section</a></li>
						    <li><a href="#add-parent" title="Add Parent">Add Parent</a></li>
						    <li><a href="#add-student" title="Add student">Add Student</a></li>                                       </ul> 
                                            </dd>
                                        </dl> 
                                    </li>

				    <li class="nav-item item5">
                                        <dl class="slide-down">
                                            <dt><a href="#attendance" title="attendance" class="icon-play">Attendance</a></dt>
                                            <dd></dd>
                                        </dl> 
                                    </li>
				    <li class="nav-item item6">
                                        <dl class="slide-down">
                                            <dt><a href="#Promotion" title="Promotion" class="icon-play">Promotion</a></dt>
                                            <dd></dd>
                                        </dl> 
                                    </li>
				    <li class="nav-item item7">
                                        <dl class="slide-down">
                                            <dt><a href="#mail_sms" title="Mail and SMS" class="icon-inbox">Mail/SMS</a></dt>
                                            <dd></dd>
                                        </dl> 
                                    </li>
				    <li class="nav-item item8">
                                        <dl class="slide-down">
                                            <dt><a href="#media-upload" title="Media" class="icon-file">Media</a></dt>
                                            <dd></dd>
                                        </dl> 
                                    </li>
				    <li class="nav-item item9">
                                        <dl class="slide-down">
                                            <dt><a href="#about-admin" class="icon-user">All About Admin</a></dt>
                                            <dd></dd>
                                        </dl> 
                                    </li>
				    <li class="nav-item item10">
                                        <dl class="slide-down">
                                            <dt><a href="#about-teacher" class="icon-user-md">All About Teacher</a></dt>
                                            <dd></dd>
                                        </dl> 
                                    </li>

                                    <li class="nav-item item11">
                                        <dl class="slide-down">
                                            <dt><a href="#about-accountant" class="icon-user">All About Accountant</a></dt>
                                            <dd></dd>
                                        </dl> 
                                    </li> 
				    <li class="nav-item item1">
                                        <dl class="slide-down">
                                            <dt><a href="#about-librarian" class="icon-user">All About Librarian</a></dt>
                                            <dd></dd>
                                        </dl> 
                                    </li>

                                    <li class="nav-item item1">
                                        <dl class="slide-down">
                                            <dt><a href="#about-parent" class="icon-user">All About Parent</a></dt>
                                            <dd></dd>
                                        </dl>
                                    </li>


				    <li class="nav-item item2">
                                        <dl class="slide-down">
					    <dt><a href="#about-student" class="icon-user">All About Student</a></dt>
                                            <dd></dd>
                                        </dl> 
                                    </li> 
                                    <li class="nav-item item3">
                                        <dl class="slide-down">
                                            <dt><a href="#conclusion" class="icon-question-sign">Conclusion</a></dt>
                                            <dd></dd>
                                        </dl> 
                                    </li> 
                                </ul>
                            </div>
                        </div> 


                        <div class="span9">

                            <!-- box-content
                            ================================================== -->
                            <div class="box-content">
                                <!-- block-started
                                ================================================== -->
                                <section class="block-started"  id="introduction">
                                    <h2 class="item1"><i class="icon-info-sign"></i> Introduction <small>
					    <i>What is ShuleSoft ?</i></small></h2>
				    <p>
					ShuleSoft is Cloud based management system for schools that brings efficiency in school management and interactions among students, teachers and parents. ShuleSoft School management system is used for managing education based organizations like schools, colleges, universities etc.
				    <p>

                                </section>

                                <section class="block-templates" id="modules">  
                                    <h2 class="item2"><i class="icon-check"></i><?= $user ?> Modules Available</h2>

                                    <p>ShuleSoft has number of modules available.  <?= $user ?> can access the following modules</p>
				    <ol>
					<li>User Management (Students, Teachers, Accountant, Librarian etc)</li>
					<li>User Management Module</li>
					<li>Class Management Module</li>
					<li>Exam Management Module</li>
					<li>Email & SMS Module</li>
					<li>Attendance Module</li>
					<li>Account Module</li>
					<li>Hostel Management Module</li>
					<li>Transport Management Module</li>
					<li>Report Module</li>
				    </ol>
				    <p>As <?= $user ?>, you can view and manage all modules for school operations.</p>
                                </section>

                                <section class="block-templates" id="general-information">  
                                    <h2 class="item3"><i class="icon-play"></i>ShuleSoft Features </h2>


                                    <p>The following are some of the features available in shulesoft software. </p>

				    <section id="features">
					<pre class="prettyprint lang-plain linenums">
Multiple Modules that link each other to bring more efficiency in school management
Cloud based solution. This means, the software will be accessed via the internet to enable each and every user to interact together and also to enable interlinking of system with payment systems
Linked with SMS platform to send SMS and Email notification on each major action
System support multiple accounts creation such that, each user (admin, teacher, student, parent etc) will have his/her own account and view his/her own information only 
Allow creation of Custom classes and sections.
Allow admin to create subjects and grading point as per country  or custom regulations
Allow creation of exams and exam schedule and also, teacher can mark a subject via the system and the system will help/her to generate all required reports
Allow registration of students in library, hostel and transport routes
Allow attendances (teacher, students and examination) to be taken and generate attendance reports
Allow simple creation of routines (classes and exams)
Accounting modules help a school to easily create invoices, accept electronic payments and manage all school accounting
Each user in the system have a unique profile to enable him/her to view and perform required task. These users include accountants, administrators, teachers, librarians, parents, students etc 
					</pre>
				    </section>
                                </section>


                                <section class="block-templates" id="get-started">
                                    <h2 class="item4"><i class="icon-cog"></i>How to Get Started</h2>
				    <p>This is the step by step guide on how to get started to use ShuleSoft in your school</p>
				    <section id="how_to_start">
					<h4 id="add-teacher">1. ADD Teacher</h4>
					In the first step, you need to add all teachers available in your school. To add a teacher, please following below steps :
					<pre class="prettyprint">
Login as admin
Select teacher menu in sidebar
Click Add Teacher & fill out the all information
Then Click Add Teacher Button
					</pre>
					<p>
					    <img src="<?= base_url() ?>assets/images/screenshots/new/add_teacher.png" width="1000" style="padding: 0px 20px;"/>
					</p>
					<p>
					    <img src="<?= base_url() ?>assets/images/screenshots/new/add_teacher2.png" width="1000" style="padding: 0px 20px;"/>
					</p>
					<h4 id="add-class">2. ADD Class</h4>
					Add Class. Please following below steps :
					<pre class="prettyprint">
Login as admin
Select class menu in sidebar
Click Add class & fill out the all information
Then Click Add Class Button
					</pre>
					<p><img src="<?= base_url() ?>assets/images/screenshots/new/add_class.png" width="1000" style="padding: 0px 20px;"></p>

					<p><img src="<?= base_url() ?>assets/images/screenshots/new/class_add_with_value.png" width="1000" style="padding: 0px 20px;"></p>

					<h4 id="add-section">3. ADD Section</h4>
					Add Section. Please following below steps :
					<pre class="prettyprint">
Login as admin
Select section menu in sidebar
Click Add section & fill out the all information
Then Click Add Section Button
					</pre>
					<p><img src="<?= base_url() ?>assets/images/screenshots/new/add_section.png" width="1000" style="padding: 0px 20px;"></p>

					<p><img src="<?= base_url() ?>assets/images/screenshots/new/class_add_section_with_value.png" width="1000" style="padding: 0px 20px;"></p>

					<h4 id="add-parent">4. ADD Parents</h4>
					Add Parents. Please following below steps :
					<pre class="prettyprint">
Login as admin
Select parents menu in sidebar
Click Add parents & fill out the all information
Then Click Add Parents Button
					</pre>
					<p><img src="<?= base_url() ?>assets/images/screenshots/new/add_parents.png" width="1000" style="padding: 0px 20px;"></p>

					<p><img src="<?= base_url() ?>assets/images/screenshots/new/parents_add_with_value.png" width="1000" style="padding: 0px 20px;"></p>

					<h4 id="add-student">5. ADD Student</h4>
					Add Student. Please following below steps :
					<pre class="prettyprint">
Login as admin
Select student menu in sidebar
Click Add student & fill out the all information
Then Click Add Student Button
					</pre>
					<p><img src="<?= base_url() ?>assets/images/screenshots/new/add_student.png" width="1000" style="padding: 0px 20px;"></p>
					<p><img src="<?= base_url() ?>assets/images/screenshots/new/student_add_with_value.png" width="1000" style="padding: 0px 20px;"></p>
				    </section>

                                </section>

                                <section class="block-templates" id="attendance">
                                    <h2 class="item5"><i class="icon-play-circle"></i> How to Set Attendance</h2>
				    <section id="attendance">

					<h4>1. Student Attendance</h4>
					<pre class="prettyprint">
Login as admin or teacher
Select attendance menu in sidebar
Then select student attendance
Click Add student attendance Button
Select Class and Date and press Attendance Button
Check Student row Checkbox in Action Column for student attendance.
					</pre>

					<h4>2. Teacher Attendance</h4>
					<pre class="prettyprint">
Login as admin
Select attendance menu in sidebar
Then select teacher attendance
Click Add teacher attendance Button
Select Date and press Attendance Button
Check Teacher row Checkbox in Action Column for teacher attendance.
					</pre>

					<h4>3. Exam Attendance</h4>
					<pre class="prettyprint">
Login as admin or teacher
Select attendance menu in sidebar
Then select exam attendance
Click Add exam attendance Button
Select Exam, Class and Date and press Attendance Button
Check Student row Checkbox in Action Column. That means Student attend that exam.
					</pre>
				    </section>
				</section>

				<section class="block-templates" id="Promotion">
				    <h2 class="item6"> <i class="icon-play-circle"></i> Student Promotion to Next Class</h2>	
				    <section>
					<p>To promote a student from one class into another class, follow these steps</p>
					<h5>Step 1: A list of student qualified for promotion to next class</h5>
					<pre class="prettyprint">
Login as admin or teacher
Select promotion menu in sidebar
Then select class
After select class the page load all subject under that class with extra field
Set the minimum number in each subject field for promotion to next class
Then Click Promotion Mark Setting Button
					</pre>
					<p><img src="<?= base_url() ?>assets/images/screenshots/new/promotion_setting.png" width="1000" style="padding: 0px 20px;"></p>

					<h5>Step 2: Final step of student promotion to next class</h5>
					<pre class="prettyprint">
Status of student in result column
Check Student row Checkbox in Action Column for promote student to next class.
Then Click Promotion To Next Class Button
					</pre>
					<p><img src="<?= base_url() ?>assets/images/screenshots/new/promotion_result.png" width="1000" style="padding: 0px 20px;"></p>
				    </section>
				</section>


				<section class="block-templates" id="mail_sms">
				    <h2 class="item7"><i class="icon-inbox"></i> Mail and SMS</h2>
				    <section>

					<h5>What is Mail and SMS Template ?</h5>
					<p>Mail and SMS Template is structure of multiple purposes mail and sms.</p>
					<h5>How to Add Template</h5>
					<pre class="prettyprint">
Login as admin
Select mail/sms menu in sidebar
Then Select mail/sms template
Click Add a template Button
Then Create your Template
					</pre>
					<h5>What is Tags ?</h5>
					<p>Tags is attribute of each student information. Just like Student Name, Student Class, Student Section, Student Result with Marks and Grade, Student Email etc.</p>
					<p>
					    <img src="<?= base_url() ?>assets/images/screenshots/new/mailandsms.png" width="1000" style="padding: 0px 20px;">
					</p>
				    </section>
				</section>
				
				<section class="block-templates" id="media-upload">
				    <h2 class="item8"><i class="icon-file"></i> Media</h2>
				    <section id="media">

					<h5>What is Media ?</h5>
					<p>Share important documents for all users in school management system or Share documents only for a particular Class.</p>
					<h5>How to Add Documents and Share ?</h5>
					<pre class="prettyprint">
Login as admin or teacher
Select media menu in sidebar
Then Add a File or create a folder
For Share file or folder click globe icon.
And choose public for all users or choose class for a individual class students.
					</pre>
					<p>
					    <img src="<?= base_url() ?>assets/images/screenshots/new/sharefile.png" width="300" style="padding: 0px 20px;">
					    <img src="<?= base_url() ?>assets/images/screenshots/new/sharefilepop.png" width="600" style="padding: 0px 20px;">
					</p>
				    </section>
                                </section>

                                <section class="block-templates" id="about-admin">
                                    <h2 class="item6"><i class="icon-wrench"></i>All About Admin</h2>

				    <section id="admin">
					<div class="page-header"><h3>All About Admin</h3><hr class="notop"></div>
					<pre class="prettyprint lang-plain linenums">
Dashboard user counter widget, today attendance widget, Earning graph, payment chart, profile widget, notice widget.
Teacher list, add, edit, delete, view, print PDF, ID card & send PDF to mail
Class list, add, edit & delete
Section list, add, edit & delete
Student list, add, edit, delete, view, print PDF, ID card & send PDF to mail
Parents list, add, edit, delete, view, print PDF & send PDF to mail
User (Accountant & Librarian) list, add, edit, delete, view, print PDF, ID card & send PDF to mail
Subject list, add, edit & delete
Grade list, add, edit & delete
Exam list, add, edit & delete
Exam schedule list, add, edit & delete
Mark list, add & edit
Routine list, add, edit & delete
Student Attendance list, add & view.
Teacher Attendance list, add & view.
Exam Attendance list, add.
Library member list, add, edit, delete, view, print PDF & send PDF to mail
Books list, add, edit & delete
Issue list, add, edit & delete
Fine list
Transport list, add, edit & delete
Transport member list, add, edit, delete, view, print PDF & send PDF to mail
Hostel list, add, edit & delete
Category list, add, edit & delete
Hostel member list, add, edit, delete, view, print PDF & send PDF to mail
Fee type list, add, edit & delete
Invoice list, add, edit, delete, view, payment, print, print pdf & send pdf to mail.
Balance list.
Expense list, add, edit, delete.
    <!--Payment settings edit(Paypal).-->
Promotion stduents.
Media add folder, upload, delete & share files or folders.
Mail/SMS template list, add, edit, delete & view.
Mail/SMS Sent, list, view send mail/sms.
SMS Setting(Clickatell, twilio, Bulk).
Message Compose with attachment, inbox, sent, favorite, trash, reply & delete.
Notice list, add, edit, delete, print PDF & send PDF to mail.
Report list, view & download.
Setting edit.
Profile view.
Change Password.
Change Language.
Notice Alert.
					</pre>

					<p>
					<center>
					    <h4> Admin Dashboard </h4>
					</center>
					</p>
					<p><img src="<?= base_url() ?>assets/images/admin/admin_view_dash.png" width="1000" style="padding: 0px 20px;"></p>

					<p>
					<center>
					    <h4> Create Teacher </h4>
					</center>
					</p>
					<p><img src="<?= base_url() ?>assets/images/admin/admin_create_teacher.png" width="1000" style="padding: 0px 20px;"></p>

					<p>
					<center>
					    <h4> View Teacher List</h4>
					</center>
					</p>
					<p><img src="<?= base_url() ?>assets/images/admin/admin_view_teacher_list.png" width="1000" style="padding: 0px 20px;"></p>

					<p>
					<center>
					    <h4> Edit Teacher</h4>
					</center>
					</p>
					<p><img src="<?= base_url() ?>assets/images/admin/admin_edit_teacher.png" width="1000" style="padding: 0px 20px;"></p>

					<p>
					<center>
					    <h4> Create Class</h4>
					</center>
					</p>
					<p><img src="<?= base_url() ?>assets/images/admin/admin_create_class.png" width="1000" style="padding: 0px 20px;"></p>

					<p>
					<center>
					    <h4> View Class List</h4>
					</center>
					</p>
					<p><img src="<?= base_url() ?>assets/images/admin/admin_view_class_list.png" width="1000" style="padding: 0px 20px;"></p>

					<p>
					<center>
					    <h4> Edit Class</h4>
					</center>
					</p>
					<p><img src="<?= base_url() ?>assets/images/admin/admin_edit_class.png" width="1000" style="padding: 0px 20px;"></p>

					<p>
					<center>
					    <h4> Create Student</h4>
					</center>
					</p>
					<p><img src="<?= base_url() ?>assets/images/admin/admin_create_student.png" width="1000" style="padding: 0px 20px;"></p>

					<p>
					<center>
					    <h4> View Student List</h4>
					</center>
					</p>
					<p><img src="<?= base_url() ?>assets/images/admin/admin_view_student_list.png" width="1000" style="padding: 0px 20px;"></p>

					<p>
					<center>
					    <h4> Edit Student</h4>
					</center>
					</p>
					<p><img src="<?= base_url() ?>assets/images/admin/admin_edit_student.png" width="1000" style="padding: 0px 20px;"></p>

					<p>
					<center>
					    <h4> View Student Profile</h4>
					</center>
					</p>
					<p><img src="<?= base_url() ?>assets/images/admin/admin_view_student_profile.png" width="1000" style="padding: 0px 20px;"></p>

					<p>
					<center>
					    <h4> Create Parents</h4>
					</center>
					</p>
					<p><img src="<?= base_url() ?>assets/images/admin/admin_create_parents.png" width="1000" style="padding: 0px 20px;"></p>

					<p>
					<center>
					    <h4> View Parents List</h4>
					</center>
					</p>
					<p><img src="<?= base_url() ?>assets/images/admin/admin_view_parents_list.png" width="1000" style="padding: 0px 20px;"></p>

					<p>
					<center>
					    <h4> Edit Parents</h4>
					</center>
					</p>
					<p><img src="<?= base_url() ?>assets/images/admin/admin_edit_parents.png" width="1000" style="padding: 0px 20px;"></p>

					<p>
					<center>
					    <h4> View Parents Profile</h4>
					</center>
					</p>
					<p><img src="<?= base_url() ?>assets/images/admin/admin_view_parents_profile.png" width="1000" style="padding: 0px 20px;"></p>

					<p>
					<center>
					    <h4> Create Subject</h4>
					</center>
					</p>
					<p><img src="<?= base_url() ?>assets/images/admin/admin_create_subject.png" width="1000" style="padding: 0px 20px;"></p>

					<p>
					<center>
					    <h4> View Subject List</h4>
					</center>
					</p>
					<p><img src="<?= base_url() ?>assets/images/admin/admin_view_subject_list.png" width="1000" style="padding: 0px 20px;"></p>

					<p>
					<center>
					    <h4> Edit Subject</h4>
					</center>
					</p>
					<p><img src="<?= base_url() ?>assets/images/admin/admin_edit_subject.png" width="1000" style="padding: 0px 20px;"></p>

					<p>
					<center>
					    <h4> Create Notice </h4>
					</center>
					</p>
					<p><img src="<?= base_url() ?>assets/images/admin/admin_create_notice.png" width="1000" style="padding: 0px 20px;"></p>

					<p>
					<center>
					    <h4> View Notice List</h4>
					</center>
					</p>
					<p><img src="<?= base_url() ?>assets/images/admin/admin_view_notice_list.png" width="1000" style="padding: 0px 20px;"></p>

					<p>
					<center>
					    <h4> Notice Edit</h4>
					</center>
					</p>
					<p><img src="<?= base_url() ?>assets/images/admin/admin_edit_notice.png" width="1000" style="padding: 0px 20px;"></p>

					<p>
					<center>
					    <h4> Change settings</h4>
					</center>
					</p>
					<p><img src="<?= base_url() ?>assets/images/admin/admin_change_setting.png" width="1000" style="padding: 0px 20px;"></p>

					<p>
					<center>
					    <h4> Change Password</h4>
					</center>
					</p>
					<p><img src="<?= base_url() ?>assets/images/admin/admin_change_password.png" width="1000" style="padding: 0px 20px;"></p>


				    </section>

                                </section>

				<section class="block-templates" id="about-teacher">
                                    <h2><i class="icon-copy"></i>All About Teacher</h2>

				    <section id="teacher">
					<pre class="prettyprint lang-plain linenums">
 Dashboard Student & Teahcer counter, subject counter, today attendance, profile summery, notice widget & calender.
 Student information list & view.
 Teacher information list & view.
 Subject information list.
 Exam schedule information list & view.
 Mark add, list, view, print, pdf $ send pdf to mail.
 Routine list.
 Attendance add student attendance, view own attendance, add & view exam attendance.
 Books list.
 Transport list.
 Hostel list, hostel category list.
 Promotion stduents.
 Media add folder, upload, delete & share files or folders.
 Message Compose with attachment, inbox, sent, favorite, trash, reply & delete.
 Notice list & view.
 Profile view.
 Change Password.
 Change Language.
 Notice Alert.
					</pre>
					<p>
					<center>
					    <h4> Teacher Dashboard</h4>
					</center>
					</p>
					<p><img src="<?= base_url() ?>assets/images/teacher/teacher_dash.png" width="1000" style="padding: 0px 20px;"></p>
					<br>

					<p>
					<center>
					    <h4> Teacher Add Mark</h4>
					</center>
					</p>
					<p><img src="<?= base_url() ?>assets/images/teacher/teacher_add_mark.png" width="1000" style="padding: 0px 20px;"></p>

					<br>

					<p>
					<center>
					    <h4> Teacher Profile</h4>
					</center>
					</p>
					<p><img src="<?= base_url() ?>assets/images/teacher/teacher_profile.png" width="1000" style="padding: 0px 20px;"></p>
				    </section>

                                </section>

                                <section class="block-templates" id="about-accountant">
                                    <h2 class="item7"><i class="icon-cogs"></i> All About Accountant</h2>

				    <section id="accountant">
					<div class="page-header"><h3>Account Module</h3><hr class="notop"></div>
					<pre class="prettyprint lang-plain linenums">
Can view a Dashboard with Teacher counter, Fee Type counter, Invoice counter, Expense counter, profile summery, notice widget & calender.
View Teacher information list & view.
View Transport list & member list, edit, delete, view, print PDF & send PDF to mail.
View Hostel list & member list edit & delete.
Fee type list, add, edit & delete.
Invoice list, add, edit, delete, view, payment, print, print pdf & send pdf to mail.
Balance list.
Expense list, add, edit, delete.
Media view shared files or folders.
Message Compose with attachment, inbox, sent, favorite, trash, reply & delete.
Notice Board.
Profile view.
Change Password.
Change Language.
Notice Alert.
					</pre>
					<br>
					<p>
					<center>
					    <h4>Accountant Dashboard</h4>
					</center>
					</p>
					<p><img src="<?= base_url() ?>assets/images/account/account_dash.png" width="1000" style="padding: 0px 20px;"></p>
					<br>
					<p>
					<center>
					    <h4>Accountant Add Expense</h4>
					</center>
					</p>
					<p><img src="<?= base_url() ?>assets/images/account/account_add_expense.png" width="1000" style="padding: 0px 20px;"></p>
					<br>
					<p>
					<center>
					    <h4>Accountant Add Invoice</h4>
					</center>
					</p>
					<p><img src="<?= base_url() ?>assets/images/account/account_add_invoice.png" width="1000" style="padding: 0px 20px;"></p>
					<br>

					<p>
					<center>
					    <h4>Accountant View Invoice</h4>
					</center>
					</p>
					<p><img src="<?= base_url() ?>assets/images/account/account_view_invoice.png" width="1000" style="padding: 0px 20px;"></p>
					<br>

					<p>
					<center>
					    <h4>Accountant View Invoice History</h4>
					</center>
					</p>
					<p><img src="<?= base_url() ?>assets/images/account/account_view_invoice_history.png" width="1000" style="padding: 0px 20px;"></p>
					<br>

					<p>
					<center>
					    <h4>Accountant View Student Balance</h4>
					</center>
					</p>
					<p><img src="<?= base_url() ?>assets/images/account/account_view_student_balance.png" width="1000" style="padding: 0px 20px;"></p>
					<br>

					<p>
					<center>
					    <h4>Accountant View Fee Type List</h4>
					</center>
					</p>
					<p><img src="<?= base_url() ?>assets/images/account/account_view_fee_type_list.png" width="1000" style="padding: 0px 20px;"></p>
					<br>

                      <!--<p>-->
        <!--		<center>-->
					<!--			<h4>Accountant Add Payment Method</h4>-->
					<!--		</center>-->
					<!--		</p>-->
					<!--		<p><img src="--><?//= base_url() ?><!--assets/images/account/account_add_payment_method.png" width="1000" style="padding: 0px 20px;"></p>-->
					<!--		<br>-->


				    </section>


                                </section>
				<section class="block-templates" id="about-librarian">
                                    <h2 class="item8"><i class="icon-question-sign"></i>All About Librarian </h2>

				    <section id="librarian">

					<pre class="prettyprint lang-plain linenums">
View Dashboard with Teacher counter, Library members counter, Books counter, Issue counter widget.
Teacher information list & view.
Subject information list & view.
Library member list view, edit & delete.
Book list, add, edit & delete.
Issue book list, add, view & delete.
Fine add.
Transport list.
Hostel list.
Media view shared files or folders.
Message Compose with attachment, inbox, sent, favorite, trash, reply & delete.
Notice Board.
Profile view.
Change Password.
Change Language.
Notice Alert.					</pre>
					<br>
					<p>
					<center>
					    <h4>Library Dashboard</h4>
					</center>
					</p>
					<p><img src="<?= base_url() ?>assets/images/lib/lib_dash.png" width="1000" style="padding: 0px 20px;"></p>
					<br>
					<p>
					<center>
					    <h4>Library Members</h4>
					</center>
					</p>
					<p><img src="<?= base_url() ?>assets/images/lib/lib_member_list.png" width="1000" style="padding: 0px 20px;"></p>
					<br>
					<p>
					<center>
					    <h4>Library Issue Books</h4>
					</center>
					</p>
					<p><img src="<?= base_url() ?>assets/images/lib/lib_add_book_issue.png" width="1000" style="padding: 0px 20px;"></p>
					<br>
					<p>
					<center>
					    <h4>Library Books List</h4>
					</center>
					</p>
					<p><img src="<?= base_url() ?>assets/images/lib/lib_book_list.png" width="1000" style="padding: 0px 20px;"></p>
				    </section>




                                </section>

				<section class="block-templates" id="about-student">
                                    <h2 class="item8"><i class="icon-question-sign"></i> All About Student</h2>
				    <section id="student">
					<pre class="prettyprint lang-plain linenums">
Dashboard teacher counter, Subject counter, Issue book counter, Own invoice counter, profile summery, notice widget & calender.
Teacher information list & view.
Subject information list of own class.
Exam schedule information list & view of own class.
Mark information view of own.
Routine list of own class.
Attendance information of own.
Library Books list.
Issue books list & own library profile information & history.
Transport list & own transport profile information & history.
Hostel list, category list & own hostel profile information & history.
Invoice list & own invoice/account history.
Media view shared files or folders.
Message Compose with attachment, inbox, sent, favorite, trash, reply & delete.
Notice Board.
Profile view.
Change Password.
Change Language.
Notice Alert.
					</pre>
					<br>
					<p>
					<center>
					    <h4>Student Dashboard</h4>
					</center>
					</p>
					<p><img src="<?= base_url() ?>assets/images/student/student_view_dash.png" width="1000" style="padding: 0px 20px;"></p>
					<br>

					<p>
					<center>
					    <h4>Student Attendance View</h4>
					</center>
					</p>
					<p><img src="<?= base_url() ?>assets/images/student/student_view_attendance.png" width="1000" style="padding: 0px 20px;"></p>
					<br>
				    </section>

                                </section>


                                <section class="block-templates" id="about-parent">
                                    <h2 class="item8"><i class="icon-question-sign"></i> All About Parents </h2>
				    
				    <?php $this->load->view('help/parent_help'); ?>
                                </section>

                            </div>
                        </div>
                    </div>

                </div>
		<footer id="conclusion">
		    <div class="container">        
			@ <a href="http://www.inetstz.com" target="_blank">INETS Company Limited </a>
		    </div>
		</footer>
            </div>
        </div>
        <!-- Footer
         ================================================== -->


        <div id="back-top">
            <a href="#"><i class="icon-double-angle-up"></i></a>
        </div>
<!-- <script src="js/bootstrap-scrollspy.js"></script>  -->
	<!--        <div class="language-modal">
		    <div class="modal_bg"></div>
		    <div class="modal">
			<div class="modal-header">
			    <span class="modal_remove pull-right"><i class="icon-remove"></i></span>
			    <h3>Choose language</h3>
			</div>
			<div class="modal-body">
			    <ul id="modal_languages" class="nav nav-list bs-docs-sidenav"></ul>
			</div>
		    </div>
		</div>-->
    </body> 
</html>
