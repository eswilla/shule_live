<!DOCTYPE html>
<html lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<!-- Meta, title, CSS, favicons, etc. -->
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<title>ShuleSoft</title>
	<link rel="SHORTCUT ICON" href="<?= base_url("uploads/images/$siteinfos->photo") ?>" />
	<!-- bootstrap 3.0.2 -->
	<link href="<?php echo base_url('assets/bootstrap/bootstrap.min.css'); ?>" rel="stylesheet"  type="text/css">
	<!-- font Awesome -->
	<link href="<?php echo base_url('assets/fonts/font-awesome.css'); ?>" rel="stylesheet"  type="text/css">
	<!-- Style -->
	<link href="<?php echo base_url('assets/custom.css'); ?>" rel="stylesheet"  type="text/css">
	<link href="<?php echo base_url('assets/materialize.min.css'); ?>" rel="stylesheet"  type="text/css">
	<!--Import Google Icon Font-->
	<link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

</head>

<body style="background:#F7F7F7;">
<div class="">
	<a class="hiddenanchor" id="toregister"></a>
	<a class="hiddenanchor" id="tologin"></a>

	<div id="wrapper">
	    <h5 class="text-center"><?=$siteinfos->sname?></h5>
		<div id="login" class=" form">
		       <!--<a href="<?php //echo base_url('/admission/index'); ?>">Online Admission</a>-->
			<section class="login_content">
				<form method="post">
				    <h1 class="text-center" style="margin: 50px 0 30px;">Login</h1>
					<?php
					if ($form_validation == "No") {

					} else {
						if (count($form_validation)) {
							echo "<div class=\"alert alert-danger alert-dismissable\">
                        <i class=\"fa fa-ban\"></i>
                        <button aria-hidden=\"true\" data-dismiss=\"alert\" class=\"close\" type=\"button\">×</button>
                        $form_validation
                    </div>";
						}
					}
					if ($this->session->flashdata('reset_success')) {
						$message = $this->session->flashdata('reset_success');
						echo "<div class=\"alert alert-success alert-dismissable\">
                    <i class=\"fa fa-ban\"></i>
                    <button aria-hidden=\"true\" data-dismiss=\"alert\" class=\"close\" type=\"button\">×</button>
                    $message
                </div>";
					}
					?>
					<div class="input-field">
						<i class="material-icons prefix">account_circle</i>
						<input type="text" name="username" id="inputName" value="<?= set_value('username') ?>" class="validate"/>
						<label for="inputName"><?=$this->lang->line('username')?></label>
					</div>
					<div class="input-field">

						<i class="material-icons prefix">lock_open</i>
						<input id="inputPassword" name="password" type="password" class="validate"/>
						<label for="inputPassword">Password</label>
					</div>
					<div class="input-field">


						<button class="btn btn-default  btn-block text-uppercase" type="submit" >Log in</button>
						<input type="checkbox" id="remember" name="remember" >
						<label for="remember"> Remember Me</label>
						<a class="reset_pass" href="<?= base_url('reset/index') ?>">Lost your password?</a>

					</div>
					<div class="clearfix"></div>
					<div class="separator">
						<div class="clearfix"></div>
						<br />
						<div>
							<h1 class="text-center"><i class="fa fa-paw" style="font-size: 26px;"></i> ShuleSoft</h1>

							<p class="text-center">©<?=  date('Y')?> All Rights Reserved.</p>
						</div>

                        <div class="text-capitalize  text-center">
                            <?php

                            echo anchor('termsandprivacy/index', 'Terms and Privacy');

                            ?>

                        </div>
					</div>
				</form>
			    <p align='right'> 
				<a href="http://www.inetstz.com" target="_blank"><img src="<?php echo base_url('/assets/images/inets.png'); ?>"  height="40" title="Owned by Inets Company Limited"/></a>
			    </p>
			</section>
		</div>

	</div>
</div>
<script type="text/javascript" src="<?php echo base_url('assets/shulesoft/jquery.js'); ?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/bootstrap/bootstrap.min.js'); ?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/materialize.min.js'); ?>"></script>
</body>
</html>