
<div class="box">
    <div class="box-header">
        <h3 class="box-title"><i class="fa fa-signal"></i> <?=$this->lang->line('panel_title')?></h3>

       
        <ol class="breadcrumb">
            <li><a href="<?=base_url("dashboard/index")?>"><i class="fa fa-laptop"></i> <?=$this->lang->line('menu_dashboard')?></a></li>
            <li><a href="<?=base_url("classlevel/index")?>"><?=$this->lang->line('menu_classlevel')?></a></li>
            <li class="active"><?=$this->lang->line('menu_add')?> <?=$this->lang->line('menu_classlevel')?></li>
        </ol>
    </div><!-- /.box-header -->
    <div class="box-body">
        <div class="row">
            <div class="col-sm-8">
                <form class="form-horizontal" role="form" method="post">

                    <?php 
                        if(form_error('name')) 
                            echo "<div class='form-group has-error' >";
                        else     
                            echo "<div class='form-group' >";
                    ?>
                        <label for="name" class="col-sm-2 control-label">
                            <?=$this->lang->line("name")?>
                        </label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" id="name" name="name" value="<?=set_value('name',$classlevel->name)?>" >
                        </div>
                        <span class="col-sm-4 control-label">
                            <?php echo form_error('name'); ?>
                        </span>
                    </div>

                    <?php 
                        if(form_error('start_date')) 
                            echo "<div class='form-group has-error' >";
                        else     
                            echo "<div class='form-group' >";
                    ?>
                        <label for="start_date" class="col-sm-2 control-label">
                            <?=$this->lang->line("start_date")?>
                        </label>
                        <div class="col-sm-6">
                            <input type="date" class="form-control" id="start_date" name="start_date" value="<?=set_value('start_date',$classlevel->start_date)?>" >
                        </div>
                        <span class="col-sm-4 control-label">
                            <?php echo form_error('start_date'); ?>
                        </span>
                    </div>

                    <?php 
                        if(form_error('end_date')) 
                            echo "<div class='form-group has-error' >";
                        else     
                            echo "<div class='form-group' >";
                    ?>
                        <label for="end_date" class="col-sm-2 control-label">
                            <?=$this->lang->line("end_date")?>
                        </label>
                        <div class="col-sm-6">
                            <input type="date" class="form-control" id="end_date" name="end_date" value="<?=set_value('end_date',$classlevel->end_date)?>" >
                        </div>
                        <span class="col-sm-4 control-label">
                            <?php echo form_error('end_date'); ?>
                        </span>
                    </div>

                    <?php 
                        if(form_error('span_number')) 
                            echo "<div class='form-group has-error' >";
                        else     
                            echo "<div class='form-group' >";
                    ?>
                        <label for="span_number" class="col-sm-2 control-label">
                            <?=$this->lang->line("span_number")?>
                        </label>
                        <div class="col-sm-6">
                            <input type="number" class="form-control" id="span_number" name="span_number" value="<?=set_value('span_number',$classlevel->span_number)?>" >
                        </div>
                        <span class="col-sm-4 control-label">
                            <?php echo form_error('span_number'); ?>
                        </span>
                    </div>


 <?php
		    if (form_error('result_format'))
			echo "<div class='form-group has-error' >";
		    else
			echo "<div class='form-group' >";
		    ?>
		    <label for="classlevel" class="col-sm-2 control-label">
			<?=$this->lang->line('result_format')?>
		    </label>
		    <div class="col-sm-6">
			<?php
			$array = array();
			$array['ACSEE'] = 'ACSEE';
			$array['CSEE'] = 'CSEE';
			$array['Other'] = 'Other';
			
			echo form_dropdown("result_format", $array, set_value("result_format",$classlevel->result_format), "id='result_format' class='form-control'");
			?>
		    </div>
		    <span class="col-sm-4 control-label">
			<?php echo form_error('result_format'); ?>
		    </span>
	    </div>

                    <?php 
                        if(form_error('note')) 
                            echo "<div class='form-group has-error' >";
                        else     
                            echo "<div class='form-group' >";
                    ?>
                        <label for="note" class="col-sm-2 control-label">
                            <?=$this->lang->line("note")?>
                        </label>
                        <div class="col-sm-6">
                            <textarea style="resize:none;" class="form-control" id="note" name="note"><?=set_value('note',$classlevel->note)?></textarea>
                        </div>
                        <span class="col-sm-4 control-label">
                            <?php echo form_error('note'); ?>
                        </span>
                    </div>

                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-8">
                            <input type="submit" class="btn btn-success" value="<?=$this->lang->line("add_level")?>" >
                        </div>
                    </div>
                </form>
            </div>    
        </div>
    </div>
</div>
