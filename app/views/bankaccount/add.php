
<div class="box">
    <div class="box-header">
        <h3 class="box-title"><i class="fa icon-feetype"></i> <?=$this->lang->line('panel_title')?></h3>

        <ol class="breadcrumb">
            <li><a href="<?=base_url("dashboard/index")?>"><i class="fa fa-laptop"></i> <?=$this->lang->line('menu_dashboard')?></a></li>
            <li><a href="<?=base_url("bankaccount/index")?>"><?=$this->lang->line('menu_bankaccount')?></a></li>
            <li class="active"><?=$this->lang->line('menu_add')?> <?=$this->lang->line('menu_bankaccount')?></li>
        </ol>
    </div><!-- /.box-header -->
    <!-- form start -->
    <div class="box-body">
        <div class="row">
            <div class="col-sm-8">
                <form class="form-horizontal" role="form" method="post">

                    <?php 
                        if(form_error('bankaccount_name')) 
                            echo "<div class='form-group has-error' >";
                        else     
                            echo "<div class='form-group' >";
                    ?>
                        <label for="feetype" class="col-sm-2 control-label">
                            <?=$this->lang->line("bankaccount_name")?>
                        </label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" id="bankaccount_name" name="bankaccount_name" value="<?=set_value('bankaccount_name')?>" >
                        </div>
                        <span class="col-sm-4 control-label">
                            <?php echo form_error('bankaccount_name'); ?>
                        </span>
                    </div>
            
	       <?php 
                        if(form_error('bankaccount_branch')) 
                            echo "<div class='form-group has-error' >";
                        else     
                            echo "<div class='form-group' >";
                    ?>
                        <label for="feetype" class="col-sm-2 control-label">
                            <?=$this->lang->line("bankaccount_branch")?>
                        </label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" id="bankaccount_branch" name="bankaccount_branch" value="<?=set_value('bankaccount_branch')?>" >
                        </div>
                        <span class="col-sm-4 control-label">
                            <?php echo form_error('bankaccount_branch'); ?>
                        </span>
                    </div>
	
	
	    <?php 
                        if(form_error('account_name')) 
                            echo "<div class='form-group has-error' >";
                        else     
                            echo "<div class='form-group' >";
                    ?>
                        <label for="feetype" class="col-sm-2 control-label">
                            <?=$this->lang->line("bankaccount_account_name")?>
                        </label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" id="account_name" name="account_name" value="<?=set_value('bankaccount_account')?>" >
                        </div>
                        <span class="col-sm-4 control-label">
                            <?php echo form_error('account_name'); ?>
                        </span>
                    </div>
	
	    <?php 
                        if(form_error('bankaccount_account')) 
                            echo "<div class='form-group has-error' >";
                        else     
                            echo "<div class='form-group' >";
                    ?>
                        <label for="feetype" class="col-sm-2 control-label">
                            <?=$this->lang->line("bankaccount_account")?>
                        </label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" id="bankaccount_account" name="bankaccount_account" value="<?=set_value('bankaccount_account')?>" >
                        </div>
                        <span class="col-sm-4 control-label">
                            <?php echo form_error('bankaccount_account'); ?>
                        </span>
                    </div>
                 
                        <?php 
                        if(form_error('bankaccount_currency')) 
                            echo "<div class='form-group has-error' >";
                        else     
                            echo "<div class='form-group' >";
                    ?>
                        <label for="feetpeAccount" class="col-sm-2 control-label">
                            <?=$this->lang->line("bankaccount_currency")?>
                        </label>
                        <div class="col-sm-6">

                            <?php
                                $array = array('0' => $this->lang->line("bankaccount_select_currency"));
				
                                $array['TSH'] = 'TZS';
                                $array['USD'] = 'USD';
				$array['EURO'] = 'EURO';
				$array['KSH'] = 'KSH';
                                echo form_dropdown("bankaccount_currency", $array, set_value("bankaccount_currency"), "id='bankaccount_currency' class='form-control'");
                            ?>
                        </div>
                        <span class="col-sm-4 control-label">
                            <?php echo form_error('bankaccount_currency'); ?>
                        </span>
                    </div>   
            

	    <?php 
                        if(form_error('bankaccount_swiftcode')) 
                            echo "<div class='form-group has-error' >";
                        else     
                            echo "<div class='form-group' >";
                    ?>
                        <label for="amount" class="col-sm-2 control-label">
                            <?=$this->lang->line("bankaccount_swiftcode")?>
                        </label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" id="bankaccount_swiftcode" name="bankaccount_swiftcode" value="<?=set_value('bankaccount_swiftcode')?>" >
                        </div>
                        <span class="col-sm-4 control-label">
                            <?php echo form_error('bankaccount_swiftcode'); ?>
                        </span>
                    </div>
	
	
    
                    <?php 
                        if(form_error('bankaccount_note')) 
                            echo "<div class='form-group has-error' >";
                        else     
                            echo "<div class='form-group' >";
                    ?>
                        <label for="note" class="col-sm-2 control-label">
                            <?=$this->lang->line("bankaccount_note")?>
                        </label>
                        <div class="col-sm-6">
                            <textarea class="form-control" style="resize:none;" id="note" name="note"><?=set_value('bankaccount_note')?></textarea>
                        </div>
                        <span class="col-sm-4 control-label">
                            <?php echo form_error('bankaccount_note'); ?>
                        </span>
                    </div>

                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-8">
                            <input type="submit" class="btn btn-success" value="<?=$this->lang->line("add_bankaccount")?>" >
                        </div>
                    </div>

                </form>

            </div>
        </div>
    </div>
</div>
<script type="text/javascript">

    $(document).ready(function(){

        $("#startdate").click(function(){
            $("#startdate").pickadate({

                selectYears: 50,
                selectMonths: true,
                max:new Date("2018")
            });
        });

    });

    $(document).ready(function(){

        $("#enddate").click(function(){
            $("#enddate").pickadate({

                selectYears: 50,
                selectMonths: true,
                max:new Date("2018")
            });
        });

    });

</script>