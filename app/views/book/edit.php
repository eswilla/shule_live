

<div class="box">
    <div class="box-header">
        <h3 class="box-title"><i class="fa icon-lbooks"></i> <?= $this->lang->line('panel_title') ?></h3>
        <ol class="breadcrumb">
            <li><a href="<?= base_url("dashboard/index") ?>"><i class="fa fa-laptop"></i> <?= $this->lang->line('menu_dashboard') ?></a></li>
            <li><a href="<?= base_url("book/index") ?>"><?= $this->lang->line('menu_books') ?></a></li>
            <li class="active"><?= $this->lang->line('menu_edit') ?> <?= $this->lang->line('menu_books') ?></li>
        </ol>
    </div><!-- /.box-header -->
    <!-- form start -->
    <div class="box-body">
        <div class="row">
            <div class="col-sm-8">

                <form class="form-horizontal" role="form" method="post">
		    <?php
		    if (form_error('book'))
			echo "<div class='form-group has-error' >";
		    else
			echo "<div class='form-group' >";
		    ?>
		    <label for="book" class="col-sm-2 control-label">
			<?= $this->lang->line("book_name") ?>
		    </label>
		    <div class="col-sm-6">
			<input type="text" class="form-control" id="book" name="book" value="<?= set_value('book', $book->book) ?>" >
		    </div>
		    <span class="col-sm-4 control-label">
			<?php echo form_error('book'); ?>
		    </span>
	    </div>

	    <?php
	    if (form_error('author'))
		echo "<div class='form-group has-error' >";
	    else
		echo "<div class='form-group' >";
	    ?>
	    <label for="author" class="col-sm-2 control-label">
		<?= $this->lang->line("book_author") ?>
	    </label>
	    <div class="col-sm-6">
		<input type="text" class="form-control" id="author" name="author" value="<?= set_value('author', $book->author) ?>" >
	    </div>
	    <span class="col-sm-4 control-label">
		<?php echo form_error('author'); ?>
	    </span>
	</div>


	<?php
	if (form_error('book_edition'))
	    echo "<div class='form-group has-error' >";
	else
	    echo "<div class='form-group' >";
	?>
	<label for="price" class="col-sm-2 control-label">
	    <?= $this->lang->line("book_edition") ?>
	</label>
	<div class="col-sm-6">
	    <input type="text" class="form-control" id="price" name="edition" value="<?= set_value('edition', $book->edition) ?>" >
	</div>
	<span class="col-sm-4 control-label">
	    <?php echo form_error('book_edition'); ?>
	</span>
    </div>


    <?php
    if (form_error('book_class'))
	echo "<div class='form-group has-error' >";
    else
	echo "<div class='form-group' >";
    ?>
    <label for="book_class" class="col-sm-2 control-label">
	<?= $this->lang->line("book_class") ?>
    </label>
    <div class="col-sm-6">
	 <?php
	    if (!empty($classes)) {
		echo "<input type=\"checkbox\" id='checkboxs' onclick='check_all()'>Check All</option>";
		$arr = [];
		foreach ($book_classes as $class) {
		    array_push($arr, $class->classes_id);
		}
		foreach ($classes as $class) {
		    $checked = in_array($class->classesID, $arr) ? 'checked' : '';
		    echo "&nbsp;&nbsp; <input type=\"checkbox\" class='check'  name=\"classe[]\"  " . $checked . "  value=\"$class->classesID\">", $class->classes, "";
		}
	    }
	    ?>
    </div>
    <span class="col-sm-4 control-label">
	<?php echo form_error('book_class'); ?>
    </span>
</div>

<?php
if (form_error('book_subject'))
    echo "<div class='form-group has-error' >";
else
    echo "<div class='form-group' >";
?>
<label for="book_subject" class="col-sm-2 control-label">
    <?= $this->lang->line("book_subject") ?>
</label>
<div class="col-sm-6">
    <select name="subject" class="form-control" >
	<?php foreach ($subjects as $subject) {
	    ?>
    	<option value="<?= $subject->subject ?>" ><?= ucwords($subject->subject) ?></option>
	<?php } ?>
    </select>
</div>
<span class="col-sm-4 control-label">
    <?php echo form_error('book_subject'); ?>
</span>
</div>

<?php
if (form_error('book_for'))
    echo "<div class='form-group has-error' >";
else
    echo "<div class='form-group' >";
?>
<label for="book_subject" class="col-sm-2 control-label">
    <?= $this->lang->line("book_for") ?>
</label>
<div class="col-sm-6">
    <select name="book_for" class="form-control" >
	<option value="teachers" >Teachers</option>
	<option value="students" >Students</option>
	<option value="both" >both</option>
    </select>
</div>
<span class="col-sm-4 control-label">
    <?php echo form_error('book_for'); ?>
</span>
</div>


<?php
if (form_error('quantity'))
    echo "<div class='form-group has-error' >";
else
    echo "<div class='form-group' >";
?>
<label for="quantity" class="col-sm-2 control-label">
    <?= $this->lang->line("book_quantity") ?>
</label>
<div class="col-sm-6">
    <input type="text" class="form-control" id="quantity" name="quantity" value="<?= set_value('quantity', $book->quantity) ?>" >
</div>
<span class="col-sm-4 control-label">
    <?php echo form_error('quantity'); ?>
</span>
</div>

<?php
if (form_error('rack'))
    echo "<div class='form-group has-error' >";
else
    echo "<div class='form-group' >";
?>
<label for="rack" class="col-sm-2 control-label">
    <?= $this->lang->line("book_rack_no") ?>
</label>
<div class="col-sm-6">
    <input type="text" class="form-control" id="rack" name="rack" value="<?= set_value('rack', $book->rack) ?>" >
</div>
<span class="col-sm-4 control-label">
    <?php echo form_error('rack'); ?>
</span>
</div>

<div class="form-group">
    <div class="col-sm-offset-2 col-sm-8">
	<input type="submit" class="btn btn-success" value="<?= $this->lang->line("update_book") ?>" >
    </div>
</div>
</form>

</div>
</div>
</div>
</div>
<script type="text/javascript"> 
    function check_all() {
	$('.check').prop('checked', true);
    }
</script>