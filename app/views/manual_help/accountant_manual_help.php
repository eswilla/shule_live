<!--/**-->
<!--* ------------------------------------------->
<!--*-->
<!--* ******* Address****************-->
<!--* INETS COMPANY LIMITED-->
<!--* P.O BOX 32258, DAR ES SALAAM-->
<!--* TANZANIA-->
<!--*-->
<!--*
-->
<!--* *******Office Location *********-->
<!--* 11th block, Bima Road, Mikocheni B, Kinondoni, Dar es salaam-->
<!--*-->
<!--*-->
<!--* ********Contacts***************-->
<!--* Email: <info@inetstz.com>-->
<!--* Website: <www.inetstz.com>-->
<!--    * Mobile: <+255 655 406 004>-->
<!--    * Tel:    <+255 22 278 0228>-->
<!--    * ------------------------------------------->
<!--    */-->

<?php $this->load->view("components/page_header"); ?>
<?php $this->load->view("components/page_topbar"); ?>
<?php $this->load->view("components/page_menu"); ?>


<div class="container-fluid">
    <div class="row">

        <div class="col-sm-4"></div>
        <div class="col-sm-8">
            <div class="col-sm-2"></div
            <div class="col-sm-12">
                <h1>SHULESOFT USER HELP</h1>
                <p>This is the user help for helping a teacher to navigate through the system
                    according to his level of access.</p>
            </div>
            <div class="col-sm-2"></div>

        </div>

        <div class="col-sm-3"></div>
        <div class="col-sm-8">
            <div class="col-sm-2"></div>
            <div class="col-sm-12">
                <h3>ACCOUNTANT MAIN DASHBOARD</h3>
                <p>This is the main part of teacher dashboard where the navigation starts after logging in
                    to the system.Check navigation items in the left side of the dashboard.</p>
                <img class="img-responsive center-block" src="http://192.168.2.19:8888/shulesoft/uploads/manual_help_images/teacher_dashboard/TeacherDashboard.png"/>

            </div>

            <div class="col-sm-2"></div>
            <div></div>
        </div>

        <br/>
        <hr>

   

        <div class="col-sm-3"></div>
        <div class="col-sm-8">
            <div class="col-sm-2"></div>
            <div class="col-sm-12"
                 <h3>CHECKING SUBJECT FOR SPECIFIC CLASS </h3>
                <p>By clicking "Subject" then "Select class"in drop down menu</p>
                <br/>
                <img class="img-responsive center-block" src="http://192.168.2.19:8888/shulesoft/uploads/manual_help_images/teacher_dashboard/SubjectList.png"/>

            </div>
            <div class="col-sm-2"></div>
        </div>



        <div class="col-sm-3"></div>
        <div class="col-sm-8">
            <div class="col-sm-2"></div>
            <div class="col-sm-12"
                 <h3>VIEW TRANSPORT DETAILS</h3>
                <p>By Clicking Transport ,you can view transport route with their fares</p>
                <br/>
                <img class="img-responsive center-block" src="http://192.168.2.19:8888/shulesoft/uploads/manual_help_images/teacher_dashboard/Transport.png"/>

            </div>
            <div class="col-sm-2"></div>
        </div>

        <div class="col-sm-3"></div>
        <div class="col-sm-8">
            <div class="col-sm-2"></div>
            <div class="col-sm-12"
                 <h3>VIEW HOSTEL</h3>
                <p>By Clicking Hostels ,you can view all hostel and their categories 
                    the select hostel to know the number of hostels and category to view hostel category</p>
                <br/>
                <img class="img-responsive center-block" src="http://192.168.2.19:8888/shulesoft/uploads/manual_help_images/teacher_dashboard/Hostels.png"/>
                <img class="img-responsive center-block" src="http://192.168.2.19:8888/shulesoft/uploads/manual_help_images/teacher_dashboard/HostelCat.png"/>
            </div>
            <div class="col-sm-2"></div>
        </div>




        <div class="col-sm-3"></div>
        <div class="col-sm-8">
            <div class="col-sm-2"></div>
            <div class="col-sm-12"
                 <h3>MESSAGE</h3>
                <p>By Clicking Message,you can view inbox messages and you can compose a message to the other stuff member  </p>
                <br/>
                <img class="img-responsive center-block" src="http://192.168.2.19:8888/shulesoft/uploads/manual_help_images/teacher_dashboard/msg.png"/>

            </div>
            <div class="col-sm-2"></div>
        </div>


        <div class="col-sm-3"></div>
        <div class="col-sm-8">
            <div class="col-sm-2"></div>
            <div class="col-sm-12"
                 <h3>VIEW NOTICE</h3>
                <p>By Clicking Notice,here you can see if there is new notification from the administrator </p>
                <br/>
                <img class="img-responsive center-block" src="http://192.168.2.19:8888/shulesoft/uploads/manual_help_images/teacher_dashboard/Notice.png"/>

            </div>
            <div class="col-sm-2"></div>
        </div>

    </div>
</div>

<?php $this->load->view("components/page_footer"); ?>

