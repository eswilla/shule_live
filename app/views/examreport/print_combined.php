<?php
/**
 * Description of print_combined
 *
 *  -----------------------------------------------------
 *  Copyright: INETS COMPANY LIMITED
 *  Website: www.inetstz.com
 *  Email: info@inetstz.com
 *  -----------------------------------------------------
 * @author Ephraim Swilla
 */
$usertype = $this->session->userdata("usertype");
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <META HTTP-EQUIV="Pragma" CONTENT="no-cache">
        <title>report</title>

 <!--<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/many_partners_certificate.css" >-->     
	<link href="<?php echo base_url('assets/css/print_all/print.css'); ?>" rel="stylesheet" type="text/css" media="print">
	<!--<link href="<?php echo base_url('assets/bootstrap/bootstrap.min.css'); ?>" rel="stylesheet">-->
	<link href="<?php echo base_url('assets/css/print_all/certificate.css'); ?>" rel="stylesheet" type="text/css">

        <link rel="shortcut" href="<?= base_url(); ?>assets/images/favicon.ico">
	<!-- Begin shared CSS values -->
	<style type="text/css" >
	    @font-face {
		font-family: DejaVuSans_b;
		src: url("../../assets/fonts/DejaVuSans_b.woff") format("woff");
	    }
	    table.table.table-striped td,table.table.table-striped th, table.table {
		border: 1px solid #000000;
		margin: 1px auto !important;
	    }

	    #report_footer{
		margin-left: 2em;
	    }
	    .headings{
		margin-left: 2em;
	    }
	    .school_logo{
		float: left; height: 150px; width: 20%
	    }
	    .head_letter_photo{
		float: right; height: 150px; width: 79%
	    }
	    .school_info_heading{
		float: left; text-align: center; width: 77%; height: 149px;
	    }
	    .student_photo{
		float: right; width: 22%; height: 149px;
	    }
	    @media print {
		/*.panel{*/
		/*padding-left: 10%;*/
		/*}*/
		/*table.table.table-striped td,table.table.table-striped th, table.table {*/
		/*border: 1px solid #000000 !important;*/
		/*margin: 1px auto !important;*/
		/*}*/
		/*.table-stripe {*/
		/*background-color: #dedede !important;*/
		/*border: 1px solid #000000 !important;*/
		/*}*/
		/*body, .panel {*/
		/*margin: 0;*/
		/*box-shadow: 0;*/
		/*}*/
		/*.headings{*/
		/*margin-left:2em;*/
		/*}*/
		/*#t7_1{*/
		/*width: 23em !important;*/
		/*}*/
		#wrappers{
		    page-break-before: always;
		}
		h3, h4 {
		    page-break-after: avoid;
		    font-size: 15px;
		    line-height: 18px;
		}

	    }
	    h3, h4 {
		page-break-after: avoid;
		font-size: 15px;
		line-height: 18px;
	    }

	</style>
	<?php if ($level->result_format == 'CSEE') { ?>
    	<style  type="text/css" >
    	  .panel{
    		color: #000000 !important;

    	    }
    	    table.table.table-striped td,table.table.table-striped th, table.table {
    		font-size: 12px !important;
    		margin-left: 10%;
    	    }
    	</style>
	<?php } ?>
    </head>
    <body onload="window.print()">
	<?php
	foreach ($students as $student_info) {
	    $student = (object) $student_info;
	    ?>
    	<div class="rows">
    	    <div class="col-md-12">

    		<div id="wrappers" style="overflow:hidden;  height: 265mm;  height: 265mm; border: 1px solid white; margin: 10mm auto; line-break: auto" >

    		    <section class="panel">
    			<div>
				<?php
				$array = array(
				    "src" => base_url('uploads/images/' . $siteinfos->photo),
				    'width' => '100%',
				    'height' => '100%',
				    'class' => 'img-rounded',
				);
				$photo = array(
				    "src" => base_url('uploads/images/' . $student->photo),
				    'width' => '100%',
				    'height' => '100%',
				    'class' => 'img-rounded',
				);
				?>
    			    <div class="row">
    				<div class="school_logo">
					<?php echo img($array);
					?>
    				</div>
    				<div class="head_letter_photo">
    				    <div class="school_info_heading">
    					<br/>
    					<h2 style="font-weight: bolder; font-size: 25px;" class="text-uppercase"><?= $siteinfos->sname ?></h2>
    					<h4><?= 'P.O BOX ' . $siteinfos->box . ', ' . $siteinfos->address ?>.</h4>
    					<h3>Cell: <?= str_replace(',', ' / ', $siteinfos->phone) ?></h3>
    					<h3> Email:<?= $siteinfos->email ?></h3>
    					<h3>Website: <?= $siteinfos->website ?></h3>
    				    </div>
    				    <div class="student_photo">
					    <?php echo img($photo);
					    ?>
    				    </div>
    				    <div style="clear: both"></div>
    				</div>
    				<div style="clear: both"></div>
    			    </div>
    			    <hr/>
    			</div>

    			<div class="">
    			    <h1 align='center' style="font-size: 23px; font-weight: bolder">STUDENT PROGRESSIVE REPORT (<?= strtoupper($classes->classes) . ' ' . $year ?>)</h1>
    			</div>
    			<div class="panel-body profile-view-dis" style="max-height: 40; margin-top: 1em; padding: 0 5%;">
    			    <div class="row">
    				<div class="col-sm-9" style="float:left;" >
    				    <h1 style="font-weight: bolder;"> NAME: <?= strtoupper($student->name) ?></h1>
    				</div>
    				<div class="col-sm-3" style="float: right;">
    				    <h1 style="font-weight: bolder; font-size: 14px;"><?=
					    (isset($section_id) && $section_id == '') ?
						    'CLASS: ' . strtoupper($classes->classes) . '' :
						    'STREAM: ' . $section
					    ?></h1>
    				</div>
    				<div style="clear:both;"></div>
    			    </div>

    			</div>

			    <?php if (count($exams) > 0 && !empty($exams)) {
				?>

				<div class="row"  style="margin: 2%;">
				    <?php
				    if ($exams) {

					$examController = new Exam();
					?>
	    			    <div class="col-lg-12">
	    				<table class="table table-striped table-bordered">
	    				    <thead>
	    					<tr>
	    					    <th style="font-weight: bolder">SUBJECT</th>

							<?php foreach ($exams as $exam) { ?>
							    <th style="font-weight: bolder"><?= strtoupper($exam->exam) ?></th>
							<?php } ?>

	    					    <th style="font-weight: bolder">AVERAGE</th>
	    					    <th style="font-weight: bolder">GRADE</th>
							<?php if ($level->result_format == 'ACSEE') { ?>
							    <th style="font-weight: bolder">POINTS</th>
							<?php } ?>
	    					    <th style="font-weight: bolder">REMARKS</th>
	    					</tr>
	    				    </thead>
	    				    <tbody>
						    <?php
						    $total_marks = 0;
						    $total_subjects_done = 0;
						    foreach ($subjects as $subject) {
							?>
							<tr>
							    <td style="font-weight: bolder"><?= strtoupper($subject->subject) ?></td>

							    <?php
							    $total = 0;
							    $penalty = 0;
							    $exam_done = 0;
							    foreach ($exams as $exam) {

								$mark = $examController->get_total_marks($student->studentID, $exam->examID, $classesID, $subject->subjectID);
								$total+=$mark->total_mark;

								/*
								 * check penalty for that subject
								 */
								if ($subject->is_penalty == 1) {
//						foreach ($grades as $grade) {
//						    if ($grade->gradefrom == 0) {
//							if ($mark->total_mark < $grade->gradeupto) {
//							    $penalty = 1;
//							}
//						    }
//						}
								    $penalty = $mark->total_mark < $subject->pass_mark ? 1 : 0;
								}
								?>
		    					    <th><?= $mark->total_mark == 0 ? '-' : $mark->total_mark ?></th>
								<?php
								if ($mark->total_mark == '' || $mark->total_mark == 0) { //later we will change this condition to match exactly case for student who do the subject
								} else {
								    $exam_done++;
								}
							    }
							    $avg = $exam_done != 0 ? round($total / $exam_done, 1) : 0;
							    ?>

							    <td><?= $avg == 0 ? '-' : $avg ?></td>

							    <?php
							    if ($avg == 0) {
								//this is a short time solution, we put empty but
								//later on we need to see if this student takes this subject or not
								echo '<td></td><td></td>';
							    } else {
								echo return_grade($grades, round($avg), $level->result_format);
							    }
							    $total_marks+=$avg;
							    ?>
							</tr>
							<?php
							is_float($avg) ? $total_subjects_done++ : 0;
						    }

						    /**
						     * 
						     * 
						     * ----------------------------------------------------------------
						     * 
						     * We show Division & points only to A-level students. Those with
						     * grade format of ACSEE
						     */
						    if ($level->result_format == 'ACSEE') {
							?>
							<tr>
							    <td style="font-weight: bolder">DIVISION</td>
							    <?php
							    $i = 0;
							    $total_points = 0;
							    foreach ($exams as $exam) {
								$division[$i] = $examController->getDivision($student->studentID, $exam->examID);
								?>
		    					    <td><?= $division[$i][0] ?></td>
								<?php
								$total_points+=$division[$i][1];
								$i++;
							    }
							    $total_i = $i;
							    $total_average_points = floor($total_points / $total_i);
							    if ($level->result_format == 'CSEE') {
								$div = cseeDivision($total_average_points, $penalty);
							    } elseif ($level->result_format == 'ACSEE') {
								$div = acseeDivision($total_average_points, $penalty);
							    }
							    ?>
							    <td><?= $div ?></td>
							    <td></td>
							    <td></td>
							    <td></td>
							</tr>
							<tr>
							    <td style="font-weight: bolder">POINTS</td>
							    <?php
							    for ($i = 0; $i < $total_i; $i++) {
								?>
		    					    <td><?= $division[$i][1] ?></td>
								<?php
							    }
							    ?>
							    <td><?= $total_average_points ?></td>
							    <td></td>
							    <td></td>
							    <td></td>
							</tr>
							<?php
						    }

						    /**
						     * Otherwise, we don't show points and division
						     * ------------------------------------------------------
						     */
						    $section_name = isset($section_id) && $section_id == '' ? 'CLASS' : 'STREAM';
						    $sectionID = $section == 'CLASS' ? NULL : $sectionID;
						    ?>
	    					<tr>
	    					    <td style="font-weight: bolder">POSITION IN <?= $section_name ?></td>
							<?php
							foreach ($exams as $exam) {

							    $rank_info = $examController->get_rank_per_all_subjects($exam->examID, $classesID, $student->studentID, $sectionID);
							    ?>
							    <td><?= $rank_info->rank ?></td>
							    <?php
							}
							?>
	    					    <td><?= $student->rank ?></td>
							<?= $level->result_format == 'ACSEE' ? "<td></td>" : '' ?>
	    					    <td></td>
	    					    <td></td>
	    					</tr>
	    				    </tbody>
	    				</table>


	    				<table class="table table-striped table-bordered" style=" width: 100%; margin: 0%;">
	    				    <thead>
	    					<tr>
	    					    <th style="font-weight: bolder; width:25.4%;">TOTAL:
	    						<strong><?= $total_marks ?></strong></th>
	    					    <th style="font-weight: bolder;width:33.4%;">AVERAGE MARK:
	    						<strong><?php
								$total_avg = round($total_marks / $total_subjects_done, 1);
								echo $total_avg;
								?></strong></th>
	    					    <th style="font-weight: bolder">Position
	    						In <?= isset($section_id) && $section_id == '' ? 'Class:' : 'Stream:' ?>
	    						<strong><?= $student->rank ?></strong> out of <?= count($students) ?></th>
	    					<!--								<th style="font-weight: bolder">Points</th>-->
	    					<!--								<th style="font-weight: bolder">Division</th>-->
	    					</tr>
	    				    </thead>
	    				    <tbody>
	    				    <!--             <tr>-->
	    				    <!--          <td><? $report->sum ?></td>-->
	    				    <!--    <td><? round($report->average, 1) ?><!--</td>-->
	    				    <!--     <td><? count($total_students) ?><!--</td>-->
	    					<!--         <? strtolower($usertype) == 'parent' ? '<td>' . $totalstudent_insection->total_student . '</td>' : '' ?>
	    					<!--    <td><? $report->rank ?><!--</td>-->
	    					<!--     <? strtolower($usertype) == 'parent' ? '<td>' . $sectionreport->rank . '</td>' : '' ?>
	    					<!--      <td>--><?php //echo $points   ?><!--<!--</td>-->
	    					<!--             <!--<td>-->
						    <?php
						    //                		echo $division
						    //                               					
						    // 
						    ?><!--</td>-->
	    					<!---->
	    					<!--                            </tr>-->
	    				    </tbody>
	    				</table>

	    				<div style="float: left; width: 50%">
	    				    <table class="table table-striped table-bordered" style="  float: left; margin: 1% 0 0 0;">
	    					<thead>
	    					    <tr>
	    						<th style="font-weight: bolder; text-align: center; border-bottom: 1px solid #fff;">
	    						    GRADING
	    						</th>
	    					    </tr>
	    					</thead>
	    				    </table>

	    				    <table class="table table-striped table-bordered" style=" margin-bottom: 1%;">
	    					<thead>
	    					    <tr>
							    <?php foreach ($grades as $grade) { ?>
								<th style="font-weight: bolder; text-align: center;"><?= $grade->grade ?>
								</th>
							    <?php } ?>
	    					    </tr>
	    					</thead>
	    					<tbody>
	    					    <tr>
							    <?php foreach ($grades as $grade) { ?>
								<td style="font-weight: bold;font-size: smaller; text-align: center;background-color: white;color: #000;"><?= $grade->gradefrom ?>
								    - <?= $grade->gradeupto ?>
								</td>
							    <?php } ?>
	    					    </tr>
	    					</tbody>

	    				    </table>
	    				</div>
	    				<div style="float: right; width: 49.6%">
	    				    <table class="table table-striped table-bordered" style=" margin: 1% 0 0 0;">
	    					<thead>
	    					    <tr>
	    						<th style="font-weight: bolder; "><strong>KEY:</strong></th>
	    					    </tr>
	    					</thead>
	    					<tbody>
	    					    <tr>
	    						<td style="background-color: white;color: #000;"><strong>NOTES:</strong> <span
	    							style="font-size: smaller"></span
	    						</td>
	    					    </tr>
	    					</tbody>
	    				    </table>
	    				</div>
	    				<table class="table table-striped table-bordered" style=" margin: 1% 0 0 0;">
	    				    <thead>
	    					<tr>
	    					    <th style="font-weight: bolder; width:56%;"><strong style="vertical-align: top;">General
	    						    Remarks:</strong><br> <?php
							    foreach ($grades as $grade) {
								if ($grade->gradefrom <= round($report->average, 0) && $grade->gradeupto >= round($report->average, 0)) {
								    echo "<span class='bolder' style='border-bottom: 1px solid black'>";
								    echo $grade->overall_note;
								    echo "</span>";
								    break;
								}
							    }
							    ?></th>
	    					    <th>Class Teacher's Signature<br><img src="<?= $class_teacher_signature ?>" width="54"
	    										  height="32"></th>
	    					</tr>
	    				    </thead>
	    				</table>
	    				<table class="table table-striped table-bordered" style=" margin: 1% 0 0 0;">
	    				    <thead>
	    					<tr>
	    					    <th style="font-weight: bolder; width: 56%; "><strong style="vertical-align: top;">Head
	    						    Teacher's comments:</strong><br> <?php
							    foreach ($grades as $grade) {
								if ($grade->gradefrom <= round($report->average, 0) && $grade->gradeupto >= round($report->average, 0)) {
								    echo "<span class='bolder' style='border-bottom: 1px solid black'>";
								    echo $grade->overall_note;
								    echo "</span>";
								    break;
								}
							    }
							    ?></th>
	    					    <th style="width: 50%;">Signature<br></th>
	    					</tr>
	    				    </thead>
	    				</table>
	    				<table class="table table-striped table-bordered" style=" margin: 1% 0 0 0;">
	    				    <thead>
	    					<tr>
	    					    <th style="text-align: center;">Date of Reporting:
	    						<b><?= $level->result_format != 'CSEE' ? '9th' : '3th' ?> January, 2017 </b>
	    					    </th>
	    					</tr>
	    				    </thead>
	    				</table>



	    			    </div>
				    <?php } ?>
				    <div style="margin-left: 3%; margin-top: 2%; font-size: 110%; line-height: 21px;">

					<div style="padding-left:5%;">

					    <div style="z-index: 4000">
						<div style="float: right; margin-right: 4%; margin-top: 4%;"></div>
						<div><img src="../../assets/images/stamp_<?=  set_schema_name()?>png"
							  width="100" height="100"
							  style="position:relative; margin:-19% 15% 0 0; float:right;"></div>

					    </div>

					</div>


				    </div>
				</div>

			    <?php } ?>

    		    </section>

    		</div>


    	    </div>
    	</div>
	    <?php
	}
	/*      $html = ob_get_clean();
	  $dompdf = new DOMPDF();
	  $dompdf->load_html($html);
	  $dompdf->render();
	  $dompdf->stream('output.pdf'); */
	?>
    </body>
</html>